// tslint:disable
import * as mocha from 'mocha';
import * as chai from 'chai';
import chaiHttp = require('chai-http');

import app from '../src/App';

chai.use(chaiHttp);
const expect = chai.expect;

describe('GET api/v1/merchant', () => {

  it('responds with JSON array', () => {
    return chai.request(app).get('/api/v1/merchant')
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body.merchants).to.be.an('array');
      });
  });


});


describe('GET api/v1/merchant/sym/:symbol', () => {

  it('responds with single JSON object', () => {
    return chai.request(app).get('/api/v1/merchant/sym/airvend')
      .then(res => {
        expect(res.status).to.equal(200);
        expect(res).to.be.json;
        expect(res.body).to.be.an('object');
      });
  });

  it('should return airvend', () => {
    return chai.request(app).get('/api/v1/merchant/sym/airvend')
      .then(res => {
        expect(res.body.merchant.symbol).to.equal('airvend');
      });
  });

});