**Biya 1.0 Development Notes**

_MVP_

Language = Test Driven Development Typescript

Dependencies

- Node 7
- Express
- Mongoose
- Botbuilder

Features

- Credit Purchase (Self & 3rd Party)
- Bills Payment (DSTV, GoTV, Electricity) -- look at these as if they are our external vendors
- Funds transfer

Needs

- Dialogs for each feature
- API Connections; Flutterwave for charging users & Airvend for bills
- Moneywave API for Funds Transfer

ToDo

- Setup project directory 
- Initialize monitor API
- Setup database and data structure
- Data structure needs to take care of current vendors and future vendors
