import axios from 'axios';

const networks = {
    AIRTEL: '04',
    GLO: '02',
    MTN: '01',
    ETISALAT: '03',
};

export const dispenseAirtimeBackup = (payload) => {
    const postData = {
        UserID: 'CK10060991',
        APIKey:
            'Y0099NYI5EQF192VQ9T841LZ440BY55ZKF6G1MMS090UG7B01Y56PCG5Q38AHB79',
        CallBackURL: 'https://api.biya.com.ng/nello',
        Amount: payload.amount,
        MobileNumber: payload.phone,
        MobileNetwork: networks[payload.network],
    };

    return axios({
        baseURL: 'https://www.nellobytesystems.com',
        method: 'get',
        params: postData,
        url: `/APIAirtimeV1.asp`,
    });
};

export const verifyDispense = (orderid) => {
    const postData = {
        UserID: 'CK10060991',
        APIKey:
            'Y0099NYI5EQF192VQ9T841LZ440BY55ZKF6G1MMS090UG7B01Y56PCG5Q38AHB79',
        OrderID: orderid,
    };

    return axios({
        baseURL: 'https://www.nellobytesystems.com',
        method: 'get',
        params: postData,
        url: `/APIQueryV1.asp`,
    });
};
