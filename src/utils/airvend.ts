import axios from 'axios';
import * as builder from 'botbuilder';
import * as CircularJSON from 'circular-json';
import * as moment from 'moment';
import * as parseStringModule from 'xml2js';
const airvendUsername = process.env.AIRVEND_USER;
const airvendPassword = process.env.AIRVEND_PASS;
import { stripIndents } from 'common-tags';

const parseString = parseStringModule.parseString;
import { Transaction } from '../db/schemas/transaction';
import logger from '../logger';
import { updateTransaction } from './dbUtils';
import { createKueJob } from './kue';
import { sendMail } from './mailer';
import { printReciept } from './printReciept';
import { sendSpectranetPin, sendIkedcPin } from './sms';
import { SlackNotify } from '../monitor/SlackService';
// setup email data with unicode symbols
const mailOptions = {
    from: '"Dispense Error Alert ⚠" <alerts@biya.com.ng>', // sender address
    html: '', // html body.ng
    subject: 'Dispense Error Alert', // Subject line
    to: 'olumytee.ojo@gmail.com', // list of receivers
};
// timeout after 15 seconds and retry
function retryFailedRequest(err) {
    if (
        err.code === 'ECONNABORTED' &&
        err.config &&
        !err.config.__isRetryRequest
    ) {
        // change reference before retrying
        err.config.params.ref = err.config.params.ref + '-rtry';
        err.config.__isRetryRequest = true;
        return axios(err.config);
    }
    throw err;
}

import * as shortid from 'simple-id';

let logId;

axios.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger.info('Starting Request', {
        logId: logId,
        url: request.url,
    });
    return request;
});

axios.interceptors.response.use((response) => {
    logger.info('Response:', {
        ...(response && response.data),
        logId: logId,
    });
    return response;
});

function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}

MyError.prototype = new Error();
// MyError.prototype = Object.create(Error.prototype);
// MyError.prototype.constructor = MyError;

// handle dispense error
export const handleDispenseResponse = (response) => {
    return (session: any, transaction: any, type: string, plan?: any) => {
        parseString(response.data, (err, result) => {
            if (err) {
                logger.error(err);
                throw new MyError(
                    'There was an error, we will rectify this shortly. Thank you!',
                    'DispenseError',
                    transaction
                );
            }
            logger.info(`result from transaction ${transaction.id}`);
            logger.info(result);

            if (result && result.VendResponse.ResponseCode[0] === '0') {
                const myJSON = result.VendResponse;
                let msg;
                let updateObject: any = { status: 'success' };
                // set REMINDER
                if (type === 'dstv' || type === 'gotv') {
                    msg = printReciept(session, {
                        amount: transaction.amount,
                        type,
                    });
                }

                if (type === 'selfR' || type === 'otherR') {
                    msg = printReciept(session, {
                        amount: transaction.amount,
                        network: transaction.description,
                        phone: transaction.identity.phone,
                        type,
                    });
                }

                if (type === 'smile') {
                    msg = printReciept(session, {
                        account: transaction.identity.account,
                        amount: transaction.amount,
                        plan: transaction.description,
                        type,
                    });
                }

                if (
                    ['ikedc', 'ibedc', 'ekdc', 'phdc', 'enugu'].includes(
                        type.toLowerCase()
                    )
                ) {
                    const importantInfo = myJSON.vendData[0];
                    const pin = importantInfo['creditToken'][0];
                    const serial = importantInfo['exchangeReference'][0];
                    const amountOfPower = importantInfo['amountOfPower'][0];
                    updateObject = {
                        status: 'success',
                        'identity.pin': pin,
                        'identity.serial': serial,
                        'identity.ammount': importantInfo['amountOfPower'][0],
                    };

                    SlackNotify({
                        username: 'New Sale!',
                        channel: '#sales',
                        text: stripIndents`Successful ${type} dispense`,
                        fields: {
                            Service: type,
                            Amount: transaction.amount,
                            Name: transaction.identity.customerName,
                            Pin: pin,
                            AmountOfPower: amountOfPower,
                        },
                        emoji: ':white_check_mark:',
                    });

                    msg = printReciept(session, {
                        amount: transaction.amount,
                        pin: pin,
                        serial: serial,
                        amountOfPower,
                        type,
                    });
                    sendIkedcPin(transaction, pin, serial, type);
                }

                if (type === 'spectranet') {
                    const importantInfo =
                        myJSON.vendData[0]['pins'][0]['pin'][0];
                    const pin = importantInfo['pinCode'][0];
                    const serial = importantInfo['serialNumber'][0];
                    updateObject = {
                        status: 'success',
                        'identity.pin': pin,
                        'identity.serial': serial,
                    };
                    msg = printReciept(session, {
                        amount: transaction.amount,
                        pin: pin,
                        serial: serial,
                        type,
                    });
                    sendSpectranetPin(transaction, pin, serial);
                }
                // PRINT RECIEPT
                // session.send("Thanks for your patronage!")
                Transaction.findOneAndUpdate(
                    { reference: transaction.reference },
                    updateObject
                )
                    .then((tran) => {
                        // go to recipt dialog
                        session.replaceDialog('sendReciept', {
                            msg,
                            transaction: tran,
                        });
                    })
                    .catch((error) => {
                        logger.error(error);
                    });
            } else {
                // Transaction.findOneAndUpdate({reference: transaction.reference}, {status: "failed"}, function(err, doc){
                if (
                    result &&
                    result.VendResponse.ResponseCode[0] === '99999997'
                ) {
                    throw new MyError(
                        'Invalid smart card number, please try again.',
                        'InvalidError',
                        transaction
                    );
                } else if (
                    result &&
                    result.VendResponse.ResponseCode[0] === '99999999'
                ) {
                    throw new MyError(
                        'There was a network error, we will rectify this shortly. Thank you!',
                        'NetworkError',
                        transaction
                    );
                } else if (
                    result &&
                    result.VendResponse.ResponseCode[0] === '99999998'
                ) {
                    throw new MyError(
                        'There was a problem at our end, we will rectify this shortly. Thank you!',
                        'InsufficientError',
                        transaction
                    );
                } else if (
                    result &&
                    result.VendResponse.ResponseCode[0] === '94'
                ) {
                    throw new MyError(
                        'There was a problem at our end, we will rectify this shortly. Thank you!',
                        'NetworkError',
                        transaction
                    );
                } else if (
                    result &&
                    result.VendResponse.ResponseCode[0] === '301'
                ) {
                    throw new MyError(
                        'There was a problem at our end, we will rectify this shortly. Thank you!',
                        'NetworkError',
                        transaction
                    );
                } else if (
                    result &&
                    result.VendResponse.ResponseCode[0] === '2'
                ) {
                    throw new MyError(
                        'There was a problem at our end, we will rectify this shortly. Thank you!',
                        'NetworkError',
                        transaction
                    );
                } else {
                    throw new MyError(
                        'There was a problem at our end, we will check and rectify this shortly. Thank you!',
                        'UnknownError',
                        transaction
                    );
                }
            }
        });
    };
};

export const handleDispenseError = (error) => {
    return (session) => {
        logger.error(CircularJSON.stringify(error));
        let message;
        const sendText = false;
        // f.saveLastTransaction("/rechargeme", session)
        mailOptions.html = `<pre>${CircularJSON.stringify(error)}</pre>`;

        if (error.name === 'NetworkError' || error.name === 'DispenseError') {
            // send to retry handler
            // check network for transaction
            error.transaction.status = 'failed';
        }
        if (error.code === 'ECONNABORTED') {
            // do createKueJob kue
            const transaction = {
                reference: error.config.params.ref,
                status: 'failed',
            };
            mailOptions.html = `<strong> Error name is ${
                error.name
            }</strong><br/>
								<strong>Error message ${error.message}</strong><br/>
								<pre>${CircularJSON.stringify(error)}</pre>`;
            // createKueJob(transaction);
        }

        SlackNotify({
            username: 'Error Occured',
            channel: '#errors',
            text: `Error`,
            fields: {
                Error: error.name || 'Error',
                Message: error.message || 'Unknown Error',
                Transaction:
                    (error.transaction && error.transaction.reference) ||
                    'Unknown',
            },
            emoji: ':bangbang:',
        });

        if (error && error.message) {
            if (error.message === 'Charge attempted') {
                message = 'Error: We were unable to charge your card.';
            } else if (error.message === 'timeout of 120000ms exceeded') {
                message =
                    'There was an error with the network operator, we will check and get back to you shortly.';
            } else if (error.message === 'Declined') {
                message =
                    'The transaction was declined by the financial institution';
            } else if (error.message === 'Denied by Fraud System') {
                message =
                    'The transaction was declined by the financial institution';
            } else if (error.message === 'Expired card') {
                const transaction = error && error.transaction;
                session.replaceDialog('expiredCard', { transaction });
                return;
            } else if (error.message === 'Insufficient Funds') {
                const transaction = null;
                session.replaceDialog('insufficentFunds', { transaction });
                return;
            } else {
                message = error.message;
            }
        } else if (error && error.response && error.response.status === 400) {
            message = 'We were unable to charge your card';
        } else if (error && error.response && error.response.status === 401) {
            message = 'The request to charge your card, was not authorized';
        } else {
            message = 'There was an error';
        }

        // check if error is insufficient

        if (error.transaction) {
            Transaction.findOneAndUpdate(
                { reference: error.transaction.reference },
                { status: 'failed' },
                (err, doc) => {
                    if (err) {
                        logger.info(err);
                    }
                    logger.info('failed transaction');
                }
            );

            mailOptions.html = `<strong> Error name is ${
                error.name
            }</strong><br/>
								<strong>Error message ${error.message}</strong><br/>
								<pre>${CircularJSON.stringify(error.transaction)}</pre>`;
        }
        sendMail(mailOptions);
        session.endConversation(`${message}`);
    };
};

export const dispenseAirtime = (payload) => {
    const postData = {
        amount: payload.amount,
        msisdn: payload.phone,
        networkid: payload.network,
        password: airvendPassword,
        ref: payload.reference,
        type: '1',
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net/se/payments',
        method: 'post',
        params: postData,
        url: `/vtu/`,
    });
};

export const dispenseSpectranet = (payload) => {
    const postData = {
        amount: payload.amount,
        pinvalue: payload.amount,
        pinnum: 1,
        password: airvendPassword,
        ref: payload.reference,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/spectranet/`,
    });
};

export const verifyDSTV = (identifier: string) => {
    const postData = {
        password: airvendPassword,
        smartcard: identifier,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/dstv/verify/`,
    });
};

export const dispenseDSTV = (data: any) => {
    logger.info(data);
    const postData = {
        amount: data.amount - 100,
        customerName: data.identity.customerName,
        customerNumber: data.identity.customerNumber,
        invoicePeriod: data.identity.invoicePeriod,
        password: airvendPassword,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,

        url: `/vas/dstv/`,
    });
};

export const verifyGOTV = (identifier: string) => {
    const postData = {
        password: airvendPassword,
        smartcard: identifier,
        username: airvendUsername,
    };
    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/gotv/verify/`,
    });
};

export const dispenseGOTV = (data: any) => {
    const postData = {
        amount: data.amount - 100,
        customerName: data.identity.customerName,
        customerNumber: data.identity.customerNumber,
        invoicePeriod: data.identity.invoicePeriod,
        password: airvendPassword,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,

        url: `/vas/gotv/`,
    });
};

const electricityProviders = {
    ikedc: 11,
    ekdc: 13,
    phdc: 16,
    ibedc: 12,
    enugu: 21,
};

export const verifyElectricity = (identifier: string, provider: string) => {
    const postData = {
        account: identifier,
        password: airvendPassword,
        type: electricityProviders[provider.toLowerCase()],
        username: airvendUsername,
    };
    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/electricity/verify/`,
    });
};

export const dispenseElectricity = (transaction: any, provider: string) => {
    const postData = {
        account: transaction.identity.account,
        amount: transaction.amount - 100,
        password: airvendPassword,
        type: electricityProviders[provider.toLowerCase()],
        username: airvendUsername,
        customername: transaction.identity.customername || 'Biya',
        customerphone: transaction.identity.customerphone || transaction.user,
        customeraddress:
            transaction.identity.customeraddress || transaction.user,
    };
    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/electricity/`,
    });
};

export const verifySMILE = (identifier) => {
    const postData = {
        account: identifier,
        password: airvendPassword,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/smilebundle/verify/`,
    });
};

export const dispenseSMILE = (transaction: any) => {
    const postData = {
        account: transaction.identity.account,
        amount: transaction.amount - 100,
        bundleCode: transaction.identity.bundleCode,
        password: airvendPassword,
        quantity: 1,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/smilebundle/`,
    });
};
