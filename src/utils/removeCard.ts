import { User } from '../db/schemas/user';

const removeCard = (phone, param = 'authorization') => {
  return new Promise((resolve, reject) => {
    User.findOneAndUpdate(
      { phone },
      {
        [param]: null
      }
    )
      .then(() => resolve(true))
      .catch(err => reject(err));
  });
};

export default removeCard;
