import * as cloudinary from 'cloudinary';
import * as jwt from 'jsonwebtoken';
import QRCode from 'qrcode';

cloudinary.config({
  api_key: '416582552898813',
  api_secret: 'CeP_y2g79w4um-bI-uojDRgaEJo',
  cloud_name: 'pitech'
});

const base = 'https://biyatest.herokuapp.com/v1/tickets'; // api base url

const generate = url => {
  return new Promise((resolve, reject) => {
    const file = `${Date.now()} + token.png`;
    QRCode.toFile(file, url, err => {
      if (err) {
        reject(err);
      }
      cloudinary.uploader.upload(file, result => {
        if (result && result.secure_url) {
          resolve(result.secure_url);
        } else {
          reject('There was an error');
        }
      });
    });
  });
};

export const asyncIssue = payload => {
  return new Promise((resolve, reject) => {
    const token = jwt.sign(payload, process.env.jwtSecret);
    if (token) {
      resolve(token);
    }
    reject({ error: 'There was an error signing payload' });
  });
};

export const dispenseTicket = data => {
  const payload = {
    amount: data.amount,
    destination: data.identity.destination,
    reference: data.reference,
    validity: true
  };
  return new Promise((resolve, reject) => {
    asyncIssue(payload)
      .then(token => {
        const url = `${base}?token=${token}`;
        return generate(url);
      })
      .then(image => {
        resolve(image);
      })
      .catch(error => {
        reject(error);
      });
  });
};
