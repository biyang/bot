import axios from 'axios';
import * as https from 'https';

const API_URL = 'https://vtpass.com/api';
const httpsAgent = new https.Agent({
    rejectUnauthorized: false, // (NOTE: this will disable client verification)
});

const networks = {
    AIRTEL: 'airtel',
    GLO: 'glo',
    MTN: 'mtn',
    ETISALAT: 'etisalat',
};

const electricityServices = {
    ikedc: 'ikeja-electric',
    phdc: 'portharcourt-electric',
    ekdc: 'eko-electric',
    ibedc: 'ibadan-electric',
    aedc: 'abuja-electric',
    kedco: 'kano-electric',
    kaedco: 'kaduna-electric',
    jed: 'jos-electric',
};

export const dispenseWithVTPass = ({ type, ...transaction }) => {
    const isTv = ['dstv', 'gotv'].includes(type);
    const isElectricity = [
        'ikedc',
        'ekdc',
        'phdc',
        'ibedc',
        'aedc',
        'jed',
        'kedco',
        'kaedco',
    ].includes(type.toLowerCase());
    const isAirtime = type === 'airtime' && networks[transaction.description];

    const postData = {
        request_id: transaction.reference,
        amount: transaction.amount,
        phone: transaction.phone,
        ...(isAirtime && { serviceID: networks[transaction.description] }),
        ...(isTv && {
            serviceID: type,
            billersCode: transaction.identity.smartCard,
            variation_code: transaction.identity.variation_code,
        }),
        ...(isElectricity && {
            serviceID: electricityServices[type.toLowerCase()],
            billersCode: transaction.identity.account,
            variation_code: 'prepaid',
        }),
    };

    return axios({
        baseURL: API_URL,
        method: 'post',
        data: postData,
        url: `/pay`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};

export const verifyMerchant = ({ type, smartCard }) => {
    const isTv = ['dstv', 'gotv'].includes(type);
    const isElectricity = [
        'ikedc',
        'ekdc',
        'phdc',
        'ibedc',
        'aedc',
        'jed',
        'kedco',
        'kaedco',
    ].includes(type.toLowerCase());

    const postData = {
        ...(isTv && {
            serviceID: type,
            billersCode: smartCard,
        }),
        ...(isElectricity && {
            serviceID: electricityServices[type.toLowerCase()],
            billersCode: smartCard,
            type: 'prepaid',
        }),
    };

    return axios({
        baseURL: API_URL,
        method: 'post',
        data: postData,
        url: `/merchant-verify`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};

export const getVariations = (serviceID) => {
    return axios({
        baseURL: API_URL,
        method: 'get',
        params: {
            serviceID,
        },
        url: `/service-variations`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};

export const requeryTransaction = (request_id) => {
    return axios({
        baseURL: API_URL,
        method: 'post',
        data: {
            request_id,
        },
        url: `/requery`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};

export const getBalance = () => {
    return axios({
        baseURL: API_URL,
        method: 'get',
        url: `/balance`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};
