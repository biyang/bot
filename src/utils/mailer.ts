import * as nodemailer from 'nodemailer';
import * as mg from 'nodemailer-mailgun-transport';

interface IMailOptions {
  from: string;
  to: string;
  subject: string;
  text?: any;
  html?: any;
}

const auth = {
  auth: {
    api_key: 'key-a10209b5c4cbba75fd92f6fefa73050d',
    domain: 'mg.biya.com.ng'
  }
};
const transporter = nodemailer.createTransport(mg(auth));

export const sendMail = (options: IMailOptions) => {
  return new Promise((resolve, reject) => {
    // send mail with defined transport object
    transporter.sendMail(options, (error, info) => {
      if (error) {
        reject(error);
      }
      resolve(info);
    });
  });
};
