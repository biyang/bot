///////////////////////////////////////////////////////////////////////
// This file cotaims the  most basic helpers & functions for the bot //
//////////////////////////////////////////////////////////////////////
import * as _ from 'lodash';
// get a user's name
export const getName = (session: any): string => {
  const name = _.get(session, 'message.user.name', 'Pal');
  session.userData.name = name.split(' ')[0];
  return _.capitalize(name);
};

// global help text
export const helpText: string = `Send 'Menu' to begin or send something like
   * Recharge my line or
   * Buy 500 airtime or
   * Pay dstv bill
   * Send 'help' to show this message again
   * Send 'cancel' to end any operation`;

// check if platform is facebook
export const isFacebook = (session: any): boolean =>
  session.message.source === 'facebook';

// check if platform is skype
export const isSkype = (session: any): boolean =>
  session.message.source === 'skype';

// check if platform is emulator
export const isEmulator = (session: any): boolean =>
  session.message.source === 'emulator';
