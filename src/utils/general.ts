import { IMerchantModel, Merchant } from '../db/schemas/merchant';
import { ITransactionModel, Transaction } from '../db/schemas/transaction';
import { User } from '../db/schemas/user';
import { sendMail } from './mailer';

export const dispenseGeneral = async transaction => {
  try {
    const updatedTransaction = await Transaction.findOneAndUpdate(
      { reference: transaction.reference },
      { status: 'success' }
    );
    const updateMerchant = await Merchant.findOneAndUpdate(
      { symbol: transaction.merchant },
      { $push: { transactions: updatedTransaction._id } }
    );
    // notifyMerchant via email
    const mailOptions = {
      from: '"Transaction Alert" <alerts@biya.com.ng>', // sender address
      html: `<pre> ${updatedTransaction} </pre>`, // html body.ng
      subject: 'New Transaction', // Subject line
      to: updateMerchant.email // list of receivers
    };
    const notifyMerchant = await sendMail(mailOptions);
    return notifyMerchant;
  } catch (e) {
    const message = e.message ? e.message : 'There was an error';
    return message;
  }
};
