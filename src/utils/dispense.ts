import axios from 'axios';
import logger from '../logger';

const baseUrl = 'http://sandbox.vtpass.com/api';

// custom error
function MyError(message, name, transaction) {
  this.name = name;
  this.transaction = transaction;
  this.message = message || 'Default Message';
  this.stack = new Error().stack;
}
MyError.prototype = new Error();

export interface IPayload {
  reference: string;
  phone: number;
  amount: number;
  network: string;
}

export const airtime = async (payload: IPayload) => {
  // dispense airtime
  const options = {
    // auth: {
    //   password: 'sandbox',
    //   username: 'sandbox@vtpass.com'
    // },
    baseURL: baseUrl,
    method: 'post',
    headers: {
      Authorization: 'Basic c2FuZGJveEB2dHBhc3MuY29tOnNhbmRib3g='
    },
    data: {
      serviceID: payload.network, // lowercase network code
      request_id: payload.reference, // reference
      phone: payload.phone,
      amount: payload.amount
    },
    url: `/payfix`
  };
  return await axios(options);
};

export const gotv = async payload => {
  const variations = {
    lite: 'gotv-lite',
    value: 'gotv-value',
    plus: 'gotv-plus'
  };
  const options = {
    auth: {
      password: 'sandbox',
      username: 'sandbox@vtpass.com'
    },
    baseURL: baseUrl,
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    method: 'post',
    data: {
      serviceID: 'gotv', // lowercase network code
      request_id: payload.reference, // reference
      phone: payload.phone,
      billersCode: payload.smartcard,
      variation_code: variations[payload.plan]
    },
    url: `/payfix`
  };
  const dispense = await axios(options);
  if (dispense.data && dispense.data.code === '000') {
    // // successfull
    return 'success';
  } else {
    throw new MyError(dispense.data.code, 'DispenseError', payload);
  }
};

export const dstv = async payload => {
  const variations = {
    lite: 'gotv-lite',
    value: 'gotv-value',
    plus: 'gotv-plus'
  };
  const options = {
    auth: {
      password: 'sandbox',
      username: 'sandbox@vtpass.com'
    },
    baseURL: baseUrl,
    data: {
      serviceID: 'dstv', // lowercase network code
      request_id: payload.reference, // reference
      phone: payload.phone,
      billersCode: payload.smartcard,
      variation_code: variations[payload.plan]
    },
    method: 'post',
    url: `/payfix`
  };
  const dispense = await axios(options);
  if (dispense.data && dispense.data.code === '000') {
    // // successfull
    return 'success';
  } else {
    throw new MyError(dispense.data.code, 'DispenseError', payload);
  }
};
