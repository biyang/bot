// const accountSid = process.env.TWILIO_USERNAME;
// const authToken = process.env.TWILIO_PASS;

// // require the Twilio module and create a REST client
// import * as twilio from 'twilio';

// const client = twilio(accountSid, authToken);

// export const sendText = (to, body) => {
//   return client.messages.create({
//     body,
//     from: 'Biya',
//     to
//   });
// };
import axios from 'axios';
import logger from '../logger';

import * as shortid from 'simple-id';

let logId;

axios.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger.info('Starting Request', {
        logId: logId,
        url: request.url,
        ...(request.data && request.data.body),
    });
    return request;
});

axios.interceptors.response.use((response) => {
    logger.info('Response:', {
        ...(response && response.data),
        logId: logId,
    });
    return response;
});

export const sendSpectranetPin = (transaction, pin, serial) => {
    const text = `Done! Your spectranet ${transaction.amount} PIN is ${pin}. and the serial number is ${serial}.`;
    const postData = {
        from: 'Biya',
        message: text,
        to: transaction.user,
    };
    axios({
        auth: {
            password: '13bcf950a6366526964fda4e35da1717',
            username: '424df0de3e1a1167fa4c09c60e5d1f24',
        },
        baseURL: 'https://jusibe.com/smsapi/',
        method: 'post',
        params: postData,
        url: `send_sms`,
    })
        .then((info) => logger.info('done'))
        .catch((err) => logger.error(err));
};

export const sendIkedcPin = (transaction, pin, serial, type) => {
    const text = `Done! Your ${type.toUpperCase()} ${
        transaction.amount
    } PIN is ${pin}.`;
    const postData = {
        from: 'Biya',
        message: text,
        to: transaction.user,
    };
    axios({
        auth: {
            password: '13bcf950a6366526964fda4e35da1717',
            username: '424df0de3e1a1167fa4c09c60e5d1f24',
        },
        baseURL: 'https://jusibe.com/smsapi/',
        method: 'post',
        params: postData,
        url: `send_sms`,
    })
        .then((info) => logger.info('done'))
        .catch((err) => logger.error(err));
};
