import axios from 'axios';
import logger from '../logger';
import { sendMail } from './mailer';
import * as shortid from 'simple-id';

let logId;

axios.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger.info('Starting Request', {
        logId: logId,
        url: request.url,
        ...(request.data && request.data.body),
    });
    return request;
});

axios.interceptors.response.use((response) => {
    logger.info('Response:', {
        ...(response && response.data),
        logId: logId,
    });
    return response;
});

export const getNetworkSync = (phoneNumber: string, callback) => {
    const postData = {
        CountryCode: 'NG',
        Type: 'carrier',
    };
    axios({
        auth: {
            password: process.env.TWILIO_PASS,
            username: process.env.TWILIO_USERNAME,
        },
        baseURL: process.env.TWILIO_BASE,
        method: 'get',
        params: postData,
        url: `/${phoneNumber}`,
    })
        .then((result) => {
            if (result && result.data && result.data.carrier) {
                const network = result.data.carrier.name;
                const networks = {
                    MTN: 'MTN',
                    'EMST Etisalat': 'ETISALAT',
                    'Globacom Ltd': 'GLO',
                    'Globacom (GLO)': 'GLO',
                    'Celtel Nigeria Limited': 'AIRTEL',
                    'Airtel Nigeria': 'AIRTEL',
                };
                callback({ carrier: networks[network] });
            } else {
                throw Error('Network not found');
            }
        })
        .catch((err) => {
            const message = err.error.msg
                ? err.error.msg
                : 'There was an error';
            logger.error(message);
            callback(new Error(message));
        });
};

// getNetwork(recipient)
// .then(network => {
//   session.dialogData.network = network;
//   next();
// })
// .catch(err => {
//   logger.error(err);
//   const message = err.message ? err.message : 'There was an error';
//   session.endDialog(`${message}, please try again`);
// });
export const getNetwork = async (phoneNumber: string) => {
    try {
        const postData = {
            CountryCode: 'NG',
            Type: 'carrier',
        };
        const result = await axios({
            auth: {
                password: process.env.TWILIO_PASS,
                username: process.env.TWILIO_USERNAME,
            },
            baseURL: process.env.TWILIO_BASE,
            method: 'get',
            params: postData,
            url: `/${phoneNumber}`,
        });
        if (result && result.data && result.data.carrier) {
            const network = result.data.carrier.name;
            const networks = {
                MTN: 'MTN',
                'EMST Etisalat': 'ETISALAT',
                'Globacom Ltd': 'GLO',
                'Celtel Nigeria Limited': 'AIRTEL',
                'Airtel Nigeria': 'AIRTEL',
            };
            return networks[network];
        } else {
            throw Error('Network not found');
        }
    } catch (e) {
        const mailOptions = {
            from: '"⚠" <alerts@biya.com.ng>', // sender address
            html: `<pre>${JSON.stringify(e)}</pre>`, // html body
            subject: 'Network Error Alert', // Subject line
            to: 'olumytee.ojo@gmail.com', // list of receivers
        };
        sendMail(mailOptions);
        logger.error('Could not find network');
        throw Error('Could not resolve phone number');
    }
};

export const handleGetNetwork = (result) => {
    if (result && result.data && result.data.carrier) {
        const network = result.data.carrier.name;
        const networks = {
            MTN: 'MTN',
            'EMST Etisalat': 'ETISALAT',
            'Globacom Ltd': 'GLO',
            'Celtel Nigeria Limited': 'AIRTEL',
            'Airtel Nigeria': 'AIRTEL',
        };
        return networks[network];
    } else {
        const mailOptions = {
            from: '"⚠" <alerts@biya.com.ng>', // sender address
            html: `<pre>${JSON.stringify(result)}</pre>`, // html body
            subject: 'Network Error Alert', // Subject line
            to: 'olumytee.ojo@gmail.com', // list of receivers
        };
        sendMail(mailOptions);
        logger.error('Could not find network');
        throw Error('Network not found');
    }
};
