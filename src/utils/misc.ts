import logger from '../logger';
import { retryTransaction } from './mWave';

const data = {
  id: '97161',
  recipient_account_number: '0058209268',
  recipient_bank: '044'
};

retryTransaction(data)
  .then(result => {
    logger.info(result);
  })
  .catch(error => {
    logger.error(error);
  });
