import axios from 'axios';
import * as CircularJSON from 'circular-json';
import { SlackNotify } from '../monitor/SlackService';

import { getBalance } from '../utils/vtpass';

import logger from '../logger';

import * as shortid from 'simple-id';

import * as parseStringModule from 'xml2js';

const parseString = parseStringModule.parseString;

let logId;

axios.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger.info('Starting Request', {
        logId: logId,
        url: request.url,
        ...(request.data && request.data.body),
    });
    return request;
});

axios.interceptors.response.use((response) => {
    logger.info('Response:', {
        ...(response && response.data),
        logId: logId,
    });
    return response;
});

// const getBalance = () => {
//     return axios({
//         baseURL: 'https://api.airvendng.net',
//         method: 'post',
//         params: {
//             username: process.env.AIRVEND_USER,
//             password: process.env.AIRVEND_PASS,
//         },
//         url: `/vtu/balance.php`,
//     });
// };

function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}

MyError.prototype = new Error();

const services = [
    'ikedc',
    'ekdc',
    'phdc',
    'ibedc',
    'aedc',
    'jed',
    'kedco',
    'kaedco',
    'airtime',
    'otherr',
    'dstv',
    'gotv',
];

export const initializePaystack = (token) => (data, metaData) => {
    if (!services.includes(metaData.product.toLowerCase())) {
        throw Error(
            'This service is currently unavailable. Please try again later'
        );
    }
    const postData = {
        amount: data.amount * 100,
        email: data.email,
        metadata: {
            biya: metaData,
            cancel_action: 'https://biya.com.ng/',
        },
        reference: data.reference,
    };
    return new Promise((resolve, reject) => {
        getBalance()
            .then(({ data }) => {
                const balance = data && data.contents && data.contents.balance;
                const balanceInKobo = Number(balance) * 100;

                if (balanceInKobo >= Number(postData.amount)) {
                    return axios({
                        baseURL: 'https://api.paystack.co/transaction/',
                        data: postData,
                        headers: { authorization: `Bearer ${token}` },
                        method: 'post',
                        url: `initialize`,
                    });
                } else {
                    SlackNotify({
                        username: 'Danger!',
                        channel: '#vtpass',
                        text: `VTpass balance below threshold`,
                        fields: {
                            Balance: balance,
                        },
                        emoji: ':white_check_mark:',
                    });

                    throw Error(
                        'There was an error, we will rectify this shortly. Thank you!'
                    );
                }
            })
            .then((response: any) => {
                if (response.data && response.data.status) {
                    resolve(response.data.data.authorization_url);
                } else {
                    throw Error('Could not initialize');
                }
            })
            .catch((error) => {
                logger.error(CircularJSON.stringify(error));
                reject(error);
            });
    });
};

export const verifyTransaction =
    (token) =>
    (reference): any => {
        return new Promise((resolve, reject) => {
            axios({
                baseURL: 'https://api.paystack.co/transaction/',
                headers: { authorization: `Bearer ${token}` },
                method: 'get',
                url: `verify/${reference}`,
            })
                .then((response: any) => {
                    if (response.data && response.data.status) {
                        if (response.data.data.status === 'success') {
                            resolve(response.data.data);
                        } else {
                            const newError = new MyError(
                                response.data.data.gateway_response,
                                'PaymentError',
                                { reference }
                            );
                            logger.error(CircularJSON.stringify(newError));
                            reject(newError);
                        }
                    } else {
                        const newError = new MyError(
                            response.data.data.gateway_response,
                            'PaymentError',
                            { reference }
                        );
                        logger.error(CircularJSON.stringify(newError));
                        reject(newError);
                    }
                })
                .catch((error) => {
                    logger.error(CircularJSON.stringify(error));
                    reject(error);
                });
        });
    };

export const chargeWithPaystack = (token) => (data: any, metaData) => {
    if (!services.includes(metaData.product.toLowerCase())) {
        throw Error(
            'This service is currently unavailable. Please try again later'
        );
    }
    const postData = {
        amount: data.amount * 100,
        authorization_code: data.paymentAuth,
        email: data.email,
        metadata: {
            biya: metaData,
            cancel_action: 'https://biya.com.ng/',
        },
        reference: data.reference,
    };
    return new Promise((resolve, reject) => {
        getBalance()
            .then(({ data }) => {
                const balance = data && data.contents && data.contents.balance;
                const balanceInKobo = Number(balance) * 100;

                if (balanceInKobo >= Number(postData.amount)) {
                    return axios({
                        baseURL: 'https://api.paystack.co/transaction/',
                        data: postData,
                        headers: { authorization: `Bearer ${token}` },
                        method: 'post',
                        url: `charge_authorization `,
                    });
                } else {
                    SlackNotify({
                        username: 'Danger!',
                        channel: '#vtpass',
                        text: `VTpass balance below threshold`,
                        fields: {
                            Balance: balance,
                        },
                        emoji: ':white_check_mark:',
                    });

                    throw Error(
                        'There was an error, we will rectify this shortly. Thank you!'
                    );
                }
            })
            .then((response: any) => {
                if (response.data && response.data.status) {
                    if (response.data.data.status === 'success') {
                        resolve(response.data);
                    } else {
                        const newError = new MyError(
                            response.data.data.gateway_response,
                            'PaymentError',
                            data
                        );
                        logger.error(CircularJSON.stringify(newError));
                        reject(newError);
                    }
                } else {
                    const newError = new MyError(
                        response.data.data.gateway_response,
                        'PaymentError',
                        data
                    );
                    logger.error(CircularJSON.stringify(newError));
                    reject(newError);
                }
            })
            .catch((error) => {
                logger.error(CircularJSON.stringify(error));
                reject(error);
            });
    });
};

// customer, plan, start_date
export const createPaystackSubscription = (token) => (data) => {
    return new Promise((resolve, reject) => {
        axios({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${token}` },
            method: 'post',
            url: `subscription`,
        })
            .then((response: any) => {
                if (response.data && response.data.status) {
                    resolve(response.data);
                } else {
                    throw Error('Could not create subscription');
                }
            })
            .catch((error) => {
                logger.error(CircularJSON.stringify(error));
                reject(error);
            });
    });
};

export const disableSubscription = (token) => (data) => {
    return new Promise((resolve, reject) => {
        axios({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${token}` },
            method: 'post',
            url: `disable`,
        })
            .then((response: any) => {
                if (response.data && response.data.status) {
                    resolve(response.data);
                } else {
                    throw Error('Could not remove subscription');
                }
            })
            .catch((error) => {
                logger.error(CircularJSON.stringify(error));
                reject(error);
            });
    });
};

export const enableSubscription = (token) => (data) => {
    return new Promise((resolve, reject) => {
        axios({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${token}` },
            method: 'post',
            url: `enable`,
        })
            .then((response: any) => {
                if (response.data && response.data.status) {
                    resolve(response.data);
                } else {
                    throw Error('Could not remove subscription');
                }
            })
            .catch((error) => {
                logger.error(CircularJSON.stringify(error));
                reject(error);
            });
    });
};
