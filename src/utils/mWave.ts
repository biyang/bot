import * as _ from 'lodash';
import * as Moneywave from 'moneywave';
import logger from '../logger';
// const options = {
//   apiKey: 'ts_OD93CAKH30JYNYVQ9VEW',
//   apiSecret: 'ts_9A4JMJ7LCGVQJUJLUUTNK2CXQXEPJI',
//   cache: true
// }

const options = {
  apiKey: 'lv_TKXV3P4FFT6DV5NQLBUE',
  apiSecret: 'lv_WD15FDC14BQQY09BRPWWVQTDYEKOZ5',
  cache: true,
  env: 'live'
};

const moneywave = Moneywave.MakeWave(options);

// Validate Account function which returns a promise
export const validateAccount = (accountNumber, bankCode) =>
  moneywave.resources.validateBankAccount(accountNumber, bankCode);

export const validate = (accountNumber, bankCode, callback) => {
  console.log(accountNumber, bankCode);
  moneywave.resources
    .validateBankAccount(accountNumber, bankCode)
    .then(account => {
      if (account && account.status === 'success') {
        callback(account);
      } else {
        callback(null);
      }
    })
    .catch(err => {
      const message = err.error.msg ? err.error.msg : 'There was an error';
      logger.error(message);
      callback(new Error(message));
    });
};
export const initiateFundsTransfer = () => {
  // use async await
  moneywave.card
    .transfer()
    .then(trans => {
      logger.info(trans);
    })
    .catch(error => {
      logger.error(error);
    });
};

// get bank Code
export const getBankCode = bankName => {
  return new Promise((resolve, reject) => {
    moneywave.resources
      .getBanks()
      .then(result => {
        if (result.status === 'success') {
          const banksObject = result.data;
          const code = _.findKey(banksObject, o => {
            return o === bankName;
          });
          resolve(code);
        }
      })
      .catch(error => {
        reject(error.error);
      });
  });
};

// tokenize card

export const tokenizeCard = card => {
  return new Promise((resolve, reject) => {
    moneywave.card
      .tokenizeCard(card)
      .then(result => {
        resolve(result);
      })
      .catch(error => {
        reject(error.error);
      });
  });
};

export const transfer = data => {
  return new Promise((resolve, reject) => {
    moneywave.card
      .transfer(data)
      .then(result => {
        resolve(result);
      })
      .catch(error => {
        reject(error.error);
      });
  });
};

export const verify = ref => {
  return new Promise((resolve, reject) => {
    moneywave.card
      .transfer(ref)
      .then(result => {
        resolve(result);
      })
      .catch(error => {
        reject(error.error);
      });
  });
};

export const retryTransaction = data => {
  return new Promise((resolve, reject) => {
    moneywave.card
      .retryTransaction(data)
      .then(result => {
        resolve(result);
      })
      .catch(error => {
        reject(error.error);
      });
  });
};
