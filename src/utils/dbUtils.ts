import * as shortid from 'simple-id';
import { Beneficiary, IBeneficiaryModel } from '../db/schemas/beneficiary';
import { IMerchantModel, Merchant } from '../db/schemas/merchant';
import { ISubscriptionModel, Subscription } from '../db/schemas/subscription';
import { ITransactionModel, Transaction } from '../db/schemas/transaction';
import { User } from '../db/schemas/user';
import logger from '../logger';

import { getNetwork } from './finder';

export const checkIfRegisteredUser = (phone: string) =>
    User.findOne({ phone: phone });

export const cardCheck = (phone: string) => User.findOne({ phone: phone });

export const deriveEmail = (session: any): string => {
    let emailPrefix;
    let email;
    const random = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    const emailSuffix = '@biya.com.ng';
    const source = session.message.source;
    switch (source) {
        case 'facebook':
            emailPrefix = 'fb';
            break;
        case 'telegram':
            emailPrefix = 'tg';
            break;
        case 'skype':
            emailPrefix = 'gl';
            break;
        case 'slack':
            emailPrefix = 'at';
            break;
        default:
            emailPrefix = 'rd';
    }
    email = emailPrefix + random + emailSuffix;
    return email;
};

export const mockPhone = (session: any) =>
    'bot' + session.message.user.id.substr(0, 6);

export const createTransaction = async (transaction: any) => {
    try {
        const user = await User.findOne({ phone: transaction.user });
        const merchant = await Merchant.findOne({
            symbol: transaction.merchant,
        });
        const newTransaction = new Transaction({
            address: transaction.address,
            amount: transaction.amount,
            description: transaction.description,
            identity: transaction.identity,
            merchant: merchant._id,
            product: transaction.product,
            reference: transaction.reference,
            status: transaction.status,
            tags: transaction.tags,
            user: user._id,
        });
        return newTransaction.save();
    } catch (e) {
        // throw error
        logger.error(e);
        throw Error('Error creating transaction');
    }
};

export const createAirTransaction = async (transaction: any) => {
    try {
        const user = await User.findOne({ phone: transaction.user });
        let network;
        if (transaction.description) {
            network = transaction.description;
        } else {
            network = await getNetwork(transaction.identity.phone);
        }
        const merchant = await Merchant.findOne({
            symbol: transaction.merchant,
        });
        const newTransaction = new Transaction({
            amount: transaction.amount,
            address: transaction.address,
            description: network,
            identity: transaction.identity,
            merchant: merchant._id,
            product: transaction.product,
            reference: transaction.reference,
            status: transaction.status,
            tags: transaction.tags,
            user: user._id,
        });
        return newTransaction.save();
    } catch (e) {
        // throw error
        throw Error('Error creating transaction');
    }
};

export const createLocalSubscription = async (transaction: any, data) => {
    const newSubscription = new Subscription({
        data,
        identity: transaction.identity,
        address: transaction.address,
        product: transaction.product,
        user: transaction.user,
    });
    const subscription = await newSubscription.save();
    // update user with subscription
    return User.findByIdAndUpdate(transaction.user, {
        $push: { subscriptions: subscription._id },
    });
};

export const saveNewUser = (userData: any) => {
    const newUser = new User({
        address: userData.address,
        email: userData.email,
        name: userData.name,
        network: userData.network,
        phone: userData.phone,
        userBotId: userData.userBotId,
    });
    return newUser.save();
};

export const updateTransaction = async (transaction) => {
    // find by reference and update
    return new Promise((resolve, reject) => {
        Transaction.findOneAndUpdate(
            { reference: transaction.reference },
            transaction
        )
            .then((doc) => {
                resolve(doc);
            })
            .catch((error) => {
                reject(error);
            });
    });
};

export const updateAll = async () => {
    const transactions = await Transaction.find({});
    transactions.forEach((trans) => {
        if (trans.user.length === 11) {
            User.findOne({ phone: trans.user })
                .then((user) => {
                    return Transaction.findByIdAndUpdate(trans._id, {
                        user: user._id,
                    });
                })
                .then((update) => {
                    logger.info('updated');
                })
                .catch((error) => {
                    logger.error(error);
                });
        }
    });
};
