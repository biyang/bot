// paystack transaction creation

                    createTransaction(transaction)
                        .then(function(trx: any){                            

                            let data = {
                                amount: amount,
                                reference: reference,
                                email: deriveEmail(session)
                            }

                            return initializePaystack(data)
                        })
                        .then(function(result: any){
                            let url = result;
                            let msg = new builder.Message(session)
                                .attachments([
                                    new builder.HeroCard(session)
                                        .text("Click 'Pay Now'")
                                        .images([
                                            builder.CardImage.create(session, payImageURL)
                                        ])
                                        .buttons([
                                            builder.CardAction.openUrl(session, url, "Pay Now")
                                        ])
                                ]);
                            session.endDialog(msg);
                        })
                        .catch(function(error){
                            logger.error(error)
                            session.endDialog('Error')
                        });
                }
            })
            .catch(function(error){

                logger.error(error)
                session.endDialog('Error')
            })


// flutter wave


                    createTransaction(transaction)
                        .then(function(trx: any){
                            // use jsonwebtokens to send to https://secure.biya.com.ng/cc?jwonwebtoken

                            let payload = {amount: amount, reference: reference, user: session.message.user.id, product: transaction.product};

                            let token = issue(payload);
                            
                            let url = process.env.DOMAIN + 'pay/?token=' + token

                            let msg = new builder.Message(session)
                                .attachments([
                                    new builder.HeroCard(session)
                                        .text("Click 'Pay Now'")
                                        .images([
                                            builder.CardImage.create(session, payImageURL)
                                        ])
                                        .buttons([
                                            builder.CardAction.openUrl(session, url, "Pay Now")
                                        ])
                                ]);
                            session.endDialog(msg);
                        })
                        .catch(function(error){
                            logger.error(error)
                            session.endDialog('Error')
                        });


// fluuerwave handle result of charge

                if (res && res.headers['content-type'] === 'text/html') {
                    throw Error ("Internal server error")
                } else if (res && res.body.data.responsecode === "00") {
                    return dispenseAirtime(recipient, networks[network], amount, res.body.data.transactionreference)
                } else {
                    const message = res.body.data.responsemessage ? res.body.data.responsemessage : 'There was an error' 
                    throw Error(message) 
                }


/// 


/// firstRun Middleware
// bot.dialog('/firstRun', [
//     (session) => {
//         if (getName(session) !== "Friend") {
//             session.userData.name = getName(session)
//         }
//         if(!session.userData.name) {
//             builder.Prompts.text(session, 'Hi, I\'m Biya. What may I call you?');
//         }
//         else {
//             session.send(`Hi ${session.userData.name}, I\'m Biya. I am a bot that can help you buy airtime, transfer funds and pay bills securely anytime you want !`)
//             builder.Prompts.confirm(session, "Wanna see more of what I can do?");
//         }
//     },
//     (session, results, next) => {
//         if (results.response == true) {
//                 // Ask the user to select an item from a carousel.
//                 let msg = new builder.Message(session)
//                     .attachmentLayout(builder.AttachmentLayout.carousel)
//                     .attachments([
//                         new builder.HeroCard(session)
//                             .title("Buy Airtime")
//                             .subtitle("Seamlessly buy MTN, Airtel, Glo and Etisalat airtime for you or your loved ones.")
//                             .images([
//                                 builder.CardImage.create(session, "http://res.cloudinary.com/pitech/image/upload/v1492714509/thumbs/mtn-glo-airtel-etisalat.jpg")
//                                     .tap(builder.CardAction.showImage(session, "http://res.cloudinary.com/pitech/image/upload/v1492714509/thumbs/mtn-glo-airtel-etisalat.jpg")),
//                             ])
//                             .buttons([
//                                 builder.CardAction.imBack(session, "recharge", "Recharge") 
//                             ]),
//                         new builder.HeroCard(session)
//                             .title("Pay Bills")
//                             .subtitle("Pay your DSTv, GOTv and Electricity bills with many more coming soon.")
//                             .images([
//                                 builder.CardImage.create(session, "http://res.cloudinary.com/pitech/image/upload/v1492714730/thumbs/bills.jpg")
//                                     .tap(builder.CardAction.showImage(session, "http://res.cloudinary.com/pitech/image/upload/v1492714730/thumbs/bills.jpg")),
//                             ])
//                             .buttons([
//                                 builder.CardAction.imBack(session, "bills", "Pay a bill")
//                             ]),
//                         new builder.HeroCard(session)
//                             .title("Transfer funds")
//                             .subtitle("ransfer money from your bank account to any bank account in Nigeria")
//                             .images([
//                                 builder.CardImage.create(session, "http://res.cloudinary.com/pitech/image/upload/v1492715059/thumbs/Nigerian-Banks.jpg")
//                                     .tap(builder.CardAction.showImage(session, "http://res.cloudinary.com/pitech/image/upload/v1492715059/thumbs/Nigerian-Banks.jpg"))
//                             ])
//                             .buttons([
//                                 builder.CardAction.imBack(session, "transfer", "Try transfer")
//                             ])
//                     ]);
//                     session.endDialog(msg)

//         } else if (results.response == false) {
//             session.send(helpText)
//             session.endDialog()
//         } else {
//             session.userData.name = results.response;
//             session.send(`Good to meet you, ${session.userData.name} 😊`)
//             session.send(`I am a bot that can help you buy airtime, transfer funds and pay bills securely anytime you want !`)
//             builder.Prompts.confirm(session, "Wanna see more of what I can do?");
//         }
        
//     },
//     (session, results) => {
//         if (results.response == true) {
//                 // Ask the user to select an item from a carousel.
//                 let msg = new builder.Message(session)
//                     .attachmentLayout(builder.AttachmentLayout.carousel)
//                     .attachments([
//                         new builder.HeroCard(session)
//                             .title("Buy Airtime")
//                             .subtitle("Seamlessly buy MTN, Airtel, Glo and Etisalat airtime for you or your loved ones.")
//                             .images([
//                                 builder.CardImage.create(session, "http://res.cloudinary.com/pitech/image/upload/v1492714509/thumbs/mtn-glo-airtel-etisalat.jpg")
//                                     .tap(builder.CardAction.showImage(session, "http://res.cloudinary.com/pitech/image/upload/v1492714509/thumbs/mtn-glo-airtel-etisalat.jpg")),
//                             ])
//                             .buttons([
//                                 builder.CardAction.imBack(session, "recharge", "Recharge") 
//                             ]),
//                         new builder.HeroCard(session)
//                             .title("Pay Bills")
//                             .subtitle("Pay your DSTv, GOTv and Electricity bills with many more coming soon.")
//                             .images([
//                                 builder.CardImage.create(session, "http://res.cloudinary.com/pitech/image/upload/v1492714730/thumbs/bills.jpg")
//                                     .tap(builder.CardAction.showImage(session, "http://res.cloudinary.com/pitech/image/upload/v1492714730/thumbs/bills.jpg")),
//                             ])
//                             .buttons([
//                                 builder.CardAction.imBack(session, "bills", "Pay a bill")
//                             ]),
//                         new builder.HeroCard(session)
//                             .title("Transfer funds")
//                             .subtitle("ransfer money from your bank account to any bank account in Nigeria")
//                             .images([
//                                 builder.CardImage.create(session, "http://res.cloudinary.com/pitech/image/upload/v1492715059/thumbs/Nigerian-Banks.jpg")
//                                     .tap(builder.CardAction.showImage(session, "http://res.cloudinary.com/pitech/image/upload/v1492715059/thumbs/Nigerian-Banks.jpg"))
//                             ])
//                             .buttons([
//                                 builder.CardAction.imBack(session, "transfer", "Try transfer")
//                             ])
//                     ]);
//                 session.endDialog(msg)
//         } else {
//             session.send(helpText)
//             session.endDialog()
//         }
//     }
// ])