import * as builder from 'botbuilder';

const networks = {
    AIRTEL: 1,
    ETISALAT: 4,
    GLO: 3,
    MTN: 2,
};

const fbShareLink = 'https://biya.com.ng';

export const printReciept = (session: any, data: any) => {
    let text;
    let imageURL;
    switch (data.type.toLowerCase()) {
        case 'dstv':
            text = `DSTv payment of ₦${data.amount} was successful. Thanks for using Biya. 😁`;
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1483994623/thumbs/DStv-logo.jpg';
            break;
        case 'gotv':
            text = `GOTv payment of ₦${data.amount} was successful. Thanks for using Biya. 😁`;
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1483994623/thumbs/GOtv.jpg';
            break;
        case 'selfR':
            text = `Your line, ${data.phone} has been recharged with ₦${data.amount} airtime 😁`;
            imageURL = `http://res.cloudinary.com/pitech/image/upload/v1487970365/thumbs/${
                networks[data.network]
            }.jpg`;
            if (data.network === 'ETISALAT') {
                imageURL =
                    'https://res.cloudinary.com/pitech/image/upload/v1503317115/thumbs/4.jpg';
            }
            break;
        case 'otherR':
            text = `${data.phone} has been recharged with ₦${data.amount} airtime 😁`;
            imageURL = `http://res.cloudinary.com/pitech/image/upload/v1487970365/thumbs/${
                networks[data.network]
            }.jpg`;
            if (data.network === 'ETISALAT') {
                imageURL =
                    'https://res.cloudinary.com/pitech/image/upload/v1503317115/thumbs/4.jpg';
            }
            break;
        case 'smile':
            text = `${data.plan} subscription on Account ${data.account} was successful 😁`;
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1483994623/thumbs/Smile.jpg';
            break;
        case 'spectranet':
            text = `Done! Your spectranet ${data.amount} PIN is ${data.pin}. and the serial number is ${data.serial}.`;
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1509224991/thumbs/spec.jpg';
            break;
        case 'ikedc':
        case 'ibedc':
        case 'ekdc':
        case 'phdc':
        case 'enugu':
            text = `🎉 Your ${data.type.toUpperCase()} PIN is ${
                data.pin
            } for a total of ${data.amountOfPower} 😁`;
            imageURL =
                'https://res.cloudinary.com/pitech/image/upload/v1590346246/thumbs/check.jpg';
            break;
    }
    const msg = new builder.Message(session).attachments([
        new builder.HeroCard(session)
            .text(text)
            .images([builder.CardImage.create(session, imageURL)])
            .buttons([
                builder.CardAction.openUrl(session, fbShareLink, 'Share'),
            ]),
    ]);
    return msg;
};
