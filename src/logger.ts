import * as fs from 'fs';
import * as winston from 'winston';
require('winston-loggly-bulk');

const env = process.env.NODE_ENV || 'development';
const logDir = 'log';
// Create the log directory if it does not exist
if (!fs.existsSync(logDir)) {
  fs.mkdirSync(logDir);
}

const loggly = new winston.transports.Loggly({
  subdomain: 'biya',
  inputToken: 'f697844e-08ed-49e6-aec2-849c0d52aab0',
  tags: ['Winston-NodeJS'],
  json: true
});
const tsFormat = () => new Date().toLocaleTimeString();

const konsole = new winston.transports.Console({
  colorize: true,
  timestamp: tsFormat,
  prettyPrint: true,
  handleExceptions: process.env.NODE_ENV === 'production'
});

const logger = new winston.Logger({
  transports: []
});

logger.add(konsole, null, true);
logger.add(loggly, null, true);

logger.stream = {
  write(message, encoding) {
    logger.info(message.trim());
  }
};

export default logger;
