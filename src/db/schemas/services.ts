import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { IService } from '../interfaces/services';

export interface IServiceModel extends IService, Document {}

export let serviceSchema: Schema = new Schema(
  {
    service: Schema.Types.Mixed,
    status: { type: String },
    since: Date
  },
  { timestamps: true }
);

export const Services: Model<IServiceModel> = model<IServiceModel>(
  'Services',
  serviceSchema
);
