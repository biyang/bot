import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { IBeneficiary } from '../interfaces/beneficiary';

export interface IBeneficiaryModel extends IBeneficiary, Document {}

export let beneficiarySchema: Schema = new Schema(
  {
    identifier: Schema.Types.Mixed,
    name: { type: String },
    user: { ref: 'User', type: String }
  },
  { timestamps: true }
);

const handleE11000 = (error, res, next) => {
  if (error.name === 'MongoError' && error.code === 11000) {
    logger.error(error);
    next(new Error('There was a duplicate key error'));
  } else {
    next();
  }
};

beneficiarySchema.post('save', handleE11000);
beneficiarySchema.post('update', handleE11000);
beneficiarySchema.post('findOneAndUpdate', handleE11000);
beneficiarySchema.post('insertMany', handleE11000);

export const Beneficiary: Model<IBeneficiaryModel> = model<IBeneficiaryModel>(
  'Beneficiary',
  beneficiarySchema
);
