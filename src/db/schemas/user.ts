import * as bcrypt from 'bcryptjs';
import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { IUser } from '../interfaces/user';

export interface IUserModel extends IUser, Document {}
/// DB AUTH

// db.auth("username", "password")

export let userSchema: Schema = new Schema(
  {
    activated: Boolean,
    address: Object,
    addresses: { type: Object },
    whitelisted: Boolean,
    authorization: Object, // for paystack
    billsAuth: Object,
    beneficiaries: [{ ref: 'Beneficiary', type: Schema.Types.ObjectId }],
    email: { type: String, required: true }, // derived from botId
    initialReference: String,
    name: String,
    network: String,
    passcode: String,
    paymentAuth: Object, // for flutterwave
    phone: { type: String, unique: true },
    primaryChannel: String,
    subscriptions: [{ ref: 'Subscription', type: Schema.Types.ObjectId }],
    transactions: [{ ref: 'Transaction', type: Schema.Types.ObjectId }],
    userBotId: { type: String },
    wallet: { type: Object }
  },
  { timestamps: true, strict: false }
);

userSchema.pre('save', function(next) {
  const user = this;

  if (!user.isModified('passcode')) {
    return next();
  }

  bcrypt.hash(user.passcode, 10, (err, hash) => {
    if (err) {
      return next(err);
    }
    user.passcode = hash;
    next();
  });
});

const handleE11000 = (error, res, next) => {
  if (error.name === 'MongoError' && error.code === 11000) {
    logger.error(error);
    next(new Error('There was a duplicate key error'));
  } else {
    next();
  }
};

userSchema.post('save', handleE11000);
userSchema.post('update', handleE11000);
userSchema.post('findOneAndUpdate', handleE11000);
userSchema.post('insertMany', handleE11000);

export const User: Model<IUserModel> = model<IUserModel>('User', userSchema);
