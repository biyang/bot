import { Document, Model, model, Schema } from 'mongoose';
import { ISubscription } from '../interfaces/subscription';

export interface ISubscriptionModel extends ISubscription, Document {}

export let subscriptionSchema: Schema = new Schema(
  {
    data: { type: Object, required: true },
    identity: Object,
    address: Object,
    product: String,
    user: { ref: 'User', type: String }
  },
  { timestamps: true }
);

export const Subscription: Model<ISubscriptionModel> = model<
  ISubscriptionModel
>('Subscription', subscriptionSchema);
