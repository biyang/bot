import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { IUtterance } from '../interfaces/utterances';

export interface IUtteranceModel extends IUtterance, Document {}

export let utteranceSchema: Schema = new Schema(
  {
    event: Schema.Types.Mixed,
    type: { type: String }
  },
  { timestamps: true }
);

export const Utterances: Model<IUtteranceModel> = model<IUtteranceModel>(
  'Utterances',
  utteranceSchema
);
