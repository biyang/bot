import { Document, Model, model, Schema } from 'mongoose';
import { ITransaction } from '../interfaces/transaction';

export interface ITransactionModel extends ITransaction, Document {}

// remember to map to customer and merchant moodels
export let transactionSchema: Schema = new Schema(
  {
    amount: { type: String, required: true },
    address: Object,
    description: { type: String, required: true },
    identity: Object,
    merchant: { ref: 'Merchant', type: String },
    product: { type: String, required: true },
    reference: { type: String, required: true, unique: true },
    status: { type: String, required: true },
    tags: { type: Array, required: true },
    user: { ref: 'User', type: String }
  },
  { timestamps: true }
);

export const Transaction: Model<ITransactionModel> = model<ITransactionModel>(
  'Transaction',
  transactionSchema
);
