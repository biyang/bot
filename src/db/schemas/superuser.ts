import * as bcrypt from 'bcryptjs';
import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { ISuperUser } from '../interfaces/superuser';

export interface ISuperUserModel extends ISuperUser, Document {}

export let superuserSchema: Schema = new Schema(
  {
    password: { type: String, required: true },
    roles: Array,
    username: { type: String, required: true, unique: true }
  },
  { timestamps: true }
);

superuserSchema.pre('save', function(next) {
  const superuser = this;
  if (!superuser.isModified('password')) {
    return next();
  }
  bcrypt.hash(superuser.password, 10, (err, hash) => {
    if (err) {
      return next(err);
    }
    superuser.password = hash;
    next();
  });
});

const handleE11000 = (error, res, next) => {
  if (error.name === 'MongoError' && error.code === 11000) {
    next(new Error('There was a duplicate key error'));
  } else {
    next();
  }
};

superuserSchema.post('save', handleE11000);
superuserSchema.post('update', handleE11000);
superuserSchema.post('findOneAndUpdate', handleE11000);
superuserSchema.post('insertMany', handleE11000);

export const Superuser: Model<ISuperUserModel> = model<ISuperUserModel>(
  'Superuser',
  superuserSchema
);
