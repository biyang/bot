import * as bcrypt from 'bcryptjs';
import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { IMerchant } from '../interfaces/merchant';

export interface IMerchantModel extends IMerchant, Document {}

export let merchantSchema: Schema = new Schema(
    {
        email: { type: String, required: true, unique: true },
        logoURL: String,
        name: String,
        password: String,
        phone: String,
        symbol: { type: String, unique: true },
        transactions: [{ ref: 'Transaction', type: String }],
        wallet: { type: Object },
    },
    { timestamps: true }
);

merchantSchema.pre('save', function (next) {
    const merchant = this;
    if (!merchant.isModified('password')) {
        return next();
    }
    bcrypt.hash(merchant.password, 10, (err, hash) => {
        if (err) {
            return next(err);
        }
        merchant.password = hash;
        next();
    });
});

merchantSchema.pre('save', function (next) {
    const merchant = this;
    if (merchant.symbol) {
        return next();
    }
    const symbol = 'BT' + merchant.name.substr(0, 4);
    merchant.symbol = symbol.toUpperCase();
    next();
});

const handleE11000 = (error, res, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        next(new Error('There was a duplicate key error'));
    } else {
        next();
    }
};

merchantSchema.post('save', handleE11000);
merchantSchema.post('update', handleE11000);
merchantSchema.post('findOneAndUpdate', handleE11000);
merchantSchema.post('insertMany', handleE11000);

export const Merchant: Model<IMerchantModel> = model<IMerchantModel>(
    'Merchant',
    merchantSchema
);
