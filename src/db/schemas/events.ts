import { Document, Model, model, Schema } from 'mongoose';
import logger from '../../logger';
import { IEvent } from '../interfaces/events';

export interface IEventModel extends IEvent, Document {}

export let eventSchema: Schema = new Schema(
  {
    event: Schema.Types.Mixed
  },
  { timestamps: true }
);

export const Events: Model<IEventModel> = model<IEventModel>(
  'Events',
  eventSchema
);
