import * as Agenda from 'agenda';
import { bot } from '../App';
import logger from '../logger';
import { User } from './schemas/user';

const agenda = new Agenda({
    db: { address: process.env.MONGODB_URI, collection: 'jobs' },
});

agenda.on('ready', () => {
    logger.info('agenda ready');
    agenda.start();
});

agenda.define('setReminder', (job, done) => {
    // set setReminder to 27 days after data of renewal
    const data: any = job.attrs.data;
    // find User
    User.findOne({ userBotId: data.data.userId })
        .then((user) => {
            bot.beginDialog(user.address, '/reminder', { data: data });
        })
        .catch((error) => {
            logger.error(error);
        });
    done();
});

export default agenda;
