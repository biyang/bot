export interface IMerchant {
  email?: string;
  name?: string;
  symbol?: string;
  password?: string;
  phone?: string;
  transactions?: any;
  logoURL?: string;
  wallet?: any;
}
