export interface IUser {
  email?: string;
  name?: string;
  passcode?: string;
  paymentAuth?: any;
  authorization?: any;
  billsAuth?: any;
  network?: string;
  phone?: string;
  address?: any;
  activated?: boolean;
  addresses?: any;
  primaryChannel?: any;
  initialReference?: string;
  transactions?: any;
  userBotId?: string;
  wallet?: any;
  beneficiaries?: any;
  subscriptions?: any;
  whitelisted?: boolean;
}
