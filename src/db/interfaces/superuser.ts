export interface ISuperUser {
  username?: string;
  password?: string;
  roles?: string[];
}
