export interface ITransaction {
  amount?: number;
  address: object;
  status?: string;
  product?: string;
  description?: string;
  tags?: any;
  reference?: string;
  user?: any;
  merchant?: any;
  identity?: any;
}
