export interface IBeneficiary {
  name?: string;
  identifier?: any;
  user?: any;
}
