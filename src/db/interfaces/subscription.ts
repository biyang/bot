export interface ISubscription {
  data?: any;
  identity?: any;
  product?: string;
  address?: any;
  user?: any;
}
