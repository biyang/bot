export interface IService {
  service: string;
  status?: string;
  since?: any;
}
