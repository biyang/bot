import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import * as express from 'express';
import mongoose = require('mongoose');
import * as morgan from 'morgan';
import * as path from 'path';
import logger from './logger';
const dd_options = {
    response_code: true,
    tags: ['app:my_app'],
};

const connect_datadog = require('connect-datadog')(dd_options);

import * as builder from 'botbuilder';

import * as passport from 'passport';
import { jwtLogin } from './routes/merchants/passport';

passport.use(jwtLogin);

import { MongoDbBotStorage, MongoDBStorageClient } from 'mongo-bot-storage';

import mWaveRouter from './routes/mWaveRouter';
import PaystackRouter from './routes/paystack';

import MerchantRouter from './routes/merchants/admin';
import AuthRouter from './routes/merchants/auth';
import InvoiceRouter from './routes/merchants/invoices';
import TransactionsRouter from './routes/merchants/transactions';

const connector = new builder.ChatConnector({
    appId: '8f713deb-7849-428c-b1ae-85e844a9051b',
    appPassword: '1VEEPV73JhsBrxCniz1kSGM',
});

const bot = new builder.UniversalBot(connector);

import intents from './bot/Intents';
import BroadcastRouter from './routes/BroadcastRouter';
import MonitorRouter from './routes/MonitorRouter';
import SupportRouter from './routes/SupportRouter';
import Tap2PayRouter from './routes/Tap2PayRouter';
import TicketsRouter from './routes/transport/index';

import './utils/kue';

const whitelist = [
    'http://localhost:8080',
    'http://localhost:3000',
    'http://localhost:80',
    'http://localhost',
    'https://secure.biya.com.ng',
    'biyamerch.herokuapp.com',
    'https://biyamerch.herokuapp.com',
    'support.biya.com.ng',
    'merchants.biya.com.ng',
    'https://support.biya.com.ng',
    'https://merchants.biya.com.ng',
    'https://botservice.hosting.portal.azure.net',
    'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop',
];
const corsOptions = {
    origin(origin, callback) {
        callback(null, true);
        // if (whitelist.indexOf(origin) !== -1 || origin === undefined) {
        //     callback(null, true);
        // } else {
        //     callback(new Error('Not allowed by CORS'));
        // }
    },
    optionsSuccessStatus: 200, // some legacy browsers (IE11, various SmartTVs) choke on 204
};

/////////////////////////
/// import middleware ///
/////////////////////////
import './bot/Middleware';

//////////////////////
/// import dialogs ///
/////////////////////
import './bot/dialogs/badWords';
import './bot/dialogs/bills/DSTV';
import './bot/dialogs/bills/GOTV';
import './bot/dialogs/bills/IKEDC';
import './bot/dialogs/bills/PayBill';
import './bot/dialogs/bills/SMILE';
import './bot/dialogs/bills/spectranet';
import './bot/dialogs/bills/subscriptions';
import './bot/dialogs/broadcast';
import './bot/dialogs/deleteAccount';
import './bot/dialogs/Emoji';
import './bot/dialogs/error';
import './bot/dialogs/firstRun';
import './bot/dialogs/general/index';
import './bot/dialogs/general/sendReciept';
import './bot/dialogs/general/stuckOrCancel';
import './bot/dialogs/goodbye';
import './bot/dialogs/goodWords';
import './bot/dialogs/hello';
import './bot/dialogs/Help';
import './bot/dialogs/info';
import './bot/dialogs/longtime';
import './bot/dialogs/menu';
import './bot/dialogs/notify';
import './bot/dialogs/onDefault';
import './bot/dialogs/recharge/index';
import './bot/dialogs/recharge/otherRecharge';
import './bot/dialogs/recharge/selfRecharge';
import './bot/dialogs/resolutions/ExpiredCards';
import './bot/dialogs/resolutions/InsufficientFunds';
import './bot/dialogs/settings';
import './bot/dialogs/support';
import './bot/dialogs/tap2pay/index';
import './bot/dialogs/thanks';
import './bot/dialogs/tickets/index';
import './bot/dialogs/transfer/index';

/////////////////////////
/// import prompts ///
/////////////////////////
import * as askDstvSmartNo from './bot/prompts/askDstvSmartNo';
import * as askGotvSmartNo from './bot/prompts/askGotvSmartNo';
import * as askIkedcPrepaid from './bot/prompts/askIkedcPrepaid';
import * as askNUBAN from './bot/prompts/askNUBAN';
import * as askPhoneNumber from './bot/prompts/askPhoneNumber';
import * as askPhoneNumberForRecharge from './bot/prompts/askPhoneNumberForRecharge';
import * as askRechargeAmount from './bot/prompts/askRechargeAmount';
import * as askSmileNo from './bot/prompts/askSmileNo';
//////////////////////
/// load intents /////
//////////////////////
bot.dialog('/', intents);

/// CREATE CUSTOM PROMPTS
askRechargeAmount.create(bot);
askPhoneNumber.create(bot);
askPhoneNumberForRecharge.create(bot);
askDstvSmartNo.create(bot);
askGotvSmartNo.create(bot);
askSmileNo.create(bot);
askIkedcPrepaid.create(bot);
askNUBAN.create(bot);

// Creates and configures an ExpressJS web server.
class App {
    // ref to Express instance
    public express: express.Application;

    // Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.database();
        this.routes();
    }

    // Configure Express middleware.
    private middleware(): void {
        this.express.use(morgan('dev', { stream: logger.stream }));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(cors(corsOptions));
        this.express.use(passport.initialize());
        this.express.use(connect_datadog);
    }

    // configure mongo database connection

    private database(): void {
        const MONGODB_CONNECTION: string = process.env.MONGODB_URI;

        // use q promises
        global.Promise = require('q').Promise;
        mongoose.Promise = global.Promise;
        require('./db/schemas/beneficiary');
        mongoose.connect(MONGODB_CONNECTION, {
            promiseLibrary: global.Promise,
            useMongoClient: true,
        });
        // mongoose.createConnection(MONGODB_CONNECTION);
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', () => {
            // we're connected!
            bot.set(
                'storage',
                new MongoDbBotStorage(
                    new MongoDBStorageClient({
                        mongooseConnection: db,
                    })
                )
            );
            logger.info('db connected');
            require('./monitor');
        });
        process.on('unhandledRejection', (error) => {
            // Will print "unhandledRejection err is not defined"
            logger.info(error.message);
        });
    }

    // Configure API endpoints.
    private routes(): void {
        const router = express.Router();
        // placeholder route handler
        router.get('/', (req, res, next) => {
            res.json({
                message: 'Look away...',
            });
        });
        this.express.use('/', router);

        /////////////////////
        // bot connector ////
        //////////////////////

        this.express.use('/messages', connector.listen());
        ////////////////////
        // other routes ///
        ///////////////////
        this.express.use('/v1/merchant', MerchantRouter);
        this.express.use('/v1/auth', AuthRouter);
        this.express.use('/v1/invoices', InvoiceRouter);
        this.express.use('/v1/transactions', TransactionsRouter);

        this.express.use('/v1/paystack', PaystackRouter);
        this.express.use('/v1/mwave', mWaveRouter);

        this.express.use('/v1/broadcast', BroadcastRouter);
        this.express.use('/v1/tap2pay', Tap2PayRouter);

        this.express.use('/v1/support', SupportRouter);

        this.express.use('/v1/monitor', MonitorRouter);

        this.express.use('/v1/tickets', TicketsRouter);
    }
}

export { bot };

export default new App().express;
