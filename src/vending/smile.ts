import * as CircularJSON from 'circular-json';
import * as parseStringModule from 'xml2js';
const kue = require('kue');
import logger from '../logger';
import { bot } from '../App';
import { Transaction } from '../db/schemas/transaction';
import { stripIndents } from 'common-tags';
import { SlackNotify } from '../monitor/SlackService';
import { dispenseSMILE } from '../utils/airvend';
const parseString = parseStringModule.parseString;

var Redis = require('ioredis');

// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});

// var jobs = kue.createQueue();

jobs.on('error', function (err) {
    logger.error(err);
});

function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();

export const doSmileDispense = (transaction) => {
    const job = jobs
        .create('smile', transaction)
        .attempts(5)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    logger.info(transaction);
    job.on('complete', function () {
        // send message
        SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: stripIndents`Successful SMILE dispense`,
            fields: {
                Service: 'SMILE',
                Amount: transaction.amount,
                Name: transaction.identity.customerName,
                Account: transaction.identity.customerNumber,
            },
            emoji: ':white_check_mark:',
        });
        Transaction.findOneAndUpdate(
            { reference: transaction.reference },
            { status: 'success' }
        )
            .then((info) => logger.info(info))
            .catch((error) => logger.error(error));

        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    plan: transaction.description,
                    type: transaction.product,
                });
            });
        }
        logger.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.send(
                    'Network error, we will automatically try again in a few seconds.'
                );
            });
        }
        logger.error('Job failed');
    });
    job.save();
};

jobs.process('smile', async (job, done) => {
    const transaction = job.data;
    logger.info(transaction);
    try {
        const invoicePeriod = transaction.identity.invoicePeriod;
        const customerNumber = transaction.identity.customerNumber;
        const amount = transaction.amount;
        const customerName = transaction.identity.customerName;

        logger.info({
            amount,
            identity: { customerNumber, invoicePeriod, customerName },
        });
        const dispense = await dispenseSMILE(transaction);
        if (dispense.status) {
            if (dispense.status === 200) {
                parseString(dispense.data, async (err, result) => {
                    try {
                        if (err) {
                            logger.error(err);
                            throw Error('There has been a problem');
                        }
                        if (
                            result &&
                            result.VendResponse.ResponseCode[0] === '0'
                        ) {
                            await Transaction.findOneAndUpdate(
                                { reference: transaction.reference },
                                { status: 'success' }
                            );
                            done();
                        } else {
                            logger.error(result);
                            const error =
                                result && result.VendResponse.ResponseCode[0];
                            logger.error('error', error);
                            throw new MyError(
                                error,
                                'DispenseError',
                                transaction
                            );
                        }
                    } catch (error) {
                        logger.error('error 2', CircularJSON.stringify(error));
                        const message = error.message
                            ? error.message
                            : 'There was an error';
                        return done(new Error(message));
                    }
                });
            } else {
                logger.error('error 1', dispense.data);
                throw new MyError(
                    dispense.data.message,
                    'DispenseError',
                    transaction
                );
            }
        } else {
            logger.error('error 2', dispense.data);
            throw new MyError(
                dispense.data.message,
                'DispenseError',
                transaction
            );
        }
    } catch (error) {
        logger.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
