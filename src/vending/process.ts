import { Transaction } from '../db/schemas/transaction';
import { doDstvDispense, doGotvDispense, dispenseAirtimeB } from './index';
import { doElectirictyDispense } from './electricity';
import { doSmileDispense } from './smile';
import logger from '../logger';

export default async function processTransaction(reference) {
    // find transaction
    // swicth kue based on transaction type
    const transaction = await Transaction.findOne({ reference });
    logger.info(transaction, 'transaction');
    if (transaction) {
        if (['init', 'pending'].includes(transaction.status)) {
            switch (transaction.product.toLowerCase()) {
                case 'ikedc':
                case 'ibedc':
                case 'ekdc':
                case 'phdc':
                case 'aedc':
                case 'jed':
                case 'kedco':
                case 'kaedco':
                    doElectirictyDispense(transaction);
                    break;
                case 'airtime':
                case 'otherr':
                    dispenseAirtimeB(transaction);
                    break;
                case 'dstv':
                    doDstvDispense(transaction);
                    break;
                case 'gotv':
                    doGotvDispense(transaction);
                    break;
                case 'smile':
                    doSmileDispense(transaction);
                    break;
                default:
                    break;
            }
        }
    }
}
