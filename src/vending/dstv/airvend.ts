import axios from 'axios';
import * as shortid from 'simple-id';

import logger from '../../logger';

let logId;

axios.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger.info('Starting Request', {
        logId: logId,
        url: request.url,
        ...(request.data && request.data.body),
    });
    return request;
});

axios.interceptors.response.use((response) => {
    logger.info('Response:', {
        ...(response && response.data),
        logId: logId,
    });
    return response;
});

const networks = {
    AIRTEL: 1,
    GLO: 3,
    MTN: 2,
    ETISALAT: 4,
};

const airvendUsername = process.env.AIRVEND_USER;
const airvendPassword = process.env.AIRVEND_PASS;

export const airvend_credit = (amount, phone, network) => {
    const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    const postData = {
        amount: amount,
        msisdn: phone,
        networkid: networks[network],
        password: airvendPassword,
        ref: reference,
        type: '1',
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net/se/payments',
        method: 'post',
        params: postData,
        url: `/vtu/`,
    });
};
