import * as CircularJSON from 'circular-json';
const kue = require('kue');
import logger from '../logger';
import { bot } from '../App';
import { Transaction } from '../db/schemas/transaction';
import { stripIndents } from 'common-tags';
import { SlackNotify } from '../monitor/SlackService';
import { dispenseWithVTPass } from '../utils/vtpass';
import { sendIkedcPin } from '../utils/sms';

var Redis = require('ioredis');

// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});

// var jobs = kue.createQueue();

jobs.on('error', function (err) {
    logger.error(err);
});

function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();

export const doElectirictyDispense = (transaction) => {
    const job = jobs
        .create('electricity', transaction)
        .attempts(1)
        .removeOnComplete(true);

    job.on('complete', function () {
        // send message

        Transaction.findOne({ reference: transaction.reference })
            .then((info) => {
                if (info.address) {
                    bot.loadSession(transaction.address, (err, session) => {
                        session.replaceDialog('/notify', {
                            ...info.identity,
                            type: transaction.product,
                        });
                    });
                }

                sendIkedcPin(
                    info,
                    info.identity.pin,
                    info.identity.serial,
                    info.product
                );

                SlackNotify({
                    username: 'New Sale!',
                    channel: '#sales',
                    text: stripIndents`Successful ${transaction.product.toUpperCase()} dispense`,
                    fields: {
                        Service: transaction.product.toUpperCase(),
                        Amount: transaction.amount,
                        Name: transaction.identity.customername,
                        Account: transaction.identity.customerNumber,
                        PIN: info.identity.pin,
                        Units: info.identity.amount,
                        SerialNo: info.identity.serial,
                    },
                    emoji: ':white_check_mark:',
                });

                logger.info('Job complete');
            })
            .catch((error) => logger.error(error));
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.send(
                    'Network error, we will automatically try again in a few seconds.'
                );
            });
        }
        logger.error('Job failed');
    });
    job.save();
};

jobs.process('electricity', async (job, done) => {
    const transaction = job.data;
    logger.info(transaction);
    try {
        const invoicePeriod = transaction.identity.invoicePeriod;
        const customerNumber = transaction.identity.customerNumber;
        const phone = transaction.identity.phone;
        const amount = transaction.amount;
        const customerName = transaction.identity.customerName;

        logger.info({
            amount,
            identity: { customerNumber, invoicePeriod, customerName },
        });

        const { data } = await dispenseWithVTPass({
            ...transaction,
            amount,
            phone,
            type: transaction.product,
        });

        if (data.code && data.code === '000') {
            if (
                data.response_description === 'TRANSACTION PROCESSING - PENDING'
            ) {
                const error = data && data.code;
                throw new MyError(error, 'DispenseError', transaction);
            } else {
                const pin =
                    data.purchased_code || data.token || data.mainToken || '';
                const serial = '';
                const amountOfPower = data.units || data.mainTokenUnits || '';
                const updateObject = {
                    status: 'success',
                    'identity.pin': pin,
                    'identity.serial': serial || '',
                    'identity.amount': amountOfPower || '',
                };
                await Transaction.findOneAndUpdate(
                    { reference: transaction.reference },
                    updateObject
                );
            }
            done();
        } else {
            const error = data && data.code;
            logger.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    } catch (error) {
        logger.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
