import * as CircularJSON from 'circular-json';
import * as parseStringModule from 'xml2js';
const kue = require('kue');
import logger from '../logger';
import { airvend_credit } from './airtime/airvend';
import { dispenseAirtimeBackup, verifyDispense } from '../utils/vendbackup';
import { bot } from '../App';
import { Transaction } from '../db/schemas/transaction';
import { sendMail } from '../utils/mailer';
import { stripIndents } from 'common-tags';
import { SlackNotify } from '../monitor/SlackService';
import { dispenseDSTV, dispenseGOTV } from '../utils/airvend';
import { dispenseWithVTPass } from '../utils/vtpass';
const parseString = parseStringModule.parseString;

var Redis = require('ioredis');

// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});

// var jobs = kue.createQueue();

jobs.on('error', function (err) {
    logger.error(err);
});

function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();

export const dispenseAirtime = (transaction) => {
    const job = jobs
        .create('airtime', transaction)
        .attempts(1)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    job.on('complete', function () {
        // send message
        SlackNotify({
            username: 'New Sale via backup!',
            channel: '#sales',
            text: stripIndents`Succesful airtime dispense`,
            fields: {
                Network: transaction.description,
                Amount: transaction.amount,
                Number: transaction.identity.phone,
                Name: transaction.identity.customerName,
            },
            emoji: ':white_check_mark:',
        });
        Transaction.findOneAndUpdate(
            { reference: transaction.reference },
            { status: 'success' }
        )
            .then(console.log)
            .catch(console.log);

        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    network: transaction.description,
                    amount: transaction.amount,
                    number: transaction.identity.phone,
                    type: transaction.product === 'airtime' ? 'self' : 'otherR',
                });
            });
        }
        logger.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.send(
                    'Network error, we will automatically try again in a few seconds.'
                );
            });
        }
        // dispenseAirtimeB(transaction);
        logger.error('Job failed');
    });
    job.save();
};

export const dispenseAirtimeB = (transaction) => {
    const backupjob = jobs
        .create('airtimeBackup', transaction)
        .attempts(6)
        .removeOnComplete(true);
    backupjob
        .on('complete', function () {
            // send message
            SlackNotify({
                username: 'New Sale!',
                channel: '#sales',
                text: stripIndents`Succesful airtime dispense`,
                fields: {
                    Network: transaction.description,
                    Amount: transaction.amount,
                    Number: transaction.identity.phone,
                    Name: transaction.identity.customerName,
                },
                emoji: ':white_check_mark:',
            });
            Transaction.findOneAndUpdate(
                { reference: transaction.reference },
                { status: 'success' }
            )
                .then(console.log)
                .catch(console.log);

            if (transaction.address) {
                bot.loadSession(transaction.address, (err, session) => {
                    session.replaceDialog('/notify', {
                        network: transaction.description,
                        amount: transaction.amount,
                        number: transaction.identity.phone,
                        type:
                            transaction.product === 'airtime'
                                ? 'self'
                                : 'otherR',
                    });
                });
            }
            logger.info('Job complete');
        })
        .on('failed', function () {
            const mailOptions = {
                from: '"Dispense Error Alert ⚠" <alerts@biya.com.ng>', // sender address
                html: `<pre>${transaction}</pre>`, // html body.ng
                subject: 'Dispense Error Alert', // Subject line
                to: 'olumytee.ojo@gmail.com', // list of receivers
            };
            SlackNotify({
                username: 'Failed dispense',
                channel: '#errors',
                text: stripIndents`Failed Airtime dispense`,
                fields: {
                    Network: transaction.description,
                    Amount: transaction.amount,
                    Number: transaction.identity.phone,
                    Name: transaction.identity.customerName,
                },
                emoji: ':bangbang:',
            });
            sendMail(mailOptions);
            Transaction.findOneAndUpdate(
                { reference: transaction.reference },
                { status: 'pending' }
            )
                .then(console.log)
                .catch(console.log);
            if (transaction.address) {
                bot.loadSession(transaction.address, (err, session) => {
                    session.send(
                        'Network error, we will automatically try again in a few seconds.'
                    );
                });
            }
            logger.error('Job failed');
            // dispenseAirtime(transaction);
        });
    backupjob.save();
};

jobs.process('airtime', async (job, done) => {
    const transaction = job.data;
    try {
        const reference = transaction.reference;
        const phone = transaction.identity.phone;
        const amount = transaction.amount;
        const network = transaction.description;
        const dispense = await dispenseAirtimeBackup({
            amount,
            phone,
            network,
        });
        if (dispense.data) {
            if (dispense.data && dispense.data.status === 'ORDER_RECEIVED') {
                await Transaction.findOneAndUpdate(
                    { reference: transaction.reference },
                    { status: 'success' }
                );
                done();
            } else {
                logger.error('error 1', dispense.data);
                throw new MyError(
                    dispense.data.message,
                    'DispenseError',
                    transaction
                );
            }
        } else {
            logger.error('error 2', dispense.data);
            throw new MyError(
                dispense.data.message,
                'DispenseError',
                transaction
            );
        }

        // const dispense = await credit(network, phone, amount, reference);
        // if (dispense.data) {
        //     if (dispense.data.status === 'OK') {
        //         await Transaction.findOneAndUpdate(
        //             { reference: transaction.reference },
        //             { status: 'success' }
        //         );
        //         done();
        //     } else {
        //         logger.error('error 1', dispense.data);
        //         throw new MyError(
        //             dispense.data.message,
        //             'DispenseError',
        //             transaction
        //         );
        //     }
        // } else {
        //     logger.error('error 2', dispense.data);
        //     throw new MyError(
        //         dispense.data.message,
        //         'DispenseError',
        //         transaction
        //     );
        // }
    } catch (error) {
        logger.error('error 3', CircularJSON.stringify(error));
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});

jobs.process('airtimeBackup', async (job, done) => {
    logger.info('running backup job');
    const transaction = job.data;
    try {
        const phone = transaction.identity.phone;
        const amount = transaction.amount;
        const network = transaction.description;

        const { data } = await dispenseWithVTPass({
            ...transaction,
            network,
            amount,
            phone,
            type: 'airtime',
        });
        if (data.code && data.code === '000') {
            await Transaction.findOneAndUpdate(
                { reference: transaction.reference },
                { status: 'success' }
            );
            done();
        } else {
            logger.info(data);
            const error = data && data.code;
            logger.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    } catch (error) {
        logger.error('error 3', CircularJSON.stringify(error));
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});

export const doDstvDispense = (transaction) => {
    const job = jobs
        .create('dstv', transaction)
        .attempts(5)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    logger.info(transaction);
    job.on('complete', function () {
        // send message
        SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: stripIndents`Successful dstv dispense`,
            fields: {
                Service: 'DSTV',
                Amount: transaction.amount,
                Name: transaction.identity.customerName,
                Account: transaction.identity.customerNumber,
            },
            emoji: ':white_check_mark:',
        });
        Transaction.findOneAndUpdate(
            { reference: transaction.reference },
            { status: 'success' }
        )
            .then((info) => logger.info(info))
            .catch((error) => logger.error(error));

        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    amount: transaction.amount,
                    type: transaction.product,
                });
            });
        }
        logger.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.send(
                    'Network error, we will automatically try again in a few seconds.'
                );
            });
        }
        logger.error('Job failed');
    });
    job.save();
};

jobs.process('dstv', async (job, done) => {
    const transaction = job.data;
    logger.info(transaction);
    try {
        const amount = transaction.amount;
        const phone = transaction.identity.phone;

        const { data } = await dispenseWithVTPass({
            ...transaction,
            amount,
            phone,
            type: 'dstv',
        });
        if (data.code && data.code === '000') {
            await Transaction.findOneAndUpdate(
                { reference: transaction.reference },
                { status: 'success' }
            );
            done();
        } else {
            const error = data && data.code;
            logger.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    } catch (error) {
        logger.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});

export const doGotvDispense = (transaction) => {
    const job = jobs
        .create('gotv', transaction)
        .attempts(5)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    logger.info(transaction);
    job.on('complete', function () {
        // send message
        SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: stripIndents`Successful gotv dispense`,
            fields: {
                Service: 'GoTV',
                Amount: transaction.amount,
                Name: transaction.identity.customerName,
                Account: transaction.identity.customerNumber,
            },
            emoji: ':white_check_mark:',
        });
        Transaction.findOneAndUpdate(
            { reference: transaction.reference },
            { status: 'success' }
        )
            .then((info) => logger.info(info))
            .catch((error) => logger.error(error));

        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    amount: transaction.amount,
                    type: transaction.product,
                });
            });
        }
        logger.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            bot.loadSession(transaction.address, (err, session) => {
                session.send(
                    'Network error, we will automatically try again in a few seconds.'
                );
            });
        }
        logger.error('Job failed');
    });
    job.save();
};

jobs.process('gotv', async (job, done) => {
    const transaction = job.data;
    logger.info(transaction);
    try {
        const amount = transaction.amount;
        const phone = transaction.identity.phone;

        const { data } = await dispenseWithVTPass({
            ...transaction,
            amount,
            phone,
            type: 'gotv',
        });
        if (data.code && data.code === '000') {
            await Transaction.findOneAndUpdate(
                { reference: transaction.reference },
                { status: 'success' }
            );
            done();
        } else {
            const error = data && data.code;
            logger.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    } catch (error) {
        logger.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
