import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import { IMerchantModel, Merchant } from '../../db/schemas/merchant';
import { Transaction } from '../../db/schemas/transaction';
import logger from '../../logger';
import { verify } from '../../utils/jwt';
export class TicketsRouter {
  public router: Router;

  /**
   * Initialize the MerchantRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }
  public async verify(req: Request, res: Response, next: NextFunction) {
    try {
      const token = req.query.token;
      verify(token, (err, decoded) => {
        if (err) {
          logger.info('token incorrect');
          res.redirect('https://biya.com.ng/error');
          // res.status(401)
          // .send({
          //     message: "Invalid Token!",
          //     status: res.status,
          // });
        } else {
          Transaction.findOne({ reference: decoded.reference })
            .then(transaction => {
              if (transaction && transaction.identity.valid) {
                res.redirect('https://biya.com.ng/success');
              } else {
                res.redirect('https://biya.com.ng/error');
              }
            })
            .catch(error => {
              logger.error(err);
              const message = err.message ? err.message : 'There was an error';
              res.status(400).send({
                message: message
              });
            });
        }
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  public init() {
    this.router.get('/', this.verify);
  }
}

// Create the MerchantRouter, and export its configured Express.Router
const ticketsRoutes = new TicketsRouter();
ticketsRoutes.init();

export default ticketsRoutes.router;
