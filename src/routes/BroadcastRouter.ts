import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import * as moment from 'moment';
import { bot } from '../App';
import { IUserModel, User } from '../db/schemas/user';
import logger from '../logger';

export class BroadcastRouter {
    public router: Router;

    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    /**
     * Broadcast to all users.
     */
    public broadCast(req: Request, res: Response, next: NextFunction) {
        const formData = req.body;

        User.find({})
            .then((users: any) => {
                const message = formData.message;
                const dialog = formData.dialog;
                let redirectDialog;
                if (dialog) {
                    redirectDialog = dialog;
                } else {
                    redirectDialog = 'broadcast';
                }
                users.forEach((user: any) => {
                    bot.beginDialog(
                        user.addresses[user.primaryChannel],
                        `/${redirectDialog}`,
                        { message: message }
                    );
                });
                res.status(200).send({
                    message: 'Success',
                });
            })
            .catch((error) => {
                logger.error(error);
                const message = error.message
                    ? error.message
                    : 'There was an error';
                res.status(500).send({
                    message: message,
                });
            });
    }

    public broadCastPeriod(req: Request, res: Response, next: NextFunction) {
        const formData = req.body;
        const days = formData.days;

        User.find({
            updatedAt: {
                $lte: moment().subtract(parseInt(days, 10), 'days').toDate(),
            },
        })
            .then((users: any) => {
                // console.log('users', users)
                const message = formData.message;
                const dialog = formData.dialog;
                let redirectDialog;
                if (dialog) {
                    redirectDialog = dialog;
                } else {
                    redirectDialog = 'broadcast';
                }
                _.forEach(users, (user: any) => {
                    bot.beginDialog(
                        user.addresses[user.primaryChannel],
                        `/${redirectDialog}`,
                        { message: message }
                    );
                });
                res.status(200).send({
                    message: 'Success',
                    status: res.status,
                });
            })
            .catch((error) => {
                logger.error(error);
                const message = error.message
                    ? error.message
                    : 'There was an error';
                res.status(500).send({
                    message: message,
                });
            });
    }

    public cc(req, res, next) {
        User.find({})
            .then((user) => {
                user.forEach((element) => {
                    const channelId = element.primaryChannel;
                    User.findByIdAndUpdate(element._id, {
                        addresses: {
                            [channelId]: element.address,
                        },
                    })
                        .then(() => {
                            logger.info('done');
                            // session.replaceDialog('welcomeUser');
                        })
                        .catch((error) => {
                            // logger.error(error);
                        });
                });
            })
            .catch((error) => {
                logger.error(error);
            });
        res.sendStatus(200);
    }

    public broadCastChannel(req: Request, res: Response, next: NextFunction) {
        const formData = req.body;
        const channel = formData.channel;

        User.find({ primaryChannel: channel })
            .then((users: any) => {
                // console.log('users', users)
                const message = formData.message;
                const dialog = formData.dialog;
                let redirectDialog;
                if (dialog) {
                    redirectDialog = dialog;
                } else {
                    redirectDialog = '/broadcast';
                }
                _.forEach(users, (user: any) => {
                    bot.beginDialog(
                        user.addresses[user.primaryChannel],
                        `${redirectDialog}`,
                        { message: message }
                    );
                });
                res.status(200).send({
                    message: 'Success',
                });
            })
            .catch((error) => {
                logger.error(error);
                const message = error.message
                    ? error.message
                    : 'There was an error';
                res.status(500).send({
                    message: message,
                });
            });
    }
    /**
     * send message to one user.
     */
    public singleCast(req: Request, res: Response, next: NextFunction) {
        const formData = req.body;
        User.findOne({ phone: formData.phone })
            .then((user: IUserModel) => {
                const args = formData.args;
                const dialog = formData.dialog ? formData.dialog : 'broadcast';
                const channel = formData.channel
                    ? formData.channel
                    : user.primaryChannel;

                bot.beginDialog(user.addresses[channel], dialog, args);
                res.status(200).send({
                    message: 'Success',
                    status: res.status,
                });
            })
            .catch((error) => {
                logger.error(error);
                const message = error.message
                    ? error.message
                    : 'There was an error';
                res.status(500).send({
                    message: message,
                });
            });
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    public init() {
        this.router.post('/', this.broadCast);
        this.router.post('/single', this.singleCast);
        this.router.post('/days', this.broadCastPeriod);
        this.router.post('/channel', this.broadCastChannel);
        this.router.post('/ema', this.cc);
    }
}

// Create the MerchantRouter, and export its configured Express.Router
const broadcastRoutes = new BroadcastRouter();
broadcastRoutes.init();

export default broadcastRoutes.router;
