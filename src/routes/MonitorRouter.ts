import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import { bot } from '../App';
import { IUserModel, User } from '../db/schemas/user';
import logger from '../logger';

export class MonitorRouter {
    public router: Router;

    constructor() {
        this.router = Router();
        this.init();
    }

    public async monitor(req: Request, res: Response, next: NextFunction) {
        res.sendStatus(200);
    }

    public init() {
        this.router.get('/', this.monitor);
    }
}

// Create the MerchantRouter, and export its configured Express.Router
const monitorRoutes = new MonitorRouter();
monitorRoutes.init();

export default monitorRoutes.router;
