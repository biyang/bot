import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import * as queryString from 'query-string';
import { bot } from '../App';
import { ITransactionModel, Transaction } from '../db/schemas/transaction';
import { IUserModel, User } from '../db/schemas/user';
import logger from '../logger';
import { asyncVerify, verify } from '../utils/jwt';
import { sendMail } from '../utils/mailer';
import { tokenizeCard, transfer } from '../utils/mWave';

export class WaveRouter {
    public router: Router;

    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }
    public async testtrans(req: Request, res: Response, next: NextFunction) {
        const ref = req.params.ref;
        const transaction = await Transaction.findOneAndUpdate(
            { reference: ref },
            { status: 'success' }
        );
        const user = await User.findById(transaction.user);
        res.status(401).send({
            message: 'Format is Authorization: Bearer [token]',
            user: user,
        });
    }

    // link 'http://localhost:3001/v1/mwave/confirm/qdslx9ic?rc=00&transa=200'

    public async sendSuccess(req: Request, res: Response, next: NextFunction) {
        let address;
        try {
            const ref = req.params.ref;
            const transaction = await Transaction.findOne({ reference: ref });
            const user = await User.findById(transaction.user);
            // // save beneficiary
            address = user.addresses[transaction.identity.sourceChannel];
            const url = req.url;
            const parsed = queryString.parse(url.split('?')[1]);
            // verify transaction
            if (parsed['rc'] === '00') {
                await Transaction.findOneAndUpdate(
                    { reference: ref },
                    { status: 'success' }
                );
                bot.beginDialog(address, '/notify', {
                    transaction: transaction,
                    type: 'transfer',
                });
                res.redirect('https://biya.com.ng/success');
            } else {
                await Transaction.findOneAndUpdate(
                    { reference: ref },
                    { status: 'failed' }
                );
                const responseCode = parsed['rc'];
                let responseMessage;
                // swicth responsemessage
                switch (responseCode) {
                    case 'RR-57':
                        responseMessage =
                            'Transaction declined by financial institution. Please try again later. Thanks!';
                        break;
                    case 'RR-91':
                        responseMessage =
                            'Bank or Switch network error. Please try again later. Thanks!';
                        break;
                    default:
                        responseMessage = `Transfer could not be completed. Please try again later. Thanks.`;
                        break;
                }
                throw Error(`${responseMessage}`);
            }
            // res.sendStatus(200);
        } catch (error) {
            logger.error(error);

            const mailOptions = {
                from: '"Transfer Error Alert ⚠" <alerts@biya.com.ng>', // sender address
                html: `<pre> ${error} </pre>`, // html body.ng
                subject: 'Trabsfer Error Alert', // Subject line
                to: 'olumytee.ojo@gmail.com', // list of receivers
            };
            sendMail(mailOptions);
            const message = error.message
                ? error.message
                : 'There was an error';
            if (address) {
                bot.beginDialog(address, '/error', { error: message });
            }
            // res.status(401).json({ message: message, status: 401,})
            res.redirect('https://biya.com.ng/error');
        }
    }
    public async chargeWithoutToken(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        let token;

        if (req.headers && req.headers.authorization) {
            const headers: any = req.headers;
            const parts = headers.authorization.split(' ');
            if (parts.length === 2) {
                const scheme = parts[0];
                const credentials = parts[1];

                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                }
            } else {
                logger.info('Format is Authorization: Bearer [token]');
                res.status(401).send({
                    message: 'Format is Authorization: Bearer [token]',
                });
            }
        } else {
            logger.info('No auth header found');
            res.status(401).send({
                message: 'No authorization header was found',
            });
        }

        const decoded: any = await asyncVerify(token);
        const parameters = ['card_no', 'cvv', 'expiry_month', 'expiry_year'];
        const formData: any = _.pick(req.body, parameters);

        try {
            const transaction = await Transaction.findOne({
                reference: decoded.reference,
            });
            const user: IUserModel = await User.findOne({
                phone: decoded.user,
            });
            const data = {
                amount: transaction.amount,
                card_no: formData.card_no,
                cvv: formData.cvv,
                email: user.email,
                expiry_month: formData.expiry_month,
                expiry_year: formData.expiry_year,
                fee: 5,
                firstname: user.name ? user.name : 'Biya',
                lastname: user.phone,
                medium: 'mobile',
                phonenumber: '+234' + user.phone.slice(1),
                recipient_account_number: transaction.identity.accountNumber, // bank account number
                recipient_bank: transaction.identity.bankCode, // bank code
                redirecturl: `${process.env.CURRENT_ENV}mwave/confirm/${decoded.reference}`,
            };
            const transferInit: any = await transfer(data);
            if (transferInit.status === 'success') {
                const transactionData = transferInit.data;
                if (
                    transactionData.failedValidation &&
                    transactionData.authurl
                ) {
                    res.status(200).send({
                        data: transactionData.authurl,
                        message: 'url',
                    });
                } else {
                    res.status(200).send({
                        message: 'success',
                    });
                }
            }
        } catch (error) {
            let message = error.message ? error.message : 'There was an error';
            logger.error(error.error);
            if (error.msg) {
                message = error.msg;
            }
            res.status(504).send({
                message: message,
                status: 'Error',
            });
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    public init() {
        this.router.post('/charge', this.chargeWithoutToken);
        this.router.get('/confirm/:ref', this.sendSuccess);
    }
}

// Create the MerchantRouter, and export its configured Express.Router
const mWaveRoutes = new WaveRouter();
mWaveRoutes.init();

export default mWaveRoutes.router;
