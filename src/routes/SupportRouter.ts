import * as bcrypt from 'bcryptjs';
import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import * as parseStringModule from 'xml2js';

import { bot } from '../App';
import { Merchant } from '../db/schemas/merchant';
import { Superuser } from '../db/schemas/superuser';
import { Services } from '../db/schemas/services';
import { Transaction } from '../db/schemas/transaction';
import { User } from '../db/schemas/user';
import { adminSchema, validateBody, verifyToken } from '../helpers/validation';
import logger from '../logger';
import { asyncIssue, asyncVerify } from '../utils/jwt';

import processTransaction from '../vending/process';

import {
    dispenseDSTV,
    dispenseGOTV,
    dispenseSMILE,
    verifyDSTV,
    verifyGOTV,
    verifySMILE,
} from '../utils/airvend';
import { getNetwork, handleGetNetwork } from '../utils/finder';

import { verifyTransaction } from '../utils/paystack';
import { dispenseAirtime } from '../vending/';

const secret = process.env.PAYSTACK_TOKEN;
const newSecret = process.env.NEW_PAYSTACK_TOKEN;

import { sendSpectranetPin } from '../utils/sms';
import { dispenseTicket } from '../utils/tickets';

const parseString = parseStringModule.parseString;

export class SupportRouter {
    public router: Router;

    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }
    public async login(req: Request | any, res: Response, next: NextFunction) {
        try {
            const details = req.value.body;
            const admin = await Superuser.findOne({
                username: details.username,
            });
            if (admin) {
                // compare password
                bcrypt.compare(
                    details.password,
                    admin.password,
                    (err, result) => {
                        // res === true
                        if (err) {
                            res.status(401).send({
                                message: 'Invalid Username or Password',
                            });
                        } else {
                            if (result) {
                                admin.password = undefined;
                                asyncIssue(admin.toObject())
                                    .then((token) => {
                                        res.status(200).send({
                                            message: 'Success',
                                            token: token,
                                        });
                                    })
                                    .catch((error) => {
                                        logger.error(error);
                                        const message = error.message
                                            ? error.message
                                            : 'There was an error';
                                        res.status(400).send({
                                            message: message,
                                        });
                                    });
                            } else {
                                res.status(401).send({
                                    message: 'Invalid Username or Password',
                                });
                            }
                        }
                    }
                );
            } else {
                // user does not exist
                throw Error('Not a registered Admin');
            }
        } catch (err) {
            logger.error(err.message);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    public async register(
        req: Request | any,
        res: Response,
        next: NextFunction
    ) {
        try {
            const newAdmin = new Superuser(req.value.body);
            const user: any = await newAdmin.save();
            user.password = user.updatedAt = user.createdAt = undefined;
            const token = await asyncIssue(user.toObject());
            res.status(200).send({
                message: 'Success',
                token: token,
                user: user,
            });
        } catch (err) {
            logger.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    public async deleteTrx(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const transaction = await Transaction.findById(id);
            if (transaction.status === 'init') {
                await Transaction.remove({ _id: id });
                res.status(200).send({
                    message: 'Success',
                });
            } else {
                throw Error('Not Allowed');
            }
        } catch (error) {
            logger.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }
    public async markResolved(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const transaction = await Transaction.findById(id);
            if (['success', 'pending'].includes(transaction.status)) {
                await Transaction.findByIdAndUpdate(id, {
                    status: 'success',
                });
                const user = await User.findById(transaction.user);
                const address: any = transaction.address;
                if (address) {
                    if (transaction.product === 'airtime') {
                        bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            network: transaction.description,
                            number: transaction.identity.phone,
                            type: 'self',
                        });
                    } else if (transaction.product === 'otherR') {
                        bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            network: transaction.description,
                            number: transaction.identity.phone,
                            type: 'otherR',
                        });
                    } else if (transaction.product === 'dstv') {
                        bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            type: 'dstv',
                        });
                    } else if (transaction.product === 'gotv') {
                        bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            type: 'gotv',
                        });
                    } else if (transaction.product === 'smile') {
                        bot.beginDialog(address, '/notify', { type: 'smile' });
                    } else if (
                        ['ikedc', 'ibedc', 'ekdc', 'phdc', 'enugu'].includes(
                            transaction.product.toLowerCase()
                        )
                    ) {
                        bot.beginDialog(address, '/notify', {
                            type: transaction.product,
                            pin: transaction.identity.pin,
                            amount: transaction.identity.amount,
                        });
                    } else if (transaction.product === 'spectranet') {
                        bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            pin: transaction.identity['pinCode'],
                            serial: transaction.identity['serialNumber'],
                            type: 'spectranet',
                        });
                    }
                }
                res.status(200).send({
                    message: 'Success',
                });
            } else {
                throw Error('Not allowed');
            }
        } catch (error) {
            logger.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }
    public async autoResolve(req: Request, res: Response, next: NextFunction) {
        try {
            // get transactionId
            // check status
            // resolve if unresolved
            // send recipe
            function MyError(message, name, trx) {
                this.name = name;
                this.transaction = trx;
                this.message = message || 'Default Message';
                this.stack = new Error().stack;
            }
            MyError.prototype = new Error();

            const id = req.params.id;
            const transaction = await Transaction.findById(id);
            const secretToUse = ['airtime', 'otherR'].includes(
                transaction.product
            )
                ? secret
                : newSecret;

            const { status } = await verifyTransaction(secretToUse)(
                transaction.reference
            );

            if (!(status === 'success')) {
                return res.status(400).send({
                    message: 'Not a successful payment',
                });
            }

            if (['init', 'pending'].includes(transaction.status)) {
                processTransaction(transaction.reference);
                return res.status(200).send({ message: 'Retry queued' });
            } else {
                res.status(500).send({
                    message: 'Error',
                });
            }
        } catch (error) {
            logger.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }

    public async allUsers(req: Request, res: Response, next: NextFunction) {
        try {
            let page;
            if (req.query) {
                page = parseInt(req.query.page, 10);
            } else {
                page = 1;
            }

            const perPage = 50;
            let total;
            let users;

            total = await User.count({});
            users = await User.find({})
                .sort({ updatedAt: -1 })
                .limit(perPage)
                .skip((page - 1) * perPage);

            const nextPageUrl =
                page > 0 && total > perPage
                    ? process.env.CURRENT_ENV + `support/users?page=${page + 1}`
                    : '';
            const prevPageUrl =
                page > 1 && total > perPage
                    ? process.env.CURRENT_ENV + `support/users?page=${page - 1}`
                    : '';

            if (users) {
                res.status(200).send({
                    current_page: page,
                    data: users,
                    from: page * perPage - 49,
                    last_page: Math.floor(total / perPage) + 1,
                    message: 'Success',
                    next_page_url: nextPageUrl,
                    per_page: perPage,
                    prev_page_url: prevPageUrl,
                    to: page * perPage,
                    total: total,
                });
            } else {
                res.status(404).send({
                    error: 'No transactions',
                    message: 'Error',
                });
            }
        } catch (err) {
            logger.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    public async deleteUser(req: Request, res: Response, next: NextFunction) {
        try {
            const id = req.params.id;
            const transaction = await User.remove({ _id: id });
            res.status(200).send({
                message: 'Success',
            });
        } catch (error) {
            logger.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }

    /**
     * dispense ikedc
     * endpoints.
     */

    public async getTransactions(
        req: Request,
        res: Response,
        next: NextFunction
    ) {
        try {
            const {
                page: PageNumber = 1,
                per_page: PerPage = 50,
                status,
                product,
            } = req.query;

            const page = parseInt(PageNumber, 10);
            const per_page = parseInt(PerPage, 10);
            const skip = page > 1 ? (page - 1) * per_page : 0;

            const criteria = {
                ...(status && { status }),
                ...(product && { product }),
            };
            const total = await Transaction.count(criteria);
            const transactions = await Transaction.find(criteria)
                .sort({
                    updatedAt: -1,
                })
                .sort({ createdAt: -1 })
                .limit(per_page)
                .skip(skip)
                .populate('user');

            const nextPageUrl =
                page > 0 && total > per_page * page
                    ? process.env.CURRENT_ENV +
                      `transactions/transactions?page=${page + 1}`
                    : '';
            const prevPageUrl =
                page > 1
                    ? process.env.CURRENT_ENV +
                      `transactions/transactions?page=${page - 1}`
                    : '';

            return res.status(200).send({
                current_page: page,
                data: transactions,
                from: page * per_page + 1 - per_page,
                last_page: Math.floor(total / per_page) + 1,
                message: 'Success',
                next_page_url: nextPageUrl,
                per_page: per_page,
                prev_page_url: prevPageUrl,
                to: page * per_page,
                total: total,
            });
        } catch (err) {
            logger.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }

    public async allServices(req: Request, res: Response, next: NextFunction) {
        try {
            const services = await Services.find({});

            if (services) {
                res.status(200).send({
                    data: services,
                    message: 'Success',
                });
            } else {
                res.status(404).send({
                    error: 'No Services',
                    message: 'Error',
                });
            }
        } catch (err) {
            logger.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }

    public init() {
        this.router.post('/register', validateBody(adminSchema), this.register);
        this.router.post('/login', validateBody(adminSchema), this.login);
        this.router.get('/transactions', verifyToken(), this.getTransactions);
        this.router.get('/users', verifyToken(), this.allUsers);
        this.router.get('/status', verifyToken(), this.allServices);
        this.router.delete('/users/:id', verifyToken(), this.deleteUser);
        this.router.get('/resolve/:id', verifyToken(), this.autoResolve);
        this.router.get('/mark/:id', verifyToken(), this.markResolved);
        this.router.delete('/delete/:id', verifyToken(), this.deleteTrx);
    }
}

// Create the MerchantRouter, and export its configured Express.Router
const supportRoutes = new SupportRouter();

supportRoutes.init();

export default supportRoutes.router;
