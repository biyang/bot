import * as bcrypt from 'bcryptjs';
import { NextFunction, Request, Response, Router } from 'express';

import { IMerchantModel, Merchant } from '../../db/schemas/merchant';
import {
  merchantCreateSchema,
  merchantLoginSchema,
  resetSchema,
  validateBody
} from '../../helpers/validation';
import logger from '../../logger';

import { resetEmail } from '../../emails/reset';
import { welcomeEmail } from '../../emails/welcome';
import { asyncIssue, asyncVerify } from '../../utils/jwt';
import { sendMail } from '../../utils/mailer';

export class AuthRouter {
  public router: Router;

  /**
   * Initialize the MerchantRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  public async generateResetEmail(req, res, next) {
    try {
      const details = req.value.body;
      const merchant = await Merchant.findOne({ email: details.email });
      if (merchant) {
        const html = resetEmail({
          name: merchant.name,
          url: 'http://biya.com.ng'
        });
        const mailOptions = {
          from: '"Biya" <reset@biya.com.ng>', // sender address
          html: html, // html body.ng
          subject: 'Reset your password', // Subject line
          to: details.email // list of receivers
        };
        await sendMail(mailOptions);
        res.status(200).send({
          message: 'Success'
        });
      } else {
        res.status(404).send({
          message: 'This merchant does not exist '
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async handleReset(req, res, next) {
    try {
      const details = req.value.body;
      const merchant = await Merchant.findOne({ email: details.email });
      if (merchant) {
        // send email with token
      } else {
        throw Error('User does not exist');
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async login(req: Request | any, res: Response, next: NextFunction) {
    try {
      const details = req.value.body;
      const merchant = await Merchant.findOne({ email: details.email });
      if (merchant) {
        // compare password
        bcrypt.compare(details.password, merchant.password, (err, result) => {
          // res === true
          if (err) {
            res.status(401).send({
              message: 'Invalid Username or Password'
            });
          } else {
            if (result) {
              merchant.password = undefined;
              asyncIssue(merchant.toObject())
                .then(token => {
                  res.status(200).send({
                    message: 'Success',
                    token: token
                  });
                })
                .catch(error => {
                  logger.error(error);
                  const message = error.message
                    ? error.message
                    : 'There was an error';
                  res.status(400).send({
                    message: message
                  });
                });
            } else {
              res.status(401).send({
                message: 'Invalid Username or Password'
              });
            }
          }
        });
      } else {
        // user does not exist
        throw Error('This user is not registered with Biya');
      }
    } catch (err) {
      logger.error(err.message);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async register(req: Request | any, res: Response, next: NextFunction) {
    try {
      const newMerchant = new Merchant(req.value.body);
      const merchant: any = await newMerchant.save();
      const html = welcomeEmail({ name: merchant.name, email: merchant.email });

      const mailOptions = {
        from: '"Eniola from Biya" <hello@biya.com.ng>', // sender address
        html: html, // html body.ng
        subject: 'Welcome to Biya Merchants', // Subject line
        to: merchant.email // list of receivers
      };
      await sendMail(mailOptions);
      merchant.password = merchant.updatedAt = merchant.createdAt = undefined;
      const token = await asyncIssue(merchant.toObject());
      res.status(200).send({
        merchant: merchant,
        message: 'Success',
        token: token
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  public init() {
    this.router.post(
      '/register',
      validateBody(merchantCreateSchema),
      this.register
    );
    this.router.post('/login', validateBody(merchantLoginSchema), this.login);
    this.router.post(
      '/reset',
      validateBody(resetSchema),
      this.generateResetEmail
    );
    this.router.post(
      '/new-password',
      validateBody(resetSchema),
      this.generateResetEmail
    );
    this.router.post(
      '/change-password',
      validateBody(resetSchema),
      this.generateResetEmail
    );
  }
}

// Create the MerchantRouter, and export its configured Express.Router
const authRoutes = new AuthRouter();
authRoutes.init();
export default authRoutes.router;
