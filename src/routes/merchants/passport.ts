import * as passportJwt from 'passport-jwt';
import { IMerchantModel, Merchant } from '../../db/schemas/merchant';

const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;

const opts: any = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
opts.secretOrKey = process.env.jwtSecret;
// opts.issuer = 'api.biya.com.ng';
// opts.audience = 'merchants.biya.com.ng';

// JWT Login Strategy for Merchants
export const jwtLogin = new JwtStrategy(opts, (payload, done) => {
  Merchant.findOne({ email: payload.email }, (err, merchant) => {
    if (err) {
      return done(err, false);
    }
    if (merchant) {
      return done(null, merchant);
    } else {
      return done(null, false);
      // or you could create a new account
    }
  });
});

// API Validation Strategy for Merchants
export const apiValidation = new JwtStrategy(opts, (payload, done) => {
  Merchant.findOne({ email: payload.email }, (err, merchant) => {
    if (err) {
      return done(err, false);
    }
    if (merchant) {
      return done(null, merchant);
    } else {
      return done(null, false);
      // or you could create a new account
    }
  });
});
