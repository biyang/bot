import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import { IMerchantModel, Merchant } from '../../db/schemas/merchant';
import { ITransactionModel, Transaction } from '../../db/schemas/transaction';

import { invoiceSchema, validateBody } from '../../helpers/validation';
import logger from '../../logger';

interface Iparameters {
  phoneNumber: string;
  product: string;
  merchant: string;
}
export class PublicApiRouter {
  public router: Router;

  constructor() {
    this.router = Router();
    this.init();
  }

  public createToken(parameters: Iparameters) {
    //
  }

  public async initialize(
    req: Request | any,
    res: Response,
    next: NextFunction
  ) {
    try {
      // create invoice, expects user phone number, reference, amount, product, description, merchant id
      const details = req.value.body;
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async verify(req: Request | any, res: Response, next: NextFunction) {
    try {
      // expects reference
      const reference = req.params.reference;
      const transaction = await Transaction.findOne({ reference: reference });
      if (transaction) {
        const status = transaction.status;
        switch (status) {
          case 'success':
            res.status(200).send({
              data: transaction,
              message: 'Transaction Success',
              status: true
            });
            break;
          default:
            res.status(200).send({
              data: transaction,
              message: `Transaction ${status}`,
              status: false
            });
        }
      } else {
        res.status(404).send({
          message: 'Transaction does not exist',
          status: 'NotFound'
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  public init() {
    this.router.get('/token', this.createToken);
    this.router.post('/initialize', this.initialize);
    this.router.post('/verify/:reference', this.verify);
  }
}

// Create the MerchantRouter, and export its configured Express.Router
const apiRoutes = new PublicApiRouter();
apiRoutes.init();

export default apiRoutes.router;
