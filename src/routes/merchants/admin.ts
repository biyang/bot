import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import { IMerchantModel, Merchant } from '../../db/schemas/merchant';
import logger from '../../logger';

export class MerchantRouter {
  public router: Router;

  /**
   * Initialize the MerchantRouter
   */
  constructor() {
    this.router = Router();
    this.init();
  }

  /**
   * GET all merchnats.
   */
  public async getAll(req: Request, res: Response, next: NextFunction) {
    try {
      const merchants = await Merchant.find({});
      res.status(200).send({
        merchants: merchants,
        message: 'Success'
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(404).send({
        message: message
      });
    }
  }
  /**
   * GET one merchant.
   */
  public async findOne(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    try {
      const merchant = await Merchant.findById(id);
      res.status(200).send({
        merchant: merchant,
        message: 'Success'
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(404).send({
        message: message
      });
    }
  }

  /**
   * GET one merchant.
   */
  public async findOneBySymbol(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    const symbol = req.params.symbol;
    const id = req.params.id;
    try {
      const merchant = await Merchant.findOne({ symbol: symbol });
      res.status(200).send({
        merchant: merchant,
        message: 'Success'
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(404).send({
        message: message
      });
    }
  }

  /**
   * post all merchnats.
   */
  public async postOne(req: Request, res: Response, next: NextFunction) {
    const allowedParameters = ['email', 'name', 'password', 'phone', 'symbol'];
    const data = _.pick(req.body, allowedParameters);
    const newMerchant = new Merchant(data);
    try {
      const merchant = await newMerchant.save();
      merchant.password = undefined;
      res.status(200).send({
        merchant: merchant,
        message: 'Success'
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(404).send({
        message: message
      });
    }
  }

  /**
   * put one merhcnats.
   */
  public async putOne(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    const allowedParameters = ['email', 'name', 'password', 'phone', 'symbol'];
    const data = _.pick(req.body, allowedParameters);

    try {
      const merchant = await Merchant.findByIdAndUpdate(id, data);
      merchant.password = undefined;
      res.status(200).send({
        merchant: merchant,
        message: 'Success'
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(404).send({
        message: message
      });
    }
  }

  /**
   * delete one
   */
  public async deleteOne(req: Request, res: Response, next: NextFunction) {
    const id = req.params.id;
    try {
      const merchant = Merchant.remove({ _id: id });
      res.status(200).send({
        message: 'Success'
      });
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(404).send({
        message: message
      });
    }
  }
  /**
   * Take each handler, and attach to one of the Express.Router's
   * endpoints.
   */
  public init() {
    this.router.get('/', this.getAll);
    this.router.get('/:id', this.findOne);
    this.router.get('/sym/:symbol', this.findOneBySymbol);
    this.router.post('/', this.postOne);
    this.router.put('/:id', this.putOne);
    this.router.delete('/:id', this.deleteOne);
  }
}

// Create the MerchantRouter, and export its configured Express.Router
const merchantRoutes = new MerchantRouter();
merchantRoutes.init();

export default merchantRoutes.router;
