import axios from 'axios';
import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import * as passport from 'passport';
import * as shortid from 'simple-id';

import { IMerchantModel, Merchant } from '../../db/schemas/merchant';
import { ITransactionModel, Transaction } from '../../db/schemas/transaction';
import { IUserModel, User } from '../../db/schemas/user';
import { createTransaction } from '../../utils/dbUtils';

import { bot } from '../../App';
import { invoiceSchema, validateBody } from '../../helpers/validation';
import logger from '../../logger';

interface Iparameters {
    phoneNumber: string;
    product: string;
    merchant: string;
}
export class InvoiceRouter {
    public router: Router;

    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    public sendText(parameters: Iparameters) {
        //
        const postData = {
            from: 'Biya',
            message: `Hi there! ${parameters.merchant} has requested a payment for ${parameters.product} from you via Biya.
           Please click this link to get started https://biya.com.ng/#get-started`,
            to: parameters.phoneNumber,
        };

        return axios({
            auth: {
                password: '13bcf950a6366526964fda4e35da1717',
                username: '424df0de3e1a1167fa4c09c60e5d1f24',
            },
            baseURL: 'https://jusibe.com/smsapi/',
            method: 'post',
            params: postData,
            url: `send_sms`,
        });
    }

    public async sendInvoice(
        req: Request | any,
        res: Response,
        next: NextFunction
    ) {
        try {
            const details = req.value.body;
            const user = await User.findOne({ phone: details.phone });
            if (user) {
                const reference = 'bybt' + Date.now();
                const newTransaction = {
                    amount: details.amount + 100,
                    description: details.description,
                    identity: { type: 'merchantPay' },
                    merchant: req.user.symbol,
                    product: details.product,
                    reference: reference,
                    status: 'init',
                    tags: [''],
                    user: user.phone,
                };
                const transaction = await createTransaction(newTransaction);

                if (!user.authorization) {
                    const updateUser = await User.findOneAndUpdate(
                        { phone: details.phone },
                        { initialReference: reference }
                    );
                }

                bot.beginDialog(
                    user.addresses[user.primaryChannel],
                    '/tap2pay',
                    {
                        transaction: transaction.toObject(),
                    }
                );
                res.status(200).send({
                    message: 'Success, bill sent',
                });
            } else {
                // text a link to the number
                const parameters = {
                    merchant: req.user.name,
                    phoneNumber: details.phone,
                    product: details.product,
                };
                const sendText = this.sendText(parameters);

                if (sendText) {
                    res.status(200).send({
                        message: 'Success, link sent',
                    });
                } else {
                    throw new Error('Could not send text');
                }
            }
        } catch (err) {
            logger.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }

    public async resendInvoice(
        req: Request | any,
        res: Response,
        next: NextFunction
    ) {
        try {
            const details = req.value.body;
            const transaction = await Transaction.findById(details.transaction);
            const user = await User.findOne({ phone: transaction.user });
            if (user) {
                bot.beginDialog(
                    user.addresses[user.primaryChannel],
                    '/tap2pay',
                    {
                        transaction: transaction.toObject(),
                    }
                );
                res.status(200).send({
                    message: 'Success, bill sent',
                });
            } else {
                // text a websitelink to the number
                const parameters = {
                    merchant: req.user.name,
                    phoneNumber: details.phone,
                    product: details.product,
                };
                const sendText = this.sendText(parameters);

                if (sendText) {
                    res.status(200).send({
                        message: 'Success, link sent',
                    });
                } else {
                    throw new Error('Could not send text');
                }
            }
        } catch (err) {
            logger.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    public init() {
        this.router.post(
            '/send',
            passport.authenticate('jwt', { session: false }),
            validateBody(invoiceSchema),
            this.sendInvoice
        );
        this.router.post(
            '/resend',
            passport.authenticate('jwt', { session: false }),
            this.resendInvoice
        );
    }
}

// Create the MerchantRouter, and export its configured Express.Router
const invoiceRoutes = new InvoiceRouter();
invoiceRoutes.init();
export default invoiceRoutes.router;
