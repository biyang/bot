import { NextFunction, Request, Response, Router } from 'express';
import * as _ from 'lodash';
import { ITransactionModel, Transaction } from '../../db/schemas/transaction';
import { IUserModel, User } from '../../db/schemas/user';
import logger from '../../logger';

import * as moment from 'moment';
import * as passport from 'passport';

export class TransactionsRouter {
  public router: Router;

  /*
    * Initialize the TransactionsRouter
  */
  constructor() {
    this.router = Router();
    this.init();
  }

  public async initDashboard(
    req: Request | any,
    res: Response,
    next: NextFunction
  ) {
    try {
      const merchant = req.user;
      const total = await Transaction.count({
        merchant: merchant._id,
        status: 'success'
      });
      const transactions = await Transaction.find({
        merchant: merchant._id,
        status: 'success'
      }).sort({ createdAt: -1 });

      const cleanTransactions = transactions.map((item: any) => {
        item.fees = 0.03 * parseFloat(item.amount);
        item.netAmount = parseFloat(item.amount) - item.fees;
        return item;
      });
      const data = {
        netRevenues: _.reduce(
          cleanTransactions,
          (sum, n) => sum + n.netAmount,
          0
        ),
        totalFees: _.reduce(cleanTransactions, (sum, n) => sum + n.fees, 0),
        transactionCount: total
      };
      if (transactions) {
        res.status(200).send({
          data: data,
          message: 'Success'
        });
      } else {
        res.status(404).send({
          error: 'No transactions',
          message: 'Error'
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        error: message,
        message: 'Error'
      });
    }
  }

  public async getUsers(req: Request | any, res: Response, next: NextFunction) {
    try {
      let page;
      if (req.query) {
        page = parseInt(req.query.page, 10);
      } else {
        page = 1;
      }

      const merchant = req.user;
      const perPage = 50;
      const total = await Transaction.count({ merchant: merchant._id });
      const transactions = await Transaction.find({ merchant: merchant._id })
        .sort({ createdAt: -1 })
        .limit(perPage)
        .skip((page - 1) * perPage)
        .populate('user');
      const nextPageUrl =
        page > 0 && total > perPage
          ? process.env.CURRENT_ENV + `merchants/transactions?page=${page + 1}`
          : '';
      const prevPageUrl =
        page > 1 && total > perPage
          ? process.env.CURRENT_ENV + `merchants/transactions?page=${page - 1}`
          : '';

      if (transactions) {
        res.status(200).send({
          current_page: page,
          data: transactions,
          from: page * perPage - 49,
          last_page: Math.floor(total / perPage) + 1,
          message: 'Success',
          next_page_url: nextPageUrl,
          per_page: perPage,
          prev_page_url: prevPageUrl,
          to: page * perPage,
          total: total
        });
      } else {
        res.status(404).send({
          error: 'No transactions',
          message: 'Error'
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async getSingle(
    req: Request | any,
    res: Response,
    next: NextFunction
  ) {
    try {
      const transactionId = req.value.params;
      const transaction = await Transaction.findById(transactionId);
      if (transaction) {
        res.status(200).send({
          data: transaction,
          message: 'Success'
        });
      } else {
        res.status(404).send({
          error: 'No transactions',
          message: 'Error'
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async getTransactions(
    req: Request | any,
    res: Response,
    next: NextFunction
  ) {
    try {
      const range: any = {};
      if (req.query && req.query.from && req.query.to) {
        range.from = req.query.from;
        range.to = req.query.to;
      } else {
        range.to = new Date();
        range.from = moment(range.to)
          .subtract(1, 'month')
          .toDate();
      }

      const merchant = req.user;
      const transactions = await Transaction.find({
        merchant: merchant._id,
        status: 'success',
        updatedAt: { $gte: range.from, $lte: range.to }
      }).sort({ updatedAt: 1 });

      const cleanTransactions = transactions.map((item: any) => {
        item.fees = 0.03 * parseFloat(item.amount);
        item.netAmount = parseFloat(item.amount) - item.fees;
        return item;
      });

      if (transactions) {
        res.status(200).send({
          data: cleanTransactions,
          message: 'Success'
        });
      } else {
        res.status(404).send({
          error: 'No transactions',
          message: 'Error'
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public async getAll(req: Request | any, res: Response, next: NextFunction) {
    try {
      let page;
      if (req.query) {
        page = parseInt(req.query.page, 10);
      } else {
        page = 1;
      }

      const merchant = req.user;
      const perPage = 50;
      let total;
      let transactions;
      const type = req.params.type;
      if (type) {
        total = await Transaction.count({
          merchant: merchant._id,
          status: type
        });
        transactions = await Transaction.find({
          merchant: merchant._id,
          status: type
        })
          .sort({ createdAt: -1 })
          .limit(perPage)
          .skip((page - 1) * perPage)
          .populate('user');
      } else {
        total = await Transaction.count({ merchant: merchant._id });
        transactions = await Transaction.find({ merchant: merchant._id })
          .sort({ createdAt: -1 })
          .limit(perPage)
          .skip((page - 1) * perPage)
          .populate('user');
      }
      const nextPageUrl =
        page > 0 && total > perPage
          ? process.env.CURRENT_ENV + `merchants/transactions?page=${page + 1}`
          : '';
      const prevPageUrl =
        page > 1 && total > perPage
          ? process.env.CURRENT_ENV + `merchants/transactions?page=${page - 1}`
          : '';

      if (transactions) {
        res.status(200).send({
          current_page: page,
          data: transactions,
          from: page * perPage - 49,
          last_page: Math.floor(total / perPage) + 1,
          message: 'Success',
          next_page_url: nextPageUrl,
          per_page: perPage,
          prev_page_url: prevPageUrl,
          to: page * perPage,
          total: total
        });
      } else {
        res.status(404).send({
          error: 'No transactions',
          message: 'Error'
        });
      }
    } catch (err) {
      logger.error(err);
      const message = err.message ? err.message : 'There was an error';
      res.status(400).send({
        message: message
      });
    }
  }

  public init() {
    this.router.get(
      '/',
      passport.authenticate('jwt', { session: false }),
      this.getAll
    );
    this.router.get(
      '/type/:type',
      passport.authenticate('jwt', { session: false }),
      this.getAll
    );
    this.router.get(
      '/single/:id',
      passport.authenticate('jwt', { session: false }),
      this.getSingle
    );
    this.router.get(
      '/dashboard',
      passport.authenticate('jwt', { session: false }),
      this.initDashboard
    );
    this.router.get(
      '/chart',
      passport.authenticate('jwt', { session: false }),
      this.getTransactions
    );
  }
}

// Create the MerchantRouter, and export its configured Express.Router
const transactionsRoutes = new TransactionsRouter();
transactionsRoutes.init();

export default transactionsRoutes.router;
