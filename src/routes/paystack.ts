import * as CircularJSON from 'circular-json';
import { NextFunction, Request, Response, Router } from 'express';
import { stripIndents } from 'common-tags';
import { Events } from '../db/schemas/events';
import { Subscription } from '../db/schemas/subscription';
import { Transaction } from '../db/schemas/transaction';
import { User } from '../db/schemas/user';

import { SlackNotify } from '../monitor/SlackService';
import { sendIkedcPin } from '../utils/sms';

import {
    dispenseDSTV,
    dispenseElectricity,
    dispenseGOTV,
    dispenseSMILE,
    dispenseSpectranet,
} from '../utils/airvend';

import { dispenseAirtime } from '../vending/index';

import processTransaction from '../vending/process';

import { createTransaction } from '../utils/dbUtils';
import { dispenseTicket } from '../utils/tickets';

import * as shortid from 'simple-id';
import * as parseStringModule from 'xml2js';
import { bot } from '../App';
import logger from '../logger';
import { sendMail } from '../utils/mailer';

const parseString = parseStringModule.parseString;

// import verify
import { verifyTransaction } from '../utils/paystack';

// Instantiate the class
import * as crypto from 'crypto';
const secret = process.env.PAYSTACK_TOKEN;
const newSecret = process.env.NEW_PAYSTACK_TOKEN;
export class PaystackRouter {
    public router: Router;

    /**
     * Initialize the PaystackRouter
     */
    constructor() {
        this.router = Router();
        this.init();
    }

    public async verifyPaystack(
        req: Request | any,
        res: Response,
        next: NextFunction
    ) {
        // call paystack verify function
        const reference = req.query.trxref;
        const networks = {
            AIRTEL: 1,
            ETISALAT: 4,
            GLO: 3,
            MTN: 2,
        };

        function MyError(message, name, transaction) {
            this.name = name;
            this.transaction = transaction;
            this.message = message || 'Default Message';
            this.stack = new Error().stack;
        }
        MyError.prototype = new Error();

        let address: any = {};

        try {
            const findTransaction = await Transaction.findOne({
                reference: reference,
            });

            if (findTransaction.status !== 'init') {
                res.redirect('https://biya.com.ng/success');
            }

            let verify;
            const product = findTransaction.product;
            if (product === 'airtime' || product === 'otherR') {
                verify = await verifyTransaction(secret)(reference);
            } else {
                verify = await verifyTransaction(newSecret)(reference);
            }

            if (verify.status === 'success') {
                // update authorization code=
                let updateAuthCode;
                if (verify.authorization && verify.authorization.reusable) {
                    if (product === 'airtime' || product === 'otherR') {
                        updateAuthCode = await User.findOneAndUpdate(
                            { initialReference: reference },
                            { authorization: verify.authorization }
                        );
                    } else {
                        updateAuthCode = await User.findOneAndUpdate(
                            { initialReference: reference },
                            { billsAuth: verify.authorization }
                        );
                    }
                } else {
                    updateAuthCode = await User.findOne({
                        initialReference: reference,
                    });
                }

                if (updateAuthCode) {
                    address =
                        updateAuthCode.addresses[
                            findTransaction.identity.sourceChannel
                        ];
                }

                if (findTransaction.identity.type === 'merchantPay') {
                    await Transaction.findOneAndUpdate(
                        { reference: reference },
                        { status: 'success' }
                    );
                    bot.beginDialog(address, '/notify', {
                        transaction: findTransaction,
                        type: 'merchantPay',
                    });
                    res.redirect('https://biya.com.ng/success');
                }

                if (findTransaction.product === 'ticket') {
                    await Transaction.findOneAndUpdate(
                        { reference: reference },
                        { status: 'success' }
                    );
                    bot.beginDialog(address, '/notify', {
                        ticket: await dispenseTicket(findTransaction),
                        transaction: findTransaction,
                        type: 'ticket',
                    });
                    res.redirect('https://biya.com.ng/success');
                }
                return res.redirect('https://biya.com.ng/success');
            } else {
                Transaction.findOneAndUpdate(
                    { reference: reference },
                    { status: 'failed' }
                )
                    .then((r) => logger.info(r))
                    .catch((e) => {
                        logger.error(e);
                    });
                throw new MyError(verify.gateway_response, 'PaymentError', {
                    reference: reference,
                });
            }
        } catch (error) {
            logger.error(error);
            res.redirect('https://biya.com.ng/error');
        }
    }

    public async paystackWebhook(
        req: Request | any,
        res: Response,
        next: NextFunction
    ) {
        // validate event
        let address;
        try {
            const hash = crypto
                .createHmac('sha512', secret)
                .update(CircularJSON.stringify(req.body))
                .digest('hex');
            const hash2 = crypto
                .createHmac('sha512', newSecret)
                .update(CircularJSON.stringify(req.body))
                .digest('hex');
            if (
                hash === req.headers['x-paystack-signature'] ||
                hash2 === req.headers['x-paystack-signature']
            ) {
                // Retrieve the request's body
                const event = req.body;
                const newEvent = new Events({ event: event });
                await newEvent.save();
                // Do something with event
                function MyError(message, name, transaction) {
                    this.name = name;
                    this.transaction = transaction;
                    this.message = message || 'Default Message';
                    this.stack = new Error().stack;
                }
                const _domain =
                    process.env.NODE_ENV === 'staging' ? 'test' : 'live';
                if (event.event === 'charge.success') {
                    const { data } = event;
                    if (data && data.domain === _domain) {
                        processTransaction(data.reference);
                    }
                }

                MyError.prototype = new Error();
                if (event.event === 'invoice.update') {
                    if (event.data && event.data.paid) {
                        // if there's a plan, do dispense
                        const subscriptionCode =
                            event.data.subscription.subscription_code;
                        const userEmail = event.data.customer.email;
                        const subscription = await Subscription.findOne({
                            'data.subscription_code': subscriptionCode,
                        });
                        const user = await User.findOne({ email: userEmail });
                        const reference = shortid(
                            8,
                            '0123456789abcdefghijklmnopqrstuvwxyz'
                        );
                        const transaction = {
                            amount: event.data.subscription.amount / 100,
                            description: subscription.product,
                            identity: subscription.identity,
                            merchant: 'airvend',
                            product: subscription.product,
                            reference: reference,
                            status: 'success',
                            tags: [],
                            user: user.phone,
                            address: subscription.address,
                        };
                        const newTransaction = await createTransaction(
                            transaction
                        );
                        let dispense;
                        if (transaction.product === 'dstv') {
                            dispense = await dispenseDSTV(transaction);
                        }
                        if (transaction.product === 'gotv') {
                            dispense = await dispenseGOTV(transaction);
                        }
                        address =
                            user.addresses[transaction.identity.sourceChannel];
                        if (dispense && dispense.data) {
                            parseString(dispense.data, (err, result) => {
                                if (err) {
                                    logger.error(err);
                                    throw new MyError(
                                        'There was an error, we will rectify this shortly. Thank you!',
                                        'DispenseError',
                                        transaction
                                    );
                                }
                                if (
                                    result &&
                                    result.VendResponse.ResponseCode[0] === '0'
                                ) {
                                    const myJSON = result.VendResponse;
                                    if (transaction.product === 'dstv') {
                                        bot.beginDialog(address, '/notify', {
                                            amount: myJSON.Amount[0],
                                            type: 'dstv',
                                        });
                                    } else if (transaction.product === 'gotv') {
                                        bot.beginDialog(address, '/notify', {
                                            amount: myJSON.Amount[0],
                                            type: 'gotv',
                                        });
                                    }
                                } else {
                                    if (
                                        result &&
                                        result.VendResponse.ResponseCode[0] ===
                                            '99999997'
                                    ) {
                                        throw new MyError(
                                            'Invalid smart card number, please try again.',
                                            'InvalidError',
                                            transaction
                                        );
                                    } else if (
                                        result &&
                                        result.VendResponse.ResponseCode[0] ===
                                            '99999999'
                                    ) {
                                        throw new MyError(
                                            'There was a network error, we will rectify this shortly. Thank you!',
                                            'NetworkError',
                                            transaction
                                        );
                                    } else if (
                                        result &&
                                        result.VendResponse.ResponseCode[0] ===
                                            '99999998'
                                    ) {
                                        throw new MyError(
                                            'There was a problem at our end, we will rectify this shortly. Thank you!',
                                            'InsufficientError',
                                            transaction
                                        );
                                    } else {
                                        throw new MyError(
                                            'There was a problem at our end, we will check and rectify this shortly. Thank you!',
                                            'UnknownError',
                                            transaction
                                        );
                                    }
                                }
                            });
                        }
                    }
                }
                if (event.event === 'invoice.create') {
                    // send impending notification to user
                    const userEmail = event.data.customer.email;
                    const user = await User.findOne({ email: userEmail });
                    const userAddress = user.addresses[user.primaryChannel];
                    const subscriptionCode =
                        event.data.subscription.subscription_code;
                    bot.beginDialog(userAddress, '/subreminder', {
                        amount: 0,
                        type: 'dstv',
                    });
                }
            }
        } catch (error) {
            logger.error(error);

            const mailOptions = {
                from: '"Init Dispense Error Alert ⚠" <alerts@biya.com.ng>', // sender address
                html: `<pre>${CircularJSON.stringify(error)}</pre>`, // html body.ng
                subject: 'Init Dispense Error Alert', // Subject line
                to: 'olumytee.ojo@gmail.com', // list of receivers
            };
            if (error.transaction) {
                Transaction.findOneAndUpdate(
                    { reference: error.transaction.reference },
                    { status: 'failed' },
                    (err, doc) => {
                        if (err) {
                            logger.info(err);
                        }
                        logger.info('failed transaction');
                    }
                );

                mailOptions.html = `<strong>${error.name}</strong><br/>
                                    <strong>${error.message}</strong><br/>
                                    <pre>${CircularJSON.stringify(
                                        error.transaction
                                    )}</pre>`;
            }
            sendMail(mailOptions);
            const message = error.message
                ? error.message
                : 'There was an error';
            // if (address) {
            //   bot.beginDialog(address, '/error', {});
            // }
        } finally {
            // respond to paystack
            res.sendStatus(200);
        }
    }

    public async vtpassWebhook(
        req: Request | any,
        res: Response,
        next: NextFunction
    ) {
        try {
            const { type, data } = req.body;

            SlackNotify({
                username: 'New Event!',
                channel: '#feed-vtpass',
                text: 'New event: ' + type,
                fields: {
                    Data: JSON.stringify(data),
                },
                emoji: ':pinched_fingers:',
            });

            if (type === 'transaction-update') {
                const { code, requestId } = data;
                const transactionReference = requestId;

                if (code === '000') {
                    let updateObject = { status: 'success' };

                    if (data.token) {
                        const pin = data.token;
                        const serial = '';
                        const amountOfPower = data.units;
                        updateObject = Object.assign(
                            {},
                            {
                                status: 'success',
                                'identity.pin': pin,
                                'identity.serial': serial || '',
                                'identity.amount': amountOfPower || '',
                            }
                        );
                    }

                    Transaction.findOneAndUpdate(
                        { reference: transactionReference },
                        updateObject
                    ).then((result) => {
                        Transaction.findOne({ reference: transactionReference })
                            .then((info) => {
                                if (info.address) {
                                    bot.loadSession(
                                        // @ts-ignore
                                        info.address,
                                        (err, session) => {
                                            session.replaceDialog('/notify', {
                                                ...info.identity,
                                                type: info.product,
                                            });
                                        }
                                    );
                                }

                                sendIkedcPin(
                                    info,
                                    info.identity.pin,
                                    info.identity.serial,
                                    info.product
                                );

                                SlackNotify({
                                    username: 'New Sale!',
                                    channel: '#sales',
                                    text: stripIndents`Successful ${info.product.toUpperCase()} dispense`,
                                    fields: {
                                        Service: info.product.toUpperCase(),
                                        Amount: info.amount,
                                        Name: info.identity.customername,
                                        Account: info.identity.customerNumber,
                                        PIN: info.identity.pin,
                                        Units: info.identity.amount,
                                    },
                                    emoji: ':white_check_mark:',
                                });

                                logger.info('Job complete');
                            })
                            .catch((error) => logger.error(error));
                    });
                }

                if (code === '040') {
                    Transaction.findOneAndUpdate(
                        { reference: transactionReference },
                        { status: 'failed' },
                        (err, doc) => {
                            if (err) {
                                logger.info(err);
                            }
                            logger.info('failed transaction');
                        }
                    );
                }
            }
            logger.info(req.body);
        } catch (error) {
            logger.error(error);
        } finally {
            // respond to paystack
            res.send({ response: 'success' });
        }
    }

    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    public init() {
        this.router.get('/verify', this.verifyPaystack);
        this.router.post('/webhook', this.paystackWebhook);
        this.router.post('/vtpasswebhook', this.vtpassWebhook);
    }
}

// Create the PaystackRouter, and export its configured Express.Router
const paystackRoutes = new PaystackRouter();
paystackRoutes.init();

export default paystackRoutes.router;
