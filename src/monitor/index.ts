import * as Agenda from 'agenda';
import * as moment from 'moment';
import { bot } from '../App';
import logger from '../logger';
import { Services } from '../db/schemas/services';
import { User } from '../db/schemas/user';

import axios from 'axios';

const airvendUsername = process.env.AIRVEND_USER;
const airvendPassword = process.env.AIRVEND_PASS;

const agenda = new Agenda({
    db: { address: process.env.MONGODB_URI, collection: 'jobs' },
});

agenda.on('ready', () => {
    logger.info('status check ready');
    // agenda.start();
});

export const verifyDSTV = (identifier: string) => {
    const postData = {
        password: airvendPassword,
        smartcard: identifier,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/dstv/verify/`,
    });
};

export const verifySMILE = (identifier) => {
    const postData = {
        account: identifier,
        password: airvendPassword,
        username: airvendUsername,
    };

    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/smilebundle/verify/`,
    });
};

export const verifyGOTV = (identifier: string) => {
    const postData = {
        password: airvendPassword,
        smartcard: identifier,
        username: airvendUsername,
    };
    return axios({
        baseURL: 'https://api.airvendng.net',
        method: 'post',
        params: postData,
        url: `/vas/gotv/verify/`,
    });
};

agenda.define('status', (job, done) => {
    try {
        const services = ['gotv', 'dstv', 'smile'];
        services.forEach(async (service) => {
            let result;
            switch (service) {
                case 'dstv':
                    result = await verifyDSTV('1020730094');
                    break;
                case 'gotv':
                    result = await verifyGOTV('2020793089');
                    break;
                case 'smile':
                    result = await verifySMILE('1604003741');
                    break;
                default:
                    break;
            }
            const item = await Services.findOne({ service: service });
            if (result && result.data && result.data.details) {
                const obj1 = {
                    status: 'online',
                    since: new Date(),
                };
                const obj2 = {
                    status: 'online',
                };
                await Services.findOneAndUpdate(
                    { service: service },
                    {
                        $set: item.status === 'online' ? obj2 : obj1,
                    }
                );
            } else {
                const obj1 = {
                    status: 'offline',
                    since: new Date(),
                };
                const obj2 = {
                    status: 'offline',
                };
                await Services.findOneAndUpdate(
                    { service: service },
                    {
                        $set: item.status === 'offline' ? obj2 : obj1,
                    }
                );
            }
        });
    } catch (error) {
        logger.error(error);
    }
    done();
});

agenda.define('weMissYou', async (job, done) => {
    try {
        // check if the person has not made a transaction in 2 weeks
        const date = moment().subtract(3, 'weeks').toDate();
        const users = await User.find({
            whitelisted: { $ne: true },
            updatedAt: { $lt: date },
        });
        for (const user of users) {
            if (user.primaryChannel && user.addresses) {
                const address = user.addresses[user.primaryChannel];
                // bot.beginDialog(address, 'weMissYou');
                await User.findByIdAndUpdate(user.id, {
                    updatedAt: new Date(),
                });
            }
        }
    } catch (error) {
        logger.error(error);
    }
    done();
});

agenda.on('ready', function () {
    // agenda.every('5 minutes', 'status');
    // agenda.every('24 hours', 'weMissYou');
    // agenda.start();
});

export default agenda;
