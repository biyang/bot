import * as Joi from 'joi';
import { asyncVerify } from '../utils/jwt';
export const idSchema = Joi.object().keys({
  param: Joi.string()
    .regex(/^[0-9a-fA-F]{24}$/)
    .required()
});

export const merchantLoginSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string().required()
});

export const adminSchema = Joi.object().keys({
  password: Joi.string().required(),
  username: Joi.string().required()
});

export const resetSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required()
});

export const merchantCreateSchema = Joi.object().keys({
  email: Joi.string()
    .email()
    .required(),
  name: Joi.string().required(),
  password: Joi.string().required(),
  phone: Joi.string()
    .regex(/\b\d{11}\b/g)
    .required(),
  symbol: Joi.string()
});

export const invoiceSchema = Joi.object().keys({
  amount: Joi.number().required(),
  description: Joi.string().required(),
  phone: Joi.string()
    .regex(/\b\d{11}\b/g)
    .required(),
  product: Joi.string().required()
});

export const validateParams = (schema, name) => {
  return (req, res, next) => {
    const result: any = Joi.validate({ param: req.params[name] }, schema);
    if (result.error) {
      if (result.error.isJoi) {
        return res.status(400).json({
          error: result.error.name,
          message: result.error.details[0].message
        });
      }
      return res.status(400).json(result.error);
    } else {
      if (!req.value) {
        req.value = {};
      }

      if (!req.value.params) {
        req.value.params = {};
      }

      req.value.params[name] = result.value.param;
      next();
    }
  };
};

/**
 *
 *
 * @param schema
 * @returns
 */

export const validateBody = schema => {
  return (req, res, next) => {
    const result: any = Joi.validate(req.body, schema);
    if (result.error) {
      if (result.error.isJoi) {
        return res.status(400).json({
          error: result.error.name,
          message: result.error.details[0].message
        });
      }
      return res.status(400).json(result.error);
    } else {
      if (!req.value) {
        req.value = {};
      }

      if (!req.value.body) {
        req.value.body = {};
      }

      req.value.body = result.value;
      next();
    }
  };
};

export const verifyToken = () => {
  return (req, res, next) => {
    const token = req.headers.authorization;
    if (token) {
      asyncVerify(token)
        .then(decoded => {
          next();
        })
        .catch(error => {
          res.status(401).send({
            message: 'Invalid Username or Password'
          });
        });
    } else {
      res.status(401).send({
        message: 'Auth Token Required'
      });
    }
  };
};
