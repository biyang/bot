import * as builder from 'botbuilder';
import * as Fuse from 'fuse.js';
import * as _ from 'lodash';
import logger from '../../../logger';
import { bot } from '../../../App';

import { issue } from '../../../utils/jwt';
import * as askNUBAN from '../../prompts/askNUBAN';
import * as askAmount from '../../prompts/askRechargeAmount';

import {
    Beneficiary,
    IBeneficiaryModel,
} from '../../../db/schemas/beneficiary';
import { IUserModel, User } from '../../../db/schemas/user';
import {
    cardCheck,
    checkIfRegisteredUser,
    createTransaction,
    deriveEmail,
    saveNewUser,
} from '../../../utils/dbUtils';
import {
    getBankCode,
    initiateFundsTransfer,
    validateAccount,
} from '../../../utils/mWave';

import * as shortid from 'simple-id';
import tracker from '../mixpanel';

function validateAmount(Amount: string): boolean {
    const re = /^\d+$/;
    return re.test(Amount);
}

function validateNuban(cardNumber: string): boolean {
    const re = /\b\d{10}\b/;
    return re.test(cardNumber);
}

bot.dialog('/TransferIndex', [
    (session, args, next) => {
        let recipient;
        let amount;
        tracker(session, 'TransferIndex', 1);
        if (session.dialogData.amount) {
            amount = session.dialogData.amount;
        }
        // check if recipient is in dialog data
        if (session.dialogData.recipient) {
            recipient = session.dialogData.recipient;
        }
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (
                builder.EntityRecognizer.findEntity(
                    args.entities,
                    'builtin.number'
                )
            ) {
                const amountEntity: any = builder.EntityRecognizer.findEntity(
                    args.entities,
                    'builtin.number'
                );
                // resolve amount
                session.dialogData.amount = amountEntity.entity;
                // check if amount is only numbers
                if (validateAmount(session.dialogData.amount)) {
                    amount = session.dialogData.amount = _.toNumber(
                        amountEntity.entity
                    );
                } else {
                    amount = session.dialogData.amount = _.toNumber(
                        amountEntity.resolution.value
                    );
                }
            }
            if (
                builder.EntityRecognizer.findEntity(args.entities, 'Recipient')
            ) {
                recipient = builder.EntityRecognizer.findEntity(
                    args.entities,
                    'Recipient'
                ).entity;
                session.dialogData.recipient = recipient;
            }
        }
        // amount should be a number now if it's defined

        if (!amount) {
            askAmount.beginDialog(session, {
                prompt: 'Please enter the amount you want to transfer :-)',
            });
        } else {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 2);
        let amount = session.dialogData.amount;
        if (results && results.response) {
            amount = session.dialogData.amount = results.response;
        }

        const recipient = session.dialogData.recipient;
        // if no recipient
        if (recipient) {
            if (validateNuban(recipient)) {
                // rcipient is confirmed as account number
                session.dialogData.recipientIsNuban = true;
                next();
            } else {
                session.dialogData.recipientIsName = true;
                // recipient is name
                next();
            }
            // check if name or number
        } else {
            // no recipient, prompt for recipients account number
            // no recipient
            User.findOne({ phone: session.userData.phone })
                .populate('beneficiaries')
                .then((user) => {
                    const beneficiaries = user.beneficiaries;
                    const transferBeneficiaries = _.filter(
                        beneficiaries,
                        (b: any) => b.identifier.type === 'nuban'
                    );
                    if (transferBeneficiaries.length >= 1) {
                        // user is not beneficiary, break to next
                        session.dialogData.useExisting = true;
                        const listToSend = _.map(transferBeneficiaries, (o) => {
                            return `${o.name.split(' ')[0]} - ${
                                o.identifier.bankName
                            } - ${o.identifier.accountNumber}`;
                        });
                        listToSend.push('New');
                        const name = session.userData.name
                            ? session.userData.name
                            : session.message.user.name.split(' ')[0];
                        builder.Prompts.choice(
                            session,
                            `Which account do you want to send money to, ${name}`,
                            listToSend,
                            { listStyle: builder.ListStyle.button }
                        );
                    } else {
                        next();
                    }
                })
                .catch((error) => {
                    askNUBAN.beginDialog(session, {});
                });
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 3);
        if (session.dialogData.useExisting) {
            if (results.response) {
                if (results.response.entity !== 'New') {
                    session.dialogData.bankCode = session.dialogData.recipient =
                        results.response.entity.split('-')[2].trim();
                    Beneficiary.findOne({
                        'identifier.accountNumber':
                            session.dialogData.recipient,
                    })
                        .then((ben: any) => {
                            if (ben) {
                                session.dialogData.bankCode =
                                    ben.identifier.bankCode;
                                session.dialogData.account_name = ben.name;
                                session.dialogData.recipientIsNuban = true;
                                session.dialogData.useExisting = true;
                                next(); //
                            } else {
                                session.endDialog('Recipient not found');
                            }
                        })
                        .catch((err) => {
                            logger.error(err);
                            const message = err.message
                                ? err.message
                                : 'There was an error';
                            session.endDialog(message);
                        });
                } else {
                    session.dialogData.useExisting = false;
                    askNUBAN.beginDialog(session, {});
                }
            }
        } else {
            askNUBAN.beginDialog(session, {});
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 4);
        let recipient = session.dialogData.recipient;
        if (results && results.response && !recipient) {
            session.dialogData.recipientIsNuban = true;
            recipient = session.dialogData.recipient = results.response;
            next();
        } else {
            next();
        }
    },
    (session, results, next) => {
        const recipient = session.dialogData.recipient;
        tracker(session, 'TransferIndex', 5);
        if (session.dialogData.recipientIsNuban) {
            // check if account number is in beneficiary list
            User.findOne({ phone: session.userData.phone })
                .populate('beneficiaries')
                .then((user: any) => {
                    if (!user) {
                        // if user does not exist, create user
                        const newUser = {
                            address: session.message.address,
                            email: deriveEmail(session),
                            name: session.message.user.name,
                            userBotId: session.message.user.id,
                        };
                        saveNewUser(newUser)
                            .then((newU: any) => {
                                session.dialogData.userId = newU._id;
                                session.dialogData.recipientIsBeneficiary =
                                    false;
                                next();
                            })
                            .catch((err) => {
                                const message = err.message
                                    ? err.message
                                    : 'There was an error';
                                session.endDialog(
                                    `${message}, please try again`
                                );
                            });
                    } else {
                        const beneficiaries = user.beneficiaries;
                        const transferBeneficiaries = _.filter(
                            beneficiaries,
                            (b: any) => b.identifier.type === 'nuban'
                        );
                        if (transferBeneficiaries.length < 1) {
                            // user is not beneficiary, break to next
                            session.dialogData.userId = user._id;
                            session.dialogData.recipientIsBeneficiary = false;
                            next();
                        } else {
                            // check if our recipient is amongst these beneficiary
                            const beneficiary = _.find(
                                transferBeneficiaries,
                                (b: any) =>
                                    b.identifier.accountNumber === recipient
                            );
                            if (beneficiary) {
                                session.dialogData.recipientIsBeneficiary =
                                    true;
                                session.dialogData.recipient =
                                    beneficiary.identifier.accountNumber;
                                session.dialogData.bankName =
                                    beneficiary.identifier.bankName;
                                next();
                            } else {
                                session.dialogData.userId = user._id;
                                session.dialogData.recipientIsBeneficiary =
                                    false;
                                next();
                            }
                        }
                    }
                })
                .catch((err) => {
                    logger.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(message);
                });
        } else {
            next();
        }
    },
    (session, results, next) => {
        const recipient = session.dialogData.recipient;
        tracker(session, 'TransferIndex', 6);
        if (session.dialogData.recipientIsName) {
            User.findOne({ phone: session.userData.phone })
                .populate('beneficiaries')
                .then((user: any) => {
                    if (!user) {
                        // if user does not exist, create user
                        const newUser = {
                            address: session.message.address,
                            email: deriveEmail(session),
                            name: session.message.user.name,
                            userBotId: session.message.user.id,
                        };
                        saveNewUser(newUser)
                            .then((newU: any) => {
                                session.dialogData.userId = newU._id;
                                session.dialogData.recipientIsBeneficiary =
                                    false;
                                next();
                            })
                            .catch((err) => {
                                logger.error(err);
                                const message = err.message
                                    ? err.message
                                    : 'There was an error';
                                session.endDialog(
                                    `${message}, please try again`
                                );
                            });
                    } else {
                        const beneficiaries = user.beneficiaries;
                        const transferBeneficiaries = _.filter(
                            beneficiaries,
                            (b: any) => b.identifier.type === 'nuban'
                        );
                        if (transferBeneficiaries.length < 1) {
                            // user is not beneficiary, break to next
                            session.dialogData.userId = user._id;
                            session.dialogData.recipientIsBeneficiary = false;
                            next();
                        } else {
                            // check if our recipient is amongst these beneficiary
                            const options = {
                                distance: 100,
                                keys: ['name', 'key'],
                                location: 0,
                                maxPatternLength: 32,
                                minMatchCharLength: 1,
                                shouldSort: true,
                                threshold: 0.3,
                            };
                            const fuse = new Fuse(
                                transferBeneficiaries,
                                options
                            ); // "list" is the item array
                            const result: any = fuse.search(
                                session.dialogData.recipient
                            );

                            const beneficiary = result[0];

                            if (beneficiary) {
                                session.dialogData.recipientIsBeneficiary =
                                    true;
                                session.dialogData.recipient =
                                    beneficiary.identifier.accountNumber;
                                session.dialogData.bankName =
                                    beneficiary.identifier.bankName;
                                next();
                            } else {
                                session.dialogData.userId = user._id;
                                session.dialogData.recipientIsBeneficiary =
                                    false;
                                next();
                            }
                        }
                    }
                })
                .catch((err) => {
                    logger.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(message);
                });
        } else {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 7);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        if (!session.dialogData.recipientIsBeneficiary) {
            //  ask for bank
            builder.Prompts.choice(
                session,
                `What bank does the recipient use, ${name}`,
                [
                    'GTB',
                    'FirstBank',
                    'UBA',
                    'Access',
                    'WEMA',
                    'Diamond',
                    'Union',
                    'STANBIC',
                    'SKYE',
                    'Zenith',
                ],
                { listStyle: builder.ListStyle.button }
            );
            // then save the beneficiary
        } else {
            next();
        }
    },
    (session, results, next) => {
        const recipient = session.dialogData.recipient;
        tracker(session, 'TransferIndex', 8);
        if (!session.dialogData.recipientIsBeneficiary) {
            // handleResponse and save beneficiary
            if (results && results.response) {
                let bankName;
                switch (results.response.entity) {
                    case 'GTB':
                        bankName = 'GTBANK PLC';
                        break;
                    case 'FirstBank':
                        bankName = 'FIRST BANK PLC';
                        break;
                    case 'UBA':
                        bankName = 'FIRST BANK PLC';
                        break;
                    case 'Access':
                        bankName = 'ACCESS BANK NIGERIA';
                        break;
                    case 'WEMA':
                        bankName = 'WEMA BANK PLC';
                        break;
                    case 'Diamond':
                        bankName = 'DIAMOND BANK PLC';
                        break;
                    case 'Union':
                        bankName = 'UNION BANK OF NIGERIA PLC';
                        break;
                    case 'STANBIC':
                        bankName = 'STANBIC IBTC BANK PLC';
                        break;
                    case 'SKYE':
                        bankName = 'SKYE BANK PLC';
                        break;
                    case 'Zenith':
                        bankName = 'ZENITH BANK PLC';
                        break;
                }
                session.dialogData.bankName = bankName;
                //
                session.sendTyping();
                // run account number validation
                getBankCode(bankName)
                    .then((code: any) => {
                        session.dialogData.bankCode = code;
                        return validateAccount(recipient, code);
                    })
                    .then((account) => {
                        if (account && account.status === 'success') {
                            const accountName = account.data.account_name;
                            session.dialogData.account_name = accountName;
                            const amount = session.dialogData.amount;
                            builder.Prompts.confirm(
                                session,
                                `Please confirm that you want to send ₦${amount} to: \n
                  Bank: ${bankName} \n Account Number: ${recipient} \n Account Name: ${accountName}`,
                                { listStyle: builder.ListStyle.button }
                            );
                        } else {
                            session.endDialog(
                                'The account number is invalid, please check it then try again'
                            );
                        }
                    })
                    .catch((err) => {
                        let message = err.error.msg
                            ? err.error.msg
                            : 'There was an error';
                        logger.error(message);
                        if (err.code) {
                            switch (err.code) {
                                case 'RESOLVE_FAILED':
                                    message =
                                        'Account number resolution failed. Please try again later. Thanks!';
                                    break;
                                default:
                                    message = `Transfer service currently unavailable, please try again later. Thanks.`;
                                    break;
                            }
                        } else {
                            message = `Transfer service currently unavailable, please try again later. Thanks.`;
                        }
                        session.endConversation(message);
                    });
            }
        } else {
            next();
        }
    },

    (session, results, next) => {
        const amount = session.dialogData.amount;
        tracker(session, 'TransferIndex', 9);
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');

        const transaction = {
            amount: amount,
            description: 'session.dialogData.plan',
            identity: {
                accountName: session.dialogData.account_name,
                accountNumber: session.dialogData.recipient,
                bankCode: session.dialogData.bankCode,
                sourceChannel: session.message.address.channelId,
            },
            merchant: 'mwave',
            product: 'transfer',
            reference: reference,
            status: 'init',
            tags: ['funds transfer'],
            user: session.userData.phone,
        };

        if (results.response) {
            // save beneficiary

            createTransaction(transaction)
                .then((trans) => {
                    const payload = {
                        amount: amount,
                        product: 'transfer',
                        reference: reference,
                        user: session.userData.phone,
                    };

                    const token = issue(payload);
                    const payImageURL =
                        'http://res.cloudinary.com/pitech/image/upload/v1496360241/thumbs/wave.jpg';

                    const url = process.env.DOMAIN + 'transfer/?token=' + token;

                    const msg = new builder.Message(session).attachments([
                        new builder.HeroCard(session)
                            .text(`Click 'Pay Now'`)
                            .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                            .buttons([
                                builder.CardAction.openUrl(
                                    session,
                                    url,
                                    'Pay Now'
                                ),
                            ]),
                    ]);
                    session.send(msg);
                    const ben = new Beneficiary({
                        identifier: {
                            accountNumber: session.dialogData.recipient,
                            bankCode: session.dialogData.bankCode,
                            bankName: session.dialogData.bankName,
                            type: 'nuban',
                        },
                        name: transaction.identity.accountName,
                        user: session.dialogData.userId,
                    });
                    return ben.save();
                })
                .then((ben) => {
                    return User.findOneAndUpdate(
                        { phone: session.userData.phone },
                        { $push: { beneficiaries: ben } }
                    );
                })
                .then((user) => {
                    session.endDialog();
                })
                .catch((error) => {
                    logger.error('error');
                    const message = error.message
                        ? error.message
                        : 'There was an error';
                    session.endDialog(message);
                });
        } else if (session.dialogData.useExisting) {
            createTransaction(transaction)
                .then((trans) => {
                    const payload = {
                        amount: amount,
                        product: 'transfer',
                        reference: reference,
                        user: session.userData.phone,
                    };

                    const token = issue(payload);
                    const payImageURL =
                        'http://res.cloudinary.com/pitech/image/upload/v1496360241/thumbs/wave.jpg';

                    const url = process.env.DOMAIN + 'transfer/?token=' + token;

                    const msg = new builder.Message(session).attachments([
                        new builder.HeroCard(session)
                            .text(`Click 'Pay Now'`)
                            .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                            .buttons([
                                builder.CardAction.openUrl(
                                    session,
                                    url,
                                    'Pay Now'
                                ),
                            ]),
                    ]);
                    session.endDialog(msg);
                })
                .catch((error) => {
                    const message = error.message
                        ? error.message
                        : 'There was an error';
                    session.endDialog(message);
                });
        } else {
            session.endDialog('Cancelled');
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /^transfer$/i })
    .beginDialogAction('transferHelp', 'transferHelp', { matches: /^help$/i });

//////////////////////////////////////////////
//////////// HELP ///////////////////////////
// trigger contextual help if user types help in the bot///
// send user here if they get stuck in a dialog
// send message specific to the dialog step

bot.dialog('transferHelp', (session, args, next) => {
    logger.info(args);
    const msg = 'Transfer specific help';
    session.endDialog(msg);
});
