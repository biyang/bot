import * as builder from 'botbuilder';
import { stripIndents } from 'common-tags';
import * as Fuse from 'fuse.js';
import * as _ from 'lodash';
import logger from '../../../logger';
import { bot } from '../../../App';

import { issue } from '../../../utils/jwt';

import {
    Beneficiary,
    IBeneficiaryModel,
} from '../../../db/schemas/beneficiary';
import { IUserModel, User } from '../../../db/schemas/user';
import {
    initiateFundsTransfer,
    validate,
    validateAccount,
} from '../../../utils/mWave';

import { createTransaction } from '../../../utils/dbUtils';

import * as shortid from 'simple-id';
import tracker from '../mixpanel';

function validateAmount(Amount: string): boolean {
    const re = /^[0-9]+$/;
    const amount = parseInt(Amount, 10);
    if (re.test(Amount)) {
        return amount >= 100 && amount < 50001;
    }
    return false;
}

function validateNuban(cardNumber: string): boolean {
    const re = /\b\d{10}\b/;
    return re.test(cardNumber);
}

bot.dialog('transfer.controller', [
    (session, args, next) => {
        // first step
        session.replaceDialog('transfer.suspended');
        let amount;
        let recipient;
        tracker(session, 'TransferIndex', 1);
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (
                builder.EntityRecognizer.findEntity(
                    args.entities,
                    'builtin.number'
                )
            ) {
                const amountEntity: any = builder.EntityRecognizer.findEntity(
                    args.entities,
                    'builtin.number'
                );
                // resolve amount
                session.dialogData.amount = amountEntity.entity;
                // check if amount is only numbers
                if (validateAmount(session.dialogData.amount)) {
                    amount = session.dialogData.amount = _.toNumber(
                        amountEntity.entity
                    );
                } else {
                    amount = session.dialogData.amount = _.toNumber(
                        amountEntity.resolution.value
                    );
                }
            }
            if (
                builder.EntityRecognizer.findEntity(args.entities, 'Recipient')
            ) {
                recipient = builder.EntityRecognizer.findEntity(
                    args.entities,
                    'Recipient'
                ).entity;
                session.dialogData.recipient = recipient;
            }
        }

        if (amount && validateAmount(amount)) {
            next();
        } else {
            session.beginDialog('transfer.askAmount');
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 2);
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        if (amount) {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 2.5);
        const recipient = session.dialogData.recipient;
        if (recipient && validateNuban(recipient)) {
            session.dialogData.recipientSet = true;
        }
        next();
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 3);
        if (session.dialogData.recipientSet) {
            next();
        } else {
            Beneficiary.find({
                user: session.userData.phone,
                'identifier.type': 'nuban',
            })
                .then((beneficiaries) => {
                    if (beneficiaries && beneficiaries.length > 0) {
                        const listToSend = _.map(beneficiaries, (o) => {
                            return `${o.name.split(' ')[0]} - ${
                                o.identifier.bankName
                            } - ${o.identifier.accountNumber}`;
                        });
                        listToSend.push('New');
                        const name = session.userData.name
                            ? session.userData.name
                            : 'Pal';
                        builder.Prompts.choice(
                            session,
                            `Select one of your recent transfer recipients or 'new' to add a new one, ${name}`,
                            listToSend,
                            { listStyle: builder.ListStyle.button }
                        );
                    } else {
                        next();
                    }
                })
                .catch((e) => {
                    next();
                });
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 4);
        if (results.response) {
            if (results.response.entity !== 'New') {
                session.dialogData.select = true;
                session.dialogData.recipient = results.response.entity
                    .split('-')[2]
                    .trim();
                next();
            } else {
                next();
            }
        } else {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 5);
        const recipient = session.dialogData.recipient;
        if (recipient && validateNuban(recipient)) {
            next();
        } else {
            session.beginDialog('transfer.askAccount');
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 6);
        if (results.response) {
            session.dialogData.recipient = results.response;
        }
        const recipient = session.dialogData.recipient;
        if (recipient) {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 7);
        if (session.dialogData.select) {
            Beneficiary.findOne({
                'identifier.accountNumber': session.dialogData.recipient,
            })
                .then((r) => {
                    session.dialogData.bank = {
                        code: r.identifier.bankCode,
                        name: r.identifier.bankName,
                    };
                    next();
                })
                .catch((e) => {
                    session.beginDialog('transfer.askBank');
                });
        } else {
            session.beginDialog('transfer.askBank');
        }
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 8);
        if (results.response) {
            session.dialogData.bank = results.response;
        }
        next();
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 9);
        const recipient = session.dialogData.recipient;
        const bank = session.dialogData.bank;
        validate(recipient, bank.code, (account) => {
            // do something
            if (account && account.status) {
                const accountName = account.data.account_name;
                session.dialogData.account_name = accountName;
                const amount = session.dialogData.amount;
                session.send(`The service fee is NGN50 only.`);
                session.sendTyping();
                builder.Prompts.confirm(
                    session,
                    stripIndents`Please confirm that you want to send ₦${amount} to:
            Bank: ${bank.name}
            Account Number: ${recipient}
            Account Name: ${accountName}`,
                    { listStyle: builder.ListStyle.button }
                );
            } else {
                session.endConversation(
                    'The account number could not be resolved. Please try again later.'
                );
            }
        });
    },
    (session, results, next) => {
        tracker(session, 'TransferIndex', 10);
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const transaction = {
            amount: session.dialogData.amount,
            description: 'session.dialogData.plan',
            identity: {
                accountName: session.dialogData.account_name,
                bankName: session.dialogData.bank.name,
                accountNumber: session.dialogData.recipient,
                bankCode: session.dialogData.bank.code,
                sourceChannel: session.message.address.channelId,
            },
            merchant: 'mwave',
            product: 'transfer',
            reference: reference,
            status: 'init',
            tags: ['funds transfer'],
            user: session.userData.phone,
        };

        if (results.response) {
            // continue
            // break to Go
            session.replaceDialog('transfer.Go', { transaction });
        } else {
            // cance;
            session.endDialog('Cancelled');
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /^transfer$/i })
    .beginDialogAction('transferHelp', 'transferHelp', { matches: /^help$/i });

bot.dialog('transfer.Go', [
    (session, args, next) => {
        // check params
        tracker(session, 'TransferIndex', 11);
        const transaction = args.transaction;
        session.send('Working my magic... ⏳');
        session.sendTyping();

        createTransaction(transaction)
            .then((trans) => {
                const payload = {
                    amount: transaction.amount,
                    product: 'transfer',
                    reference: transaction.reference,
                    user: session.userData.phone,
                };

                const token = issue(payload);
                const payImageURL =
                    'http://res.cloudinary.com/pitech/image/upload/v1496360241/thumbs/wave.jpg';

                const url = process.env.DOMAIN + 'transfer/?token=' + token;

                const msg = new builder.Message(session).attachments([
                    new builder.HeroCard(session)
                        .text(`Click 'Pay'`)
                        .images([
                            builder.CardImage.create(session, payImageURL),
                        ])
                        .buttons([
                            builder.CardAction.openUrl(
                                session,
                                url,
                                `Pay NGN${payload.amount + 50}`
                            ),
                        ]),
                ]);
                session.send(msg);
                if (!session.dialogData.select) {
                    const ben = new Beneficiary({
                        identifier: {
                            accountNumber: transaction.identity.accountNumber,
                            bankCode: transaction.identity.bankCode,
                            bankName: transaction.identity.bankName,
                            type: 'nuban',
                        },
                        name: transaction.identity.accountName,
                        user: session.userData.phone,
                    });
                    return ben.save();
                } else {
                    return;
                }
            })
            .then((ben) => {
                if (ben) {
                    return User.findOneAndUpdate(
                        { phone: session.userData.phone },
                        { $push: { beneficiaries: ben._id } }
                    );
                } else {
                    return;
                }
            })
            .then((user) => {
                session.endDialog();
            })
            .catch((error) => {
                logger.error('error');
                const message = error.message
                    ? error.message
                    : 'There was an error';
                session.endDialog(message);
            });
    },
]);

bot.dialog('transfer.askAmount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        const name = session.userData.name ? session.userData.name : 'Friend';
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: `Okay ${name}, how much do you want to transfer ?`,
            1: 'Please enter the amount you want to transfer :-)',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.number(session, message);
        } else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('transfer.help');
        }
    },
    (session, results, next) => {
        if (results.response && validateAmount(results.response)) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response });
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('transfer.askAmount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);

bot.dialog('transfer.askAccount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'What is the 10 digit account number of the recipient ? :-)',
            1: 'Please reply with the 10 digit account number of the recipient',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.text(session, message);
        } else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('transfer.help');
        }
    },
    (session, results, next) => {
        if (results.response && validateNuban(results.response)) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response });
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('transfer.askAccount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);

const dict = {
    Access: { name: 'Access Bank', code: '044' },
    Citibank: { name: 'Citibank Nigeria', code: '023' },
    Diamond: { name: 'Diamond Bank', code: '063' },
    Ecobank: { name: 'Ecobank Nigeria', code: '050' },
    Zenith: { name: 'Zenith Bank', code: '057' },
    Fidelity: { name: 'Fidelity Bank', code: '070' },
    FirstBank: { name: 'First Bank of Nigeria', code: '011' },
    FCMB: { name: 'First City Monument Bank', code: '214' },
    GTB: { name: 'Guaranty Trust Bank', code: '058' },
    SKYE: { name: 'Skye Bank', code: '076' },
    More: 'More',
};

const dictTwo = {
    Heritage: { name: 'Heritage Bank', code: '030' },
    Keystone: { name: 'Keystone Bank', code: '082' },
    Mainstreet: { name: 'MainStreet Bank', code: '014' },
    STANBIC: { name: 'Stanbic IBTC Bank', code: '221' },
    StandardChartered: { name: 'Standard Chartered Bank', code: '068' },
    Sterling: { name: 'Sterling Bank', code: '232' },
    Union: { name: 'Union Bank of Nigeria', code: '032' },
    UBA: { name: 'United Bank For Africa', code: '033' },
    Unity: { name: 'Unity Bank', code: '215' },
    Wema: { name: 'Wema Bank', code: '035' },
    Enterprise: { name: 'Enterprise Bank', code: '084' },
};

bot.dialog('transfer.askBank', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        const name = session.userData.name;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: `What bank does the recipient use, ${name}`,
            1: 'Please select a bank from the list',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.choice(session, message, dict, {
                listStyle: builder.ListStyle.button,
            });
        } else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('transfer.help');
        }
    },
    (session, results, next) => {
        if (results.response && results.response.entity) {
            if (results.response.entity === 'More') {
                const message = `Okay, here are more banks you can choose from`;
                builder.Prompts.choice(session, message, dictTwo, {
                    listStyle: builder.ListStyle.button,
                });
            } else {
                session.endDialogWithResult({
                    response: dict[results.response.entity],
                });
            }
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('transfer.askBank', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
    (session, results, next) => {
        if (results.response && results.response.entity) {
            session.endDialogWithResult({
                response: dictTwo[results.response.entity],
            });
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('transfer.askBank', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);

bot.dialog('transfer.help', [
    (session, args, next) => {
        const choices = ['Start over', 'Continue'];
        builder.Prompts.choice(
            session,
            'Seems you are stuck, should I clear this conversation and start from the beginning ?',
            choices,
            { listStyle: builder.ListStyle.button }
        );
    },
    (session, results) => {
        const response = results.response ? results.response.entity : null;
        const choices = {
            'Start over': 'transfer.controller',
            Continue: 'Okay, going back to our convo ;-)',
        };
        choices[response]
            ? response === 'Start over'
                ? session.replaceDialog(choices[response])
                : session.endDialog(choices[response])
            : session.endDialog();
    },
]);

bot.dialog('transfer.suspended', [
    (session, args, next) => {
        const choices = ['Recharge', 'Pay Bill', 'exit'];
        session.userData.transferText = true;
        builder.Prompts.choice(
            session,
            'Our transfer service is currently undergoing maintenance, please try our other services instead. Thanks.',
            choices,
            { listStyle: builder.ListStyle.button }
        );
    },
    (session, results) => {
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            Recharge: '/recharge',
            Tickets: '/buyTicketIndex',
            'Pay Bill': '/PayBill',
            Transfer: 'transfer.controller',
            Settings: '/settings',
            default: 'defaultEnd',
        };
        session.replaceDialog(options[choice] || options['default']);
    },
]);
