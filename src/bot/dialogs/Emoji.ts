import * as builder from 'botbuilder';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/Emoji', [
    (session, args, next) => {
        tracker(session, 'Emoji', 1);
        session.endConversation(['👍', '😁', '✋']);
    },
]);

bot.dialog('/lolz', [
    (session, args, next) => {
        tracker(session, 'lolz', 1);
        session.endConversation(['😁', '😁']);
    },
]).triggerAction({ matches: /^ode|^lol|^lmao|^ugh|^aha|^lols|^lolz|^lmfao/i });

bot.dialog('/love', [
    (session, args, next) => {
        tracker(session, 'love', 1);

        session.endConversation(['💖', 'Love you too 😍']);
    },
]).triggerAction({ matches: /^(love|luv|i love you)/i });
