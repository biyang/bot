const min = process.env.minAmount || 100;
const max = process.env.maxAmount || 3000;

const validateAmount = (Amount, minumum = min, maximum = max) => {
    const amount = parseInt(Amount, 10);
    if (amount < minumum) {
        return 'tooLow';
    } else if (amount > maximum) {
        return 'tooHigh';
    } else {
        return amount >= minumum && amount <= maximum;
    }
};

export default validateAmount;
