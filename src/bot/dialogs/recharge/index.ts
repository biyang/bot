import * as builder from 'botbuilder';
import { bot } from '../../../App';
import tracker from '../mixpanel';

bot.dialog('/recharge', [
    (session, args) => {
        // use array of choices
        tracker(session, 'recharge', 1);
        const name = session.userData.name ? session.userData.name : 'Friend';
        const choices = ['Me', 'Another line', 'Cancel'];
        const textMessage = `Do you want to recharge your registered phone number or another line?, ${name} ?`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'My line' }],
                                [{ text: 'Another line' }],
                                [{ text: 'Cancel' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        } else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        tracker(session, 'recharge', 2);
        console.log('results', results);
        const choice = results.response ? results.response.entity : null;
        const options = {
            Me: 'selfRecharge.controller',
            'Another line': 'otherRecharge.controller',
            default: 'defaultEnd',
        };
        session.replaceDialog(options[choice] || options['default']);
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /^top-up|\/^credit|\/^airtime|^recharge$/i });
