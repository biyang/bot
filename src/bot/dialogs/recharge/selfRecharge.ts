import * as builder from 'botbuilder';
import * as shortid from 'simple-id';
import logger from '../../../logger';
import { bot } from '../../../App';
import tracker from '../mixpanel';

import { Transaction } from '../../../db/schemas/transaction';
import { User } from '../../../db/schemas/user';

import { createAirTransaction } from '../../../utils/dbUtils';
import {
    chargeWithPaystack,
    initializePaystack,
} from '../../../utils/paystack';

import { handleDispenseError } from '../../../utils/airvend';

import { dispenseAirtime } from '../../../vending/index';

import validateAmount from './validateAmount';

const payImageURL =
    'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const token = process.env.PAYSTACK_TOKEN;

bot.dialog('selfRecharge.controller', [
    (session, args, next) => {
        // first step
        let amount;
        tracker(session, 'selfRecharge', 1);
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (
                builder.EntityRecognizer.findEntity(
                    args.entities,
                    'builtin.number'
                )
            ) {
                amount =
                    builder.EntityRecognizer.findEntity(
                        args.entities,
                        'builtin.number'
                    ).entity || null;
            }
        }
        if (amount && validateAmount(amount) === true) {
            session.dialogData.amount = amount;
            next();
        } else {
            if (
                validateAmount(amount) === 'tooLow' ||
                validateAmount(amount) === 'tooHigh'
            ) {
                session.beginDialog('airtime.askAmount', {
                    numberOfRetries: 1,
                });
            } else {
                session.beginDialog('airtime.askAmount');
            }
        }
    },
    (session, results, next) => {
        tracker(session, 'selfRecharge', 2);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        if (amount) {
            next();
        } else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        // check phone number
        tracker(session, 'selfRecharge', 3);
        if (session.userData.phone) {
            // proceed to confirmation
            next();
        } else {
            // sencondRun for email and number
            session.beginDialog('phoneDialog');
        }
    },
    (session, results, next) => {
        tracker(session, 'selfRecharge', 4);
        const phone = session.userData.phone;
        if (phone) {
            next();
        }
    },
    (session, results, next) => {
        // check params
        session.sendTyping();
        tracker(session, 'selfRecharge', 5);
        const amount = session.dialogData.amount;
        User.findOne({ phone: session.userData.phone })
            .then((user) => {
                // check user for name and auth
                if (user && user.authorization) {
                    session.dialogData.network = user.network;
                    session.dialogData.paymentAuth =
                        user.authorization.authorization_code;
                    session.dialogData.authorization = user.authorization;
                    session.userData.email = user.email;
                    next();
                } else {
                    // break to firstPayment
                    const reference = shortid(
                        8,
                        '0123456789abcdefghijklmnopqrstuvwxyz'
                    );
                    session.dialogData.network = user.network;
                    const transaction = {
                        amount: amount,
                        address: session.message.address,
                        description: user.network,
                        identity: {
                            phone: session.userData.phone,
                            sourceChannel: session.message.address.channelId,
                        },
                        merchant: 'airvend',
                        product: 'airtime',
                        reference: reference,
                        status: 'init',
                        tags: ['airtime', 'self'],
                        user: session.userData.phone,
                    };
                    session.replaceDialog('firstPay', { transaction });
                }
            })
            .catch((error) => logger.error(error));
    },
    (session, results, next) => {
        tracker(session, 'selfRecharge', 6);
        const phone = session.userData.phone;
        const network = session.dialogData.network;
        const amount = session.dialogData.amount;

        const { bank, brand, last4, channel } =
            session.dialogData.authorization;

        const msg = `Just to confirm, you want to recharge your line ${phone} with ₦${amount}. ${
            channel === 'card' && brand && bank && last4
                ? `We'll automatically debit your ${bank} ${brand} card ending with ${last4}.`
                : ''
        }`;
        // ask for confirmation
        builder.Prompts.choice(session, msg, 'Yes|Change amount|Abort', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        // ceck
        tracker(session, 'selfRecharge', 7);
        session.sendTyping();
        const auth = session.dialogData.paymentAuth;
        const phone = session.userData.phone;
        const network = session.dialogData.network;
        const amount = session.dialogData.amount;
        const type = 'selfR';
        if (results && results.response) {
            const option = results.response.entity;
            if (option === 'Yes') {
                const reference = shortid(
                    8,
                    '0123456789abcdefghijklmnopqrstuvwxyz'
                );
                const transaction = {
                    amount: amount,
                    address: session.message.address,
                    description: network,
                    identity: {
                        phone: session.userData.phone,
                        sourceChannel: session.message.address.channelId,
                    },
                    merchant: 'airvend',
                    product: 'airtime',
                    reference: reference,
                    status: 'init',
                    tags: ['airtime', 'self'],
                    user: session.userData.phone,
                };
                session.replaceDialog('airtime.Go', {
                    transaction,
                    auth,
                    type,
                });
            } else if (option === 'Change amount') {
                session.beginDialog('airtime.askAmount');
            } else {
                session.replaceDialog('abort');
            }
        }
    },
    (session, results, next) => {
        tracker(session, 'selfRecharge', 8);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const network = session.dialogData.network;
        const auth = session.dialogData.paymentAuth;
        const type = 'selfR';
        const transaction = {
            amount: amount,
            address: session.message.address,
            description: network,
            identity: {
                phone: session.userData.phone,
                sourceChannel: session.message.address.channelId,
            },
            merchant: 'airvend',
            product: 'airtime',
            reference: reference,
            status: 'init',
            tags: ['airtime', 'self'],
            user: session.userData.phone,
        };
        session.replaceDialog('airtime.Go', { transaction, auth, type });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({
        matches: /^(My line)/i,
    });

bot.dialog('airtime.askAmount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'How much airtime do you want to buy? (Reply with an amount between 100 and 3000)',
            1: 'Please enter an amount between NGN100 and NGN3000 :-)',
            2: 'Please enter a valid amount to proceed :-)',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.text(session, message);
        } else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('airtime.help', {
                fromDialog: 'selfRecharge.controller',
            });
        }
    },
    (session, results, next) => {
        const cleanAmount =
            results.response &&
            String(results.response).replace(/[^\d\.\-]/g, '');
        const amount = cleanAmount && parseInt(cleanAmount, 10);
        if (amount && validateAmount(amount) === true) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response });
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('airtime.askAmount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);

bot.dialog('airtime.Go', [
    (session, args, next) => {
        // check params
        if (args) {
            const transaction = args.transaction;
            const auth = args.auth;
            const type = args.type;
            session.send('Working my magic... ⏳');
            session.sendTyping();
            const networks = {
                AIRTEL: 1,
                ETISALAT: 4,
                GLO: 3,
                MTN: 2,
            };
            const chargeData = {
                amount: transaction.amount,
                email: session.userData.email,
                paymentAuth: auth,
                reference: transaction.reference,
            };
            // charge user
            let transactionResult;
            createAirTransaction(transaction)
                .then((result) => {
                    transactionResult = result;
                    return chargeWithPaystack(token)(
                        chargeData,
                        transactionResult
                    );
                })
                .then((res: any) => {
                    if (res.data.status === 'success') {
                        // const payload = {
                        //   amount: transaction.amount,
                        //   network: networks[transactionResult.description],
                        //   phone: transaction.identity.phone,
                        //   reference: transaction.reference
                        // };
                        // dispenseAirtime(transactionResult);
                    } else {
                        Transaction.findByIdAndUpdate(transactionResult._id, {
                            status: 'failed',
                        })
                            .then((r) => {
                                logger.info(r);
                            })
                            .catch((e) => {
                                logger.error(e);
                            });
                        const message = res.message
                            ? res.message
                            : 'There was an error';
                        throw Error(message);
                    }
                })
                .catch((error) => {
                    handleDispenseError(error)(session);
                });
        } else {
            session.replaceDialog('/recharge');
        }
    },
]);

bot.dialog('firstPay', [
    (session, args, next) => {
        // check params
        session.sendTyping();
        const transaction = args.transaction;
        createAirTransaction(transaction)
            .then((trans) => {
                return User.findOneAndUpdate(
                    { phone: session.userData.phone },
                    { initialReference: transaction.reference }
                );
            })
            .then((u: any) => {
                session.userData.email = u.email;
                const data = {
                    amount: transaction.amount,
                    email: u.email,
                    reference: transaction.reference,
                };
                return initializePaystack(token)(data, transaction);
            })
            .then((result: any) => {
                const url = result;
                const msg = new builder.Message(session).attachments([
                    new builder.HeroCard(session)
                        .text(`Click 'Pay Now'`)
                        .images([
                            builder.CardImage.create(session, payImageURL),
                        ])
                        .buttons([
                            builder.CardAction.openUrl(session, url, 'Pay Now'),
                        ]),
                ]);
                session.endDialog(msg);
            })
            .catch((error) => {
                logger.error(error);
                session.endDialog('Error');
            });
    },
]);

bot.dialog('airtime.help', [
    (session, args, next) => {
        session.sendTyping();
        session.dialogData.fromDialog = args.fromDialog;
        const choices = ['Start over', 'Continue'];
        builder.Prompts.choice(
            session,
            'Seems you are stuck, should I clear this conversation and start from the beginning ?',
            choices,
            { listStyle: builder.ListStyle.button }
        );
    },
    (session, results) => {
        const response = results.response ? results.response.entity : null;
        const choices = {
            'Start over': session.dialogData.fromDialog,
            Continue: 'Okay, going back to our convo ;-)',
        };
        choices[response]
            ? response === 'Start over'
                ? session.replaceDialog(choices[response])
                : session.endDialog(choices[response])
            : session.endDialog();
    },
]);
