import * as builder from 'botbuilder';
import * as _ from 'lodash';
import * as shortid from 'simple-id';
import logger from '../../../logger';
import { bot } from '../../../App';
import tracker from '../mixpanel';

import { Transaction } from '../../../db/schemas/transaction';
import { User } from '../../../db/schemas/user';

import { getNetworkSync } from '../../../utils/finder';
import validateAmount from './validateAmount';

const validateNumber = (cardNumber: string): boolean => {
    const re = /^[0]\d{10}$/;
    return re.test(cardNumber);
};

bot.dialog('otherRecharge.controller', [
    (session, args, next) => {
        // first step
        let amount;
        let recipient;
        let numbers;
        tracker(session, 'otherRecharge', 1);
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (
                builder.EntityRecognizer.findEntity(
                    args.entities,
                    'builtin.number'
                )
            ) {
                numbers = builder.EntityRecognizer.findAllEntities(
                    args.entities,
                    'builtin.number'
                );
                const min = _.minBy(numbers, (o: any) => {
                    return parseInt(o.resolution.value, 10);
                });
                amount = min.resolution.value;
            }
            if (
                builder.EntityRecognizer.findEntity(args.entities, 'Recipient')
            ) {
                recipient = builder.EntityRecognizer.findEntity(
                    args.entities,
                    'Recipient'
                ).entity;
                session.dialogData.recipient = recipient;
            }
        }
        if (amount && validateAmount(amount) === true) {
            session.dialogData.amount = amount;
            next();
        } else {
            if (
                validateAmount(amount) === 'tooLow' ||
                validateAmount(amount) === 'tooHigh'
            ) {
                session.beginDialog('airtimeo.askAmount', {
                    numberOfRetries: 1,
                });
            } else {
                session.beginDialog('airtimeo.askAmount');
            }
        }
    },
    (session, results, next) => {
        tracker(session, 'otherRecharge', 2);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        if (amount) {
            next();
        } else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        // check phone number
        tracker(session, 'otherRecharge', 3);
        session.sendTyping();
        const recipient = session.dialogData.recipient;
        if (recipient) {
            // proceed to confirmation
            session.userData.recentNumbers = session.userData.recentNumbers
                ? session.userData.recentNumbers
                : [];
            session.userData.recentNumbers.push(recipient);
            session.userData.recentNumbers = _.uniq(
                session.userData.recentNumbers
            ).slice(0, 3);
            next();
        } else {
            // check recent recipients
            session.userData.recentNumbers = session.userData.recentNumbers
                ? session.userData.recentNumbers
                : [];
            const recent = session.userData.recentNumbers.filter((e) =>
                validateNumber(e)
            );

            if (recent && recent.length > 0) {
                // prompt with recent
                const arr = [...recent, 'New'];
                const msg = `Here are the numbers you recharged recently, select new to add a new one`;
                // ask for confirmation
                builder.Prompts.choice(session, msg, arr, {
                    listStyle: builder.ListStyle.button,
                });
            } else {
                session.beginDialog('airtime.askNumber');
            }
        }
    },
    (session, results, next) => {
        tracker(session, 'otherRecharge', 4);
        session.sendTyping();
        if (results.response) {
            if (
                results.response.entity &&
                validateNumber(results.response.entity)
            ) {
                // set number and go to next
                session.dialogData.recipient = results.response.entity;
                next();
            } else {
                // validate number and go to next
                if (validateNumber(results.response)) {
                    session.dialogData.recipient = results.response;
                    session.userData.recentNumbers = session.userData
                        .recentNumbers
                        ? session.userData.recentNumbers
                        : [];
                    session.userData.recentNumbers.push(results.response);
                    session.userData.recentNumbers = _.uniq(
                        session.userData.recentNumbers
                    ).slice(0, 3);
                    next();
                } else {
                    session.beginDialog('airtime.askNumber', {
                        numberOfRetries: 1,
                    });
                }
            }
        }

        if (session.dialogData.recipient) {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'otherRecharge', 5);
        session.sendTyping();
        // check for recipiend
        if (results.response) {
            session.userData.recentNumbers = session.userData.recentNumbers
                ? session.userData.recentNumbers
                : [];
            session.dialogData.recipient = results.response;
            session.userData.recentNumbers.push(results.response);
            session.userData.recentNumbers = _.uniq(
                session.userData.recentNumbers
            ).slice(0, 3);
        }
        const recipient = session.dialogData.recipient;
        if (recipient) {
            next();
        }
    },
    (session, results, next) => {
        session.sendTyping();
        tracker(session, 'otherRecharge', 6);
        // check network
        const recipient = session.dialogData.recipient;
        getNetworkSync(recipient, (network) => {
            // do something
            if (network && network.carrier) {
                session.dialogData.network = network.carrier;
                next({ response: { network: network.carrier } });
            } else {
                const msg = `Please select your network`;
                builder.Prompts.choice(
                    session,
                    msg,
                    'MTN|ETISALAT|GLO|AIRTEL',
                    {
                        listStyle: builder.ListStyle.button,
                    }
                );
            }
        });
    },
    (session, results, next) => {
        if (session.dialogData.network) {
            next();
        }
        if (results && results.response) {
            const network = results.response.entity;
            if (network) {
                session.dialogData.network = network;
                next();
            }
        }
    },
    (session, results, next) => {
        // check params
        tracker(session, 'otherRecharge', 7);
        session.sendTyping();
        const amount = session.dialogData.amount;
        const network = session.dialogData.network;
        const recipient = session.dialogData.recipient;
        User.findOne({ phone: session.userData.phone })
            .then((user) => {
                // check user for name and auth
                if (user && user.authorization) {
                    session.dialogData.paymentAuth =
                        user.authorization.authorization_code;
                    session.userData.email = user.email;
                    session.dialogData.authorization = user.authorization;
                    next();
                } else {
                    // break to firstPayment
                    const reference = shortid(
                        8,
                        '0123456789abcdefghijklmnopqrstuvwxyz'
                    );

                    const transaction = {
                        amount: amount,
                        address: session.message.address,
                        description: network,
                        identity: {
                            phone: recipient,
                            sourceChannel: session.message.address.channelId,
                        },
                        merchant: 'airvend',
                        product: 'otherR',
                        reference: reference,
                        status: 'init',
                        tags: ['airtime', 'self'],
                        user: session.userData.phone,
                    };
                    session.replaceDialog('firstPay', { transaction });
                }
            })
            .catch((error) => logger.error(error));
    },
    (session, results, next) => {
        session.sendTyping();
        tracker(session, 'otherRecharge', 8);
        const phone = session.dialogData.recipient;
        const amount = session.dialogData.amount;

        const { bank, brand, last4, channel } =
            session.dialogData.authorization;

        const msg = `Just to confirm, you want to top up ${phone} with ₦${amount}. ${
            channel === 'card' && brand && bank && last4
                ? `We'll automatically debit your ${bank} ${brand} card ending with ${last4}.`
                : ''
        }`;

        // ask for confirmation
        builder.Prompts.choice(session, msg, 'Yes|Change amount|Abort', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        // ceck
        session.sendTyping();
        tracker(session, 'otherRecharge', 9);
        const auth = session.dialogData.paymentAuth;
        const amount = session.dialogData.amount;
        const network = session.dialogData.network;
        const recipient = session.dialogData.recipient;
        const type = 'otherR';
        if (results && results.response) {
            const option = results.response.entity;
            if (option === 'Yes') {
                const reference = shortid(
                    8,
                    '0123456789abcdefghijklmnopqrstuvwxyz'
                );
                const transaction = {
                    amount: amount,
                    address: session.message.address,
                    description: network,
                    identity: {
                        phone: recipient,
                        sourceChannel: session.message.address.channelId,
                    },
                    merchant: 'airvend',
                    product: 'otherR',
                    reference: reference,
                    status: 'init',
                    tags: ['airtime', 'self'],
                    user: session.userData.phone,
                };
                session.replaceDialog('airtime.Go', {
                    transaction,
                    auth,
                    type,
                });
            } else if (option === 'Change amount') {
                session.beginDialog('airtimeo.askAmount');
            } else {
                session.replaceDialog('abort');
            }
        }
    },
    (session, results, next) => {
        tracker(session, 'otherRecharge', 10);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const network = session.dialogData.network;
        const recipient = session.dialogData.recipient;
        const auth = session.dialogData.paymentAuth;
        const type = 'otherR';
        const transaction = {
            amount: amount,
            address: session.message.address,
            description: network,
            identity: {
                phone: recipient,
                sourceChannel: session.message.address.channelId,
            },
            merchant: 'airvend',
            product: 'otherR',
            reference: reference,
            status: 'init',
            tags: ['airtime', 'self'],
            user: session.userData.phone,
        };
        session.replaceDialog('airtime.Go', { transaction, auth, type });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({
        matches: /^(Another line)/i,
    });

bot.dialog('airtimeo.askAmount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'How much airtime do you want to buy? (Reply with an amount between 100 and 3000)',
            1: 'Please enter an amount between NGN100 and NGN3000 :-)',
            2: 'Please enter a valid amount to proceed :-)',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.text(session, message);
        } else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('airtime.help', {
                fromDialog: 'otherRecharge.controller',
            });
        }
    },
    (session, results, next) => {
        const cleanAmount =
            results.response &&
            String(results.response).replace(/[^\d\.\-]/g, '');
        const amount = cleanAmount && parseInt(cleanAmount, 10);
        if (amount && validateAmount(amount) === true) {
            // check if account number is valid
            session.endDialogWithResult({ response: amount });
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('airtimeo.askAmount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);

bot.dialog('airtime.askNumber', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'What number do you want to top up ?',
            1: 'Please enter a valid Nigerian phone number, like 08056130076 :-)',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.text(session, message);
        } else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('airtime.help', {
                fromDialog: 'otherRecharge.controller',
            });
        }
    },
    (session, results, next) => {
        if (results.response && validateNumber(results.response)) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response });
        } else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('airtime.askNumber', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);
