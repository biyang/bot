import * as builder from 'botbuilder';
import * as shortid from 'simple-id';
import logger from '../../../logger';
import removeCard from '../../../utils/removeCard';
import { bot } from '../../../App';
import tracker from '../mixpanel';
import { createAirTransaction } from '../../../utils/dbUtils';
import {
    chargeWithPaystack,
    initializePaystack,
} from '../../../utils/paystack';
import { User } from '../../../db/schemas/user';

const token = process.env.PAYSTACK_TOKEN;
const payImageURL =
    'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';

bot.dialog('insufficentFunds', [
    (session, args, next) => {
        // check params
        session.sendTyping();
        // delete card
        const transaction = args && args.transaction;
        session.dialogData._transaction = transaction;
        if (transaction) {
            const msg = `We were unable to charge you due to insufficient funds.`;
            removeCard(session.userData.phone)
                .then(() => {
                    builder.Prompts.choice(
                        session,
                        msg,
                        'Use a new card this time|Cancel',
                        {
                            listStyle: builder.ListStyle.button,
                        }
                    );
                })
                .catch((error) => {
                    logger.error(error);
                });
        }
        session.endDialog(
            'We were unable to charge you due to insufficient funds.'
        );
    },
    (session, results, next) => {
        if (results && results.response) {
            const option = results.response.entity;
            if (option === 'Use a new card this time') {
                const transaction = session.dialogData._transaction;
                const reference = shortid(
                    8,
                    '0123456789abcdefghijklmnopqrstuvwxyz'
                );
                transaction.reference = reference;
                transaction.merchant = 'airvend';
                transaction.user = session.userData.phone;
                createAirTransaction(transaction)
                    .then((trans) => {
                        return User.findOneAndUpdate(
                            { phone: session.userData.phone },
                            { initialReference: transaction.reference }
                        );
                    })
                    .then((u: any) => {
                        session.userData.email = u.email;
                        const data = {
                            amount: transaction.amount,
                            email: u.email,
                            reference: transaction.reference,
                        };
                        return initializePaystack(token)(data, transaction);
                    })
                    .then((result: any) => {
                        const url = result;
                        const msg = new builder.Message(session).attachments([
                            new builder.HeroCard(session)
                                .text(`Click 'Pay Now'`)
                                .images([
                                    builder.CardImage.create(
                                        session,
                                        payImageURL
                                    ),
                                ])
                                .buttons([
                                    builder.CardAction.openUrl(
                                        session,
                                        url,
                                        'Pay Now'
                                    ),
                                ]),
                        ]);
                        session.endDialog(msg);
                    })
                    .catch((error) => {
                        logger.error(error);
                    });
            }
        }
        session.endDialog(
            'We were unable to charge you due to insufficient funds.'
        );
    },
]);
