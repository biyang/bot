import * as builder from 'botbuilder';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/broadcast', [
    (session, args) => {
        tracker(session, 'broadcast', 1);
        const message = args.message;
        session.endConversation(message);
    },
]);

bot.dialog('/broadcasterrr', [
    (session, args) => {
        tracker(session, 'broadcast', 1);
        const message = args.message;
        session.endConversation(message);
    },
]);
