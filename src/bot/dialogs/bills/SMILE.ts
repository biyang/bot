import * as builder from 'botbuilder';
import * as Fuse from 'fuse.js';
import * as _ from 'lodash';
import * as moment from 'moment';
import logger from '../../../logger';
import { bot } from '../../../App';

// import models
import { Beneficiary } from '../../../db/schemas/beneficiary';
import { Transaction } from '../../../db/schemas/transaction';
import { IUserModel, User } from '../../../db/schemas/user';

// import database utils
import {
    cardCheck,
    createTransaction,
    deriveEmail,
    mockPhone,
    saveNewUser,
} from '../../../utils/dbUtils';

// import prompt
import * as askSmileNo from '../../prompts/askSmileNo';

// import airvend
import {
    dispenseSMILE,
    handleDispenseError,
    handleDispenseResponse,
    verifySMILE,
} from '../../../utils/airvend';

import {
    chargeWithPaystack,
    initializePaystack,
} from '../../../utils/paystack';

const token = process.env.NEW_PAYSTACK_TOKEN;

import * as shortid from 'simple-id';

// import jwt issue
import { issue } from '../../../utils/jwt';
import tracker from '../mixpanel';

const payImageURL =
    'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';

const smileData = {
    '100GB': {
        amount: 40000,
        description: '100GB BumpaValue',
        typeCode: 623,
    },
    '10GB': {
        amount: 3500,
        description: '10GB Bigga',
        typeCode: 611,
    },
    '1GB': {
        amount: 300,
        description: '1GB Flexi plan',
        typeCode: 624,
    },
    '200GB': {
        amount: 70000,
        description: '200GB Anytime data plan',
        typeCode: 604,
    },
    '50GB': {
        amount: 15000,
        description: '50GB BumpaValue',
        typeCode: 621,
    },
    UnlimitedLite: {
        amount: 10000,
        description: '30 day UnlimitedLite plan',
        typeCode: 629,
    },
    UnlimitedPremium: {
        amount: 20000,
        description: '30 day UnlimitedPremium plan',
        typeCode: 655,
    },
};

const smileMap = [
    { name: '1Gb anytime', key: '1GB' },
    { name: '10Gb anytime', key: '10GB' },
    { name: '15Gb anytime', key: '15GB' },
    { name: '20Gb anytime', key: '20GB Anytime' },
    { name: 'UnlimitedLite', key: 'UnlimitedLite' },
    { name: 'UnlimitedPremium', key: 'UnlimitedPremium' },
    { name: '50Gb Anytime', key: '50GB' },
    { name: '100Gb Anytime', key: '100GB' },
    { name: '200Gb Anytime', key: '200GB' },
];

bot.dialog('/smile', [
    (session, args, next) => {
        tracker(session, 'smile', 1);
        let plan;
        // check if amount is stored in dialog data
        if (session.dialogData.plan) {
            plan = session.dialogData.plan;
        }
        if (args.type) {
            plan = session.dialogData.plan = args.type;
        }

        if (!plan) {
            const plans = _.map(smileMap, (p) => p.name);
            const name = session.userData.name
                ? session.userData.name
                : 'Friend';
            builder.Prompts.choice(
                session,
                `Please select the SMILE data plan you would like to subscribe to, ${name}`,
                plans,
                { listStyle: builder.ListStyle.button }
            );
        } else {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 2);
        if (results && results.response) {
            session.dialogData.selectedPlan = results.response.entity;
            next();
        } else {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 3);
        let plan = session.dialogData.plan;
        const selectedPlan = session.dialogData.selectedPlan;
        // normalize plan
        if (!plan) {
            plan = session.dialogData.plan = selectedPlan;
        }
        // we now have the plan so let's get the smartcard number
        // ask if touse one of saved smart card numbers
        User.findOne({ phone: session.userData.phone })
            .populate('beneficiaries')
            .then((user: any) => {
                if (!user) {
                    const newUser = {
                        address: session.message.address,
                        email: deriveEmail(session),
                        name: session.message.user.name,
                        phone: mockPhone(session),
                        userBotId: session.message.user.id,
                    };
                    saveNewUser(newUser)
                        .then((u: IUserModel) => {
                            session.userData.email = u.email;
                            session.dialogData.useExisting = false;
                            next();
                        })
                        .catch((err) => {
                            logger.error(err);
                            const message = err.message
                                ? err.message
                                : 'There was an error';
                            session.endDialog(`${message}, please try again`);
                        });
                } else if (user && user.beneficiaries.length < 1) {
                    session.dialogData.useExisting = false;
                    session.userData.email = user.email;
                    next();
                } else {
                    const beneficiaries = user.beneficiaries;
                    session.userData.email = user.email;
                    const checkIfBeneficiaryExists = _.filter(
                        beneficiaries,
                        (o: any) => {
                            return o.identifier.type === 'smile';
                        }
                    );
                    if (checkIfBeneficiaryExists.length > 0) {
                        session.dialogData.useExisting = true;
                        const listToSend = _.map(
                            checkIfBeneficiaryExists,
                            (o) => {
                                return o.name;
                            }
                        );
                        listToSend.push('New');
                        const name = session.userData.name
                            ? session.userData.name
                            : 'Friend';
                        builder.Prompts.choice(
                            session,
                            `Which SMILE account do you want to subscribe, ${name}`,
                            listToSend,
                            { listStyle: builder.ListStyle.button }
                        );
                    } else {
                        next();
                    }
                }
            });
    }, // 1410004094
    (session, results, next) => {
        tracker(session, 'smile', 4);
        if (session.dialogData.useExisting) {
            if (results.response) {
                if (results.response.entity !== 'New') {
                    session.dialogData.smartCard = results.response.entity;
                    next();
                } else {
                    session.dialogData.useExisting = false;
                    askSmileNo.beginDialog(session, {});
                }
            }
        } else {
            askSmileNo.beginDialog(session, {});
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 5);
        let smartCard = session.dialogData.smartCard;
        if (results && results.response) {
            smartCard = session.dialogData.smartCard = results.response;
            User.findOne({ phone: session.userData.phone })
                .then((user) => {
                    const ben = new Beneficiary({
                        identifier: { type: 'smile', smartCard: smartCard },
                        name: smartCard,
                        user: user._id,
                    });
                    return ben.save();
                })
                .then((ben) => {
                    return User.findOneAndUpdate(
                        { phone: session.userData.phone },
                        { $push: { beneficiaries: ben } }
                    );
                })
                .then((user) => {
                    next();
                })
                .catch((err) => {
                    logger.error(err);
                    next();
                });
        } else if (session.dialogData.useExisting) {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 6);
        let smartCard = session.dialogData.smartCard;

        if (session.dialogData.smartCard.entity) {
            smartCard = session.dialogData.smartCard =
                session.dialogData.smartCard.entity;
        }
        // verify smartcard number then prompt for confirmation
        const options = {
            distance: 100,
            keys: ['name', 'key'],
            location: 0,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            shouldSort: true,
            threshold: 0.3,
        };
        const fuse = new Fuse(smileMap, options); // "list" is the item array
        const result: any = fuse.search(session.dialogData.plan);

        const planDetails = smileData[result[0].key];
        verifySMILE(smartCard)
            .then((res) => {
                if (res && res.data && res.data.details) {
                    let msg;
                    const userdetails = (session.dialogData.dstvData =
                        res.data.details);
                    if (res.data.details.accountStatus === 'SUSPENDED') {
                        msg = `The ${result[0].name} package costs ₦${
                            planDetails.amount + 100
                        }. Please confirm that you want to pay for the following account: \n*
                Account Name: ${userdetails.firstName} ${
                            userdetails.lastName
                        } \n*
                Account Status: Service Suspended`;
                    } else if (res.data.details.accountStatus === 'CLOSED') {
                        msg = `The ${result[0].name} package costs ₦${
                            planDetails.amount + 100
                        }. Please confirm that you want to pay for the following account: \n*
                Account Name: ${userdetails.firstName} ${
                            userdetails.lastName
                        } \n*
                Account Status: CLOSED`;
                    } else {
                        msg = `The ${result[0].name} package costs ₦${
                            planDetails.amount + 100
                        }. Please confirm that you want to pay for the following account: \n*
                Account Name: ${userdetails.firstName} ${
                            userdetails.lastName
                        } \n*
                Due Date: ${moment(userdetails.dueDate).format(
                    'dddd, MMMM Do YYYY'
                )}`;
                    }
                    // send prompt
                    builder.Prompts.choice(
                        session,
                        msg,
                        'Yes|Change Package|Change Account Number|Abort',
                        { listStyle: builder.ListStyle.button }
                    );
                } else {
                    logger.error(res);

                    throw Error('There was an error, please try again later');
                }
            })
            .catch((error) => {
                logger.error(error);
                if (error && error.message) {
                    session.endDialog(error.message);
                } else {
                    session.endDialog(
                        'We could not verify your DSTv smart card number'
                    );
                }
            });
    },
    (session, results, next) => {
        tracker(session, 'smile', 7);
        const name = session.userData.name ? session.userData.name : 'Friend';
        if (results && results.response) {
            const option = results.response.entity;
            switch (option) {
                case 'Yes':
                    // dispense gotv
                    session.dialogData.goodToGo = true;
                    next();
                    break;
                case 'Change Package':
                    // prompt for amount
                    session.dialogData.changePackage = true;
                    const plans = _.map(smileMap, (p) => p.name);
                    builder.Prompts.choice(
                        session,
                        `Please select the Smile data plan you would like to subscribe to, ${name}`,
                        plans,
                        { listStyle: builder.ListStyle.button }
                    );
                    break;
                case 'Change Account Number':
                    // prompt for amount
                    session.dialogData.changeSmartCard = true;
                    askSmileNo.beginDialog(session, {});
                    break;
                default:
                    // doStuff
                    session.endDialog(`Okay, ${name}`);
                    break;
            }
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 8);
        if (session.dialogData.changePackage) {
            if (results && results.response) {
                session.dialogData.plan = results.response.entity;
                session.dialogData.goodToGo = true;
                next();
            }
        } else if (session.dialogData.changeSmartCard) {
            let smartCard = session.dialogData.smartCard;
            if (results && results.response) {
                smartCard = session.dialogData.smartCard = results.response;
            }
            // verify smartcard number then prompt for confirmation
            const options = {
                distance: 100,
                keys: ['name', 'key'],
                location: 0,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                shouldSort: true,
                threshold: 0.3,
            };
            const fuse = new Fuse(smileMap, options); // "list" is the item array
            const result: any = fuse.search(session.dialogData.plan);

            const planDetails = smileData[result[0].key];

            verifySMILE(smartCard)
                .then((res) => {
                    if (res && res.data && res.data.details) {
                        let msg;
                        const userdetails = (session.dialogData.gotvData =
                            res.data.details);
                        if (res.data.details.accountStatus === 'SUSPENDED') {
                            msg = `The ${result[0].name} package costs ₦${
                                planDetails.amount + 100
                            }. \n Account Name: ${userdetails.firstName} ${
                                userdetails.lastName
                            } \n*
                  Account Status: Service Suspended`;
                        } else if (
                            res.data.details.accountStatus === 'CLOSED'
                        ) {
                            msg = `The ${result[0].name} package costs ₦${
                                planDetails.amount + 100
                            }. \n Account Name: ${userdetails.firstName} ${
                                userdetails.lastName
                            } \n Account Status: CLOSED`;
                        } else {
                            msg = `The ${result[0].name} package costs ₦${
                                planDetails.amount + 100
                            }. \n Account Name: ${userdetails.firstName} ${
                                userdetails.lastName
                            } \n* Due Date: ${moment(
                                userdetails.dueDate
                            ).format('dddd, MMMM Do YYYY')}`;
                        }
                        // send prompt
                        session.send(msg);
                        session.dialogData.goodToGo = true;
                        // send prompt
                        next();
                    } else {
                        logger.error(res);
                        throw Error(
                            'There was an error, please try again later'
                        );
                    }
                })
                .catch((error) => {
                    logger.error(error);
                    if (error && error.message) {
                        session.endDialog(error.message);
                    } else {
                        session.endDialog(
                            'We could not verify your GOTv smart card number'
                        );
                    }
                });
        } else if (session.dialogData.goodToGo) {
            next();
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 9);
        if (session.dialogData.goodToGo) {
            // check if we have users authorization codde, if so ask confirmation else initiate fresh payment
            const userdetails = session.dialogData.gotvData;

            const options = {
                distance: 100,
                keys: ['name', 'key'],
                location: 0,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                shouldSort: true,
                threshold: 0.3,
            };
            const fuse = new Fuse(smileMap, options); // "list" is the item array
            const result: any = fuse.search(session.dialogData.plan);

            const planDetails = smileData[result[0].key];

            cardCheck(session.userData.phone)
                .then((user) => {
                    if (user && user.billsAuth) {
                        session.dialogData.paymentAuth =
                            user.billsAuth.authorization_code;
                        next();
                    } else {
                        // save transaction details and
                        // send user to payment page
                        // create url then send to user
                        const reference = shortid(
                            8,
                            '0123456789abcdefghijklmnopqrstuvwxyz'
                        );

                        const transaction = {
                            amount: planDetails.amount + 100,
                            address: session.message.address,
                            description: session.dialogData.plan,
                            identity: {
                                account: session.dialogData.smartCard,
                                bundleCode: planDetails.typeCode,
                            },
                            merchant: 'airvend',
                            product: 'smile',
                            reference: reference,
                            status: 'init',
                            tags: ['data', 'self'],
                            user: session.userData.phone,
                        };

                        createTransaction(transaction)
                            .then((trans) => {
                                return User.findOneAndUpdate(
                                    { phone: session.userData.phone },
                                    { initialReference: reference }
                                );
                            })
                            .then((u: any) => {
                                const data = {
                                    amount: transaction.amount,
                                    email: session.userData.email,
                                    reference: reference,
                                };

                                return initializePaystack(token)(
                                    data,
                                    transaction
                                );
                            })
                            .then((initUrl: any) => {
                                const url = initUrl;
                                const msg = new builder.Message(
                                    session
                                ).attachments([
                                    new builder.HeroCard(session)
                                        .text(`Click 'Pay Now'`)
                                        .images([
                                            builder.CardImage.create(
                                                session,
                                                payImageURL
                                            ),
                                        ])
                                        .buttons([
                                            builder.CardAction.openUrl(
                                                session,
                                                url,
                                                'Pay Now'
                                            ),
                                        ]),
                                ]);
                                session.endDialog(msg);
                            })
                            .catch((error) => {
                                logger.error(error);
                                session.endDialog('Error');
                            });
                    }
                })
                .catch((error) => {
                    logger.error(error);
                    session.endDialog('Error');
                });
        }
    },
    (session, results, next) => {
        tracker(session, 'smile', 10);
        const userdetails = session.dialogData.gotvData;

        const options = {
            distance: 100,
            keys: ['name', 'key'],
            location: 0,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            shouldSort: true,
            threshold: 0.3,
        };
        const fuse = new Fuse(smileMap, options); // "list" is the item array
        const plan: any = fuse.search(session.dialogData.plan);

        const planDetails = smileData[plan[0].key];

        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');

        const transaction = {
            amount: planDetails.amount + 100,
            address: session.message.address,
            description: session.dialogData.plan,
            identity: {
                account: session.dialogData.smartCard,
                bundleCode: planDetails.typeCode,
            },
            merchant: 'airvend',
            product: 'smile',
            reference: reference,
            status: 'init',
            tags: ['data', 'self'],
            user: session.userData.phone,
        };

        // charge user and dispense airtime
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        // charge user
        session.send('Working my magic... ⏳');
        session.sendTyping();

        createTransaction(transaction)
            .then((result) => {
                return chargeWithPaystack(token)(chargeData, transaction);
            })
            .then((res: any) => {
                if (res.data.status === 'success') {
                    // return dispenseSMILE(transaction);
                } else {
                    Transaction.findOneAndUpdate(
                        { reference: transaction.reference },
                        { status: 'failed' }
                    )
                        .then((r) => logger.info(r))
                        .catch((e) => logger.error(e));
                    const message = res.message
                        ? res.message
                        : 'There was an error';
                    throw Error(message);
                }
            })
            .then((trans) => {
                session.endDialog();
            })
            .catch((error) => {
                handleDispenseError(error)(session);
            });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .reloadAction('reload')
    .triggerAction({ matches: /^smile$/i });
