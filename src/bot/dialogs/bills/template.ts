import * as builder from 'botbuilder';
import * as _ from 'lodash';
import * as shortid from 'simple-id';
import logger from '../../../logger';
import { bot } from '../../../App';
import tracker from '../mixpanel';

import { Beneficiary } from '../../../db/schemas/beneficiary';
import { Transaction } from '../../../db/schemas/transaction';
import { User } from '../../../db/schemas/user';

import { getNetwork, handleGetNetwork } from '../../../utils/finder';

const payImageURL =
    'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';

bot.dialog('bill.controller', [
    (session, args, next) => {
        // check args
        let plan;
        if (args) {
            if (args.type) {
                plan = session.dialogData.plan = args.type;
            }
        }
        if (plan) {
            session.dialogData.plan = plan;
            next();
        } else {
            session.beginDialog('bills.askPlan');
        }
    },
]);

bot.dialog('askPlan0', []);
// && validateAmount(amount) === true
