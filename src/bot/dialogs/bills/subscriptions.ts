import * as builder from 'botbuilder';
import logger from '../../../logger';
import { bot } from '../../../App';
import tracker from '../mixpanel';

bot.dialog('/subscriptions', [
    (session, args) => {
        tracker(session, 'subscriptions', 1);
        session.endConversation(
            'This feature is under development. Please check later. Thanks.'
        );
    },
]).triggerAction({ matches: /^subscriptions/i });
