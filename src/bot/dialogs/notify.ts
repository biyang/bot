import * as builder from 'botbuilder';
import logger from '../../logger';
import { bot } from '../../App';
import tracker from './mixpanel';

const fbShareLink = 'https://biya.com.ng';

const networks = {
    AIRTEL: 1,
    ETISALAT: 4,
    GLO: 3,
    MTN: 2,
};

bot.dialog('/notify', [
    (session, args) => {
        // Deliver notification to the user.
        tracker(session, 'notify', 1);
        logger.info(args.type);
        let msg;
        let imageURL;
        if (args && args.type === 'self') {
            imageURL = `http://res.cloudinary.com/pitech/image/upload/v1487970365/thumbs/${
                networks[args.network]
            }.jpg`;
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `Your line, ${args.number} has been recharged with ₦${args.amount} airtime 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'otherR') {
            imageURL = `http://res.cloudinary.com/pitech/image/upload/v1487970365/thumbs/${
                networks[args.network]
            }.jpg`;
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `${args.number} has been recharged with ₦${args.amount} airtime 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'dstv') {
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1483994623/thumbs/DStv-logo.jpg';
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `DSTv payment of ₦${args.amount} was successful. Thanks for using Biya. 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'gotv') {
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1483994623/thumbs/GOtv.jpg';
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `GOTv payment of ₦${args.amount} was successful. Thanks for using Biya. 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'smile') {
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1483994623/thumbs/Smile.jpg';
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `Your ${args.plan} SMILE data subscription was successful. Thanks for using Biya. 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (
            ['ikedc', 'ibedc', 'ekdc', 'phdc', 'enugu'].includes(
                args.type.toLowerCase()
            )
        ) {
            imageURL =
                'https://res.cloudinary.com/pitech/image/upload/v1590346246/thumbs/check.jpg';
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `Your payment was successful. Your ${args.pin} for a total of ${args.amount}. Thanks for using Biya. 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'transfer') {
            imageURL =
                'http://res.cloudinary.com/pitech/image/upload/v1496360269/thumbs/tick.jpg';
            const transaction = args.transaction;
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `Your transfer of ${transaction.amount} to ${transaction.identity.accountName} was successful. Thanks for using Biya. 😁`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'ticket') {
            session.send(`Here's your ticket`);
            imageURL = args.ticket;
            const transaction = args.transaction;
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `Amount: ${transaction.amount} // Route: ${transaction.identity.destination}`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'merchantPay') {
            session.send('Payment successful');
            imageURL =
                'https://res.cloudinary.com/pitech/image/upload/v1496360269/thumbs/tick.jpg';
            const transaction = args.transaction;
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(
                        `Amount: ${
                            transaction.amount
                        } // Product: ${transaction.product.toLowerCase()}`
                    )
                    .images([builder.CardImage.create(session, imageURL)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        } else if (args && args.type === 'spectranet') {
            session.send('Payment successful');
            const text = `Done! Your spectranet ${args.amount} PIN is ${args.pin}. and the serial number is ${args.serial}.`;
            const image =
                'http://res.cloudinary.com/pitech/image/upload/v1509224991/thumbs/spec.jpg';
            const transaction = args.transaction;
            msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(text)
                    .images([builder.CardImage.create(session, image)])
                    .buttons([
                        builder.CardAction.openUrl(
                            session,
                            fbShareLink,
                            'Share'
                        ),
                    ]),
            ]);
        }
        // SEND SUCCESS message
        session.send('Thanks for your patronage!');
        session.endConversation(msg);
    },
]);
