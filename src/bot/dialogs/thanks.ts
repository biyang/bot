import * as builder from 'botbuilder';
import * as moment from 'moment';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/thanks', [
    (session, args) => {
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const thanks = [
            `Happy to help ${name}! 😎`,
            `You're welcome!`,
            `Thank you too ${name}`,
        ];
        tracker(session, 'thanks', 2);
        session.endConversation(thanks);
    },
]).triggerAction({
    matches:
        /^(awesome|alright|aii|ok$|okay|oshay|oshe|oshee baby|thanks|thanks biya|thank you|nice|great|good$)/i,
});

// https://api.biya.com.ng/api/v1/fwave/verify_paystack?trxref=bybt1495272016150&reference=bybt1495272016150

// ^(cancel|abort|stop|nevermind)/i
