import * as builder from 'botbuilder';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/error', [
    (session, args) => {
        tracker(session, 'error', 1);
        if (args.error) {
            session.endDialog(args.error);
        } else {
            session.endDialog(
                'There was an error dispensing the service. We will automatically retry in a few seconds.'
            );
        }
    },
]);

bot.dialog('unknownError', [
    (session) => {
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const choices = ['Recharge', 'Pay Bill', 'exit'];
        const message = `Oops, ${name}, I didn't get that 😢 would you like to try one of the following instead?`;
        tracker(session, 'error', 1);
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(message);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Recharge' }],
                                [{ text: 'Pay Bill' }],
                                [{ text: 'exit' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        } else {
            builder.Prompts.choice(session, message, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            Recharge: '/recharge',
            Tickets: '/buyTicketIndex',
            'Pay Bill': '/PayBill',
            Transfer: 'transfer.controller',
            Settings: '/settings',
            default: 'defaultEnd',
        };
        tracker(session, 'error', 2);
        session.replaceDialog(options[choice] || options['default']);
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});
