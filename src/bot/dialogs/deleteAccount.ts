import * as builder from 'botbuilder';
import logger from '../../logger';
import { bot } from '../../App';

import { IUserModel, User } from '../../db/schemas/user';
import { getName } from '../../utils/botUtils';
import { updateAll } from '../../utils/dbUtils';
import tracker from './mixpanel';

bot.dialog('/deleteAccount', [
    (session) => {
        tracker(session, 'deleteAccount', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        builder.Prompts.confirm(
            session,
            `Are you sure you want to delete your account, ${name}?`,
            { listStyle: builder.ListStyle.button }
        );
    },
    (session, results) => {
        if (results.response) {
            tracker(session, 'deleteAccount', 2);
            session.userData = {};
            session.userData.name = 'Friend';
            session.conversationData = {};
            session.dialogData = {};
            User.remove({ phone: session.userData.phone })
                .then((result) => {
                    session.endDialog(
                        `Your account was successfully deleted 💔`
                    );
                })
                .catch((error) => {
                    logger.error(error);
                    session.endDialog(
                        `Sorry, your account could not be deleted. Please try again.`
                    );
                });
        } else {
            session.endDialog('Okay! 😊');
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({
        matches: /^(Delete account|reset|reload)/i,
    });

bot.dialog('/hotReload', [
    (session) => {
        tracker(session, 'hotReload', 1);
        session.userData = {};
        session.userData.name = 'Friend';
        session.conversationData = {};
        session.dialogData = {};
        User.remove({ phone: session.userData.phone }).exec((error, result) => {
            if (!error) {
                session.endConversation('Done');
            } else {
                logger.error(error);
                session.endConversation(
                    `Sorry, your account could not be deleted. Please try again.`
                );
            }
        });
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});

bot.dialog('/clearErrors', [
    (session) => {
        tracker(session, 'clearErrors', 1);
        session.endConversation(
            'The previous issue has been resolved, please try again. Thanks.'
        );
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /^(clear errors|clear error|clear)$/i });

bot.dialog('/clearConvo', [
    (session) => {
        tracker(session, 'clearConvo', 1);
        session.endConversation();
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});

bot.dialog('/changeEmail', [
    (session) => {
        session.userData.email = '1a74b-009d@biya.com.ng';
        session.endConversation();
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});

bot.dialog('/changeNama', [
    (session) => {
        session.userData.name = 'Kayode';
        session.endConversation();
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});
bot.dialog('/changeNetwork', [
    (session) => {
        session.userData.network = 'MTN';
        session.endConversation();
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});

bot.dialog('/logUser', [
    (session) => {
        // tslint:disable-next-line
        console.log('loguser', session.userData);
        session.endConversation();
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});

bot.dialog('/removeAddress', [
    (session) => {
        // tslint:disable-next-line
        session.userData.addressVersion = false;
        session.endConversation();
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches:
        /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});
bot.dialog('/paymentClear', [
    (session, args) => {
        tracker(session, 'paymentClear', 1);
        User.findOneAndUpdate(
            { phone: session.userData.phone },
            { authorization: null, billsAuth: null }
        ).exec((error, result) => {
            if (!error) {
                logger.result(result);
                session.endConversation();
            } else {
                logger.error(error);
                session.endConversation();
            }
        });
    },
]);

// bot
//   .dialog('/speciallslslslls', [
//     session => {
//       updateAll()
//         .then(all => {
//           session.endConversation('done');
//         })
//         .catch(err => {
//           session.endConversation('err');
//         });
//     }
//   ])
//   .cancelAction('cancel', 'Ok. Cancelled.', {
//     confirmPrompt: 'Are you sure you want to cancel this operation?',
//     matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i
//   })
//   .triggerAction({ matches: /^(kolasinac)$/i });

bot.dialog('/fixNames', [
    (session) => {
        if (!session.userData.name || session.userData.name.length < 1) {
            // fix names
            const name = getName(session);
            User.findOneAndUpdate(
                { userBotId: session.message.user.id },
                { name: name }
            )
                .then(console.log)
                .catch(console.log);
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /^(kolasinac)$/i });

bot.dialog('/tryAddUpdate', [
    (session) => {
        User.find({}).then((arr) => {
            arr.forEach((element) => {
                const channelId = session.message.address.channelId;
                const updateObject = {
                    addresses: {
                        [channelId]: session.message.address,
                    },
                };
                User.findByIdAndUpdate(element._id, { $set: updateObject })
                    .then((res) => {
                        logger.info('done');
                        // session.replaceDialog('welcomeUser');
                    })
                    .catch((error) => {
                        // logger.error(error);
                    });
            });
        });
        session.endConversation();
    },
]).triggerAction({ matches: /^(iwobi)$/i });

bot.dialog('/updt', [
    (session) => {
        if (session.userData.phone) {
            session.userData.active = true;
        }
        session.endConversation();
    },
]).triggerAction({ matches: /^(cassano)$/i });

bot.dialog('/makeAgent', [
    (session) => {
        // tslint:disable-next-line
        session.userData.isAgent = true;
        session.endConversation();
    },
]);
