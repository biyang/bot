import * as builder from 'botbuilder';
import logger from '../../logger';
import { bot } from '../../App';
import tracker from './mixpanel';
import { SlackNotify } from '../../monitor/SlackService';

bot.dialog('/onDefault', [
    (session, args) => {
        tracker(session, 'onDefault', 1);
        logger.error(args);
        SlackNotify({
            text: Array.isArray(args) && args.join(', '),
            username: 'Error',
            channel: '#feed-errors',
        });
        session.endConversation();
        // session.endConversation(
        //   `😿 I did not understand that. To see what I can help you with, send 'help'`
        // );
    },
]);

bot.dialog('abort', [
    (session, args) => {
        tracker(session, 'abort', 1);
        const name = session.userData.name ? session.userData.name : 'Pal';
        const messages = `Okay, ${name}.`;
        session.endConversation(messages);
    },
]);
