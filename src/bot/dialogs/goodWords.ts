import * as builder from 'botbuilder';
import * as moment from 'moment';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/goodWords', [
    (session, args) => {
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const thanks = [`Thanks for your kind words!`, `Thank you ${name}`];
        tracker(session, 'goodWords', 2);
        session.endConversation(thanks);
    },
]);
