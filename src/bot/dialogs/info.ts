import * as builder from 'botbuilder';
import { oneLine } from 'common-tags';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/info', [
    (session) => {
        const card = new builder.HeroCard(session)
            .title('')
            .text(
                oneLine`I am Biya. I am a robot that can help you buy airtime,\
        transfer funds and pay bills 24 hours a day, 7 days a week !`
            )
            .images([
                builder.CardImage.create(
                    session,
                    'http://res.cloudinary.com/pitech/image/upload/v1489243312/thumbs/icon.jpg'
                ),
            ])
            .buttons([
                builder.CardAction.openUrl(
                    session,
                    'https://biya.com.ng',
                    'Learn more'
                ),
            ]);
        tracker(session, 'info', 1);
        const msg = new builder.Message(session).attachments([card]);
        session.endDialog(msg);
    },
]);
