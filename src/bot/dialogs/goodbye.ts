import * as builder from 'botbuilder';
import * as moment from 'moment';
import { bot } from '../../App';
import tracker from './mixpanel';

bot.dialog('/goodbye', [
    (session, args) => {
        tracker(session, 'goodbye', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const choices = [
            `Goodbye ${name}, see you soon!`,
            `Talk to you later, ${name}`,
            `Bye! See you later.`,
        ];
        session.endConversation(choices);
    },
]).triggerAction({ matches: /^stop|^goodbye|^quit|^exit|^bye|^later/i });

bot.dialog('/abort', [
    (session, args) => {
        tracker(session, 'abort', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const choices = [
            `Okay ${name}, see you later!`,
            `Talk to you soon, ${name}`,
        ];
        session.endConversation(choices);
    },
]);
