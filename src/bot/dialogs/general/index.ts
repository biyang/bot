import * as builder from 'botbuilder';
import * as Fuse from 'fuse.js';
import * as _ from 'lodash';
import * as moment from 'moment';
import logger from '../../../logger';
import { bot } from '../../../App';

// import models model
import {
    Beneficiary,
    IBeneficiaryModel,
} from '../../../db/schemas/beneficiary';
import { IMerchantModel, Merchant } from '../../../db/schemas/merchant';
import { Transaction } from '../../../db/schemas/transaction';
import { IUserModel, User } from '../../../db/schemas/user';

// import ask smart card number prompt
import * as askGotvSmartNo from '../../prompts/askGotvSmartNo';

// import database utils
import {
    cardCheck,
    createTransaction,
    deriveEmail,
    mockPhone,
    saveNewUser,
} from '../../../utils/dbUtils';

// importJWR
import { issue } from '../../../utils/jwt';

// import airvend dispense
import { dispenseGeneral } from '../../../utils/general';

const payImageURL =
    'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';

import * as shortid from 'simple-id';

import {
    chargeWithPaystack,
    initializePaystack,
} from '../../../utils/paystack';

const token = process.env.NEW_PAYSTACK_TOKEN;
////////////////////////////////////////////
//
bot.dialog('/bills', [
    (session, args, next) => {
        // substitute with custom prompt
        builder.Prompts.text(
            session,
            `Please enter the code of the merchant you'd like to pay :)`
        );
    },
    (session, results, next) => {
        if (results.response) {
            // check if merchant exists
            Merchant.findOne({ symbol: results.response })
                .then((merchant) => {
                    session.dialogData.merchant = merchant;
                    next();
                })
                .catch((error) => {
                    const message = error.message
                        ? error.message
                        : 'There was an error';
                    session.endDialog(message);
                    logger.error(error);
                });
        }
    },
    (session, results, next) => {
        // we now have the plan so let's get the smartcard number
        // ask if touse one of saved smart card numbers

        User.findOne({ phone: session.userData.phone })
            .populate('beneficiaries')
            .then((user: any) => {
                if (!user) {
                    const newUser = {
                        address: session.message.address,
                        email: deriveEmail(session),
                        name: session.message.user.name,
                        phone: mockPhone(session),
                        userBotId: session.message.user.id,
                    };
                    saveNewUser(newUser)
                        .then((savedUser: IUserModel) => {
                            session.userData.email = savedUser.email;
                            session.dialogData.useExisting = false;
                            next();
                        })
                        .catch((err) => {
                            logger.error(err);
                            const message = err.message
                                ? err.message
                                : 'There was an error';
                            session.endDialog(`${message}, please try again`);
                        });
                } else if (user && user.beneficiaries.length < 1) {
                    session.userData.email = user.email;
                    session.dialogData.useExisting = false;
                    next();
                }
            });
    },
    (session, results, next) => {
        const merchant = session.dialogData.merchant;
        builder.Prompts.number(
            session,
            `Please enter the amount you'd like to pay ${merchant.name} :)`
        );
    },
    (session, results, next) => {
        const amount = (session.dialogData.amount = results.response);
        const merchant = session.dialogData.merchant;
        builder.Prompts.confirm(
            session,
            `Please confirm payment of ${amount} to  ${merchant.name}`,
            { listStyle: builder.ListStyle.button }
        );
    },
    (session, results, next) => {
        const amount = session.dialogData.amount;
        const merchant = session.dialogData.merchant;
        if (results.response) {
            cardCheck(session.userData.phone)
                .then((user) => {
                    if (user && user.billsAuth) {
                        session.dialogData.paymentAuth =
                            user.billsAuth.authorization_code;
                        next();
                    } else {
                        // save transaction details and
                        // send user to payment page
                        // create url then send to user
                        const reference = shortid(
                            8,
                            '0123456789abcdefghijklmnopqrstuvwxyz'
                        );

                        const transaction = {
                            amount: amount + 100,
                            description: `Payment to ${merchant.name}`,
                            identity: {},
                            merchant: merchant.symbol,
                            product: 'merchantPay',
                            reference: reference,
                            status: 'init',
                            tags: ['vendor'],
                            user: session.userData.phone,
                        };

                        createTransaction(transaction)
                            .then((trans) => {
                                return User.findOneAndUpdate(
                                    { phone: session.userData.phone },
                                    { initialReference: reference }
                                );
                            })
                            .then((updatedUser: any) => {
                                const data = {
                                    amount: transaction.amount,
                                    email: session.userData.email,
                                    reference: reference,
                                };

                                return initializePaystack(token)(
                                    data,
                                    transaction
                                );
                            })
                            .then((result: any) => {
                                const url = result;
                                const msg = new builder.Message(
                                    session
                                ).attachments([
                                    new builder.HeroCard(session)
                                        .text(`Click 'Pay Now'`)
                                        .images([
                                            builder.CardImage.create(
                                                session,
                                                payImageURL
                                            ),
                                        ])
                                        .buttons([
                                            builder.CardAction.openUrl(
                                                session,
                                                url,
                                                'Pay Now'
                                            ),
                                        ]),
                                ]);
                                session.endDialog(msg);
                            })
                            .catch((error) => {
                                logger.error(error);
                                session.endDialog('Error');
                            });
                    }
                })
                .catch((error) => {
                    logger.error(error);
                    session.endDialog('Error');
                });
        } else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        const amount = session.dialogData.amount;
        const merchant = session.dialogData.merchant;

        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');

        const transaction = {
            amount: amount + 100,
            description: `Payment to ${merchant.name}`,
            identity: {},
            merchant: merchant.symbol,
            product: 'merchantPay',
            reference: reference,
            status: 'init',
            tags: ['vendor'],
            user: session.userData.phone,
        };

        // charge user
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        // charge user
        createTransaction(transaction)
            .then((result) => {
                return chargeWithPaystack(token)(chargeData, transaction);
            })
            .then((res: any) => {
                if (res.data.status === 'success') {
                    // do transaction
                    dispenseGeneral(transaction)
                        .then((dis) => {
                            logger.info(dis);
                            session.endConversation('bee');
                        })
                        .catch((e) => {
                            logger.info(e);
                        });
                } else {
                    Transaction.findOneAndUpdate(
                        { reference: transaction.reference },
                        { status: 'failed' }
                    )
                        .then((r) => {
                            logger.info(r);
                        })
                        .catch((e) => {
                            logger.error(e);
                        });
                    const message = res.message
                        ? res.message
                        : 'There was an error';
                    throw Error(message);
                }
            })
            .catch((error) => {
                logger.error(error);
                session.endConversation('Error');
            });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /general/ });
