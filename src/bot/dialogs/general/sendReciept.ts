import * as builder from 'botbuilder';
import * as moment from 'moment';
import { Subscription } from '../../../db/schemas/subscription';
import { User } from '../../../db/schemas/user';
import logger from '../../../logger';
import { createLocalSubscription } from '../../../utils/dbUtils';
import { createPaystackSubscription } from '../../../utils/paystack';
import { bot } from '../../../App';
import tracker from '../mixpanel';

const token = process.env.NEW_PAYSTACK_TOKEN;

const plans = {
    dstv100: 'PLN_gzsmxs0ru6lq03f',
    dstv10000: 'PLN_3wvqjs5c1rm6ii8',
    dstv14800: 'PLN_pmsh1fb7zknex0z',
    dstv17000: 'PLN_you1kcrqr3gh8ns',
    dstv2000: 'PLN_tq2fl8hiaai6ee3',
    dstv3900: 'PLN_aekk6rorim8ew8n',
    dstv6400: 'PLN_lxpynevhh26t8y5',
    gotv1350: 'PLN_zk8246m9s1tykh0',
    gotv2000: 'PLN_raxsvkec6mnqalf',
    gotv500: 'PLN_7n4ri5b9z8ix75f',
};

bot.dialog('sendReciept', [
    (session, args, next) => {
        tracker(session, 'sendReciept', 1);
        session.sendTyping();
        if (
            args.transaction.product.toLowerCase() === 'dstv' ||
            args.transaction.product.toLowerCase() === 'gotv'
        ) {
            // ask user to subscribe
            session.send(args.msg);
            session.send('Thanks for using Biya 💓');
            session.sendTyping();
            session.dialogData.transaction = args.transaction;
            builder.Prompts.confirm(
                session,
                'Would you like to autorenew next month ?',
                { listStyle: builder.ListStyle.button }
            );
        } else {
            session.send(args.msg);
            session.sendTyping();
            session.endConversation('Thanks for using Biya 💓');
        }
    },
    (session, results, next) => {
        if (results.response) {
            // create subscription
            tracker(session, 'sendReciept', 2);
            session.sendTyping();
            const transaction = session.dialogData.transaction;
            const data = {
                customer: session.userData.email,
                plan: plans[
                    `${transaction.product.toLowerCase()}${transaction.amount}`
                ],
                start_date: moment()
                    // .add(1, 'months')
                    .add(3, 'minutes')
                    .format(),
            };
            session.sendTyping();

            // save subscription to user for cancelation
            createPaystackSubscription(token)(data)
                .then((sub: any) => {
                    if (sub.status) {
                        return createLocalSubscription(transaction, sub.data);
                    }
                })
                .then((user) => {
                    session.send(
                        'Awesome! We will automatically renew your plan next month :-)'
                    );
                    session.endConversation(
                        `You can manage your subscriptions at anytime by sending me the word 'subscriptions' 😄`
                    );
                })
                .catch((error) => {
                    logger.error(error);
                    session.endConversation(
                        'There was an error creating your subscription. We will fix this at our end.'
                    );
                });
        } else {
            session.endConversation('Okay!');
        }
    },
]).triggerAction({ matches: /^(triggerSub)$/i });

bot.dialog('nymsg', [
    (session, args, next) => {
        const name = session.userData.name ? session.userData.name : 'Pal';
        const text = `Hi ${name}, Happy New Year from all of us at Biya! We wish you a fulfilling 2018! 🎄`;
        const imageURL =
            'https://res.cloudibs/happy-new-year-2018-background-with-lighting-effect_1340-3833.jpg';
        const msg = new builder.Message(session).attachments([
            new builder.HeroCard(session)
                .text(text)
                .images([builder.CardImage.create(session, imageURL)]),
        ]);
    },
]);
