import * as builder from 'botbuilder';
import * as _ from 'lodash';
import { User } from '../../db/schemas/user';
import logger from '../../logger';
import { getNetwork, handleGetNetwork } from '../../utils/finder';
import { bot } from '../../App';
import * as askPhoneNumber from '../prompts/askPhoneNumber';

import tracker from './mixpanel';

bot.dialog('/settings', [
    (session, args) => {
        // use array of choices
        tracker(session, 'settings', 1);
        const choices = [
            'Edit phone number',
            'Change name',
            'Unlink card',
            'Delete account',
            'exit',
        ];
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const textMessage = `Please select one of the following options, ${name}`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Change phone number' }],
                                [{ text: 'Change name' }],
                                [{ text: 'Unlink card' }],
                                [{ text: 'Delete account' }],
                                [{ text: 'exit' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        } else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        tracker(session, 'settings', 2);
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            'Change phone number': '/editPhone',
            'Change name': '/editName',
            'Unlink card': '/unlinkCard',
            'Delete account': '/deleteAccount',
            default: 'defaultEnd',
        };
        session.replaceDialog(options[choice] || options['default']);
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({ matches: /^settings|^setting|^change|\/^settings/i });

bot.dialog('/editName', [
    (session) => {
        tracker(session, 'editName', 1);
        builder.Prompts.text(
            session,
            'What do you want to change your name to?'
        );
    },
    (session, results, next) => {
        if (results.response) {
            tracker(session, 'editName', 2);
            session.sendTyping();
            User.findOneAndUpdate(
                { phone: session.userData.phone },
                { name: results.response }
            )
                .then((user) => {
                    session.userData.name = _.capitalize(results.response);
                    session.endConversation(
                        `Great! Your name has been changed to ${session.userData.name}`
                    );
                })
                .catch((error) => {
                    session.endConversation(
                        `Sorry! Your name could not be changed, please try again later`
                    );
                });
        } else {
            session.endConversation('Okay! Cool!');
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({
        matches: /^(Change name)/i,
    });

bot.dialog('/autoName', [
    (session, results, next) => {
        User.findOne({ phone: session.userData.phone });
        session.userData.name = _.capitalize(
            session.message.user.name.split(' ')[0]
        );
        session.endConversation();
    },
]);

bot.dialog('/autoPhonee', [
    (session, results, next) => {
        session.userData.phone = '07031698266';
        session.endConversation();
    },
]);

bot.dialog('/autoNameDb', [
    (session, results, next) => {
        User.findOne({ phone: session.userData.phone })
            .then((u) => {
                session.userData.name = u.name;
            })
            .catch((e) => {
                logger.error(e);
            });
        session.endConversation();
    },
]);

bot.dialog('/editPhone', [
    (session) => {
        tracker(session, 'editPhone', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        builder.Prompts.confirm(
            session,
            `Are you sure you want edit your phone number, ${name}?`,
            { listStyle: builder.ListStyle.button }
        );
    },
    (session, results) => {
        if (results.response) {
            tracker(session, 'editPhone', 2);
            User.findOne({ phone: session.userData.phone }, 'phone')
                .then((person) => {
                    askPhoneNumber.beginDialog(session, {
                        prompt: `Your current number is ${person.phone}, enter your new number`,
                    });
                })
                .catch((error) => {
                    logger.error(error);
                    session.endDialog('Error! 😊');
                });
        } else {
            session.endDialog('Okay! 😊');
        }
    },
    (session, results, next) => {
        if (results.response) {
            const phone = (session.userData.phone = results.response.trim());
            // get network and save number
            tracker(session, 'editPhone', 3);
            getNetwork(phone)
                .then((result) => {
                    const network = handleGetNetwork(result);
                    session.userData.network = network;
                    return User.findOneAndUpdate(
                        { phone: session.userData.phone },
                        { phone: phone, network: network }
                    );
                })
                .then((user) => {
                    const name = session.userData.name
                        ? session.userData.name
                        : session.message.user.name.split(' ')[0];
                    session.endDialog(
                        `Great! Your phone number has been successfully changed, ${name}`
                    );
                })
                .catch((err) => {
                    logger.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(message);
                });
        } else {
            session.endDialog('Oops, invalid phone number');
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({
        matches: /^(Change phone number)/i,
    });

bot.dialog('/unlinkCard', [
    (session) => {
        tracker(session, 'unlinkCard', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        User.findOne({ phone: session.userData.phone })
            .then((user) => {
                if (user) {
                    const card = user.authorization.last4;
                    builder.Prompts.confirm(
                        session,
                        `Are you sure you want unlink your debit card (ending ***${card}), ${name}?`,
                        { listStyle: builder.ListStyle.button }
                    );
                } else {
                    throw Error('Error');
                }
            })
            .catch((error) => {
                builder.Prompts.confirm(
                    session,
                    `Are you sure you want unlink your debit card, ${name}?`,
                    { listStyle: builder.ListStyle.button }
                );
            });
    },
    (session, results) => {
        tracker(session, 'unlinkCard', 2);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        if (results.response) {
            User.findOneAndUpdate(
                { phone: session.userData.phone },
                { authorization: null, billsAuth: null }
            )
                .then((user) => {
                    session.endDialog(
                        `Your debit card was successfully unlinked, ${name}.\
              You will have to set it up again when you make your next transaction.`
                    );
                })
                .catch((err) => {
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(message);
                });
        } else {
            session.endDialog(`Okay ${name}! 😊`);
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
        confirmPrompt: 'Are you sure you want to cancel this operation?',
        matches:
            /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
    })
    .triggerAction({
        matches: /^(Unlink card)/i,
    });
