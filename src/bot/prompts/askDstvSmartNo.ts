import * as builder from 'botbuilder';

function validateSmartNumber(cardNumber: string): boolean {
  const re = /\b\d{10,11}\b/;
  return re.test(cardNumber);
}

export const beginDialog = (session, options) => {
  session.beginDialog('askDstvSmartNo', options || {});
};

export const create = bot => {
  const prompt = new builder.IntentDialog()
    .onBegin((session, args) => {
      // Save args passed to prompt
      session.dialogData.retryPrompt =
        args.retryPrompt || 'Please enter a valid smart card number';

      // Send initial prompt
      // - This isn't a waterfall so you shouldn't call any of the built-in Prompts.
      session.send(
        args.prompt || 'Ok. So what is the smart card number of the DSTv ?'
      );
    })
    .matches(/(give up|quit|skip|yes)/i, session => {
      // Return 'false' to indicate they gave up
      session.endDialogWithResult({ response: false });
    })
    .onDefault(session => {
      // Validate users reply.
      if (validateSmartNumber(session.message.text)) {
        // Return 'true' to indicate success
        session.endDialogWithResult({ response: session.message.text });
      } else {
        // Re-prompt user
        session.send(session.dialogData.retryPrompt);
      }
    });

  bot.dialog('askDstvSmartNo', prompt);
};
