import * as builder from 'botbuilder';

const minimumAmount = 100;

function validateAmount(amount: string): boolean {
    return minimumAmount <= parseInt(amount, 10);
}

export const beginDialog = (session, options) => {
    session.beginDialog('askRechargeAmount', options || {});
};

export const create = (bot) => {
    const prompt = new builder.IntentDialog()
        .onBegin((session, args) => {
            // Save args passed to prompt
            session.dialogData.retryPrompt =
                args.retryPrompt ||
                'Please enter an amount greater than or equal to 100 naira';

            // Send initial prompt
            // - This isn't a waterfall so you shouldn't call any of the built-in Prompts.
            session.send(
                args.prompt ||
                    'How much airtime do you want to buy? (Reply with an amount between 100 and 3000)'
            );
        })
        .matches(/^(give up|quit|skip|yes)/i, (session) => {
            // Return 'false' to indicate they gave up
            session.endDialogWithResult({ response: false });
        })
        .onDefault((session) => {
            // Validate users reply.
            if (validateAmount(session.message.text)) {
                // Return 'true' to indicate success
                session.endDialogWithResult({ response: session.message.text });
            } else {
                // Re-prompt user
                session.send(session.dialogData.retryPrompt);
            }
        });

    bot.dialog('askRechargeAmount', prompt);
};
