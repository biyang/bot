import * as builder from 'botbuilder';
import logger from '../logger';
import { bot } from '../App';

import { commandsMiddleware } from '../handoff/commands';
import { Handoff } from '../handoff/handoff';

import { IUserModel, User } from '../db/schemas/user';

import { trackUser } from './dialogs/mixpanel';

bot.use(
    builder.Middleware.dialogVersion({
        resetCommand: /^upgrdReset/i,
        version: 4.0,
    })
);
// bot.use(builder.Middleware.firstRun({ version: 3.0, dialogId: '*:/firstRun' }));
import { Utterances } from '../db/schemas/utterances';

bot.use({
    receive: (event, next) => {
        const newUtterance = new Utterances({
            event: event,
            type: 'incoming',
        });
        newUtterance.save();
        next();
    },
    send: (event, next) => {
        const newUtterance = new Utterances({
            event: event,
            type: 'outgoing',
        });
        newUtterance.save();
        next();
    },
});

bot.use({
    botbuilder: (session: any, next) => {
        // check if user is in mixpanel
        if (!session.userData.tracker && session.userData.active) {
            trackUser(session);
            session.userData.tracker = true;
        }
        const channel = session.message.source;
        const choices = {
            telegram: 'addresses.telegram',
            facebook: 'addresses.facebook',
            skype: 'addresses.skype',
            slack: 'addresses.slack',
            directline: 'addresses.directline',
        };
        const updateProperty = choices[channel];
        if (updateProperty) {
            if (
                !session.userData.addressVersion ||
                session.userData.addressVersion < 2
            ) {
                User.findOneAndUpdate(
                    { phone: session.userData.phone },
                    {
                        $set: {
                            [updateProperty]: session.message.address,
                        },
                    }
                )
                    .then(() => {
                        session.userData.addressVersion = 2;
                    })
                    .catch((error) => {
                        logger.error(error);
                    });
            }
        }
        session.error = (err) => {
            // Do something custom
            // session.replaceDialog('unknownError');
            logger.error(err);
        };
        next();
    },
});

// replace this function with custom login/verification for agents
const isAgent = (session: builder.Session) =>
    session.userData.isAgent ? true : false;

const handoff = new Handoff(bot, isAgent);

// ========================================================
// Bot Middleware
// ========================================================
bot.use(
    commandsMiddleware(handoff),
    handoff.routingMiddleware()
    /* other bot middlware should probably go here */
);

const whitelist = ['PiBotProject', 'Biyabot', 'Bot'];
bot.on('conversationUpdate', (message) => {
    if (message.membersAdded && message.membersAdded.length > 0) {
        if (whitelist.indexOf(message.membersAdded[0].name) !== -1) {
            bot.send(
                new builder.Message()
                    .address(message.address)
                    .text(`Hey pal! Send 'Hi' or an instruction to begin 🚀`)
            );
        }
    }
});
