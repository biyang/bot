"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const slackhook = 'https://hooks.slack.com/services/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';
const Slack = require('slack-notify')(slackhook);
exports.SlackNotify = (options) => {
    if (process.env.NODE_ENV === 'production') {
        Slack.send({
            channel: options.channel || '#general',
            icon_emoji: options.emoji || ':ghost:',
            text: options.text,
            fields: options.fields,
            unfurl_links: options.unfurl_links || 1,
            username: options.username || 'botbot'
        });
    }
};
exports.default = Slack;
