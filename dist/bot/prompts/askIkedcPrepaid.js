"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
function validateSmartNumber(cardNumber) {
    const re = /\b\d{10,15}\b/;
    return re.test(cardNumber);
}
exports.beginDialog = (session, options) => {
    session.beginDialog('askIkedcPrepaid', options || {});
};
exports.create = (bot) => {
    const prompt = new builder.IntentDialog()
        .onBegin((session, args) => {
        const provider = args.provider;
        // Save args passed to prompt
        session.dialogData.retryPrompt =
            args.retryPrompt ||
                `Please enter a valid ${provider.toUpperCase()} Prepaid account number`;
        // Send initial prompt
        // - This isn't a waterfall so you shouldn't call any of the built-in Prompts.
        session.send(args.prompt ||
            `Ok. Please enter your ${provider.toUpperCase()} Prepaid account number.`);
    })
        .matches(/(give up|quit|skip|yes)/i, (session) => {
        // Return 'false' to indicate they gave up
        session.endDialogWithResult({ response: false });
    })
        .onDefault((session) => {
        // Validate users reply.
        if (validateSmartNumber(session.message.text)) {
            // Return 'true' to indicate success
            session.endDialogWithResult({ response: session.message.text });
        }
        else {
            // Re-prompt user
            session.send(session.dialogData.retryPrompt);
        }
    });
    bot.dialog('askIkedcPrepaid', prompt);
};
