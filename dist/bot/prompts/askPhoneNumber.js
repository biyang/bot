"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
function validatePhoneNumber(phone) {
    const re = /^[0]\d{10}$/;
    return re.test(phone);
}
exports.beginDialog = (session, options) => {
    session.beginDialog('askPhoneNumber', options || {});
};
exports.create = bot => {
    const prompt = new builder.IntentDialog()
        .onBegin((session, args) => {
        // Save args passed to prompt
        session.dialogData.retryPrompt =
            args.retryPrompt ||
                'Please reply with a valid Nigerian mobile phone number';
        // Send initial prompt
        // - This isn't a waterfall so you shouldn't call any of the built-in Prompts.
        session.send(args.prompt ||
            'Before we proceed, please reply with your phone number so I can set up your account :-)');
    })
        .matches(/(give up|quit|skip|yes)/i, session => {
        // Return 'false' to indicate they gave up
        session.endDialogWithResult({ response: false });
    })
        .onDefault(session => {
        // Validate users reply.
        if (validatePhoneNumber(session.message.text)) {
            // Return 'true' to indicate success
            session.endDialogWithResult({ response: session.message.text.trim() });
        }
        else {
            // Re-prompt user
            session.send(session.dialogData.retryPrompt);
        }
    });
    bot.dialog('askPhoneNumber', prompt);
};
