"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/goodWords', [
    (session, args) => {
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const thanks = [`Thanks for your kind words!`, `Thank you ${name}`];
        mixpanel_1.default(session, 'goodWords', 2);
        session.endConversation(thanks);
    },
]);
