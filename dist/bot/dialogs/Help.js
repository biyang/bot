"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const common_tags_1 = require("common-tags");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/help', [
    (session) => {
        mixpanel_1.default(session, 'help', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        session.send(common_tags_1.stripIndents `Send 'Menu' to begin or send something like
        * Recharge my line or
        * load 500 on 08132614422 or
        * Pay DSTV
        * Send 'help' to show this message again
        * Send 'bye' to end at any time`);
        builder.Prompts.choice(session, `Do you want to see more example commands?`, 'More|No', { listStyle: builder.ListStyle.button });
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'help', 2);
        if (results.response && results.response.entity === 'More') {
            session.replaceDialog('/keywords');
        }
        else {
            session.endDialog('Okay! 😊');
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({ matches: /^(help|epp)/i });
const keywords = `* Rechage my line with 'amount'
  \n* Recharge my line
  \n* load 'amount' on 'number'
  \n* Subscribe
  \n* DSTV
  \n* GOTV`;
App_1.bot.dialog('/keywords', [
    (session) => {
        mixpanel_1.default(session, 'keywords', 1);
        session.send('Here are some more sample commands!');
        session.endDialog(keywords);
    },
]);
