"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const App_1 = require("../../../App");
App_1.bot.dialog('/stuckOrCancel', [
    (session, args, next) => {
        // check args
        // if stuck, offer to help or cancel
        // check from dialog to know where to redirect
        // redirect with available args
        // save args
        const stuck = (session.dialogData.stuckOrCancel = args.stuckOrCancel);
        if (stuck.status) {
            builder.Prompts.choice(session, `Seems like you're stuck, do you need help with that ?`, ['yes', 'cancel', 'no']);
        }
        else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        const stuck = session.dialogData.stuckOrCancel;
        switch (results.response.entity) {
            case 'yes':
                // provide help based on the help context
                session.beginDialog('contextAwareHelp', stuck.helpContext);
                break;
        }
    },
]);
// stuck
const helpDictionary = {};
App_1.bot.dialog('contextAwareHelp', [
    (session, args) => {
        const context = args;
        session.endDialog(helpDictionary[context]);
    },
]);
