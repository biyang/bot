"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const moment = require("moment");
const logger_1 = require("../../../logger");
const dbUtils_1 = require("../../../utils/dbUtils");
const paystack_1 = require("../../../utils/paystack");
const App_1 = require("../../../App");
const mixpanel_1 = require("../mixpanel");
const token = process.env.NEW_PAYSTACK_TOKEN;
const plans = {
    dstv100: 'PLN_gzsmxs0ru6lq03f',
    dstv10000: 'PLN_3wvqjs5c1rm6ii8',
    dstv14800: 'PLN_pmsh1fb7zknex0z',
    dstv17000: 'PLN_you1kcrqr3gh8ns',
    dstv2000: 'PLN_tq2fl8hiaai6ee3',
    dstv3900: 'PLN_aekk6rorim8ew8n',
    dstv6400: 'PLN_lxpynevhh26t8y5',
    gotv1350: 'PLN_zk8246m9s1tykh0',
    gotv2000: 'PLN_raxsvkec6mnqalf',
    gotv500: 'PLN_7n4ri5b9z8ix75f',
};
App_1.bot.dialog('sendReciept', [
    (session, args, next) => {
        mixpanel_1.default(session, 'sendReciept', 1);
        session.sendTyping();
        if (args.transaction.product.toLowerCase() === 'dstv' ||
            args.transaction.product.toLowerCase() === 'gotv') {
            // ask user to subscribe
            session.send(args.msg);
            session.send('Thanks for using Biya 💓');
            session.sendTyping();
            session.dialogData.transaction = args.transaction;
            builder.Prompts.confirm(session, 'Would you like to autorenew next month ?', { listStyle: builder.ListStyle.button });
        }
        else {
            session.send(args.msg);
            session.sendTyping();
            session.endConversation('Thanks for using Biya 💓');
        }
    },
    (session, results, next) => {
        if (results.response) {
            // create subscription
            mixpanel_1.default(session, 'sendReciept', 2);
            session.sendTyping();
            const transaction = session.dialogData.transaction;
            const data = {
                customer: session.userData.email,
                plan: plans[`${transaction.product.toLowerCase()}${transaction.amount}`],
                start_date: moment()
                    .add(3, 'minutes')
                    .format(),
            };
            session.sendTyping();
            // save subscription to user for cancelation
            paystack_1.createPaystackSubscription(token)(data)
                .then((sub) => {
                if (sub.status) {
                    return dbUtils_1.createLocalSubscription(transaction, sub.data);
                }
            })
                .then((user) => {
                session.send('Awesome! We will automatically renew your plan next month :-)');
                session.endConversation(`You can manage your subscriptions at anytime by sending me the word 'subscriptions' 😄`);
            })
                .catch((error) => {
                logger_1.default.error(error);
                session.endConversation('There was an error creating your subscription. We will fix this at our end.');
            });
        }
        else {
            session.endConversation('Okay!');
        }
    },
]).triggerAction({ matches: /^(triggerSub)$/i });
App_1.bot.dialog('nymsg', [
    (session, args, next) => {
        const name = session.userData.name ? session.userData.name : 'Pal';
        const text = `Hi ${name}, Happy New Year from all of us at Biya! We wish you a fulfilling 2018! 🎄`;
        const imageURL = 'https://res.cloudibs/happy-new-year-2018-background-with-lighting-effect_1340-3833.jpg';
        const msg = new builder.Message(session).attachments([
            new builder.HeroCard(session)
                .text(text)
                .images([builder.CardImage.create(session, imageURL)]),
        ]);
    },
]);
