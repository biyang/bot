"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
const merchant_1 = require("../../../db/schemas/merchant");
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
// import database utils
const dbUtils_1 = require("../../../utils/dbUtils");
// import airvend dispense
const general_1 = require("../../../utils/general");
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const shortid = require("simple-id");
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
////////////////////////////////////////////
//
App_1.bot.dialog('/bills', [
    (session, args, next) => {
        // substitute with custom prompt
        builder.Prompts.text(session, `Please enter the code of the merchant you'd like to pay :)`);
    },
    (session, results, next) => {
        if (results.response) {
            // check if merchant exists
            merchant_1.Merchant.findOne({ symbol: results.response })
                .then((merchant) => {
                session.dialogData.merchant = merchant;
                next();
            })
                .catch((error) => {
                const message = error.message
                    ? error.message
                    : 'There was an error';
                session.endDialog(message);
                logger_1.default.error(error);
            });
        }
    },
    (session, results, next) => {
        // we now have the plan so let's get the smartcard number
        // ask if touse one of saved smart card numbers
        user_1.User.findOne({ phone: session.userData.phone })
            .populate('beneficiaries')
            .then((user) => {
            if (!user) {
                const newUser = {
                    address: session.message.address,
                    email: dbUtils_1.deriveEmail(session),
                    name: session.message.user.name,
                    phone: dbUtils_1.mockPhone(session),
                    userBotId: session.message.user.id,
                };
                dbUtils_1.saveNewUser(newUser)
                    .then((savedUser) => {
                    session.userData.email = savedUser.email;
                    session.dialogData.useExisting = false;
                    next();
                })
                    .catch((err) => {
                    logger_1.default.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(`${message}, please try again`);
                });
            }
            else if (user && user.beneficiaries.length < 1) {
                session.userData.email = user.email;
                session.dialogData.useExisting = false;
                next();
            }
        });
    },
    (session, results, next) => {
        const merchant = session.dialogData.merchant;
        builder.Prompts.number(session, `Please enter the amount you'd like to pay ${merchant.name} :)`);
    },
    (session, results, next) => {
        const amount = (session.dialogData.amount = results.response);
        const merchant = session.dialogData.merchant;
        builder.Prompts.confirm(session, `Please confirm payment of ${amount} to  ${merchant.name}`, { listStyle: builder.ListStyle.button });
    },
    (session, results, next) => {
        const amount = session.dialogData.amount;
        const merchant = session.dialogData.merchant;
        if (results.response) {
            dbUtils_1.cardCheck(session.userData.phone)
                .then((user) => {
                if (user && user.billsAuth) {
                    session.dialogData.paymentAuth =
                        user.billsAuth.authorization_code;
                    next();
                }
                else {
                    // save transaction details and
                    // send user to payment page
                    // create url then send to user
                    const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                    const transaction = {
                        amount: amount + 100,
                        description: `Payment to ${merchant.name}`,
                        identity: {},
                        merchant: merchant.symbol,
                        product: 'merchantPay',
                        reference: reference,
                        status: 'init',
                        tags: ['vendor'],
                        user: session.userData.phone,
                    };
                    dbUtils_1.createTransaction(transaction)
                        .then((trans) => {
                        return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: reference });
                    })
                        .then((updatedUser) => {
                        const data = {
                            amount: transaction.amount,
                            email: session.userData.email,
                            reference: reference,
                        };
                        return paystack_1.initializePaystack(token)(data, transaction);
                    })
                        .then((result) => {
                        const url = result;
                        const msg = new builder.Message(session).attachments([
                            new builder.HeroCard(session)
                                .text(`Click 'Pay Now'`)
                                .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                                .buttons([
                                builder.CardAction.openUrl(session, url, 'Pay Now'),
                            ]),
                        ]);
                        session.endDialog(msg);
                    })
                        .catch((error) => {
                        logger_1.default.error(error);
                        session.endDialog('Error');
                    });
                }
            })
                .catch((error) => {
                logger_1.default.error(error);
                session.endDialog('Error');
            });
        }
        else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        const amount = session.dialogData.amount;
        const merchant = session.dialogData.merchant;
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const transaction = {
            amount: amount + 100,
            description: `Payment to ${merchant.name}`,
            identity: {},
            merchant: merchant.symbol,
            product: 'merchantPay',
            reference: reference,
            status: 'init',
            tags: ['vendor'],
            user: session.userData.phone,
        };
        // charge user
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        // charge user
        dbUtils_1.createTransaction(transaction)
            .then((result) => {
            return paystack_1.chargeWithPaystack(token)(chargeData, transaction);
        })
            .then((res) => {
            if (res.data.status === 'success') {
                // do transaction
                general_1.dispenseGeneral(transaction)
                    .then((dis) => {
                    logger_1.default.info(dis);
                    session.endConversation('bee');
                })
                    .catch((e) => {
                    logger_1.default.info(e);
                });
            }
            else {
                transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'failed' })
                    .then((r) => {
                    logger_1.default.info(r);
                })
                    .catch((e) => {
                    logger_1.default.error(e);
                });
                const message = res.message
                    ? res.message
                    : 'There was an error';
                throw Error(message);
            }
        })
            .catch((error) => {
            logger_1.default.error(error);
            session.endConversation('Error');
        });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({ matches: /general/ });
