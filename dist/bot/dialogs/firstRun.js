"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const builder = require("botbuilder");
const common_tags_1 = require("common-tags");
const _ = require("lodash");
const speakeasy = require("speakeasy");
const logger_1 = require("../../logger");
const App_1 = require("../../App");
const shortid = require("simple-id");
let logId;
axios_1.default.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger_1.default.info('Starting Request', Object.assign({ logId: logId, url: request.url }, (request.data && request.data.body)));
    return request;
});
axios_1.default.interceptors.response.use((response) => {
    logger_1.default.info('Response:', Object.assign({}, (response && response.data), { logId: logId }));
    return response;
});
// import Customer model
const user_1 = require("../../db/schemas/user");
const askPhoneNumber = require("../prompts/askPhoneNumber");
const botUtils_1 = require("../../utils/botUtils");
// session.userData.rechargeMe.amount
const dbUtils_1 = require("../../utils/dbUtils");
const finder_1 = require("../../utils/finder");
const mixpanel_1 = require("./mixpanel");
const SlackService_1 = require("../../monitor/SlackService");
////////////////////////////
//// BACKSTORY ////////////
///////////////////////////
// phone number has become primary key so make sure to change it in all dialogs
// check user existence
// check phone number existence
// check channel
// save primary channel
// Add first run dialog
App_1.bot.dialog('/firstRun', [
    (session, args, next) => {
        // Update version number and start Prompts
        // - The version number needs to be updated first to prevent re-triggering
        //   the dialog.
        // check name
        mixpanel_1.default(session, 'firstrun', 1);
        user_1.User.findOne({ userBotId: session.message.user.id })
            .then((user) => {
            if (user) {
                session.userData.version = 3.0;
                session.userData.email = user.email;
                session.userData.name = user.name;
                session.userData.network = user.network;
                session.userData.phone = user.phone;
                session.replaceDialog('/hello');
            }
            else {
                next();
            }
        })
            .catch((error) => {
            next();
        });
    },
    (session, results, next) => {
        const blacklist = ['User', 'You', 'Pal', 'Friend'];
        session.dialogData.name = botUtils_1.getName(session);
        if (session.userData.phone) {
            session.userData.version = 3.0;
            session.endDialog();
        }
        else {
            session.userData.version = 3.0;
            const name = session.userData.name;
            if (!name || blacklist.indexOf(name) !== -1) {
                // asking for name
                session.send(common_tags_1.oneLine `Hi, I\'m Biya. I am a bot that can help you buy airtime,
            transfer funds and pay bills securely anytime you want !`);
                session.beginDialog('nameDialog');
            }
            else {
                session.send(common_tags_1.oneLine `Hi ${name}, I\'m Biya. I am a bot that can help you buy airtime,
            transfer funds and pay bills securely anytime you want !`);
                // request phone number, if you have used biya before send text, if not create new user
                session.sendTyping();
                session.beginDialog('phoneDialog');
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'firstrun', 2);
        // check user phone, if available, save find in database, if in database, confirm user else do not confirm
        const name = session.userData.name;
        const phone = session.userData.phone;
        if (!name) {
            session.beginDialog('nameDialog');
        }
        if (!phone) {
            session.beginDialog('phoneDialog');
        }
        next();
    },
    (session, results, next) => {
        // should have both by now
        mixpanel_1.default(session, 'firstrun', 3);
        const name = session.userData.name;
        const phone = session.userData.phone;
        user_1.User.findOne({ phone: phone }).then((user) => {
            if (!user) {
                // get network and create user
                const channel = session.message.address.channelId;
                const newUser = {
                    addresses: {
                        [channel]: session.message.address,
                    },
                    email: dbUtils_1.deriveEmail(session),
                    name: session.userData.name,
                    network: null,
                    phone: phone,
                    primaryChannel: session.message.address.channelId,
                    userBotId: session.message.user.id,
                };
                finder_1.getNetwork(phone)
                    .then((result) => {
                    session.userData.network = newUser.network = result;
                    return dbUtils_1.saveNewUser(newUser);
                })
                    .then((savedUser) => {
                    session.userData.email = savedUser.email;
                    const channelId = session.message.address.channelId;
                    const updateObject = {
                        addresses: {
                            [channelId]: session.message.address,
                        },
                        primaryChannel: channelId,
                    };
                    user_1.User.findByIdAndUpdate(savedUser._id, {
                        $set: updateObject,
                    })
                        .then((res) => {
                        session.replaceDialog('welcomeUser');
                    })
                        .catch((error) => {
                        logger_1.default.error(error);
                    });
                })
                    .catch((err) => {
                    logger_1.default.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(`${message}, please try again`);
                });
            }
            if (user) {
                // send confirmation text and send to confirmation dialog
                let previousChannel = user.primaryChannel;
                if (previousChannel === 'directline') {
                    previousChannel = 'WebChat';
                }
                const prevBotId = user.userBotId;
                const currentBotId = session.message.user.id;
                if (prevBotId === currentBotId) {
                    session.userData.email = user.email;
                    session.userData.network = user.network;
                    session.replaceDialog('welcomeUser');
                }
                else {
                    session.userData.phone = null;
                    session.send(`Oh, it seems like you already used Biya on ${_.capitalize(previousChannel)}.`);
                    session.replaceDialog('smsConfirmation', {
                        tempUser: user,
                    });
                }
            }
        });
    },
]).triggerAction({
    onFindAction: (context, callback) => {
        // Trigger dialog if the users version field is less than 3.0
        // - When triggered we return a score of 1.1 to ensure the dialog is always triggered.
        const ver = context.userData.version || 0;
        const score = ver < 3.0 ? 1.1 : 0.0;
        callback(null, score);
    },
    onInterrupted: (session, dialogId, dialogArgs, next) => {
        // Prevent dialog from being interrupted.
        session.send('Sorry...Please reply with your phone number');
    },
});
App_1.bot.dialog('nameDialog', [
    (session) => {
        mixpanel_1.default(session, 'nameDialog', 1);
        session.sendTyping();
        builder.Prompts.text(session, 'What is your name ? :-)');
    },
    (session, results, next) => {
        if (results.response) {
            mixpanel_1.default(session, 'nameDialog', 2);
            session.userData.name = _.capitalize(results.response.trim());
            session.endDialog(`Great! Pleased to meet you ${session.userData.name}`);
        }
    },
]);
App_1.bot.dialog('welcomeUser', [
    (session) => {
        mixpanel_1.default(session, 'welcomeUser', 1);
        SlackService_1.SlackNotify({
            username: 'New Lead Bot',
            channel: '#leads',
            text: common_tags_1.stripIndents `Yaaay! We have a new user`,
            fields: {
                Name: session.userData.name,
                Number: session.userData.phone,
            },
            emoji: ':boom:',
        });
        session.userData.active = true;
        session.send(`☑ We're good to go!`);
        session.sendTyping();
        session.endConversation(common_tags_1.stripIndents `Send 'Menu' to begin or send something like
      * Recharge my line or
      * load 500 on 08132614422 or
      * Pay DSTV
      * Send 'help' to show this message again
      * Send 'bye' to end at any time`);
    },
]).triggerAction({ matches: /^welcome$/i });
App_1.bot.dialog('phoneDialog', [
    (session) => {
        mixpanel_1.default(session, 'phoneDialog', 1);
        session.sendTyping();
        askPhoneNumber.beginDialog(session, {});
    },
    (session, results, next) => {
        if (results.response) {
            mixpanel_1.default(session, 'phoneDialog', 2);
            session.userData.phone = results.response;
            session.endDialog();
        }
    },
]);
App_1.bot.dialog('smsConfirmation', [
    (session, args) => {
        mixpanel_1.default(session, 'smsConfirmation', 1);
        const secret = (session.dialogData.secret = speakeasy.generateSecret({
            length: 20,
        }));
        const token = speakeasy.totp({
            digits: 6,
            encoding: 'base32',
            secret: secret.base32,
            step: 200,
        });
        session.sendTyping();
        session.dialogData.token = token;
        session.dialogData.tempUser = args.tempUser;
        const phone = args.tempUser.phone;
        session.send('Please wait for a few seconds...');
        logger_1.default.info(`token is ${token}`);
        SlackService_1.SlackNotify({
            username: 'OTP Alert',
            channel: '#tokens',
            text: common_tags_1.stripIndents `OTP sent to user`,
            fields: {
                Token: token,
                Number: phone,
            },
            emoji: ':boom:',
        });
        sendText(phone, token)
            .then((result) => {
            session.sendTyping();
            if (!session.userData.smsRetryCount) {
                session.userData.smsRetryCount = 0;
            }
            if (result && result.data && result.data.status === 'Sent') {
                // verify sms status
                session.sendTyping();
                session.dialogData.TmpmgsId = result.data.message_id;
                return delay(10000);
            }
            else {
                session.userData.smsRetryCount =
                    session.userData.smsRetryCount + 1;
                if (session.userData.smsRetryCount < 3) {
                    session.replaceDialog('smsConfirmation', {
                        tempUser: session.dialogData.tempUser,
                    });
                }
                else {
                    session.userData = {};
                    session.endConversation('Sorry, we could not verify your account');
                }
            }
        })
            .then((res) => {
            return getDeliveryStatus(session.dialogData.TmpmgsId);
        })
            .then((r) => {
            session.send('Verifying phone number..');
            session.sendTyping();
            if (r && r.data && r.data.status !== 'Rejected') {
                // verify sms status
                builder.Prompts.number(session, 'To confirm that you are the same person, please enter the 6-digit OTP code that was sent to your phone');
            }
            else {
                session.userData.smsRetryCount =
                    session.userData.smsRetryCount + 1;
                if (session.userData.smsRetryCount < 3) {
                    session.replaceDialog('smsConfirmation', {
                        tempUser: session.dialogData.tempUser,
                    });
                }
                else {
                    session.userData = {};
                    session.endConversation('Sorry, we could not verify your account.');
                }
            }
        })
            .catch((err) => {
            logger_1.default.error(err);
            const message = err.message
                ? err.message
                : 'There was an error';
            session.endDialog(`${message}, please try again`);
        });
    },
    (session, results, next) => {
        if (results.response) {
            // check if code is correct
            // then load user data if correct
            mixpanel_1.default(session, 'smsConfirmation', 2);
            session.sendTyping();
            const x = parseInt(results.response, 10);
            const secret = parseInt(session.dialogData.token, 10);
            const tokenValidates = (a, b) => a === b;
            if (tokenValidates(x, secret)) {
                const user = session.dialogData.tempUser;
                session.userData.phone = user.phone;
                session.userData.network = user.network;
                session.userData.email = user.email;
                const channelId = session.message.address.channelId;
                const updateObject = {
                    addresses: {
                        [channelId]: session.message.address,
                    },
                };
                user_1.User.findOneAndUpdate({ phone: session.userData.phone }, {
                    $set: {
                        ['addresses.' + channelId]: session.message.address,
                    },
                })
                    .then((res) => {
                    // session.endDialog('User saved')
                    let channel = session.message.address.channelId;
                    if (channel === 'directline') {
                        channel = 'WebChat';
                    }
                    session.send(`Awesome! Your account has been set up here on ${_.capitalize(channel)} too -)`);
                    session.replaceDialog('welcomeUser');
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                });
            }
            else {
                session.replaceDialog('smsConfirmation', {
                    tempUser: session.dialogData.tempUser,
                });
            }
        }
    },
]);
const sendText = (phoneNumber, code) => {
    const postData = {
        from: 'Biya',
        message: `Biya SMS OTP: ${code}`,
        to: phoneNumber,
    };
    return axios_1.default({
        auth: {
            password: '13bcf950a6366526964fda4e35da1717',
            username: '424df0de3e1a1167fa4c09c60e5d1f24',
        },
        baseURL: 'https://jusibe.com/smsapi/',
        method: 'post',
        params: postData,
        url: `send_sms`,
    });
};
const delay = (t) => {
    return new Promise((resolve) => {
        setTimeout(resolve, t);
    });
};
const getDeliveryStatus = (messageId) => {
    const postData = {
        message_id: messageId,
    };
    return axios_1.default({
        auth: {
            password: '13bcf950a6366526964fda4e35da1717',
            username: '424df0de3e1a1167fa4c09c60e5d1f24',
        },
        baseURL: 'https://jusibe.com/smsapi/',
        method: 'post',
        params: postData,
        url: `delivery_status`,
    });
};
App_1.bot.dialog('/migrateDb', [
    (session) => {
        user_1.User.find({})
            .then((users) => {
            users.forEach((item) => {
                const channelId = item.address.channelId;
                const updateObject = {
                    addresses: {
                        [channelId]: item.address,
                    },
                    primaryChannel: channelId,
                };
                user_1.User.findByIdAndUpdate(item._id, { $set: updateObject })
                    .then((res) => {
                    logger_1.default.info(res);
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                });
            });
            session.endDialog('done');
        })
            .catch((error) => {
            session.endDialog('error');
        });
    },
]).triggerAction({ matches: /^kizzledhgdghgdhdhle$/i });
App_1.bot.dialog('/updateCurrentUsers', [
    (session) => {
        user_1.User.findOne({ userBotId: session.message.user.id })
            .then((res) => {
            if (res.phone && res.network && res.email) {
                session.userData.version = 3.0;
                session.userData.phone = res.phone;
                session.userData.network = res.network;
                session.userData.email = res.email;
            }
            session.endConversation();
        })
            .catch((err) => {
            session.endConversation();
        });
    },
]);
