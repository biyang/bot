"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/broadcast', [
    (session, args) => {
        mixpanel_1.default(session, 'broadcast', 1);
        const message = args.message;
        session.endConversation(message);
    },
]);
App_1.bot.dialog('/broadcasterrr', [
    (session, args) => {
        mixpanel_1.default(session, 'broadcast', 1);
        const message = args.message;
        session.endConversation(message);
    },
]);
