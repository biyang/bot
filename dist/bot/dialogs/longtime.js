"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const common_tags_1 = require("common-tags");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('weMissYou', [
    (session, args) => {
        // use array of choices
        mixpanel_1.default(session, 'longtime', 1);
        const name = session.userData.name ? session.userData.name : 'Pal';
        const choices = ['Recharge', 'exit'];
        const textMessage = common_tags_1.oneLine `Hi ${name}, we've missed you.
			As a reminder, you can still recharge your lines easily via Biya. Wanna try now?`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Recharge' }],
                                [{ text: 'exit' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        }
        else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'longtime', 2);
        const choice = results.response.entity ? results.response.entity : null;
        const name = session.userData.name ? session.userData.name : 'Pal';
        const options = {
            Recharge: '/recharge',
            default: 'defaultEnd',
        };
        session.replaceDialog(options[choice] || options['default']);
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});
