"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
const merchant_1 = require("../../../db/schemas/merchant");
const transaction_1 = require("../../../db/schemas/transaction");
const airvend_1 = require("../../../utils/airvend");
const dbUtils_1 = require("../../../utils/dbUtils");
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
const payImageURL = 'https://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const imageURL = 'https://res.cloudinary.com/pitech/image/upload/v1496360269/thumbs/tick.jpg';
const s = `https://www.facebook.com/dialog/feed?app_id=184683071273&link=http%3A%2F%2F\
  m.me%2Fbiyabot&picture=http%3A%2F%2Fres.cloudinary.com%2Fpitech%2Fimage%2Fupload%2\
  Fv1489243312%2Fthumbs%2Ficon.jpg&name=Biya%20-%20Easy%20payments%20via%20chat!&caption=%\
  20&description=Biya%20helps%20you%20buy%20airtime%20and%20pay%20bills%20from%20your%20existing\
  %20messaging%20apps%20with%20no%20extra%20apps%2Fdownloads%20required.%20&redirect_uri\
  =http%3A%2F%2Fwww.facebook.com%2F`;
App_1.bot.dialog('/tap2pay', [
    (session, args, next) => {
        let transaction;
        if (args && args.transaction) {
            transaction = session.dialogData.transaction = args.transaction;
        }
        else {
            session.endConversation();
        }
        // find merchant and send confirm prompt
        const name = session.userData.name ? session.userData.name : 'Friend';
        merchant_1.Merchant.findById(transaction.merchant)
            .then((merchant) => {
            if (merchant) {
                session.send(`Hi ${name}, you have recieved a new invoice!`);
                const msg = new builder.Message(session)
                    .textFormat(builder.TextFormat.xml)
                    .attachments([
                    new builder.HeroCard(session)
                        .title('Biya')
                        .subtitle('Tap2Pay')
                        .images([
                        builder.CardImage.create(session, 'https://res.cloudinary.com/pitech/image/upload/v14997' +
                            '17942/thumbs/W_M_MobileWallet_RGB_920x690_weiss.jpg'),
                    ]),
                ]);
                session.send(msg);
                builder.Prompts.choice(session, `Please confirm that you want to pay ${merchant.name} the sum` +
                    `of ${transaction.amount} naira only for ${transaction.description}`, 'Pay|Cancel', { listStyle: builder.ListStyle.button });
            }
            else {
                session.endConversation('Merchant does not exist');
            }
        })
            .catch((err) => {
            const message = err.message
                ? err.message
                : 'Error finding merchant';
            session.endDialog(message);
        });
    },
    (session, results, next) => {
        if (results.response) {
            const response = results.response.entity;
            switch (response) {
                case 'Pay':
                    next();
                    break;
                default:
                    session.endDialog('Ok. Cancelled.');
            }
        }
    },
    (session, results, next) => {
        const amount = session.dialogData.amount;
        // check if we have users authorization codde, if so ask confirmation else initiate fresh payment
        dbUtils_1.cardCheck(session.userData.phone)
            .then((user) => {
            if (user && user.billsAuth) {
                session.dialogData.paymentAuth =
                    user.billsAuth.authorization_code;
                next();
            }
            else {
                // save transaction details
                const transaction = session.dialogData.transaction;
                const data = {
                    amount: transaction.amount,
                    email: session.userData.email,
                    reference: transaction.reference,
                };
                paystack_1.initializePaystack(token)(data, transaction)
                    .then((result) => {
                    const url = result;
                    const msg = new builder.Message(session).attachments([
                        new builder.HeroCard(session)
                            .text(`Click 'Pay Now'`)
                            .images([
                            builder.CardImage.create(session, payImageURL),
                        ])
                            .buttons([
                            builder.CardAction.openUrl(session, url, 'Pay Now'),
                        ]),
                    ]);
                    session.endDialog(msg);
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                    session.endDialog('Error');
                });
            }
        })
            .catch((error) => {
            logger_1.default.error(error);
            session.endDialog('Error');
        });
    },
    (session, results) => {
        session.send('Working my magic... ⏳');
        session.sendTyping();
        const transaction = session.dialogData.transaction;
        // charge user and dispense airtime
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: transaction.reference,
        };
        paystack_1.chargeWithPaystack(token)(chargeData, transaction)
            .then((res) => {
            if (res.data.status === 'success') {
                // send reciept
                let msg;
                session.send('Payment successful');
                const trans = session.dialogData.transaction;
                msg = new builder.Message(session).attachments([
                    new builder.HeroCard(session)
                        .text(`Amount: ${trans.amount} // Product: ${trans.product}`)
                        .images([
                        builder.CardImage.create(session, imageURL),
                    ])
                        .buttons([
                        builder.CardAction.openUrl(session, s, 'Share'),
                    ]),
                ]);
                session.endConversation(msg);
            }
            else {
                const message = res.message
                    ? res.message
                    : 'There was an error';
                throw Error(message);
            }
        })
            .then((trans) => {
            return transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
        })
            .then((trans) => {
            logger_1.default.info(trans);
            // return Transaction.findOneAndUpdate({reference: transaction.reference}, {status: "success"})
        })
            .catch((error) => {
            airvend_1.handleDispenseError(error)(session);
        });
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});
