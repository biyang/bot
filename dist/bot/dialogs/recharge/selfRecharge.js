"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const shortid = require("simple-id");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
const mixpanel_1 = require("../mixpanel");
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
const dbUtils_1 = require("../../../utils/dbUtils");
const paystack_1 = require("../../../utils/paystack");
const airvend_1 = require("../../../utils/airvend");
const validateAmount_1 = require("./validateAmount");
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const token = process.env.PAYSTACK_TOKEN;
App_1.bot.dialog('selfRecharge.controller', [
    (session, args, next) => {
        // first step
        let amount;
        mixpanel_1.default(session, 'selfRecharge', 1);
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (builder.EntityRecognizer.findEntity(args.entities, 'builtin.number')) {
                amount =
                    builder.EntityRecognizer.findEntity(args.entities, 'builtin.number').entity || null;
            }
        }
        if (amount && validateAmount_1.default(amount) === true) {
            session.dialogData.amount = amount;
            next();
        }
        else {
            if (validateAmount_1.default(amount) === 'tooLow' ||
                validateAmount_1.default(amount) === 'tooHigh') {
                session.beginDialog('airtime.askAmount', {
                    numberOfRetries: 1,
                });
            }
            else {
                session.beginDialog('airtime.askAmount');
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'selfRecharge', 2);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        if (amount) {
            next();
        }
        else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        // check phone number
        mixpanel_1.default(session, 'selfRecharge', 3);
        if (session.userData.phone) {
            // proceed to confirmation
            next();
        }
        else {
            // sencondRun for email and number
            session.beginDialog('phoneDialog');
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'selfRecharge', 4);
        const phone = session.userData.phone;
        if (phone) {
            next();
        }
    },
    (session, results, next) => {
        // check params
        session.sendTyping();
        mixpanel_1.default(session, 'selfRecharge', 5);
        const amount = session.dialogData.amount;
        user_1.User.findOne({ phone: session.userData.phone })
            .then((user) => {
            // check user for name and auth
            if (user && user.authorization) {
                session.dialogData.network = user.network;
                session.dialogData.paymentAuth =
                    user.authorization.authorization_code;
                session.dialogData.authorization = user.authorization;
                session.userData.email = user.email;
                next();
            }
            else {
                // break to firstPayment
                const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                session.dialogData.network = user.network;
                const transaction = {
                    amount: amount,
                    address: session.message.address,
                    description: user.network,
                    identity: {
                        phone: session.userData.phone,
                        sourceChannel: session.message.address.channelId,
                    },
                    merchant: 'airvend',
                    product: 'airtime',
                    reference: reference,
                    status: 'init',
                    tags: ['airtime', 'self'],
                    user: session.userData.phone,
                };
                session.replaceDialog('firstPay', { transaction });
            }
        })
            .catch((error) => logger_1.default.error(error));
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'selfRecharge', 6);
        const phone = session.userData.phone;
        const network = session.dialogData.network;
        const amount = session.dialogData.amount;
        const { bank, brand, last4, channel } = session.dialogData.authorization;
        const msg = `Just to confirm, you want to recharge your line ${phone} with ₦${amount}. ${channel === 'card' && brand && bank && last4
            ? `We'll automatically debit your ${bank} ${brand} card ending with ${last4}.`
            : ''}`;
        // ask for confirmation
        builder.Prompts.choice(session, msg, 'Yes|Change amount|Abort', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        // ceck
        mixpanel_1.default(session, 'selfRecharge', 7);
        session.sendTyping();
        const auth = session.dialogData.paymentAuth;
        const phone = session.userData.phone;
        const network = session.dialogData.network;
        const amount = session.dialogData.amount;
        const type = 'selfR';
        if (results && results.response) {
            const option = results.response.entity;
            if (option === 'Yes') {
                const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                const transaction = {
                    amount: amount,
                    address: session.message.address,
                    description: network,
                    identity: {
                        phone: session.userData.phone,
                        sourceChannel: session.message.address.channelId,
                    },
                    merchant: 'airvend',
                    product: 'airtime',
                    reference: reference,
                    status: 'init',
                    tags: ['airtime', 'self'],
                    user: session.userData.phone,
                };
                session.replaceDialog('airtime.Go', {
                    transaction,
                    auth,
                    type,
                });
            }
            else if (option === 'Change amount') {
                session.beginDialog('airtime.askAmount');
            }
            else {
                session.replaceDialog('abort');
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'selfRecharge', 8);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const network = session.dialogData.network;
        const auth = session.dialogData.paymentAuth;
        const type = 'selfR';
        const transaction = {
            amount: amount,
            address: session.message.address,
            description: network,
            identity: {
                phone: session.userData.phone,
                sourceChannel: session.message.address.channelId,
            },
            merchant: 'airvend',
            product: 'airtime',
            reference: reference,
            status: 'init',
            tags: ['airtime', 'self'],
            user: session.userData.phone,
        };
        session.replaceDialog('airtime.Go', { transaction, auth, type });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^(My line)/i,
});
App_1.bot.dialog('airtime.askAmount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'How much airtime do you want to buy? (Reply with an amount between 100 and 3000)',
            1: 'Please enter an amount between NGN100 and NGN3000 :-)',
            2: 'Please enter a valid amount to proceed :-)',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.text(session, message);
        }
        else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('airtime.help', {
                fromDialog: 'selfRecharge.controller',
            });
        }
    },
    (session, results, next) => {
        const cleanAmount = results.response &&
            String(results.response).replace(/[^\d\.\-]/g, '');
        const amount = cleanAmount && parseInt(cleanAmount, 10);
        if (amount && validateAmount_1.default(amount) === true) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response });
        }
        else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('airtime.askAmount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);
App_1.bot.dialog('airtime.Go', [
    (session, args, next) => {
        // check params
        if (args) {
            const transaction = args.transaction;
            const auth = args.auth;
            const type = args.type;
            session.send('Working my magic... ⏳');
            session.sendTyping();
            const networks = {
                AIRTEL: 1,
                ETISALAT: 4,
                GLO: 3,
                MTN: 2,
            };
            const chargeData = {
                amount: transaction.amount,
                email: session.userData.email,
                paymentAuth: auth,
                reference: transaction.reference,
            };
            // charge user
            let transactionResult;
            dbUtils_1.createAirTransaction(transaction)
                .then((result) => {
                transactionResult = result;
                return paystack_1.chargeWithPaystack(token)(chargeData, transactionResult);
            })
                .then((res) => {
                if (res.data.status === 'success') {
                    // const payload = {
                    //   amount: transaction.amount,
                    //   network: networks[transactionResult.description],
                    //   phone: transaction.identity.phone,
                    //   reference: transaction.reference
                    // };
                    // dispenseAirtime(transactionResult);
                }
                else {
                    transaction_1.Transaction.findByIdAndUpdate(transactionResult._id, {
                        status: 'failed',
                    })
                        .then((r) => {
                        logger_1.default.info(r);
                    })
                        .catch((e) => {
                        logger_1.default.error(e);
                    });
                    const message = res.message
                        ? res.message
                        : 'There was an error';
                    throw Error(message);
                }
            })
                .catch((error) => {
                airvend_1.handleDispenseError(error)(session);
            });
        }
        else {
            session.replaceDialog('/recharge');
        }
    },
]);
App_1.bot.dialog('firstPay', [
    (session, args, next) => {
        // check params
        session.sendTyping();
        const transaction = args.transaction;
        dbUtils_1.createAirTransaction(transaction)
            .then((trans) => {
            return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: transaction.reference });
        })
            .then((u) => {
            session.userData.email = u.email;
            const data = {
                amount: transaction.amount,
                email: u.email,
                reference: transaction.reference,
            };
            return paystack_1.initializePaystack(token)(data, transaction);
        })
            .then((result) => {
            const url = result;
            const msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(`Click 'Pay Now'`)
                    .images([
                    builder.CardImage.create(session, payImageURL),
                ])
                    .buttons([
                    builder.CardAction.openUrl(session, url, 'Pay Now'),
                ]),
            ]);
            session.endDialog(msg);
        })
            .catch((error) => {
            logger_1.default.error(error);
            session.endDialog('Error');
        });
    },
]);
App_1.bot.dialog('airtime.help', [
    (session, args, next) => {
        session.sendTyping();
        session.dialogData.fromDialog = args.fromDialog;
        const choices = ['Start over', 'Continue'];
        builder.Prompts.choice(session, 'Seems you are stuck, should I clear this conversation and start from the beginning ?', choices, { listStyle: builder.ListStyle.button });
    },
    (session, results) => {
        const response = results.response ? results.response.entity : null;
        const choices = {
            'Start over': session.dialogData.fromDialog,
            Continue: 'Okay, going back to our convo ;-)',
        };
        choices[response]
            ? response === 'Start over'
                ? session.replaceDialog(choices[response])
                : session.endDialog(choices[response])
            : session.endDialog();
    },
]);
