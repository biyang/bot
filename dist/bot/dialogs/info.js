"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const common_tags_1 = require("common-tags");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/info', [
    (session) => {
        const card = new builder.HeroCard(session)
            .title('')
            .text(common_tags_1.oneLine `I am Biya. I am a robot that can help you buy airtime,\
        transfer funds and pay bills 24 hours a day, 7 days a week !`)
            .images([
            builder.CardImage.create(session, 'http://res.cloudinary.com/pitech/image/upload/v1489243312/thumbs/icon.jpg'),
        ])
            .buttons([
            builder.CardAction.openUrl(session, 'https://biya.com.ng', 'Learn more'),
        ]);
        mixpanel_1.default(session, 'info', 1);
        const msg = new builder.Message(session).attachments([card]);
        session.endDialog(msg);
    },
]);
