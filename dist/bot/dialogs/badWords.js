"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/badWords', [
    (session, args) => {
        // use array of choices
        mixpanel_1.default(session, 'badWords', 1);
        const choices = ['Chat with an agent', 'Go to menu', 'Exit'];
        const textMessage = `😭 how can I make things right?`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Chat with an agent' }],
                                [{ text: 'Go to menu' }],
                                [{ text: 'Exit' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        }
        else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'badWords', 2);
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            Recharge: '/recharge',
            Tickets: '/buyTicketIndex',
            'Pay Bill': '/PayBill',
            Transfer: 'transfer.controller',
            'Chat with an agent': '/CustomerSupport',
            'Go to menu': '/menu',
            default: 'defaultEnd',
            Exit: 'defaultEnd',
        };
        if (options[choice]) {
            // postback
            session.replaceDialog(options[choice] || options['default']);
        }
        else {
            session.replaceDialog(options['default']);
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^(Customer Support)/i,
});
