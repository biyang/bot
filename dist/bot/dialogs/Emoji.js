"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/Emoji', [
    (session, args, next) => {
        mixpanel_1.default(session, 'Emoji', 1);
        session.endConversation(['👍', '😁', '✋']);
    },
]);
App_1.bot.dialog('/lolz', [
    (session, args, next) => {
        mixpanel_1.default(session, 'lolz', 1);
        session.endConversation(['😁', '😁']);
    },
]).triggerAction({ matches: /^ode|^lol|^lmao|^ugh|^aha|^lols|^lolz|^lmfao/i });
App_1.bot.dialog('/love', [
    (session, args, next) => {
        mixpanel_1.default(session, 'love', 1);
        session.endConversation(['💖', 'Love you too 😍']);
    },
]).triggerAction({ matches: /^(love|luv|i love you)/i });
