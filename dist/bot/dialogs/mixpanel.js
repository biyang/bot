"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Mixpanel = require("mixpanel");
const Amplitude = require("amplitude");
const mixpanel = Mixpanel.init(process.env.MIXPANEL, {
    protocol: 'https'
});
const amplitude = new Amplitude('cf8429a929fe458926a7c82f8d999973');
const ampTracker = (session, dialog, step) => {
    const data = {
        event_type: dialog,
        user_id: session.userData.phone,
        event_properties: {
            channel: session.message.address.channelId,
            dialog: dialog,
            step: dialog + step
        },
        user_properties: {
            phone: session.userData.phone,
            name: session.userData.name,
            botId: session.message.user.id
        }
    };
    amplitude.track(data);
};
exports.trackUser = session => {
    if (process.env.NODE_ENV === 'production') {
        mixpanel.people.set(session.userData.phone, {
            $first_name: session.userData.name,
            $phone: '+234' + session.userData.phone.slice(1),
            $distinct_id: session.userData.phone
        });
    }
};
const tracker = (session, dialog, step) => {
    if (process.env.NODE_ENV === 'production') {
        mixpanel.track(dialog, {
            distinct_id: session.userData.phone,
            userId: session.message.user.id,
            channel: session.message.address.channelId,
            phone: session.userData.phone,
            name: session.userData.name,
            dialog: dialog,
            step: dialog + step
        });
        ampTracker(session, dialog, step);
    }
};
exports.default = tracker;
