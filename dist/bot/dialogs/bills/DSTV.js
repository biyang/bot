"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const Fuse = require("fuse.js");
const _ = require("lodash");
const moment = require("moment");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
const mixpanel_1 = require("../mixpanel");
// import Customer model
const beneficiary_1 = require("../../../db/schemas/beneficiary");
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
// import prompt
const askDstvSmartNo = require("../../prompts/askDstvSmartNo");
// import database utils
const dbUtils_1 = require("../../../utils/dbUtils");
// import airvend dispense
const airvend_1 = require("../../../utils/airvend");
const vtpass_1 = require("../../../utils/vtpass");
const shortid = require("simple-id");
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const variations = [
    {
        variation_code: 'dstv-padi',
        name: 'DStv Padi N1,850',
        variation_amount: '1850',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv-yanga',
        name: 'DStv Yanga N2,565',
        variation_amount: '2565',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv-confam',
        name: 'Dstv Confam N4,615',
        variation_amount: '4615',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv79',
        name: 'DStv Compact N7900',
        variation_amount: '7900',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv3',
        name: 'DStv Premium N18,400',
        variation_amount: '18400',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv6',
        name: 'DStv Asia N6,200',
        variation_amount: '6200',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv7',
        name: 'DStv Compact Plus N12,400',
        variation_amount: '12400',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'confam-extra',
        name: 'DStv Confam + ExtraView N7,115',
        variation_amount: '7115',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'yanga-extra',
        name: 'DStv Yanga + ExtraView N5,065',
        variation_amount: '5065',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'padi-extra',
        name: 'DStv Padi + ExtraView N4,350',
        variation_amount: '4350',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv30',
        name: 'DStv Compact + Extra View N10,400',
        variation_amount: '10400',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv33',
        name: 'DStv Premium - Extra View N20,900',
        variation_amount: '20900',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'dstv45',
        name: 'DStv Compact Plus - Extra View N14,900',
        variation_amount: '14900',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'hdpvr-access-service',
        name: 'DStv HDPVR Access Service N2,500',
        variation_amount: '2500',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'frenchplus-addon',
        name: 'DStv French Plus Add-on N8,100',
        variation_amount: '8100',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'asia-addon',
        name: 'DStv Asian Add-on N6,200',
        variation_amount: '6200',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'frenchtouch-addon',
        name: 'DStv French Touch Add-on N2,300',
        variation_amount: '2300',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'extraview-access',
        name: 'ExtraView Access N2,500',
        variation_amount: '2500',
        fixedPrice: 'Yes',
    },
];
const itemsx = [
    {
        code: 'COMPE36',
        price: 7900,
        name: 'DStv Compact',
        descrition: ' ',
    },
    {
        code: 'HDPVRE36',
        price: 10400,
        name: 'DStv Compact + HDPVR/XtraView',
        descrition: ' ',
    },
    {
        code: 'COMPLE36',
        price: 12400,
        name: 'DStv Compact Plus',
        descrition: ' ',
    },
    {
        code: 'HDPVRE36',
        price: 14900,
        name: 'DStv Compact Plus + HDPVR/XtraView',
        descrition: ' ',
    },
    {
        code: 'PRWE36',
        price: 18400,
        name: 'DStv Premium',
        descrition: ' ',
    },
    {
        code: 'HDPVRE36',
        price: 20900,
        name: 'DStv Premium + HDPVR/XtraView',
        descrition: ' ',
    },
    {
        code: 'NNJ1E36',
        price: 2565,
        name: 'DStv Yanga Bouquet E36',
        descrition: ' ',
    },
    {
        code: 'NNJ2E36',
        price: 4615,
        name: 'DStv Confam Bouquet E36',
        descrition: ' ',
    },
    {
        code: 'NLTESE36',
        price: 1850,
        name: 'Padi',
        descrition: ' ',
    },
];
const dstvData = variations.reduce((acc, item) => {
    const obj = Object.assign({}, acc, { [item.name]: {
            code: item.variation_code,
            amount: Number(item.variation_amount),
            name: item.name,
        } });
    return obj;
}, {});
// const dstvData = {
//     'DStv Padi': {
//         code: 'NLTESE36',
//         amount: 1850,
//         name: 'DStv Padi',
//     },
//     'DStv Compact': {
//         code: 'COMPE36',
//         amount: 6975,
//         name: 'DStv Compact',
//     },
//     'DStv Compact Plus': {
//         code: 'COMPLE36',
//         amount: 10925,
//         name: 'DStv Compact Plus',
//     },
//     'DStv Premium': {
//         code: 'PRWE36',
//         amount: 16200,
//         name: 'DStv Premium',
//     },
//     'DStv Yanga Bouquet': {
//         code: 'NNJ1E36',
//         amount: 2565,
//         name: 'DStv Yanga Bouquet E36',
//     },
//     'DStv Confam Bouquet': {
//         code: 'NNJ2E36',
//         amount: 4615,
//         name: 'DStv Confam Bouquet E36',
//     },
//     // 'DStv Compact + HDPVR/XtraView': {
//     //     amount: 9230,
//     //     code: 'MINIBW4',
//     //     invoicePeriods: [1, 12],
//     // },
//     // 'DStv Premium + HDPVR/XtraView': {
//     //     amount: 18000,
//     //     code: 'PRWW4',
//     //     invoicePeriods: [1, 12],
//     // },
// };
const dstvMap = Object.keys(dstvData).map((item) => {
    return { key: item, name: `${item} - ₦${dstvData[item].amount}` };
});
const options = {
    distance: 100,
    keys: ['name', 'key'],
    location: 0,
    maxPatternLength: 32,
    minMatchCharLength: 1,
    shouldSort: true,
    threshold: 0.3,
};
const fuse = new Fuse(dstvMap, options);
App_1.bot.dialog('/dstv', [
    (session, args, next) => {
        mixpanel_1.default(session, 'dstv', 1);
        let plan;
        // check if amount is stored in dialog data
        if (session.dialogData.plan) {
            plan = session.dialogData.plan;
        }
        if (args) {
            if (args.type) {
                plan = session.dialogData.plan = args.type;
            }
        }
        if (!plan) {
            const name = session.userData.name
                ? session.userData.name
                : 'Friend';
            builder.Prompts.choice(session, `Please select the DSTV plan you would like to subscribe to, ${name}`, dstvData, { listStyle: builder.ListStyle.button });
        }
        else {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 2);
        if (results && results.response) {
            session.dialogData.selectedPlan = results.response.entity;
            next();
        }
        else {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 3);
        let plan = session.dialogData.plan;
        const selectedPlan = session.dialogData.selectedPlan;
        if (!plan) {
            plan = session.dialogData.plan = selectedPlan;
        }
        // we now have the plan so let's get the smartcard number
        // ask if touse one of saved smart card numbers
        user_1.User.findOne({ phone: session.userData.phone })
            .populate('beneficiaries')
            .then((user) => {
            if (!user) {
                const newUser = {
                    address: session.message.address,
                    email: dbUtils_1.deriveEmail(session),
                    name: session.message.user.name,
                    phone: dbUtils_1.mockPhone(session),
                    userBotId: session.message.user.id,
                };
                dbUtils_1.saveNewUser(newUser)
                    .then((savedUSer) => {
                    session.userData.email = user.email;
                    session.dialogData.useExisting = false;
                    next();
                })
                    .catch((err) => {
                    logger_1.default.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(`${message}, please try again`);
                });
            }
            else if (user && user.beneficiaries.length < 1) {
                session.userData.email = user.email;
                session.dialogData.useExisting = false;
                next();
            }
            else {
                const beneficiaries = user.beneficiaries;
                session.userData.email = user.email;
                const checkIfBeneficiaryExists = _.filter(beneficiaries, (o) => {
                    return o.identifier.type === 'dstv';
                });
                if (checkIfBeneficiaryExists.length > 0) {
                    session.dialogData.useExisting = true;
                    const listToSend = _.map(checkIfBeneficiaryExists, (o) => {
                        return o.name;
                    });
                    listToSend.push('New');
                    const name = session.userData.name
                        ? session.userData.name
                        : 'Friend';
                    builder.Prompts.choice(session, `Which DSTV account do you want to subscribe, ${name}`, listToSend, { listStyle: builder.ListStyle.button });
                }
                else {
                    next();
                }
            }
        });
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 4);
        if (session.dialogData.useExisting) {
            if (results.response) {
                if (results.response.entity !== 'New') {
                    session.dialogData.smartCard = results.response.entity;
                    next();
                }
                else {
                    session.dialogData.useExisting = false;
                    askDstvSmartNo.beginDialog(session, {});
                }
            }
        }
        else {
            askDstvSmartNo.beginDialog(session, {});
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 5);
        let smartCard = session.dialogData.smartCard;
        if (results && results.response) {
            smartCard = session.dialogData.smartCard = results.response;
            user_1.User.findOne({ phone: session.userData.phone })
                .then((user) => {
                const ben = new beneficiary_1.Beneficiary({
                    identifier: { type: 'dstv', smartCard: smartCard },
                    name: smartCard,
                    user: user._id,
                });
                return ben.save();
            })
                .then((ben) => {
                return user_1.User.findOneAndUpdate({ userBotId: session.userData.phone }, { $push: { beneficiaries: ben } });
            })
                .then((user) => {
                next();
            })
                .catch((err) => {
                next();
            });
        }
        else if (session.dialogData.useExisting) {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 6);
        let smartCard = session.dialogData.smartCard;
        if (session.dialogData.smartCard.entity) {
            smartCard = session.dialogData.smartCard =
                session.dialogData.smartCard.entity;
        }
        // verify smartcard number then prompt for confirmation
        const result = fuse.search(session.dialogData.plan);
        logger_1.default.info(result);
        const planDetails = dstvData[result[0].key];
        vtpass_1.verifyMerchant({ smartCard, type: 'dstv' })
            .then((res) => {
            if (res && res.data && res.data.content) {
                if (res.data.content.error) {
                    throw Error(res.data.content.error);
                }
                const { Customer_Name, Status, Due_Date } = res.data.content;
                let msg;
                if (Status === 'SUSPENDED' || Status === 'CLOSED') {
                    msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                        Please confirm that you want to pay for the following account:
                        * Account Name: ${Customer_Name || ''}
                        * Account Status: ${Status}`;
                }
                else {
                    msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                        Please confirm that you want to pay for the following account:
                        * Account Name: ${Customer_Name || ''}
                        * Due Date: ${moment(Due_Date).format('dddd, MMMM Do YYYY')}`;
                }
                // send prompt
                builder.Prompts.choice(session, msg, 'Yes|Change Package|Change Smart Card|Abort', { listStyle: builder.ListStyle.button });
            }
            else {
                throw Error('There was an error, please try again later');
            }
        })
            .catch((error) => {
            logger_1.default.error(error);
            if (error && error.message) {
                session.endDialog(error.message);
            }
            else {
                session.endDialog('We could not verify your DSTv smart card number');
            }
        });
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 7);
        const name = session.userData.name ? session.userData.name : 'Friend';
        if (results && results.response) {
            const option = results.response.entity;
            switch (option) {
                case 'Yes':
                    // dispense dstv
                    session.dialogData.goodToGo = true;
                    next();
                    break;
                case 'Change Package':
                    // prompt for amount
                    session.dialogData.changePackage = true;
                    builder.Prompts.choice(session, `Please select the DSTV plan you would like to subscribe to, ${name}`, dstvData, { listStyle: builder.ListStyle.button });
                    break;
                case 'Change Smart Card':
                    // prompt for amount
                    session.dialogData.changeSmartCard = true;
                    askDstvSmartNo.beginDialog(session, {});
                    break;
                default:
                    // doStuff
                    session.endDialog(`Okay, ${name}`);
                    break;
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 8);
        if (session.dialogData.changePackage) {
            if (results && results.response) {
                session.dialogData.plan = results.response.entity;
                session.dialogData.goodToGo = true;
                next();
            }
        }
        else if (session.dialogData.changeSmartCard) {
            let smartCard = session.dialogData.smartCard;
            if (results && results.response) {
                smartCard = session.dialogData.smartCard = results.response;
            }
            const result = fuse.search(session.dialogData.plan);
            const planDetails = dstvData[result[0].key];
            vtpass_1.verifyMerchant({ smartCard, type: 'dstv' })
                .then((res) => {
                if (res && res.data && res.data.content) {
                    if (res.data.content.error) {
                        throw Error(res.data.content.error);
                    }
                    const { Customer_Name, Status, Due_Date } = res.data.content;
                    let msg;
                    if (Status === 'SUSPENDED' || Status === 'CLOSED') {
                        msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                            Please confirm that you want to pay for the following account:
                            * Account Name: ${Customer_Name}
                            * Account Status: ${Status}`;
                    }
                    else {
                        msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                            Please confirm that you want to pay for the following account:
                            * Account Name: ${Customer_Name}
                            * Due Date: ${moment(Due_Date).format('dddd, MMMM Do YYYY')}`;
                    }
                    // send prompt
                    session.send(msg);
                    session.dialogData.goodToGo = true;
                    // send prompt
                    next();
                }
                else {
                    throw Error('There was an error, please try again later');
                }
            })
                .catch((error) => {
                if (error && error.message) {
                    session.endDialog(error.message);
                }
                else {
                    session.endDialog('We could not verify your DSTv smart card number');
                }
            });
        }
        else if (session.dialogData.goodToGo) {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 9);
        if (session.dialogData.goodToGo) {
            // check if we have users authorization codde, if so ask confirmation else initiate fresh payment
            const userdetails = session.dialogData.dstvData;
            const result = fuse.search(session.dialogData.plan);
            const planDetails = dstvData[result[0].key];
            dbUtils_1.cardCheck(session.userData.phone)
                .then((user) => {
                if (user && user.billsAuth) {
                    session.dialogData.paymentAuth =
                        user.billsAuth.authorization_code;
                    next();
                }
                else {
                    // save transaction details and
                    // send user to payment page
                    // create url then send to user
                    const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                    const transaction = {
                        amount: planDetails.amount + 100,
                        address: session.message.address,
                        description: session.dialogData.plan,
                        identity: {
                            variation_code: planDetails.code,
                            smartCard: session.dialogData.smartCard,
                            phone: session.userData.phone,
                        },
                        merchant: 'airvend',
                        product: 'dstv',
                        reference: reference,
                        status: 'init',
                        tags: ['dstv', 'self'],
                        user: session.userData.phone,
                    };
                    dbUtils_1.createTransaction(transaction)
                        .then((trans) => {
                        return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: reference });
                    })
                        .then((u) => {
                        const data = {
                            amount: transaction.amount,
                            email: session.userData.email,
                            reference: reference,
                        };
                        return paystack_1.initializePaystack(token)(data, transaction);
                    })
                        .then((initResult) => {
                        const url = initResult;
                        const msg = new builder.Message(session).attachments([
                            new builder.HeroCard(session)
                                .text(`Click 'Pay Now'`)
                                .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                                .buttons([
                                builder.CardAction.openUrl(session, url, 'Pay Now'),
                            ]),
                        ]);
                        session.endDialog(msg);
                    })
                        .catch((error) => {
                        logger_1.default.error(error);
                        session.endDialog('Error');
                    });
                }
            })
                .catch((error) => {
                logger_1.default.error(error);
                session.endDialog('Error');
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'dstv', 9);
        const userdetails = session.dialogData.dstvData;
        // "list" is the item array
        const plan = fuse.search(session.dialogData.plan);
        const planDetails = dstvData[plan[0].key];
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const transaction = {
            amount: planDetails.amount + 100,
            address: session.message.address,
            description: session.dialogData.plan,
            identity: {
                smartCard: session.dialogData.smartCard,
                variation_code: planDetails.code,
                phone: session.userData.phone,
            },
            merchant: 'airvend',
            product: 'dstv',
            reference: reference,
            status: 'init',
            tags: ['dstv', 'self'],
            user: session.userData.phone,
        };
        // charge user
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        // charge user
        session.send('Working my magic... ⏳');
        session.sendTyping();
        dbUtils_1.createTransaction(transaction)
            .then((result) => {
            return paystack_1.chargeWithPaystack(token)(chargeData, transaction);
        })
            .then((res) => {
            if (res.data.status === 'success') {
                // return dispenseDSTV(transaction);
            }
            else {
                transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'failed' })
                    .then((r) => logger_1.default.info(r))
                    .catch((e) => logger_1.default.error(e));
                const message = res.message
                    ? res.message
                    : 'There was an error';
                throw Error(message);
            }
        })
            .then((trans) => {
            session.endDialog();
        })
            .catch((error) => {
            airvend_1.handleDispenseError(error)(session);
        });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({ matches: /^dstv$/i });
