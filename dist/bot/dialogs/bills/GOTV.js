"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const Fuse = require("fuse.js");
const _ = require("lodash");
const moment = require("moment");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
// import models model
const beneficiary_1 = require("../../../db/schemas/beneficiary");
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
// import ask smart card number prompt
const askGotvSmartNo = require("../../prompts/askGotvSmartNo");
// import database utils
const dbUtils_1 = require("../../../utils/dbUtils");
// import airvend dispense
const airvend_1 = require("../../../utils/airvend");
const vtpass_1 = require("../../../utils/vtpass");
const shortid = require("simple-id");
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
// const gotvData = {
//     'GOTv Lite Monthly': {
//         amount: 420,
//         code: 'GOLITE',
//         invoicePeriods: [1],
//     },
//     'GOtv Jinja': {
//         code: 'GOTVNJ1',
//         amount: 1640,
//         name: 'GOtv Jinja Bouquet',
//     },
//     'GOtv Jolli': {
//         code: 'GOTVNJ2',
//         amount: 2460,
//         name: 'GOtv Jolli Bouquet',
//     },
//     'GOtv Max': {
//         code: 'GOtvMax',
//         amount: 3280,
//         name: 'GOtv Max',
//     },
// };
const itemsx = [
    {
        code: 'GOTVMAX',
        price: 3600,
        name: 'GOtv Max',
        descrition: ' ',
    },
    {
        code: 'GOTVNJ1',
        price: 1640,
        name: 'GOtv Jinja Bouquet',
        descrition: ' ',
    },
    {
        code: 'GOTVNJ2',
        price: 2460,
        name: 'GOtv Jolli Bouquet',
        descrition: ' ',
    },
    {
        code: 'GOLITE',
        price: 410,
        name: 'GOtv Lite',
        descrition: ' ',
    },
];
const variations = [
    {
        variation_code: 'gotv-max',
        name: 'GOtv Max N3,600',
        variation_amount: '3600',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'gotv-jolli',
        name: 'GOtv Jolli N2,460',
        variation_amount: '2460',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'gotv-jinja',
        name: 'GOtv Jinja N1,640',
        variation_amount: '1640',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'gotv-smallie',
        name: 'GOtv Smallie - monthly N800',
        variation_amount: '800',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'gotv-smallie-3months',
        name: 'GOtv Smallie - quarterly N2,100',
        variation_amount: '2100',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'gotv-smallie-1year',
        name: 'GOtv Smallie - yearly N6,200',
        variation_amount: '6200',
        fixedPrice: 'Yes',
    },
    {
        variation_code: 'gotv-smallie-1year',
        name: 'GOtv test',
        variation_amount: '100',
        fixedPrice: 'Yes',
    },
];
const gotvData = variations.reduce((acc, item) => {
    const obj = Object.assign({}, acc, { [item.name]: {
            code: item.variation_code,
            amount: Number(item.variation_amount),
            name: item.name,
        } });
    return obj;
}, {});
const gotvMap = Object.keys(gotvData).map((item) => {
    return { key: item, name: `${item} - ₦${gotvData[item].amount}` };
});
const mixpanel_1 = require("../mixpanel");
App_1.bot.dialog('/gotv', [
    (session, args, next) => {
        mixpanel_1.default(session, 'gotv', 1);
        const name = session.userData.name ? session.userData.name : 'Friend';
        let plan;
        // check if amount is stored in dialog data
        if (session.dialogData.plan) {
            plan = session.dialogData.plan;
        }
        if (args.type) {
            plan = session.dialogData.plan = args.type;
        }
        if (!plan) {
            builder.Prompts.choice(session, `Please select the GOTv plan you would like to subscribe to, ${name}`, gotvData, { listStyle: builder.ListStyle.button });
        }
        else {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 2);
        if (results && results.response) {
            session.dialogData.selectedPlan = results.response.entity;
            next();
        }
        else {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 3);
        let plan = session.dialogData.plan;
        const selectedPlan = session.dialogData.selectedPlan;
        // normalize plan
        if (!plan) {
            plan = session.dialogData.plan = selectedPlan;
        }
        // we now have the plan so let's get the smartcard number
        // ask if touse one of saved smart card numbers
        user_1.User.findOne({ phone: session.userData.phone })
            .populate('beneficiaries')
            .then((user) => {
            if (!user) {
                const newUser = {
                    address: session.message.address,
                    email: dbUtils_1.deriveEmail(session),
                    name: session.message.user.name,
                    phone: dbUtils_1.mockPhone(session),
                    userBotId: session.message.user.id,
                };
                dbUtils_1.saveNewUser(newUser)
                    .then((savedUser) => {
                    session.userData.email = savedUser.email;
                    session.dialogData.useExisting = false;
                    next();
                })
                    .catch((err) => {
                    logger_1.default.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(`${message}, please try again`);
                });
            }
            else if (user && user.beneficiaries.length < 1) {
                session.dialogData.useExisting = false;
                session.userData.email = user.email;
                next();
            }
            else {
                const beneficiaries = user.beneficiaries;
                session.userData.email = user.email;
                const checkIfBeneficiaryExists = _.filter(beneficiaries, (o) => {
                    return o.identifier.type === 'gotv';
                });
                if (checkIfBeneficiaryExists.length > 0) {
                    session.dialogData.useExisting = true;
                    const listToSend = _.map(checkIfBeneficiaryExists, (o) => {
                        return o.name;
                    });
                    listToSend.push('New');
                    const name = session.userData.name
                        ? session.userData.name
                        : 'Friend';
                    builder.Prompts.choice(session, `Which GOTV account do you want to subscribe, ${name}`, listToSend, { listStyle: builder.ListStyle.button });
                }
                else {
                    next();
                }
            }
        });
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 4);
        if (session.dialogData.useExisting) {
            if (results.response) {
                if (results.response.entity !== 'New') {
                    session.dialogData.smartCard = results.response.entity;
                    next();
                }
                else {
                    session.dialogData.useExisting = false;
                    askGotvSmartNo.beginDialog(session, {});
                }
            }
        }
        else {
            askGotvSmartNo.beginDialog(session, {});
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 5);
        let smartCard = session.dialogData.smartCard;
        if (results && results.response) {
            smartCard = session.dialogData.smartCard = results.response;
            user_1.User.findOne({ phone: session.userData.phone })
                .then((user) => {
                const ben = new beneficiary_1.Beneficiary({
                    identifier: { type: 'gotv', smartCard: smartCard },
                    name: smartCard,
                    user: user._id,
                });
                return ben.save();
            })
                .then((ben) => {
                return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { $push: { beneficiaries: ben._id } });
            })
                .then((user) => {
                next();
            })
                .catch((err) => {
                next();
            });
        }
        else if (session.dialogData.useExisting) {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 6);
        let smartCard = session.dialogData.smartCard;
        if (session.dialogData.smartCard.entity) {
            smartCard = session.dialogData.smartCard =
                session.dialogData.smartCard.entity;
        }
        // verify smartcard number then prompt for confirmation
        const options = {
            distance: 100,
            keys: ['name', 'key'],
            location: 0,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            shouldSort: true,
            threshold: 0.3,
        };
        const fuse = new Fuse(gotvMap, options); // "list" is the item array
        const result = fuse.search(session.dialogData.plan);
        const planDetails = gotvData[result[0].key];
        vtpass_1.verifyMerchant({ smartCard, type: 'gotv' })
            .then((res) => {
            if (res && res.data && res.data.content) {
                if (res.data.content.error) {
                    throw Error(res.data.content.error);
                }
                let msg;
                const { Customer_Name, Status, Due_Date } = res.data.content;
                if (Status === 'SUSPENDED' || Status === 'CLOSED') {
                    msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                            Please confirm that you want to pay for the following account:
                            * Account Name: ${Customer_Name || ''}
                            * Account Status: ${Status}`;
                }
                else {
                    msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                            Please confirm that you want to pay for the following account:
                            * Account Name: ${Customer_Name || ''}
                            * Due Date: ${moment(Due_Date).format('dddd, MMMM Do YYYY')}`;
                }
                // send prompt
                builder.Prompts.choice(session, msg, 'Yes|Change Package|Change Smart Card|Abort', { listStyle: builder.ListStyle.button });
            }
            else {
                throw Error('There was an error, please try again later');
            }
        })
            .catch((error) => {
            if (error && error.message) {
                session.endDialog(error.message);
            }
            else {
                session.endDialog('We could not verify your GOTv smart card number');
            }
        });
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 7);
        const name = session.userData.name ? session.userData.name : 'Friend';
        if (results && results.response) {
            const option = results.response.entity;
            switch (option) {
                case 'Yes':
                    // dispense gotv
                    session.dialogData.goodToGo = true;
                    next();
                    break;
                case 'Change Package':
                    // prompt for amount
                    session.dialogData.changePackage = true;
                    builder.Prompts.choice(session, `Please select the GOTv plan you would like to subscribe to, ${name}`, gotvData, { listStyle: builder.ListStyle.button });
                    break;
                case 'Change Smart Card':
                    // prompt for amount
                    session.dialogData.changeSmartCard = true;
                    askGotvSmartNo.beginDialog(session, {});
                    break;
                default:
                    // doStuff
                    session.endDialog(`Okay, ${name}`);
                    break;
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 8);
        if (session.dialogData.changePackage) {
            if (results && results.response) {
                session.dialogData.plan = results.response.entity;
                session.dialogData.goodToGo = true;
                next();
            }
        }
        else if (session.dialogData.changeSmartCard) {
            let smartCard = session.dialogData.smartCard;
            if (results && results.response) {
                smartCard = session.dialogData.smartCard = results.response;
            }
            // verify smartcard number then prompt for confirmation
            const options = {
                distance: 100,
                keys: ['name', 'key'],
                location: 0,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                shouldSort: true,
                threshold: 0.3,
            };
            const fuse = new Fuse(gotvMap, options); // "list" is the item array
            const result = fuse.search(session.dialogData.plan);
            const planDetails = gotvData[result[0].key];
            vtpass_1.verifyMerchant({ smartCard, type: 'gotv' })
                .then((res) => {
                if (res && res.data && res.data.content) {
                    if (res.data.content.error) {
                        throw Error(res.data.content.error);
                    }
                    const { Customer_Name, Status, Due_Date } = res.data.content;
                    let msg;
                    if (Status === 'SUSPENDED' || Status === 'CLOSED') {
                        msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                            Please confirm that you want to pay for the following account:
                            * Account Name: ${Customer_Name || ''}
                            * Account Status: ${Status}`;
                    }
                    else {
                        msg = `${result[0].key} costs ₦${planDetails.amount + 100}.
                            Please confirm that you want to pay for the following account:
                            * Account Name: ${Customer_Name || ''}
                            * Due Date: ${moment(Due_Date).format('dddd, MMMM Do YYYY')}`;
                    }
                    // send prompt
                    session.send(msg);
                    session.dialogData.goodToGo = true;
                    // send prompt
                    next();
                }
                else {
                    throw Error('There was an error, please try again later');
                }
            })
                .catch((error) => {
                if (error && error.message) {
                    session.endDialog(error.message);
                }
                else {
                    session.endDialog('We could not verify your GOTv smart card number');
                }
            });
        }
        else if (session.dialogData.goodToGo) {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 9);
        if (session.dialogData.goodToGo) {
            // check if we have users authorization codde, if so ask confirmation else initiate fresh payment
            const userdetails = session.dialogData.gotvData;
            const options = {
                distance: 100,
                keys: ['name', 'key'],
                location: 0,
                maxPatternLength: 32,
                minMatchCharLength: 1,
                shouldSort: true,
                threshold: 0.3,
            };
            const fuse = new Fuse(gotvMap, options); // "list" is the item array
            const result = fuse.search(session.dialogData.plan);
            const planDetails = gotvData[result[0].key];
            dbUtils_1.cardCheck(session.userData.phone)
                .then((user) => {
                if (user && user.billsAuth) {
                    session.dialogData.paymentAuth =
                        user.billsAuth.authorization_code;
                    next();
                }
                else {
                    // save transaction details and
                    // send user to payment page
                    // create url then send to user
                    const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                    const transaction = {
                        amount: planDetails.amount + 100,
                        address: session.message.address,
                        description: session.dialogData.plan,
                        identity: {
                            variation_code: planDetails.code,
                            smartCard: session.dialogData.smartCard,
                            phone: session.userData.phone,
                        },
                        merchant: 'airvend',
                        product: 'gotv',
                        reference: reference,
                        status: 'init',
                        tags: ['gotv', 'self'],
                        user: session.userData.phone,
                    };
                    dbUtils_1.createTransaction(transaction)
                        .then((trans) => {
                        return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: reference });
                    })
                        .then((u) => {
                        const data = {
                            amount: transaction.amount,
                            email: session.userData.email,
                            reference: reference,
                        };
                        return paystack_1.initializePaystack(token)(data, transaction);
                    })
                        .then((initUrl) => {
                        const url = initUrl;
                        const msg = new builder.Message(session).attachments([
                            new builder.HeroCard(session)
                                .text(`Click 'Pay Now'`)
                                .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                                .buttons([
                                builder.CardAction.openUrl(session, url, 'Pay Now'),
                            ]),
                        ]);
                        session.endDialog(msg);
                    })
                        .catch((error) => {
                        logger_1.default.error(error);
                        session.endDialog('Error');
                    });
                }
            })
                .catch((error) => {
                logger_1.default.error(error);
                session.endDialog('Error');
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'gotv', 10);
        const userdetails = session.dialogData.gotvData;
        const options = {
            distance: 100,
            keys: ['name', 'key'],
            location: 0,
            maxPatternLength: 32,
            minMatchCharLength: 1,
            shouldSort: true,
            threshold: 0.3,
        };
        const fuse = new Fuse(gotvMap, options); // "list" is the item array
        const plan = fuse.search(session.dialogData.plan);
        const planDetails = gotvData[plan[0].key];
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const transaction = {
            amount: planDetails.amount + 100,
            address: session.message.address,
            description: session.dialogData.plan,
            identity: {
                variation_code: planDetails.code,
                smartCard: session.dialogData.smartCard,
                phone: session.userData.phone,
            },
            merchant: 'airvend',
            product: 'gotv',
            reference: reference,
            status: 'init',
            tags: ['gotv', 'self'],
            user: session.userData.phone,
        };
        // charge user and dispense airtime
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        // charge user
        session.send('Working my magic... ⏳');
        session.sendTyping();
        dbUtils_1.createTransaction(transaction)
            .then((result) => {
            return paystack_1.chargeWithPaystack(token)(chargeData, transaction);
        })
            .then((res) => {
            if (res.data.status === 'success') {
                // return dispenseGOTV(transaction);
            }
            else {
                transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'failed' })
                    .then((r) => logger_1.default.info(r))
                    .catch((e) => logger_1.default.error(e));
                const message = res.message
                    ? res.message
                    : 'There was an error';
                throw Error(message);
            }
        })
            .then((trans) => {
            session.endDialog();
        })
            .catch((error) => {
            airvend_1.handleDispenseError(error)(session);
        });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({ matches: /^gotv$/i });
