"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../../../App");
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
App_1.bot.dialog('bill.controller', [
    (session, args, next) => {
        // check args
        let plan;
        if (args) {
            if (args.type) {
                plan = session.dialogData.plan = args.type;
            }
        }
        if (plan) {
            session.dialogData.plan = plan;
            next();
        }
        else {
            session.beginDialog('bills.askPlan');
        }
    },
]);
App_1.bot.dialog('askPlan0', []);
// && validateAmount(amount) === true
