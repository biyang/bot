"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const App_1 = require("../../../App");
const mixpanel_1 = require("../mixpanel");
App_1.bot.dialog('/PayBill', [
    (session, args, next) => {
        mixpanel_1.default(session, 'PayBill', 1);
        let bill;
        let type;
        // check bill type to determin where to go, if no bill type, suggest available bill types
        if (session.dialogData.bill) {
            bill = session.dialogData.bill;
        }
        // check if recipient is in dialog data
        if (session.dialogData.type) {
            type = session.dialogData.type;
        }
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (builder.EntityRecognizer.findEntity(args.entities, 'Bill')) {
                bill = builder.EntityRecognizer.findEntity(args.entities, 'Bill').entity;
                session.dialogData.bill = bill;
            }
            if (builder.EntityRecognizer.findEntity(args.entities, 'Bill::Type')) {
                const billAndType = builder.EntityRecognizer.findEntity(args.entities, 'Bill::Type').entity;
                bill = session.dialogData.bill = billAndType.split(' ')[0];
                type = session.dialogData.type = billAndType.split(' ')[1];
            }
        }
        if (!bill) {
            // ask what kind of bills
            const name = session.userData.name
                ? session.userData.name
                : 'Friend';
            const choices = [
                'DSTv',
                'GOTv',
                // 'SMILE',
                'IKEDC Prepaid',
                'EKDC Prepaid',
                'IBEDC Prepaid',
                'AEDC Prepaid',
                'PHDC Prepaid',
                'KEDCO Prepaid',
                'KAEDCO Prepaid',
                'JED Prepaid',
            ];
            const textMessage = `What bill do you want to pay, ${name}?`;
            if (session.message.source === 'telegram') {
                const replyMessage = new builder.Message(session).text(textMessage);
                replyMessage.sourceEvent({
                    telegram: {
                        method: 'sendMessage',
                        parameters: {
                            parse_mode: 'Markdown',
                            reply_markup: JSON.stringify({
                                keyboard: [
                                    [{ text: 'DSTv' }],
                                    [{ text: 'GOTv' }],
                                    // [{ text: 'SMILE' }],
                                    [{ text: 'IKEDC Prepaid' }],
                                    [{ text: 'EKDC Prepaid' }],
                                    [{ text: 'IBEDC Prepaid' }],
                                    [{ text: 'AEDC Prepaid' }],
                                    [{ text: 'PHDC Prepaid' }],
                                    [{ text: 'KAEDCO Prepaid' }],
                                    [{ text: 'KEDCO Prepaid' }],
                                    [{ text: 'JED Prepaid' }],
                                    [{ text: 'cancel' }],
                                ],
                                one_time_keyboard: true,
                                resize_keyboard: true,
                            }),
                        },
                    },
                });
                session.send(replyMessage);
            }
            else {
                builder.Prompts.choice(session, textMessage, choices, {
                    listStyle: builder.ListStyle.button,
                });
            }
        }
        else {
            next();
        }
    },
    (session, results) => {
        mixpanel_1.default(session, 'PayBill', 2);
        const type = session.dialogData.type;
        // check if bill type, if it does not exist, set to null
        const bill = session.dialogData.bill
            ? session.dialogData.bill.toLowerCase()
            : null;
        const choice = results.response
            ? results.response.entity.toLowerCase()
            : null;
        const billType = type ? type : null;
        const options = {
            dstv: '/dstv',
            gotv: '/gotv',
            smile: '/smile',
            spectranet: 'spectranet.controller',
            'ikedc prepaid': '/ikedc',
            'ibedc prepaid': '/ikedc',
            'ekdc prepaid': '/ikedc',
            'phdc prepaid': '/ikedc',
            'aedc prepaid': '/ikedc',
            'kedco prepaid': '/ikedc',
            'kaedco prepaid': '/ikedc',
            'jed prepaid': '/ikedc',
            default: 'defaultEnd',
        };
        session.replaceDialog(options[bill] || options[choice] || options['default'], {
            type: billType,
            choice,
        });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({ matches: /^(bills|pay bill|subscribe)$/i });
