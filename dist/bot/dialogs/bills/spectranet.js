"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const shortid = require("simple-id");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
const mixpanel_1 = require("../mixpanel");
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
const dbUtils_1 = require("../../../utils/dbUtils");
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
const airvend_1 = require("../../../utils/airvend");
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
const validateAmount = (Amount) => {
    const amounts = [500, 1000, 2000, 5000, 7000, 10000];
    const amount = parseInt(Amount, 10);
    if (amounts.find((a) => a === amount)) {
        return true;
    }
    else {
        return false;
    }
};
App_1.bot.dialog('spectranet.controller', [
    (session, args, next) => {
        // first step
        let amount;
        mixpanel_1.default(session, 'spectranet', 1);
        // check if amount is passed in by LUIS
        if (args && args.entities) {
            if (builder.EntityRecognizer.findEntity(args.entities, 'builtin.number')) {
                amount =
                    builder.EntityRecognizer.findEntity(args.entities, 'builtin.number').entity || null;
            }
        }
        if (amount && validateAmount(amount) === true) {
            session.dialogData.amount = amount;
            next();
        }
        else {
            session.beginDialog('spectranet.askAmount');
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'spectranet', 2);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        if (amount) {
            next();
        }
        else {
            session.endDialog();
        }
    },
    (session, results, next) => {
        // check params
        session.sendTyping();
        mixpanel_1.default(session, 'spectranet', 3);
        const amount = session.dialogData.amount;
        user_1.User.findOne({ phone: session.userData.phone })
            .then((user) => {
            // check user for name and auth
            if (user && user.authorization) {
                session.userData.email = user.email;
                session.dialogData.paymentAuth =
                    user.billsAuth.authorization_code;
                next();
            }
            else {
                // break to firstPayment
                const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                session.dialogData.network = user.network;
                const transaction = {
                    amount: amount,
                    description: 'spectranet',
                    identity: {
                        sourceChannel: session.message.address.channelId,
                    },
                    merchant: 'airvend',
                    product: 'spectranet',
                    reference: reference,
                    status: 'init',
                    tags: ['spectranet', 'self'],
                    user: session.userData.phone,
                };
                session.replaceDialog('firstPay', { transaction });
            }
        })
            .catch((error) => logger_1.default.error(error));
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'spectranet', 4);
        const phone = session.userData.phone;
        const network = session.dialogData.network;
        const amount = session.dialogData.amount;
        const msg = `Just to confirm, you want to buy spectranet recharge airtime of ${amount}`;
        // ask for confirmation
        builder.Prompts.choice(session, msg, 'Yes|Change amount|Abort', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        // ceck
        mixpanel_1.default(session, 'spectranet', 5);
        session.sendTyping();
        const auth = session.dialogData.paymentAuth;
        const amount = session.dialogData.amount;
        const type = 'spectranet';
        if (results && results.response) {
            const option = results.response.entity;
            if (option === 'Yes') {
                const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                const transaction = {
                    amount: amount,
                    description: 'spectranet',
                    identity: {
                        sourceChannel: session.message.address.channelId,
                    },
                    merchant: 'airvend',
                    product: 'spectranet',
                    reference: reference,
                    status: 'init',
                    tags: ['spectranet', 'self'],
                    user: session.userData.phone,
                };
                session.replaceDialog('spectranet.Go', {
                    transaction,
                    auth,
                    type,
                });
            }
            else if (option === 'Change amount') {
                session.beginDialog('spectranet.askAmount');
            }
            else {
                session.replaceDialog('abort');
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'spectranet', 6);
        session.sendTyping();
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        const amount = session.dialogData.amount;
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const auth = session.dialogData.paymentAuth;
        const type = 'spectranet';
        const transaction = {
            amount: amount,
            description: 'spectranet',
            identity: {
                sourceChannel: session.message.address.channelId,
            },
            merchant: 'airvend',
            product: 'spectranet',
            reference: reference,
            status: 'init',
            tags: ['spectranet', 'self'],
            user: session.userData.phone,
        };
        session.replaceDialog('spectranet.Go', { transaction, auth, type });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^(spectranet)/i,
});
App_1.bot.dialog('spectranet.askAmount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'How much spectranet airtime do you want to buy ?',
            1: 'Please enter an amount which is a multiple of NGN500 :-)',
            2: 'Please enter a valid amount to proceed :-)',
        };
        message = options[numberOfRetries];
        const choices = ['500', '1000', '2000', '5000', '7000', '10000'];
        if (message) {
            builder.Prompts.choice(session, message, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
        else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('spectranet.help', {
                fromDialog: 'spectranet.controller',
            });
        }
    },
    (session, results, next) => {
        if (results.response &&
            results.response.entity &&
            validateAmount(results.response.entity) === true) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response.entity });
        }
        else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('spectranet.askAmount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);
App_1.bot.dialog('spectranet.Go', [
    (session, args, next) => {
        // check params
        if (args) {
            const transaction = args.transaction;
            const auth = args.auth;
            const type = args.type;
            session.send('Working my magic... ⏳');
            session.sendTyping();
            const chargeData = {
                amount: transaction.amount,
                email: session.userData.email,
                paymentAuth: auth,
                reference: transaction.reference,
            };
            // charge user
            dbUtils_1.createAirTransaction(transaction)
                .then((result) => {
                return paystack_1.chargeWithPaystack(token)(chargeData, transaction);
            })
                .then((res) => {
                if (res.data.status === 'success') {
                    const payload = {
                        amount: transaction.amount,
                        reference: transaction.reference,
                    };
                    return airvend_1.dispenseSpectranet(payload);
                }
                else {
                    transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'failed' })
                        .then((r) => {
                        logger_1.default.info(r);
                    })
                        .catch((e) => {
                        logger_1.default.error(e);
                    });
                    const message = res.message
                        ? res.message
                        : 'There was an error';
                    throw Error(message);
                }
            })
                .then((spectranet) => {
                airvend_1.handleDispenseResponse(spectranet)(session, transaction, type);
            })
                .then((trans) => {
                // do nothing
            })
                .catch((error) => {
                airvend_1.handleDispenseError(error)(session);
            });
        }
        else {
            session.replaceDialog('/recharge');
        }
    },
]);
App_1.bot.dialog('spectranet.help', [
    (session, args, next) => {
        session.sendTyping();
        session.dialogData.fromDialog = args.fromDialog;
        const choices = ['Start over', 'Continue'];
        builder.Prompts.choice(session, 'Seems you are stuck, should I clear this conversation and start from the beginning ?', choices, { listStyle: builder.ListStyle.button });
    },
    (session, results) => {
        const response = results.response ? results.response.entity : null;
        const choices = {
            'Start over': session.dialogData.fromDialog,
            Continue: 'Okay, going back to our convo ;-)',
        };
        choices[response]
            ? response === 'Start over'
                ? session.replaceDialog(choices[response])
                : session.endDialog(choices[response])
            : session.endDialog();
    },
]);
