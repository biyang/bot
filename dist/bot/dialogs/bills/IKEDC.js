"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const _ = require("lodash");
const parseStringModule = require("xml2js");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
const parseString = parseStringModule.parseString;
// import model
const beneficiary_1 = require("../../../db/schemas/beneficiary");
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
// import database utils
const dbUtils_1 = require("../../../utils/dbUtils");
// import prompt
const askIkedcPrepaid = require("../../prompts/askIkedcPrepaid");
const validateAmount_1 = require("../recharge/validateAmount");
// import aivennd dispense
const airvend_1 = require("../../../utils/airvend");
const vtpass_1 = require("../../../utils/vtpass");
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
const shortid = require("simple-id");
const mixpanel_1 = require("../mixpanel");
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
App_1.bot.dialog('/ikedc', [
    (session, args, next) => {
        const provider = args && args.choices && args.choice.split(' ')[0];
        const message = session.message &&
            session.message.text &&
            session.message.text.split(' ')[0];
        session.dialogData.provider = provider || message || 'ikedc';
        mixpanel_1.default(session, session.dialogData.provider, 1);
        let amount;
        // check if amount is stored in dialog data
        if (session.dialogData.amount) {
            amount = session.dialogData.amount;
        }
        if (args.type) {
            amount = session.dialogData.amount = args.type;
        }
        const name = session.userData.name ? session.userData.name : 'Friend';
        if (amount && validateAmount_1.default(amount) === true) {
            session.dialogData.amount = amount;
            next();
        }
        else {
            if (validateAmount_1.default(amount) === 'tooLow' ||
                validateAmount_1.default(amount) === 'tooHigh') {
                session.beginDialog('electricity.askAmount', {
                    numberOfRetries: 1,
                });
            }
            else {
                session.beginDialog('electricity.askAmount');
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 2);
        if (results.response) {
            session.dialogData.amount = results.response;
        }
        // we now have the plan so let's get the smartcard number
        // ask if touse one of saved smart card numbers
        user_1.User.findOne({ phone: session.userData.phone })
            .populate('beneficiaries')
            .then((user) => {
            if (!user) {
                const newUser = {
                    address: session.message.address,
                    email: dbUtils_1.deriveEmail(session),
                    name: session.message.user.name,
                    phone: dbUtils_1.mockPhone(session),
                    userBotId: session.message.user.id,
                };
                dbUtils_1.saveNewUser(newUser)
                    .then((savedUser) => {
                    session.userData.email = savedUser.email;
                    session.dialogData.useExisting = false;
                    next();
                })
                    .catch((err) => {
                    logger_1.default.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(`${message}, please try again`);
                });
            }
            else if (user && user.beneficiaries.length < 1) {
                session.dialogData.useExisting = false;
                session.userData.email = user.email;
                next();
            }
            else {
                const beneficiaries = user.beneficiaries;
                session.userData.email = user.email;
                const checkIfBeneficiaryExists = _.filter(beneficiaries, (o) => {
                    return (o.identifier.type ===
                        session.dialogData.provider);
                });
                if (checkIfBeneficiaryExists.length > 0) {
                    session.dialogData.useExisting = true;
                    const listToSend = _.map(checkIfBeneficiaryExists, (o) => {
                        return o.name;
                    });
                    listToSend.push('New');
                    const name = session.userData.name
                        ? session.userData.name
                        : 'Friend';
                    const provider = session.dialogData.provider;
                    builder.Prompts.choice(session, `Which ${provider.toUpperCase()} account do you want to pay for, ${name}`, listToSend, { listStyle: builder.ListStyle.button });
                }
                else {
                    next();
                }
            }
        });
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 3);
        if (session.dialogData.useExisting) {
            if (results.response) {
                if (results.response.entity !== 'New') {
                    session.dialogData.smartCard = results.response.entity;
                    next();
                }
                else {
                    session.dialogData.useExisting = false;
                    askIkedcPrepaid.beginDialog(session, {
                        provider: session.dialogData.provider,
                    });
                }
            }
        }
        else {
            askIkedcPrepaid.beginDialog(session, {
                provider: session.dialogData.provider,
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 4);
        let smartCard = session.dialogData.smartCard;
        if (results && results.response) {
            smartCard = session.dialogData.smartCard = results.response;
            user_1.User.findOne({ phone: session.userData.phone })
                .then((user) => {
                const ben = new beneficiary_1.Beneficiary({
                    identifier: {
                        type: session.dialogData.provider,
                        smartCard: smartCard,
                    },
                    name: smartCard,
                    user: user._id,
                });
                return ben.save();
            })
                .then((SavedBen) => {
                return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { $push: { beneficiaries: SavedBen } });
            })
                .then((user) => {
                next();
            })
                .catch((err) => {
                logger_1.default.error(err);
                next();
            });
        }
        else if (session.dialogData.useExisting) {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 5);
        let smartCard = session.dialogData.smartCard;
        if (session.dialogData.smartCard.entity) {
            smartCard = session.dialogData.smartCard =
                session.dialogData.smartCard.entity;
        }
        // verify account number then prompt for confirmation
        logger_1.default.info({
            smartCard,
            type: session.dialogData.provider,
        });
        vtpass_1.verifyMerchant({ smartCard, type: session.dialogData.provider })
            .then((res) => {
            if (res && res.data && res.data.content) {
                if (res.data.content.error) {
                    throw Error(res.data.content.error);
                }
                const { Customer_Name, Minimum_Amount, Can_Vend, Min_Purchase_Amount, Meter_Type, } = res.data.content;
                try {
                    if (Can_Vend && Can_Vend !== 'yes') {
                        const provider = session.dialogData.provider;
                        throw Error(`We are unable to verify your ${provider.toUpperCase()} account, please try again later`);
                    }
                    if (Number(Minimum_Amount) &&
                        Number(Minimum_Amount) > session.dialogData.amount) {
                        session.send(`The minimum amount you can pay is NGN ${Minimum_Amount} account, please try again`);
                        next({ response: 'Change Amount' });
                    }
                    if (Number(Min_Purchase_Amount) &&
                        Number(Min_Purchase_Amount) >
                            session.dialogData.amount) {
                        session.send(`The minimum amount you can pay is NGN ${Min_Purchase_Amount} account, please try again`);
                        next({ response: 'Change Amount' });
                    }
                    if (Meter_Type && Meter_Type === 'POSTPAID') {
                        session.endConversation(`Invalid meter type, please check your meter number and try again`);
                    }
                    const customerName = Customer_Name;
                    session.dialogData.ikedcData =
                        session.dialogData.ikedcData || {};
                    session.dialogData.ikedcData.name = customerName;
                    session.dialogData.ikedcData.address = '';
                    const provider = session.dialogData.provider;
                    const msg = `Please confirm that you want to pay ₦${session.dialogData.amount + 100} for ${provider.toUpperCase()}: \n Account no: ${session.dialogData.smartCard} \n Account Name: ${customerName} \n`;
                    builder.Prompts.choice(session, msg, 'Yes|Change Amount|Change Account Number|Abort', { listStyle: builder.ListStyle.button });
                }
                catch (e) {
                    throw Error(e);
                }
            }
            else {
                const provider = session.dialogData.provider;
                throw Error(`We are unable to verify your ${provider.toUpperCase()} account, please try again later`);
            }
        })
            .catch((error) => {
            // console.log(error);
            logger_1.default.error(error);
            session.endDialog(`Could not verify your ${session.dialogData.provider.toUpperCase()} account number`);
        });
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 6);
        const name = session.userData.name ? session.userData.name : 'Friend';
        if (results && results.response) {
            const option = results.response.entity;
            switch (option) {
                case 'Yes':
                    // dispense gotv
                    session.dialogData.goodToGo = true;
                    next();
                    break;
                case 'Change Amount':
                    // prompt for amount
                    builder.Prompts.number(session, `Please enter how much you want to pay, ${name}`);
                    break;
                case 'Change Account Number':
                    // prompt for amount
                    session.dialogData.changeSmartCard = true;
                    askIkedcPrepaid.beginDialog(session, {
                        provider: session.dialogData.provider,
                    });
                    break;
                default:
                    session.endDialog(`Okay, ${name}`);
                    break;
            }
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 7);
        if (session.dialogData.changePackage) {
            if (results && results.response) {
                session.dialogData.amount = results.response;
                session.dialogData.goodToGo = true;
                next();
            }
        }
        else if (session.dialogData.changeSmartCard) {
            let smartCard = session.dialogData.smartCard;
            if (results && results.response) {
                smartCard = session.dialogData.smartCard = results.response;
            }
            // verify account number then prompt for confirmation
            logger_1.default.info({
                smartCard,
                type: session.dialogData.provider,
            });
            vtpass_1.verifyMerchant({ smartCard, type: session.dialogData.provider })
                .then((res) => {
                if (res && res.data && res.data.content) {
                    if (res.data.content.error) {
                        throw Error(res.data.content.error);
                    }
                    const { Customer_Name, Minimum_Amount, Can_Vend } = res.data.content;
                    const customerName = Customer_Name;
                    const customerAddress = '';
                    session.dialogData.ikedcData =
                        session.dialogData.ikedcData || {};
                    session.dialogData.ikedcData.name = customerName;
                    session.dialogData.ikedcData.address = customerAddress;
                    const provider = session.dialogData.provider;
                    const msg = `Please confirm that you want to pay ₦${session.dialogData.amount + 100} for ${provider.toUpperCase()}: \n Account no: ${session.dialogData.smartCard} \n Account Name: ${customerName} \n`;
                    session.send(msg);
                    session.dialogData.goodToGo = true;
                    // send prompt
                    next();
                }
                else {
                    throw Error('Could not complete transaction');
                }
            })
                .catch((error) => {
                logger_1.default.error(error);
                session.endDialog(`Could not verify your ${session.dialogData.provider.toUpperCase()} account number`);
            });
        }
        else if (session.dialogData.goodToGo) {
            next();
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 8);
        if (session.dialogData.goodToGo) {
            // check if we have users authorization codde, if so ask confirmation else initiate fresh payment
            const amount = session.dialogData.amount;
            dbUtils_1.cardCheck(session.userData.phone)
                .then((user) => {
                if (user && user.billsAuth) {
                    session.dialogData.paymentAuth =
                        user.billsAuth.authorization_code;
                    next();
                }
                else {
                    // save transaction details and
                    // send user to payment page
                    // create url then send to user
                    const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                    const transaction = {
                        amount: amount + 100,
                        address: session.message.address,
                        description: session.dialogData.plan || 'electricity',
                        identity: {
                            account: session.dialogData.smartCard,
                            customername: session.dialogData.ikedcData.name,
                            phone: session.userData.phone,
                            customeraddress: session.dialogData.ikedcData.address,
                        },
                        merchant: 'airvend',
                        product: session.dialogData.provider,
                        reference: reference,
                        status: 'init',
                        tags: ['electricity', 'self'],
                        user: session.userData.phone,
                    };
                    dbUtils_1.createTransaction(transaction)
                        .then(() => {
                        return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: reference });
                    })
                        .then((u) => {
                        const data = {
                            amount: transaction.amount,
                            email: session.userData.email,
                            reference: reference,
                        };
                        return paystack_1.initializePaystack(token)(data, transaction);
                    })
                        .then((result) => {
                        const url = result;
                        const msg = new builder.Message(session).attachments([
                            new builder.HeroCard(session)
                                .text(`Click 'Pay Now'`)
                                .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                                .buttons([
                                builder.CardAction.openUrl(session, url, 'Pay Now'),
                            ]),
                        ]);
                        session.endDialog(msg);
                    })
                        .catch((error) => {
                        logger_1.default.error(error);
                        session.endDialog('Error');
                    });
                }
            })
                .catch((error) => {
                logger_1.default.error(error);
                session.endDialog('Error');
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, session.dialogData.provider, 9);
        const amount = session.dialogData.amount;
        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
        const transaction = {
            amount: amount + 100,
            address: session.message.address,
            description: session.dialogData.plan || 'electricity',
            identity: {
                account: session.dialogData.smartCard,
                customername: session.dialogData.ikedcData.name,
                phone: session.userData.phone,
                customeraddress: session.dialogData.ikedcData.address,
            },
            merchant: 'airvend',
            product: session.dialogData.provider,
            reference: reference,
            status: 'init',
            tags: ['electricity', 'self'],
            user: session.userData.phone,
        };
        // charge user and dispense airtime
        const chargeData = {
            amount: transaction.amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        // charge user
        session.send('Working my magic... ⏳');
        session.sendTyping();
        dbUtils_1.createTransaction(transaction)
            .then(() => {
            return paystack_1.chargeWithPaystack(token)(chargeData, transaction);
        })
            .then((res) => {
            if (res.data.status === 'success') {
                // return dispenseElectricity(
                //     transaction,
                //     session.dialogData.provider
                // );
            }
            else {
                transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'failed' })
                    .then((r) => logger_1.default.info(r))
                    .catch((e) => logger_1.default.error(e));
                const message = (res && res.message) || 'There was an error';
                throw Error(message);
            }
        })
            .then(() => {
            session.endDialog();
        })
            .catch((error) => {
            airvend_1.handleDispenseError(error)(session);
        });
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^(ikedc|ikedc Prepaid|ibedc|ibedc Prepaid|ekdc|ekdc Prepaid|phdc|phdc Prepaid|abuja|abuja dc|aedc|jed|kedco|kaedco|)$/i,
});
App_1.bot.dialog('electricity.askAmount', [
    (session, args) => {
        session.sendTyping();
        // check args for number of times userr has used this
        // use that to detertmine whether to help or next
        let message;
        if (args) {
            session.dialogData.numberOfRetries = args.numberOfRetries;
        }
        const numberOfRetries = session.dialogData.numberOfRetries
            ? session.dialogData.numberOfRetries
            : 0;
        const options = {
            0: 'How much do you want to pay?',
            1: 'Please enter an amount greater than NGN1000 :-)',
            2: 'Please enter a valid amount to proceed :-)',
        };
        message = options[numberOfRetries];
        if (message) {
            builder.Prompts.number(session, message);
        }
        else {
            session.dialogData.numberOfRetries = 0;
            session.beginDialog('electricity.help', {
                fromDialog: '/ikedc',
            });
        }
    },
    (session, results, next) => {
        if (results.response &&
            validateAmount_1.default(results.response, 1000, 100000) === true) {
            // check if account number is valid
            session.endDialogWithResult({ response: results.response });
        }
        else {
            const numberOfRetries = session.dialogData.numberOfRetries;
            session.beginDialog('electricity.askAmount', {
                numberOfRetries: numberOfRetries + 1,
            });
        }
    },
]);
App_1.bot.dialog('electricity.help', [
    (session, args, next) => {
        session.sendTyping();
        session.dialogData.fromDialog = args.fromDialog;
        const choices = ['Start over', 'Continue'];
        builder.Prompts.choice(session, 'Seems you are stuck, should I clear this conversation and start from the beginning ?', choices, { listStyle: builder.ListStyle.button });
    },
    (session, results) => {
        const response = results.response ? results.response.entity : null;
        const choices = {
            'Start over': session.dialogData.fromDialog,
            Continue: 'Okay, going back to our convo ;-)',
        };
        choices[response]
            ? response === 'Start over'
                ? session.replaceDialog(choices[response])
                : session.endDialog(choices[response])
            : session.endDialog();
    },
]);
