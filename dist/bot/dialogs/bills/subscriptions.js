"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../../../App");
const mixpanel_1 = require("../mixpanel");
App_1.bot.dialog('/subscriptions', [
    (session, args) => {
        mixpanel_1.default(session, 'subscriptions', 1);
        session.endConversation('This feature is under development. Please check later. Thanks.');
    },
]).triggerAction({ matches: /^subscriptions/i });
