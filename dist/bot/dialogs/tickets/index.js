"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const logger_1 = require("../../../logger");
const App_1 = require("../../../App");
// import models
const transaction_1 = require("../../../db/schemas/transaction");
const user_1 = require("../../../db/schemas/user");
// import prompts
const askPhoneNumber = require("../../prompts/askPhoneNumber");
// import database utils
const dbUtils_1 = require("../../../utils/dbUtils");
const tickets_1 = require("../../../utils/tickets");
const paystack_1 = require("../../../utils/paystack");
const token = process.env.NEW_PAYSTACK_TOKEN;
const shortid = require("simple-id");
// import airvend dispense
const airvend_1 = require("../../../utils/airvend");
function validatePhoneNumber(phone) {
    const re = /\b\d{11}\b/g;
    return re.test(phone);
}
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
/// dialog waterfall
App_1.bot.dialog('/buyTicketIndex', [
    (session) => {
        const msg = `Hi, what ticket do you want to buy`;
        builder.Prompts.choice(session, msg, 'BRT|LAGBUS|Cancel', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        if (results && results.response) {
            const option = results.response.entity;
            switch (option) {
                case 'BRT':
                    // then update transaction succeed
                    session.replaceDialog('/ticsDemo', { type: 'BRT' });
                    break;
                case 'LAGBUS':
                    // prompt for amount
                    session.replaceDialog('/ticsDemo', { type: 'LAGBUS' });
                    break;
                default:
                    const name = session.userData.name
                        ? session.userData.name
                        : 'Friend';
                    session.endDialog(`Okay, ${name}`);
                    break;
            }
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({ matches: /^ticket|^brt|^lagbus/i });
App_1.bot.dialog('/buyTicket', [
    (session, args) => {
        const type = (session.dialogData.type = args.type);
        const msg = `Please choose your route`;
        builder.Prompts.choice(session, msg, 'OSHODI-CMS|YABA-OBALENDE|Cancel', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        if (results && results.response) {
            const option = (session.dialogData.location =
                results.response.entity);
            if (option !== 'Cancel') {
                session.send(`Ok. Gotcha! Your ticket cost NGN140`);
                session.dialogData.amount = 220;
                const msg = 'Please select yes to confirm purchase';
                builder.Prompts.confirm(session, msg, {
                    listStyle: builder.ListStyle.button,
                });
            }
            else {
                session.endConversation('Ok! See you later.');
            }
        }
    },
    (session, results, next) => {
        if (results && results.response) {
            next();
        }
        else {
            session.endConversation('Ok! See you later.');
        }
    },
    (session, results, next) => {
        let amount = session.dialogData.amount;
        if (results && results.response) {
            amount = session.dialogData.amount = results.response;
        }
        // check if we have the users number, check session first, then DB
        // this also assumes we hav user's phone number
        if (session.userData.phone && session.userData.email) {
            next();
        }
        else {
            async function checkUser() {
                try {
                    const user = await dbUtils_1.checkIfRegisteredUser(session.userData.phone);
                    if (!user) {
                        // not a user, ask for their number
                        session.dialogData.userNotInDb = true;
                        askPhoneNumber.beginDialog(session, {});
                    }
                    else if (!user.phone ||
                        !validatePhoneNumber(user.phone)) {
                        // user exists but we don't have their number
                        session.dialogData.userPhoneNotExist = true;
                        session.userData.email = user.email;
                        askPhoneNumber.beginDialog(session, {});
                    }
                    else {
                        // we have their number
                        session.userData.phone = user.phone;
                        session.userData.network = user.network;
                        session.userData.email = user.email;
                        next();
                    }
                }
                catch (err) {
                    logger_1.default.error(err);
                    const message = err.message
                        ? err.message
                        : 'There was an error';
                    session.endDialog(`${message}, please try again`);
                }
            }
            checkUser();
        }
    },
    (session, results, next) => {
        let phone = session.userData.phone;
        // save user details if we don't have it already
        if (phone) {
            next();
        }
        if (results && results.response) {
            phone = session.userData.phone = results.response;
        }
        if (session.dialogData.userNotInDb) {
            const newUser = {
                address: session.message.address,
                email: dbUtils_1.deriveEmail(session),
                name: session.message.user.name,
                phone: phone,
                userBotId: session.message.user.id,
            };
            dbUtils_1.saveNewUser(newUser)
                .then((user) => {
                session.userData.email = user.email;
                next();
            })
                .catch((err) => {
                logger_1.default.error(err);
                const message = err.message
                    ? err.message
                    : 'There was an error';
                session.endDialog(`${message}, please try again`);
            });
        }
        else if (session.dialogData.userPhoneNotExist) {
            // update user phone the next
            const updatedUser = {
                address: session.message.address,
                name: session.message.user.name,
                phone: phone,
            };
            user_1.User.findOneAndUpdate({ phone: session.userData.phone }, updatedUser)
                .then((user) => {
                session.userData.email = user.email;
                next();
            })
                .catch((err) => {
                logger_1.default.error(err);
                const message = err.message
                    ? err.message
                    : 'There was an error';
                session.endDialog(`${message}, please try again`);
            });
        }
        else {
            next();
        }
    },
    (session, results, next) => {
        const amount = session.dialogData.amount;
        // check if we have users authorization codde, if so ask confirmation else initiate fresh payment
        dbUtils_1.cardCheck(session.userData.phone)
            .then((user) => {
            if (user && user.authorization) {
                session.dialogData.paymentAuth =
                    user.authorization.authorization_code;
                next();
            }
            else {
                // save transaction details and
                // send user to payment page
                // create url then send to user
                const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                const transaction = {
                    amount: amount,
                    description: 'session.dialogData.plan',
                    identity: {
                        destination: session.dialogData.location,
                        valid: true,
                    },
                    merchant: 'pibot',
                    product: 'ticket',
                    reference: reference,
                    status: 'init',
                    tags: ['ticket'],
                    user: session.userData.phone,
                };
                dbUtils_1.createTransaction(transaction)
                    .then((trans) => {
                    return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: reference });
                })
                    .then((u) => {
                    const data = {
                        amount: amount,
                        email: session.userData.email,
                        reference: reference,
                    };
                    return paystack_1.initializePaystack(token)(data, transaction);
                })
                    .then((result) => {
                    const url = result;
                    const msg = new builder.Message(session).attachments([
                        new builder.HeroCard(session)
                            .text(`Click 'Pay Now'`)
                            .images([
                            builder.CardImage.create(session, payImageURL),
                        ])
                            .buttons([
                            builder.CardAction.openUrl(session, url, 'Pay Now'),
                        ]),
                    ]);
                    session.endDialog(msg);
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                    session.endDialog('Error');
                });
            }
        })
            .catch((error) => {
            logger_1.default.error(error);
            session.endDialog('Error');
        });
    },
]).cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
});
App_1.bot.dialog('/ticsDemo', [
    (session, args) => {
        const type = (session.dialogData.type = args.type);
        const msg = `Please choose your route`;
        builder.Prompts.choice(session, msg, 'OSHODI-CMS|YABA-OBALENDE|Cancel', {
            listStyle: builder.ListStyle.button,
        });
    },
    (session, results, next) => {
        if (results && results.response) {
            const option = (session.dialogData.location =
                results.response.entity);
            if (option !== 'Cancel') {
                session.send(`Ok. Gotcha! Your ticket cost NGN220`);
                session.dialogData.amount = 220;
                const msg = `Please select 'Yes' to confirm ticket purchase`;
                builder.Prompts.confirm(session, msg, {
                    listStyle: builder.ListStyle.button,
                });
            }
            else {
                session.endConversation('Ok! See you later.');
            }
        }
    },
    (session, results, next) => {
        if (results.response) {
            dbUtils_1.cardCheck(session.userData.phone)
                .then((user) => {
                if (user.authorization) {
                    session.dialogData.paymentAuth =
                        user.authorization.authorization_code;
                    next();
                }
                else {
                    const reference = 'bybt' + Date.now();
                    const type = session.dialogData.type;
                    const amount = session.dialogData.amount;
                    const transaction = {
                        amount: amount,
                        description: 'session.dialogData.plan',
                        identity: {
                            destination: session.dialogData.location,
                            valid: true,
                        },
                        merchant: 'pibot',
                        product: 'ticket',
                        reference: reference,
                        status: 'init',
                        tags: ['ticket'],
                        user: session.userData.phone,
                    };
                    dbUtils_1.createTransaction(transaction)
                        .then((trans) => {
                        return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: reference });
                    })
                        .then((u) => {
                        const data = {
                            amount: amount,
                            email: session.userData.email,
                            reference: reference,
                        };
                        return paystack_1.initializePaystack(token)(data, transaction);
                    })
                        .then((result) => {
                        const url = result;
                        const msg = new builder.Message(session).attachments([
                            new builder.HeroCard(session)
                                .text(`Click 'Pay Now'`)
                                .images([
                                builder.CardImage.create(session, payImageURL),
                            ])
                                .buttons([
                                builder.CardAction.openUrl(session, url, 'Pay Now'),
                            ]),
                        ]);
                        session.endDialog(msg);
                    })
                        .catch((error) => {
                        logger_1.default.error(error);
                        session.endDialog('Error');
                    });
                }
            })
                .catch((error) => {
                logger_1.default.error(error);
            });
        }
        else {
            session.endConversation('Okay! ');
        }
    },
    (session, results) => {
        session.send('Working my magic... ⏳');
        session.sendTyping();
        const phone = session.userData.phone;
        const type = session.dialogData.type;
        const amount = session.dialogData.amount;
        const reference = 'bybt' + Date.now();
        const transaction = {
            amount: amount,
            description: 'session.dialogData.plan',
            identity: {
                destination: session.dialogData.location,
                valid: true,
            },
            merchant: 'pibot',
            product: 'ticket',
            reference: reference,
            status: 'init',
            tags: ['ticket'],
            user: session.userData.phone,
        };
        // charge user and dispense airtime
        const chargeData = {
            amount: amount,
            email: session.userData.email,
            paymentAuth: session.dialogData.paymentAuth,
            reference: reference,
        };
        dbUtils_1.createTransaction(transaction)
            .then((result) => {
            return paystack_1.chargeWithPaystack(token)(chargeData, transaction);
        })
            .then((res) => {
            if (res.data.status === 'success') {
                const data = {
                    amount: amount,
                    destination: session.dialogData.location,
                    reference: reference,
                    validity: true,
                };
                return tickets_1.dispenseTicket(transaction);
            }
            else {
                transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'failed' })
                    .then((r) => {
                    logger_1.default.info(r);
                })
                    .catch((e) => {
                    logger_1.default.error(e);
                });
                const message = res.message
                    ? res.message
                    : 'There was an error';
                throw Error(message);
            }
        })
            .then((image) => {
            session.send('Thanks for your patronage!');
            session.send(`Here's your ticket`);
            const imageURL = image;
            const msg = new builder.Message(session).attachments([
                new builder.HeroCard(session)
                    .text(`Amount: ${transaction.amount} // Route: ${session.dialogData.location}`)
                    .images([builder.CardImage.create(session, imageURL)]),
            ]);
            session.endDialog(msg);
            // handleDispenseResponse(airtime)(session, transaction, "selfR")
        })
            .then((trans) => {
            session.endDialog();
        })
            .catch((error) => {
            airvend_1.handleDispenseError(error)(session);
        });
    },
]);
