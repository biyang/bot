"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/CustomerSupport', [
    (session, args) => {
        // use array of choices
        mixpanel_1.default(session, 'support', 1);
        const choices = ['Exit', 'Chat with an agent'];
        const textMessage = `Email hello@biya.com.ng or select one of the options below`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Exit' }],
                                [{ text: 'Chat with an agent' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        }
        else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'support', 2);
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            default: 'defaultEnd',
        };
        if (options[choice]) {
            // postback
            session.endConversation('hello');
        }
        else {
            session.replaceDialog(options['default']);
        }
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^(Customer Support)/i,
});
