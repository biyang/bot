"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/goodbye', [
    (session, args) => {
        mixpanel_1.default(session, 'goodbye', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const choices = [
            `Goodbye ${name}, see you soon!`,
            `Talk to you later, ${name}`,
            `Bye! See you later.`,
        ];
        session.endConversation(choices);
    },
]).triggerAction({ matches: /^stop|^goodbye|^quit|^exit|^bye|^later/i });
App_1.bot.dialog('/abort', [
    (session, args) => {
        mixpanel_1.default(session, 'abort', 1);
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const choices = [
            `Okay ${name}, see you later!`,
            `Talk to you soon, ${name}`,
        ];
        session.endConversation(choices);
    },
]);
