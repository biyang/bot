"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../../logger");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
const SlackService_1 = require("../../monitor/SlackService");
App_1.bot.dialog('/onDefault', [
    (session, args) => {
        mixpanel_1.default(session, 'onDefault', 1);
        logger_1.default.error(args);
        SlackService_1.SlackNotify({
            text: Array.isArray(args) && args.join(', '),
            username: 'Error',
            channel: '#feed-errors',
        });
        session.endConversation();
        // session.endConversation(
        //   `😿 I did not understand that. To see what I can help you with, send 'help'`
        // );
    },
]);
App_1.bot.dialog('abort', [
    (session, args) => {
        mixpanel_1.default(session, 'abort', 1);
        const name = session.userData.name ? session.userData.name : 'Pal';
        const messages = `Okay, ${name}.`;
        session.endConversation(messages);
    },
]);
