"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/menu', [
    (session, args) => {
        // use array of choices
        mixpanel_1.default(session, 'menu', 1);
        const name = session.userData.name ? session.userData.name : 'Friend';
        const choices = [
            'Recharge',
            'Pay Bill',
            'Transfer',
            'Settings',
            'Customer Support',
            'exit',
        ];
        const textMessage = `Here are some quick menu options you can get started with, ${name}`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Recharge' }],
                                [{ text: 'Pay Bill' }],
                                [{ text: 'Transfer' }],
                                [{ text: 'Settings' }],
                                [{ text: 'Customer Support' }],
                                [{ text: 'exit' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        }
        else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        mixpanel_1.default(session, 'menu', 2);
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            Recharge: '/recharge',
            Tickets: '/buyTicketIndex',
            'Pay Bill': '/PayBill',
            Transfer: 'transfer.controller',
            'Customer Support': '/CustomerSupport',
            Settings: '/settings',
            default: 'defaultEnd',
        };
        session.replaceDialog(options[choice] || options['default']);
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^(show me menu|show menu|menu|Go to menu|meni|\/menu|\/start)/i,
});
App_1.bot.dialog('defaultEnd', [
    (session) => {
        mixpanel_1.default(session, 'defaultEnd', 1);
        const name = session.userData.name ? session.userData.name : 'Friend';
        session.endDialog(`Okay ${name}, talk to you later!`);
    },
]);
