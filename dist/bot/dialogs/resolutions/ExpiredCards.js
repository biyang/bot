"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const shortid = require("simple-id");
const logger_1 = require("../../../logger");
const removeCard_1 = require("../../../utils/removeCard");
const App_1 = require("../../../App");
const dbUtils_1 = require("../../../utils/dbUtils");
const paystack_1 = require("../../../utils/paystack");
const user_1 = require("../../../db/schemas/user");
const token = process.env.PAYSTACK_TOKEN;
const payImageURL = 'http://res.cloudinary.com/pitech/image/upload/v1488262709/thumbs/cards.jpg';
App_1.bot.dialog('expiredCard', [
    (session, args, next) => {
        // check params
        session.sendTyping();
        // delete card
        const transaction = args && args.transaction;
        session.dialogData._transaction = transaction;
        if (transaction) {
            const msg = `We were unable to charge you due to an expired card.`;
            removeCard_1.default(session.userData.phone)
                .then(() => {
                builder.Prompts.choice(session, msg, 'Use new card|Cancel', {
                    listStyle: builder.ListStyle.button,
                });
            })
                .catch((error) => {
                logger_1.default.error(error);
            });
        }
        removeCard_1.default(session.userData.phone).then(console.log).catch(console.log);
        session.endDialog('We were unable to charge you due to an expired card. You will be prompted to add a new card when you make your next transaction.');
    },
    (session, results, next) => {
        if (results && results.response) {
            const option = results.response.entity;
            if (option === 'Use new card') {
                const transaction = session.dialogData._transaction;
                const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                transaction.reference = reference;
                transaction.merchant = 'airvend';
                transaction.user = session.userData.phone;
                dbUtils_1.createAirTransaction(transaction)
                    .then((trans) => {
                    return user_1.User.findOneAndUpdate({ phone: session.userData.phone }, { initialReference: transaction.reference });
                })
                    .then((u) => {
                    session.userData.email = u.email;
                    const data = {
                        amount: transaction.amount,
                        email: u.email,
                        reference: transaction.reference,
                    };
                    return paystack_1.initializePaystack(token)(data, transaction);
                })
                    .then((result) => {
                    const url = result;
                    const msg = new builder.Message(session).attachments([
                        new builder.HeroCard(session)
                            .text(`Click 'Pay Now'`)
                            .images([
                            builder.CardImage.create(session, payImageURL),
                        ])
                            .buttons([
                            builder.CardAction.openUrl(session, url, 'Pay Now'),
                        ]),
                    ]);
                    session.endDialog(msg);
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                });
            }
        }
        session.endDialog('We were unable to charge you due to an expired card. You will be prompted to add a new card when you make your next transaction.');
    },
]);
