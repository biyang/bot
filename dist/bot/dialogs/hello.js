"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const moment = require("moment");
const others_1 = require("../../utils/others");
const App_1 = require("../../App");
const mixpanel_1 = require("./mixpanel");
App_1.bot.dialog('/hello', [
    (session) => {
        const time = others_1.getGreetingTime(moment());
        const name = session.userData.name
            ? session.userData.name
            : session.message.user.name.split(' ')[0];
        const choices = [
            'Recharge',
            'Pay Bill',
            'Transfer',
            'Settings',
            'exit',
        ];
        const messages = [
            `Good ${time} ${name}`,
            `Hi ${name}`,
            `Hello ${name}`,
        ];
        mixpanel_1.default(session, 'hello', 1);
        session.send(messages);
        const textMessage = `😊 How may I be of service?`;
        if (session.message.source === 'telegram') {
            const replyMessage = new builder.Message(session).text(textMessage);
            replyMessage.sourceEvent({
                telegram: {
                    method: 'sendMessage',
                    parameters: {
                        parse_mode: 'Markdown',
                        reply_markup: JSON.stringify({
                            keyboard: [
                                [{ text: 'Recharge' }],
                                [{ text: 'Pay Bill' }],
                                [{ text: 'Transfer' }],
                                [{ text: 'Settings' }],
                                [{ text: 'exit' }],
                            ],
                            one_time_keyboard: true,
                            resize_keyboard: true,
                        }),
                    },
                },
            });
            session.send(replyMessage);
        }
        else {
            builder.Prompts.choice(session, textMessage, choices, {
                listStyle: builder.ListStyle.button,
            });
        }
    },
    (session, results, next) => {
        const choice = results.response.entity ? results.response.entity : null;
        const options = {
            Recharge: '/recharge',
            Tickets: '/buyTicketIndex',
            'Pay Bill': '/PayBill',
            Transfer: 'transfer.controller',
            Settings: '/settings',
            default: 'defaultEnd',
        };
        mixpanel_1.default(session, 'hello', 2);
        session.replaceDialog(options[choice] || options['default']);
    },
])
    .cancelAction('cancel', 'Ok. Cancelled.', {
    confirmPrompt: 'Are you sure you want to cancel this operation?',
    matches: /^(cancel|abort|stop|nevermind|never mind|don't worry|fuck off|dont worry)/i,
})
    .triggerAction({
    matches: /^yo|^hallo|^hello|^hi|^hey|^begin|^start|\/^start$/i,
});
App_1.bot.dialog('/greeting', [
    (session) => {
        mixpanel_1.default(session, 'greeting', 1);
        session.replaceDialog('/hello');
    },
]);
App_1.bot.dialog('/adapt', [
    (session) => {
        var msg = new builder.Message(session).addAttachment({
            contentType: 'application/vnd.microsoft.card.adaptive',
            content: {
                type: 'AdaptiveCard',
                speak: 'Your flight is confirmed for you and 3 other passengers from San Francisco to Amsterdam on Friday, October 10 8:30 AM',
                backgroundImage: 'https://res.cloudinary.com/pitech/image/upload/v1523098013/thumbs/Group.jpg',
                body: [
                    {
                        type: 'TextBlock',
                        text: 'Biya',
                        weight: 'bolder',
                        isSubtle: false,
                        size: 'medium',
                        color: 'light',
                    },
                    {
                        type: 'ColumnSet',
                        spacing: 'medium',
                        columns: [
                            {
                                type: 'Column',
                                width: '1',
                                items: [
                                    {
                                        type: 'TextBlock',
                                        text: 'Ref',
                                        size: 'medium',
                                        isSubtle: false,
                                        color: 'light',
                                    },
                                ],
                            },
                            {
                                type: 'Column',
                                width: 1,
                                items: [
                                    {
                                        type: 'TextBlock',
                                        horizontalAlignment: 'right',
                                        text: 'xxxxxxx',
                                        size: 'medium',
                                        weight: 'bolder',
                                        color: 'light',
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        type: 'ColumnSet',
                        spacing: 'small',
                        columns: [
                            {
                                type: 'Column',
                                width: '1',
                                items: [
                                    {
                                        type: 'TextBlock',
                                        text: 'Date',
                                        size: 'medium',
                                        isSubtle: false,
                                        color: 'light',
                                    },
                                ],
                            },
                            {
                                type: 'Column',
                                width: 1,
                                items: [
                                    {
                                        type: 'TextBlock',
                                        horizontalAlignment: 'right',
                                        text: 'Today',
                                        size: 'medium',
                                        weight: 'bolder',
                                        color: 'light',
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        type: 'ColumnSet',
                        separator: true,
                        columns: [
                            {
                                type: 'Column',
                                width: 'auto',
                                items: [
                                    {
                                        type: 'Image',
                                        url: 'https://res.cloudinary.com/pitech/image/upload/v1487970365/thumbs/2.jpg',
                                        size: 'stretch',
                                        spacing: 'none',
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        type: 'ColumnSet',
                        spacing: 'small',
                        columns: [
                            {
                                type: 'Column',
                                width: '1',
                                items: [
                                    {
                                        type: 'TextBlock',
                                        text: 'Phone number',
                                        size: 'medium',
                                        isSubtle: false,
                                        color: 'light',
                                    },
                                ],
                            },
                            {
                                type: 'Column',
                                width: 1,
                                items: [
                                    {
                                        type: 'TextBlock',
                                        horizontalAlignment: 'right',
                                        text: '$4,032.54',
                                        size: 'medium',
                                        weight: 'bolder',
                                        color: 'light',
                                    },
                                ],
                            },
                        ],
                    },
                    {
                        type: 'ColumnSet',
                        spacing: 'medium',
                        columns: [
                            {
                                type: 'Column',
                                width: '1',
                                items: [
                                    {
                                        type: 'TextBlock',
                                        text: 'Total',
                                        size: 'medium',
                                        isSubtle: false,
                                        color: 'light',
                                    },
                                ],
                            },
                            {
                                type: 'Column',
                                width: 1,
                                items: [
                                    {
                                        type: 'TextBlock',
                                        horizontalAlignment: 'right',
                                        text: '$4,032.54',
                                        size: 'medium',
                                        weight: 'bolder',
                                        color: 'light',
                                    },
                                ],
                            },
                        ],
                    },
                ],
            },
        });
        session.endConversation(msg);
    },
]).triggerAction({
    matches: /^adapt$/i,
});
