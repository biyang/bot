"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const LuisModelUrl = process.env.LUIS_MODEL_URL;
// Main dialog with LUIS
const recognizer = new builder.LuisRecognizer(LuisModelUrl);
const intents = new builder.IntentDialog({ recognizers: [recognizer] })
    .matches('OtherRecharge', 'otherRecharge.controller')
    .matches('selfRecharge', 'selfRecharge.controller')
    .matches('PayBill', '/PayBill')
    .matches('Transfer', 'transfer.suspended')
    .matches('Emoji', '/Emoji')
    .matches('Help', '/help')
    .matches('badWords', '/badWords')
    .matches('goodWords', '/goodWords')
    .matches('AboutYou', '/info')
    .matches('Greeting', '/greeting')
    .matches('Age', '/age')
    .onDefault('/onDefault');
exports.default = intents;
