"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const builder = require("botbuilder");
const logger_1 = require("../logger");
const App_1 = require("../App");
const commands_1 = require("../handoff/commands");
const handoff_1 = require("../handoff/handoff");
const user_1 = require("../db/schemas/user");
const mixpanel_1 = require("./dialogs/mixpanel");
App_1.bot.use(builder.Middleware.dialogVersion({
    resetCommand: /^upgrdReset/i,
    version: 4.0,
}));
// bot.use(builder.Middleware.firstRun({ version: 3.0, dialogId: '*:/firstRun' }));
const utterances_1 = require("../db/schemas/utterances");
App_1.bot.use({
    receive: (event, next) => {
        const newUtterance = new utterances_1.Utterances({
            event: event,
            type: 'incoming',
        });
        newUtterance.save();
        next();
    },
    send: (event, next) => {
        const newUtterance = new utterances_1.Utterances({
            event: event,
            type: 'outgoing',
        });
        newUtterance.save();
        next();
    },
});
App_1.bot.use({
    botbuilder: (session, next) => {
        // check if user is in mixpanel
        if (!session.userData.tracker && session.userData.active) {
            mixpanel_1.trackUser(session);
            session.userData.tracker = true;
        }
        const channel = session.message.source;
        const choices = {
            telegram: 'addresses.telegram',
            facebook: 'addresses.facebook',
            skype: 'addresses.skype',
            slack: 'addresses.slack',
            directline: 'addresses.directline',
        };
        const updateProperty = choices[channel];
        if (updateProperty) {
            if (!session.userData.addressVersion ||
                session.userData.addressVersion < 2) {
                user_1.User.findOneAndUpdate({ phone: session.userData.phone }, {
                    $set: {
                        [updateProperty]: session.message.address,
                    },
                })
                    .then(() => {
                    session.userData.addressVersion = 2;
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                });
            }
        }
        session.error = (err) => {
            // Do something custom
            // session.replaceDialog('unknownError');
            logger_1.default.error(err);
        };
        next();
    },
});
// replace this function with custom login/verification for agents
const isAgent = (session) => session.userData.isAgent ? true : false;
const handoff = new handoff_1.Handoff(App_1.bot, isAgent);
// ========================================================
// Bot Middleware
// ========================================================
App_1.bot.use(commands_1.commandsMiddleware(handoff), handoff.routingMiddleware()
/* other bot middlware should probably go here */
);
const whitelist = ['PiBotProject', 'Biyabot', 'Bot'];
App_1.bot.on('conversationUpdate', (message) => {
    if (message.membersAdded && message.membersAdded.length > 0) {
        if (whitelist.indexOf(message.membersAdded[0].name) !== -1) {
            App_1.bot.send(new builder.Message()
                .address(message.address)
                .text(`Hey pal! Send 'Hi' or an instruction to begin 🚀`));
        }
    }
});
