"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bodyParser = require("body-parser");
const cors = require("cors");
const express = require("express");
const mongoose = require("mongoose");
const morgan = require("morgan");
const logger_1 = require("./logger");
const dd_options = {
    response_code: true,
    tags: ['app:my_app'],
};
const connect_datadog = require('connect-datadog')(dd_options);
const builder = require("botbuilder");
const passport = require("passport");
const passport_1 = require("./routes/merchants/passport");
passport.use(passport_1.jwtLogin);
const mongo_bot_storage_1 = require("mongo-bot-storage");
const mWaveRouter_1 = require("./routes/mWaveRouter");
const paystack_1 = require("./routes/paystack");
const admin_1 = require("./routes/merchants/admin");
const auth_1 = require("./routes/merchants/auth");
const invoices_1 = require("./routes/merchants/invoices");
const transactions_1 = require("./routes/merchants/transactions");
const connector = new builder.ChatConnector({
    appId: '8f713deb-7849-428c-b1ae-85e844a9051b',
    appPassword: '1VEEPV73JhsBrxCniz1kSGM',
});
const bot = new builder.UniversalBot(connector);
exports.bot = bot;
const Intents_1 = require("./bot/Intents");
const BroadcastRouter_1 = require("./routes/BroadcastRouter");
const MonitorRouter_1 = require("./routes/MonitorRouter");
const SupportRouter_1 = require("./routes/SupportRouter");
const Tap2PayRouter_1 = require("./routes/Tap2PayRouter");
const index_1 = require("./routes/transport/index");
require("./utils/kue");
const whitelist = [
    'http://localhost:8080',
    'http://localhost:3000',
    'http://localhost:80',
    'http://localhost',
    'https://secure.biya.com.ng',
    'biyamerch.herokuapp.com',
    'https://biyamerch.herokuapp.com',
    'support.biya.com.ng',
    'merchants.biya.com.ng',
    'https://support.biya.com.ng',
    'https://merchants.biya.com.ng',
    'https://botservice.hosting.portal.azure.net',
    'chrome-extension://fhbjgbiflinjbdggehcddcbncdddomop',
];
const corsOptions = {
    origin(origin, callback) {
        callback(null, true);
        // if (whitelist.indexOf(origin) !== -1 || origin === undefined) {
        //     callback(null, true);
        // } else {
        //     callback(new Error('Not allowed by CORS'));
        // }
    },
    optionsSuccessStatus: 200,
};
/////////////////////////
/// import middleware ///
/////////////////////////
require("./bot/Middleware");
//////////////////////
/// import dialogs ///
/////////////////////
require("./bot/dialogs/badWords");
require("./bot/dialogs/bills/DSTV");
require("./bot/dialogs/bills/GOTV");
require("./bot/dialogs/bills/IKEDC");
require("./bot/dialogs/bills/PayBill");
require("./bot/dialogs/bills/SMILE");
require("./bot/dialogs/bills/spectranet");
require("./bot/dialogs/bills/subscriptions");
require("./bot/dialogs/broadcast");
require("./bot/dialogs/deleteAccount");
require("./bot/dialogs/Emoji");
require("./bot/dialogs/error");
require("./bot/dialogs/firstRun");
require("./bot/dialogs/general/index");
require("./bot/dialogs/general/sendReciept");
require("./bot/dialogs/general/stuckOrCancel");
require("./bot/dialogs/goodbye");
require("./bot/dialogs/goodWords");
require("./bot/dialogs/hello");
require("./bot/dialogs/Help");
require("./bot/dialogs/info");
require("./bot/dialogs/longtime");
require("./bot/dialogs/menu");
require("./bot/dialogs/notify");
require("./bot/dialogs/onDefault");
require("./bot/dialogs/recharge/index");
require("./bot/dialogs/recharge/otherRecharge");
require("./bot/dialogs/recharge/selfRecharge");
require("./bot/dialogs/resolutions/ExpiredCards");
require("./bot/dialogs/resolutions/InsufficientFunds");
require("./bot/dialogs/settings");
require("./bot/dialogs/support");
require("./bot/dialogs/tap2pay/index");
require("./bot/dialogs/thanks");
require("./bot/dialogs/tickets/index");
require("./bot/dialogs/transfer/index");
/////////////////////////
/// import prompts ///
/////////////////////////
const askDstvSmartNo = require("./bot/prompts/askDstvSmartNo");
const askGotvSmartNo = require("./bot/prompts/askGotvSmartNo");
const askIkedcPrepaid = require("./bot/prompts/askIkedcPrepaid");
const askNUBAN = require("./bot/prompts/askNUBAN");
const askPhoneNumber = require("./bot/prompts/askPhoneNumber");
const askPhoneNumberForRecharge = require("./bot/prompts/askPhoneNumberForRecharge");
const askRechargeAmount = require("./bot/prompts/askRechargeAmount");
const askSmileNo = require("./bot/prompts/askSmileNo");
//////////////////////
/// load intents /////
//////////////////////
bot.dialog('/', Intents_1.default);
/// CREATE CUSTOM PROMPTS
askRechargeAmount.create(bot);
askPhoneNumber.create(bot);
askPhoneNumberForRecharge.create(bot);
askDstvSmartNo.create(bot);
askGotvSmartNo.create(bot);
askSmileNo.create(bot);
askIkedcPrepaid.create(bot);
askNUBAN.create(bot);
// Creates and configures an ExpressJS web server.
class App {
    // Run configuration methods on the Express instance.
    constructor() {
        this.express = express();
        this.middleware();
        this.database();
        this.routes();
    }
    // Configure Express middleware.
    middleware() {
        this.express.use(morgan('dev', { stream: logger_1.default.stream }));
        this.express.use(bodyParser.json());
        this.express.use(bodyParser.urlencoded({ extended: false }));
        this.express.use(cors(corsOptions));
        this.express.use(passport.initialize());
        this.express.use(connect_datadog);
    }
    // configure mongo database connection
    database() {
        const MONGODB_CONNECTION = process.env.MONGODB_URI;
        // use q promises
        global.Promise = require('q').Promise;
        mongoose.Promise = global.Promise;
        require('./db/schemas/beneficiary');
        mongoose.connect(MONGODB_CONNECTION, {
            promiseLibrary: global.Promise,
            useMongoClient: true,
        });
        // mongoose.createConnection(MONGODB_CONNECTION);
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error:'));
        db.once('open', () => {
            // we're connected!
            bot.set('storage', new mongo_bot_storage_1.MongoDbBotStorage(new mongo_bot_storage_1.MongoDBStorageClient({
                mongooseConnection: db,
            })));
            logger_1.default.info('db connected');
            require('./monitor');
        });
        process.on('unhandledRejection', (error) => {
            // Will print "unhandledRejection err is not defined"
            logger_1.default.info(error.message);
        });
    }
    // Configure API endpoints.
    routes() {
        const router = express.Router();
        // placeholder route handler
        router.get('/', (req, res, next) => {
            res.json({
                message: 'Look away...',
            });
        });
        this.express.use('/', router);
        /////////////////////
        // bot connector ////
        //////////////////////
        this.express.use('/messages', connector.listen());
        ////////////////////
        // other routes ///
        ///////////////////
        this.express.use('/v1/merchant', admin_1.default);
        this.express.use('/v1/auth', auth_1.default);
        this.express.use('/v1/invoices', invoices_1.default);
        this.express.use('/v1/transactions', transactions_1.default);
        this.express.use('/v1/paystack', paystack_1.default);
        this.express.use('/v1/mwave', mWaveRouter_1.default);
        this.express.use('/v1/broadcast', BroadcastRouter_1.default);
        this.express.use('/v1/tap2pay', Tap2PayRouter_1.default);
        this.express.use('/v1/support', SupportRouter_1.default);
        this.express.use('/v1/monitor', MonitorRouter_1.default);
        this.express.use('/v1/tickets', index_1.default);
    }
}
exports.default = new App().express;
