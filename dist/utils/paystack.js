"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const CircularJSON = require("circular-json");
const SlackService_1 = require("../monitor/SlackService");
const vtpass_1 = require("../utils/vtpass");
const logger_1 = require("../logger");
const shortid = require("simple-id");
const parseStringModule = require("xml2js");
const parseString = parseStringModule.parseString;
let logId;
axios_1.default.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger_1.default.info('Starting Request', Object.assign({ logId: logId, url: request.url }, (request.data && request.data.body)));
    return request;
});
axios_1.default.interceptors.response.use((response) => {
    logger_1.default.info('Response:', Object.assign({}, (response && response.data), { logId: logId }));
    return response;
});
// const getBalance = () => {
//     return axios({
//         baseURL: 'https://api.airvendng.net',
//         method: 'post',
//         params: {
//             username: process.env.AIRVEND_USER,
//             password: process.env.AIRVEND_PASS,
//         },
//         url: `/vtu/balance.php`,
//     });
// };
function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();
const services = [
    'ikedc',
    'ekdc',
    'phdc',
    'ibedc',
    'aedc',
    'jed',
    'kedco',
    'kaedco',
    'airtime',
    'otherr',
    'dstv',
    'gotv',
];
exports.initializePaystack = (token) => (data, metaData) => {
    if (!services.includes(metaData.product.toLowerCase())) {
        throw Error('This service is currently unavailable. Please try again later');
    }
    const postData = {
        amount: data.amount * 100,
        email: data.email,
        metadata: {
            biya: metaData,
            cancel_action: 'https://biya.com.ng/',
        },
        reference: data.reference,
    };
    return new Promise((resolve, reject) => {
        vtpass_1.getBalance()
            .then(({ data }) => {
            const balance = data && data.contents && data.contents.balance;
            const balanceInKobo = Number(balance) * 100;
            if (balanceInKobo >= Number(postData.amount)) {
                return axios_1.default({
                    baseURL: 'https://api.paystack.co/transaction/',
                    data: postData,
                    headers: { authorization: `Bearer ${token}` },
                    method: 'post',
                    url: `initialize`,
                });
            }
            else {
                SlackService_1.SlackNotify({
                    username: 'Danger!',
                    channel: '#vtpass',
                    text: `VTpass balance below threshold`,
                    fields: {
                        Balance: balance,
                    },
                    emoji: ':white_check_mark:',
                });
                throw Error('There was an error, we will rectify this shortly. Thank you!');
            }
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data.data.authorization_url);
            }
            else {
                throw Error('Could not initialize');
            }
        })
            .catch((error) => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.verifyTransaction = (token) => (reference) => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/transaction/',
            headers: { authorization: `Bearer ${token}` },
            method: 'get',
            url: `verify/${reference}`,
        })
            .then((response) => {
            if (response.data && response.data.status) {
                if (response.data.data.status === 'success') {
                    resolve(response.data.data);
                }
                else {
                    const newError = new MyError(response.data.data.gateway_response, 'PaymentError', { reference });
                    logger_1.default.error(CircularJSON.stringify(newError));
                    reject(newError);
                }
            }
            else {
                const newError = new MyError(response.data.data.gateway_response, 'PaymentError', { reference });
                logger_1.default.error(CircularJSON.stringify(newError));
                reject(newError);
            }
        })
            .catch((error) => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.chargeWithPaystack = (token) => (data, metaData) => {
    if (!services.includes(metaData.product.toLowerCase())) {
        throw Error('This service is currently unavailable. Please try again later');
    }
    const postData = {
        amount: data.amount * 100,
        authorization_code: data.paymentAuth,
        email: data.email,
        metadata: {
            biya: metaData,
            cancel_action: 'https://biya.com.ng/',
        },
        reference: data.reference,
    };
    return new Promise((resolve, reject) => {
        vtpass_1.getBalance()
            .then(({ data }) => {
            const balance = data && data.contents && data.contents.balance;
            const balanceInKobo = Number(balance) * 100;
            if (balanceInKobo >= Number(postData.amount)) {
                return axios_1.default({
                    baseURL: 'https://api.paystack.co/transaction/',
                    data: postData,
                    headers: { authorization: `Bearer ${token}` },
                    method: 'post',
                    url: `charge_authorization `,
                });
            }
            else {
                SlackService_1.SlackNotify({
                    username: 'Danger!',
                    channel: '#vtpass',
                    text: `VTpass balance below threshold`,
                    fields: {
                        Balance: balance,
                    },
                    emoji: ':white_check_mark:',
                });
                throw Error('There was an error, we will rectify this shortly. Thank you!');
            }
        })
            .then((response) => {
            if (response.data && response.data.status) {
                if (response.data.data.status === 'success') {
                    resolve(response.data);
                }
                else {
                    const newError = new MyError(response.data.data.gateway_response, 'PaymentError', data);
                    logger_1.default.error(CircularJSON.stringify(newError));
                    reject(newError);
                }
            }
            else {
                const newError = new MyError(response.data.data.gateway_response, 'PaymentError', data);
                logger_1.default.error(CircularJSON.stringify(newError));
                reject(newError);
            }
        })
            .catch((error) => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
// customer, plan, start_date
exports.createPaystackSubscription = (token) => (data) => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${token}` },
            method: 'post',
            url: `subscription`,
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data);
            }
            else {
                throw Error('Could not create subscription');
            }
        })
            .catch((error) => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.disableSubscription = (token) => (data) => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${token}` },
            method: 'post',
            url: `disable`,
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data);
            }
            else {
                throw Error('Could not remove subscription');
            }
        })
            .catch((error) => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.enableSubscription = (token) => (data) => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${token}` },
            method: 'post',
            url: `enable`,
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data);
            }
            else {
                throw Error('Could not remove subscription');
            }
        })
            .catch((error) => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
