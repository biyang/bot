"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const baseUrl = 'http://sandbox.vtpass.com/api';
// custom error
function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();
exports.airtime = async (payload) => {
    // dispense airtime
    const options = {
        // auth: {
        //   password: 'sandbox',
        //   username: 'sandbox@vtpass.com'
        // },
        baseURL: baseUrl,
        method: 'post',
        headers: {
            Authorization: 'Basic c2FuZGJveEB2dHBhc3MuY29tOnNhbmRib3g='
        },
        data: {
            serviceID: payload.network,
            request_id: payload.reference,
            phone: payload.phone,
            amount: payload.amount
        },
        url: `/payfix`
    };
    return await axios_1.default(options);
};
exports.gotv = async (payload) => {
    const variations = {
        lite: 'gotv-lite',
        value: 'gotv-value',
        plus: 'gotv-plus'
    };
    const options = {
        auth: {
            password: 'sandbox',
            username: 'sandbox@vtpass.com'
        },
        baseURL: baseUrl,
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        },
        method: 'post',
        data: {
            serviceID: 'gotv',
            request_id: payload.reference,
            phone: payload.phone,
            billersCode: payload.smartcard,
            variation_code: variations[payload.plan]
        },
        url: `/payfix`
    };
    const dispense = await axios_1.default(options);
    if (dispense.data && dispense.data.code === '000') {
        // // successfull
        return 'success';
    }
    else {
        throw new MyError(dispense.data.code, 'DispenseError', payload);
    }
};
exports.dstv = async (payload) => {
    const variations = {
        lite: 'gotv-lite',
        value: 'gotv-value',
        plus: 'gotv-plus'
    };
    const options = {
        auth: {
            password: 'sandbox',
            username: 'sandbox@vtpass.com'
        },
        baseURL: baseUrl,
        data: {
            serviceID: 'dstv',
            request_id: payload.reference,
            phone: payload.phone,
            billersCode: payload.smartcard,
            variation_code: variations[payload.plan]
        },
        method: 'post',
        url: `/payfix`
    };
    const dispense = await axios_1.default(options);
    if (dispense.data && dispense.data.code === '000') {
        // // successfull
        return 'success';
    }
    else {
        throw new MyError(dispense.data.code, 'DispenseError', payload);
    }
};
