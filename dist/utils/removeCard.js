"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const user_1 = require("../db/schemas/user");
const removeCard = (phone, param = 'authorization') => {
    return new Promise((resolve, reject) => {
        user_1.User.findOneAndUpdate({ phone }, {
            [param]: null
        })
            .then(() => resolve(true))
            .catch(err => reject(err));
    });
};
exports.default = removeCard;
