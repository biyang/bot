"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cloudinary = require("cloudinary");
const jwt = require("jsonwebtoken");
const qrcode_1 = require("qrcode");
cloudinary.config({
    api_key: '416582552898813',
    api_secret: 'CeP_y2g79w4um-bI-uojDRgaEJo',
    cloud_name: 'pitech'
});
const base = 'https://biyatest.herokuapp.com/v1/tickets'; // api base url
const generate = url => {
    return new Promise((resolve, reject) => {
        const file = `${Date.now()} + token.png`;
        qrcode_1.default.toFile(file, url, err => {
            if (err) {
                reject(err);
            }
            cloudinary.uploader.upload(file, result => {
                if (result && result.secure_url) {
                    resolve(result.secure_url);
                }
                else {
                    reject('There was an error');
                }
            });
        });
    });
};
exports.asyncIssue = payload => {
    return new Promise((resolve, reject) => {
        const token = jwt.sign(payload, process.env.jwtSecret);
        if (token) {
            resolve(token);
        }
        reject({ error: 'There was an error signing payload' });
    });
};
exports.dispenseTicket = data => {
    const payload = {
        amount: data.amount,
        destination: data.identity.destination,
        reference: data.reference,
        validity: true
    };
    return new Promise((resolve, reject) => {
        exports.asyncIssue(payload)
            .then(token => {
            const url = `${base}?token=${token}`;
            return generate(url);
        })
            .then(image => {
            resolve(image);
        })
            .catch(error => {
            reject(error);
        });
    });
};
