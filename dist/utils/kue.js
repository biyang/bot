"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable
// import * as kue from 'kue';
const App_1 = require("../App");
const transaction_1 = require("../db/schemas/transaction");
const kue = require('kue');
const parseStringModule = require("xml2js");
const parseString = parseStringModule.parseString;
const speakeasy = require("speakeasy");
const logger_1 = require("../logger");
const CircularJSON = require("circular-json");
// create our job queue
var Redis = require('ioredis');
// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});
// var jobs = kue.createQueue();
jobs.on('error', function (err) {
    logger_1.default.error(err);
});
const vendbackup_1 = require("./vendbackup");
// start redis with $ redis-server
exports.createKueJob = (transaction) => {
    var job = jobs
        .create('dispense', {
        transaction: transaction,
    })
        .attempts(1);
    job.on('complete', function () {
        console.log('Job complete');
    })
        .on('failed', function () {
        console.log('Job failed');
    })
        .on('progress', function (progress) {
        // process.stdout.write( '\r  job #' + job.id + ' ' + progress + '% complete' );
    });
    job.save();
};
exports.createKueVerifyJob = (transaction) => {
    var job = jobs
        .create('verify', {
        transaction: transaction,
    })
        .attempts(100)
        .backoff({ type: 'exponential' });
    job.on('complete', function () {
        console.log('Job complete');
    })
        .on('failed', function () {
        console.log('Job failed');
    })
        .on('progress', function (progress) {
        // process.stdout.write( '\r  job #' + job.id + ' ' + progress + '% complete' );
    });
    job.save();
};
// process video conversion jobs, 1 at a time.
jobs.process('dispense', async (job, done) => {
    const secret = speakeasy.generateSecret({
        length: 20,
    });
    const token = speakeasy.totp({
        digits: 6,
        encoding: 'base32',
        secret: secret.base32,
        step: 200,
    });
    const Jobtransaction = job.data.transaction;
    // const transaction = Jobtransaction;
    const reference = Jobtransaction.reference + token; // add random characters
    const networks = {
        AIRTEL: 1,
        ETISALAT: 4,
        GLO: 3,
        MTN: 2,
    };
    function MyError(message, name, transaction) {
        this.name = name;
        this.transaction = transaction;
        this.message = message || 'Default Message';
        this.stack = new Error().stack;
    }
    MyError.prototype = new Error();
    try {
        const transaction = await transaction_1.Transaction.findOne({
            reference: Jobtransaction.reference,
        });
        if (transaction.status === 'pending') {
            // do dispense
            let dispense;
            if (transaction.product === 'airtime' ||
                transaction.product === 'otherR') {
                const payload = {
                    amount: transaction.amount,
                    network: networks[transaction.description],
                    phone: transaction.identity.phone,
                    reference: reference,
                };
                const action = await vendbackup_1.dispenseAirtimeBackup(payload);
                if (action.data && action.data.orderid) {
                    const verify = await vendbackup_1.verifyDispense(action.data.orderid);
                    if (verify.data &&
                        verify.data.status === 'ORDER_COMPLETED') {
                        await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
                        App_1.bot.beginDialog(transaction.address, '/notify', {
                            amount: transaction.amount,
                            network: transaction.description,
                            number: transaction.identity.phone,
                            type: transaction.product === 'airtime'
                                ? 'self'
                                : 'otherR',
                        });
                        done();
                    }
                    else {
                        // ordercreated, reverify
                        transaction.identity.orderid = action.data.orderid;
                        throw new MyError('Verification Error', 'VerificationError', transaction);
                    }
                }
                else {
                    // no order
                    throw new MyError('Unknown Error', 'UnknownError', transaction);
                }
            }
        }
    }
    catch (error) {
        logger_1.default.error(CircularJSON.stringify(error));
        const message = error.message ? error.message : 'There was an error';
        if (error.name === 'VerificationError') {
            // do createKueJob kue
            exports.createKueVerifyJob(error.transaction);
            return done();
            // ideally send to another kue
        }
        return done(new Error(message));
    }
});
jobs.process('verify', async (job, done) => {
    const Jobtransaction = job.data.transaction;
    try {
        const verify = await vendbackup_1.verifyDispense(Jobtransaction.identity.orderid);
        console.log('verify', verify);
        if (verify.data && verify.data.status === 'ORDER_COMPLETED') {
            await transaction_1.Transaction.findOneAndUpdate({ reference: Jobtransaction.reference }, { status: 'success' });
            App_1.bot.beginDialog(Jobtransaction.address, '/notify', {
                amount: Jobtransaction.amount,
                network: Jobtransaction.description,
                number: Jobtransaction.identity.phone,
                type: Jobtransaction.product === 'airtime' ? 'self' : 'otherR',
            });
            done();
        }
        else {
            throw new Error('Verification Error');
        }
    }
    catch (error) {
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
// start the UI
if (process.env.NODE_ENV === 'production' ||
    process.env.NODE_ENV === 'development') {
    kue.app.listen(3003); // remove for heroku
}
logger_1.default.info('UI started on port 3003');
