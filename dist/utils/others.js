"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getGreetingTime = m => {
    let g = null;
    if (!m || !m.isValid()) {
        return;
    }
    const splitAfternoon = 12;
    const splitEvening = 17;
    const currentHour = parseFloat(m.format('HH'));
    if (currentHour >= splitAfternoon && currentHour <= splitEvening) {
        g = 'Afternoon';
    }
    else if (currentHour >= splitEvening) {
        g = 'Evening';
    }
    else {
        g = 'Morning';
    }
    return g;
};
