"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const merchant_1 = require("../db/schemas/merchant");
const transaction_1 = require("../db/schemas/transaction");
const mailer_1 = require("./mailer");
exports.dispenseGeneral = async (transaction) => {
    try {
        const updatedTransaction = await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
        const updateMerchant = await merchant_1.Merchant.findOneAndUpdate({ symbol: transaction.merchant }, { $push: { transactions: updatedTransaction._id } });
        // notifyMerchant via email
        const mailOptions = {
            from: '"Transaction Alert" <alerts@biya.com.ng>',
            html: `<pre> ${updatedTransaction} </pre>`,
            subject: 'New Transaction',
            to: updateMerchant.email // list of receivers
        };
        const notifyMerchant = await mailer_1.sendMail(mailOptions);
        return notifyMerchant;
    }
    catch (e) {
        const message = e.message ? e.message : 'There was an error';
        return message;
    }
};
