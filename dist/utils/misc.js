"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const logger_1 = require("../logger");
const mWave_1 = require("./mWave");
const data = {
    id: '97161',
    recipient_account_number: '0058209268',
    recipient_bank: '044'
};
mWave_1.retryTransaction(data)
    .then(result => {
    logger_1.default.info(result);
})
    .catch(error => {
    logger_1.default.error(error);
});
