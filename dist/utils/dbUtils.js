"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const shortid = require("simple-id");
const merchant_1 = require("../db/schemas/merchant");
const subscription_1 = require("../db/schemas/subscription");
const transaction_1 = require("../db/schemas/transaction");
const user_1 = require("../db/schemas/user");
const logger_1 = require("../logger");
const finder_1 = require("./finder");
exports.checkIfRegisteredUser = (phone) => user_1.User.findOne({ phone: phone });
exports.cardCheck = (phone) => user_1.User.findOne({ phone: phone });
exports.deriveEmail = (session) => {
    let emailPrefix;
    let email;
    const random = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    const emailSuffix = '@biya.com.ng';
    const source = session.message.source;
    switch (source) {
        case 'facebook':
            emailPrefix = 'fb';
            break;
        case 'telegram':
            emailPrefix = 'tg';
            break;
        case 'skype':
            emailPrefix = 'gl';
            break;
        case 'slack':
            emailPrefix = 'at';
            break;
        default:
            emailPrefix = 'rd';
    }
    email = emailPrefix + random + emailSuffix;
    return email;
};
exports.mockPhone = (session) => 'bot' + session.message.user.id.substr(0, 6);
exports.createTransaction = async (transaction) => {
    try {
        const user = await user_1.User.findOne({ phone: transaction.user });
        const merchant = await merchant_1.Merchant.findOne({
            symbol: transaction.merchant,
        });
        const newTransaction = new transaction_1.Transaction({
            address: transaction.address,
            amount: transaction.amount,
            description: transaction.description,
            identity: transaction.identity,
            merchant: merchant._id,
            product: transaction.product,
            reference: transaction.reference,
            status: transaction.status,
            tags: transaction.tags,
            user: user._id,
        });
        return newTransaction.save();
    }
    catch (e) {
        // throw error
        logger_1.default.error(e);
        throw Error('Error creating transaction');
    }
};
exports.createAirTransaction = async (transaction) => {
    try {
        const user = await user_1.User.findOne({ phone: transaction.user });
        let network;
        if (transaction.description) {
            network = transaction.description;
        }
        else {
            network = await finder_1.getNetwork(transaction.identity.phone);
        }
        const merchant = await merchant_1.Merchant.findOne({
            symbol: transaction.merchant,
        });
        const newTransaction = new transaction_1.Transaction({
            amount: transaction.amount,
            address: transaction.address,
            description: network,
            identity: transaction.identity,
            merchant: merchant._id,
            product: transaction.product,
            reference: transaction.reference,
            status: transaction.status,
            tags: transaction.tags,
            user: user._id,
        });
        return newTransaction.save();
    }
    catch (e) {
        // throw error
        throw Error('Error creating transaction');
    }
};
exports.createLocalSubscription = async (transaction, data) => {
    const newSubscription = new subscription_1.Subscription({
        data,
        identity: transaction.identity,
        address: transaction.address,
        product: transaction.product,
        user: transaction.user,
    });
    const subscription = await newSubscription.save();
    // update user with subscription
    return user_1.User.findByIdAndUpdate(transaction.user, {
        $push: { subscriptions: subscription._id },
    });
};
exports.saveNewUser = (userData) => {
    const newUser = new user_1.User({
        address: userData.address,
        email: userData.email,
        name: userData.name,
        network: userData.network,
        phone: userData.phone,
        userBotId: userData.userBotId,
    });
    return newUser.save();
};
exports.updateTransaction = async (transaction) => {
    // find by reference and update
    return new Promise((resolve, reject) => {
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, transaction)
            .then((doc) => {
            resolve(doc);
        })
            .catch((error) => {
            reject(error);
        });
    });
};
exports.updateAll = async () => {
    const transactions = await transaction_1.Transaction.find({});
    transactions.forEach((trans) => {
        if (trans.user.length === 11) {
            user_1.User.findOne({ phone: trans.user })
                .then((user) => {
                return transaction_1.Transaction.findByIdAndUpdate(trans._id, {
                    user: user._id,
                });
            })
                .then((update) => {
                logger_1.default.info('updated');
            })
                .catch((error) => {
                logger_1.default.error(error);
            });
        }
    });
};
