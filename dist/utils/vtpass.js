"use strict";
var __rest = (this && this.__rest) || function (s, e) {
    var t = {};
    for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p) && e.indexOf(p) < 0)
        t[p] = s[p];
    if (s != null && typeof Object.getOwnPropertySymbols === "function")
        for (var i = 0, p = Object.getOwnPropertySymbols(s); i < p.length; i++) if (e.indexOf(p[i]) < 0)
            t[p[i]] = s[p[i]];
    return t;
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const https = require("https");
const API_URL = 'https://vtpass.com/api';
const httpsAgent = new https.Agent({
    rejectUnauthorized: false,
});
const networks = {
    AIRTEL: 'airtel',
    GLO: 'glo',
    MTN: 'mtn',
    ETISALAT: 'etisalat',
};
const electricityServices = {
    ikedc: 'ikeja-electric',
    phdc: 'portharcourt-electric',
    ekdc: 'eko-electric',
    ibedc: 'ibadan-electric',
    aedc: 'abuja-electric',
    kedco: 'kano-electric',
    kaedco: 'kaduna-electric',
    jed: 'jos-electric',
};
exports.dispenseWithVTPass = (_a) => {
    var { type } = _a, transaction = __rest(_a, ["type"]);
    const isTv = ['dstv', 'gotv'].includes(type);
    const isElectricity = [
        'ikedc',
        'ekdc',
        'phdc',
        'ibedc',
        'aedc',
        'jed',
        'kedco',
        'kaedco',
    ].includes(type.toLowerCase());
    const isAirtime = type === 'airtime' && networks[transaction.description];
    const postData = Object.assign({ request_id: transaction.reference, amount: transaction.amount, phone: transaction.phone }, (isAirtime && { serviceID: networks[transaction.description] }), (isTv && {
        serviceID: type,
        billersCode: transaction.identity.smartCard,
        variation_code: transaction.identity.variation_code,
    }), (isElectricity && {
        serviceID: electricityServices[type.toLowerCase()],
        billersCode: transaction.identity.account,
        variation_code: 'prepaid',
    }));
    return axios_1.default({
        baseURL: API_URL,
        method: 'post',
        data: postData,
        url: `/pay`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};
exports.verifyMerchant = ({ type, smartCard }) => {
    const isTv = ['dstv', 'gotv'].includes(type);
    const isElectricity = [
        'ikedc',
        'ekdc',
        'phdc',
        'ibedc',
        'aedc',
        'jed',
        'kedco',
        'kaedco',
    ].includes(type.toLowerCase());
    const postData = Object.assign({}, (isTv && {
        serviceID: type,
        billersCode: smartCard,
    }), (isElectricity && {
        serviceID: electricityServices[type.toLowerCase()],
        billersCode: smartCard,
        type: 'prepaid',
    }));
    return axios_1.default({
        baseURL: API_URL,
        method: 'post',
        data: postData,
        url: `/merchant-verify`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};
exports.getVariations = (serviceID) => {
    return axios_1.default({
        baseURL: API_URL,
        method: 'get',
        params: {
            serviceID,
        },
        url: `/service-variations`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};
exports.requeryTransaction = (request_id) => {
    return axios_1.default({
        baseURL: API_URL,
        method: 'post',
        data: {
            request_id,
        },
        url: `/requery`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};
exports.getBalance = () => {
    return axios_1.default({
        baseURL: API_URL,
        method: 'get',
        url: `/balance`,
        auth: {
            username: 'hello@biya.com.ng',
            password: '@*cRdjMV44SHyE2',
        },
        httpsAgent,
    });
};
