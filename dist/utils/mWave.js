"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const _ = require("lodash");
const Moneywave = require("moneywave");
const logger_1 = require("../logger");
// const options = {
//   apiKey: 'ts_OD93CAKH30JYNYVQ9VEW',
//   apiSecret: 'ts_9A4JMJ7LCGVQJUJLUUTNK2CXQXEPJI',
//   cache: true
// }
const options = {
    apiKey: 'lv_TKXV3P4FFT6DV5NQLBUE',
    apiSecret: 'lv_WD15FDC14BQQY09BRPWWVQTDYEKOZ5',
    cache: true,
    env: 'live'
};
const moneywave = Moneywave.MakeWave(options);
// Validate Account function which returns a promise
exports.validateAccount = (accountNumber, bankCode) => moneywave.resources.validateBankAccount(accountNumber, bankCode);
exports.validate = (accountNumber, bankCode, callback) => {
    console.log(accountNumber, bankCode);
    moneywave.resources
        .validateBankAccount(accountNumber, bankCode)
        .then(account => {
        if (account && account.status === 'success') {
            callback(account);
        }
        else {
            callback(null);
        }
    })
        .catch(err => {
        const message = err.error.msg ? err.error.msg : 'There was an error';
        logger_1.default.error(message);
        callback(new Error(message));
    });
};
exports.initiateFundsTransfer = () => {
    // use async await
    moneywave.card
        .transfer()
        .then(trans => {
        logger_1.default.info(trans);
    })
        .catch(error => {
        logger_1.default.error(error);
    });
};
// get bank Code
exports.getBankCode = bankName => {
    return new Promise((resolve, reject) => {
        moneywave.resources
            .getBanks()
            .then(result => {
            if (result.status === 'success') {
                const banksObject = result.data;
                const code = _.findKey(banksObject, o => {
                    return o === bankName;
                });
                resolve(code);
            }
        })
            .catch(error => {
            reject(error.error);
        });
    });
};
// tokenize card
exports.tokenizeCard = card => {
    return new Promise((resolve, reject) => {
        moneywave.card
            .tokenizeCard(card)
            .then(result => {
            resolve(result);
        })
            .catch(error => {
            reject(error.error);
        });
    });
};
exports.transfer = data => {
    return new Promise((resolve, reject) => {
        moneywave.card
            .transfer(data)
            .then(result => {
            resolve(result);
        })
            .catch(error => {
            reject(error.error);
        });
    });
};
exports.verify = ref => {
    return new Promise((resolve, reject) => {
        moneywave.card
            .transfer(ref)
            .then(result => {
            resolve(result);
        })
            .catch(error => {
            reject(error.error);
        });
    });
};
exports.retryTransaction = data => {
    return new Promise((resolve, reject) => {
        moneywave.card
            .retryTransaction(data)
            .then(result => {
            resolve(result);
        })
            .catch(error => {
            reject(error.error);
        });
    });
};
