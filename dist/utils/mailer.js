"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const nodemailer = require("nodemailer");
const mg = require("nodemailer-mailgun-transport");
const auth = {
    auth: {
        api_key: 'key-a10209b5c4cbba75fd92f6fefa73050d',
        domain: 'mg.biya.com.ng'
    }
};
const transporter = nodemailer.createTransport(mg(auth));
exports.sendMail = (options) => {
    return new Promise((resolve, reject) => {
        // send mail with defined transport object
        transporter.sendMail(options, (error, info) => {
            if (error) {
                reject(error);
            }
            resolve(info);
        });
    });
};
