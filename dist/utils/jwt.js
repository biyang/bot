"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const jwt = require("jsonwebtoken");
exports.issue = payload => {
    const token = jwt.sign(payload, process.env.jwtSecret, {
        expiresIn: 180 * 60
    });
    return token;
};
exports.verify = (token, callback) => {
    return jwt.verify(token, process.env.jwtSecret, callback);
};
exports.asyncIssue = payload => {
    return new Promise((resolve, reject) => {
        const token = jwt.sign(payload, process.env.jwtSecret, { expiresIn: '1d' });
        if (token) {
            resolve(token);
        }
        reject({ error: 'There was an error signing payload' });
    });
};
exports.asyncVerify = token => {
    return new Promise((resolve, reject) => {
        const decoded = jwt.verify(token, process.env.jwtSecret);
        if (decoded) {
            resolve(decoded);
        }
        reject({ error: 'There was an error verifying token' });
    });
};
