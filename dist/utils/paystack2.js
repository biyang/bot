"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const logger_1 = require("../logger");
const CircularJSON = require("circular-json");
const newTokenSecret = process.env.NEW_PAYSTACK_TOKEN;
// customer, plan, start_date
exports.initNewPaystack = (data, metaData) => {
    const postData = {
        amount: data.amount * 100,
        email: data.email,
        metadata: {
            biya: metaData,
            cancel_action: 'https://biya.com.ng/'
        },
        reference: data.reference
    };
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/transaction/',
            data: postData,
            headers: { authorization: `Bearer ${newTokenSecret}` },
            method: 'post',
            url: `initialize`
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data.data.authorization_url);
            }
            else {
                throw Error('Could not initialize');
            }
        })
            .catch(error => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.newVerifyTransaction = reference => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/transaction/',
            headers: { authorization: `Bearer ${newTokenSecret}` },
            method: 'get',
            url: `verify/${reference}`
        })
            .then((response) => {
            if (response.data && response.data.status) {
                if (response.data.data.status === 'success') {
                    resolve(response.data.data);
                }
                else {
                    const newError = new MyError(response.data.data.gateway_response, 'PaymentError', { reference });
                    logger_1.default.error(CircularJSON.stringify(newError));
                    reject(newError);
                }
            }
            else {
                const newError = new MyError(response.data.data.gateway_response, 'PaymentError', { reference });
                logger_1.default.error(CircularJSON.stringify(newError));
                reject(newError);
            }
        })
            .catch(error => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();
exports.chargeWithNewPaystack = (data, metaData) => {
    const postData = {
        amount: data.amount * 100,
        authorization_code: data.paymentAuth,
        email: data.email,
        metadata: {
            biya: metaData,
            cancel_action: 'https://biya.com.ng/'
        },
        reference: data.reference
    };
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/transaction/',
            data: postData,
            headers: { authorization: `Bearer ${newTokenSecret}` },
            method: 'post',
            url: `charge_authorization `
        })
            .then((response) => {
            if (response.data && response.data.status) {
                if (response.data.data.status === 'success') {
                    resolve(response.data);
                }
                else {
                    const newError = new MyError(response.data.data.gateway_response, 'PaymentError', data);
                    logger_1.default.error(CircularJSON.stringify(newError));
                    reject(newError);
                }
            }
            else {
                const newError = new MyError(response.data.data.gateway_response, 'PaymentError', data);
                logger_1.default.error(CircularJSON.stringify(newError));
                reject(newError);
            }
        })
            .catch(error => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
// customer, plan, start_date
exports.createPaystackSubscription = data => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${newTokenSecret}` },
            method: 'post',
            url: `subscription`
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data);
            }
            else {
                throw Error('Could not create subscription');
            }
        })
            .catch(error => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.disableSubscription = data => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${newTokenSecret}` },
            method: 'post',
            url: `disable`
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data);
            }
            else {
                throw Error('Could not remove subscription');
            }
        })
            .catch(error => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
exports.enableSubscription = data => {
    return new Promise((resolve, reject) => {
        axios_1.default({
            baseURL: 'https://api.paystack.co/',
            data: data,
            headers: { authorization: `Bearer ${newTokenSecret}` },
            method: 'post',
            url: `enable`
        })
            .then((response) => {
            if (response.data && response.data.status) {
                resolve(response.data);
            }
            else {
                throw Error('Could not remove subscription');
            }
        })
            .catch(error => {
            logger_1.default.error(CircularJSON.stringify(error));
            reject(error);
        });
    });
};
// servicdeID:glo
// request_id:lol
// amount:100
// phone:08123456789
// billersCode:
// variation_code:
