"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
///////////////////////////////////////////////////////////////////////
// This file cotaims the  most basic helpers & functions for the bot //
//////////////////////////////////////////////////////////////////////
const _ = require("lodash");
// get a user's name
exports.getName = (session) => {
    const name = _.get(session, 'message.user.name', 'Pal');
    session.userData.name = name.split(' ')[0];
    return _.capitalize(name);
};
// global help text
exports.helpText = `Send 'Menu' to begin or send something like
   * Recharge my line or
   * Buy 500 airtime or
   * Pay dstv bill
   * Send 'help' to show this message again
   * Send 'cancel' to end any operation`;
// check if platform is facebook
exports.isFacebook = (session) => session.message.source === 'facebook';
// check if platform is skype
exports.isSkype = (session) => session.message.source === 'skype';
// check if platform is emulator
exports.isEmulator = (session) => session.message.source === 'emulator';
