"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const logger_1 = require("../logger");
const mailer_1 = require("./mailer");
const shortid = require("simple-id");
let logId;
axios_1.default.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger_1.default.info('Starting Request', Object.assign({ logId: logId, url: request.url }, (request.data && request.data.body)));
    return request;
});
axios_1.default.interceptors.response.use((response) => {
    logger_1.default.info('Response:', Object.assign({}, (response && response.data), { logId: logId }));
    return response;
});
exports.getNetworkSync = (phoneNumber, callback) => {
    const postData = {
        CountryCode: 'NG',
        Type: 'carrier',
    };
    axios_1.default({
        auth: {
            password: process.env.TWILIO_PASS,
            username: process.env.TWILIO_USERNAME,
        },
        baseURL: process.env.TWILIO_BASE,
        method: 'get',
        params: postData,
        url: `/${phoneNumber}`,
    })
        .then((result) => {
        if (result && result.data && result.data.carrier) {
            const network = result.data.carrier.name;
            const networks = {
                MTN: 'MTN',
                'EMST Etisalat': 'ETISALAT',
                'Globacom Ltd': 'GLO',
                'Globacom (GLO)': 'GLO',
                'Celtel Nigeria Limited': 'AIRTEL',
                'Airtel Nigeria': 'AIRTEL',
            };
            callback({ carrier: networks[network] });
        }
        else {
            throw Error('Network not found');
        }
    })
        .catch((err) => {
        const message = err.error.msg
            ? err.error.msg
            : 'There was an error';
        logger_1.default.error(message);
        callback(new Error(message));
    });
};
// getNetwork(recipient)
// .then(network => {
//   session.dialogData.network = network;
//   next();
// })
// .catch(err => {
//   logger.error(err);
//   const message = err.message ? err.message : 'There was an error';
//   session.endDialog(`${message}, please try again`);
// });
exports.getNetwork = async (phoneNumber) => {
    try {
        const postData = {
            CountryCode: 'NG',
            Type: 'carrier',
        };
        const result = await axios_1.default({
            auth: {
                password: process.env.TWILIO_PASS,
                username: process.env.TWILIO_USERNAME,
            },
            baseURL: process.env.TWILIO_BASE,
            method: 'get',
            params: postData,
            url: `/${phoneNumber}`,
        });
        if (result && result.data && result.data.carrier) {
            const network = result.data.carrier.name;
            const networks = {
                MTN: 'MTN',
                'EMST Etisalat': 'ETISALAT',
                'Globacom Ltd': 'GLO',
                'Celtel Nigeria Limited': 'AIRTEL',
                'Airtel Nigeria': 'AIRTEL',
            };
            return networks[network];
        }
        else {
            throw Error('Network not found');
        }
    }
    catch (e) {
        const mailOptions = {
            from: '"⚠" <alerts@biya.com.ng>',
            html: `<pre>${JSON.stringify(e)}</pre>`,
            subject: 'Network Error Alert',
            to: 'olumytee.ojo@gmail.com',
        };
        mailer_1.sendMail(mailOptions);
        logger_1.default.error('Could not find network');
        throw Error('Could not resolve phone number');
    }
};
exports.handleGetNetwork = (result) => {
    if (result && result.data && result.data.carrier) {
        const network = result.data.carrier.name;
        const networks = {
            MTN: 'MTN',
            'EMST Etisalat': 'ETISALAT',
            'Globacom Ltd': 'GLO',
            'Celtel Nigeria Limited': 'AIRTEL',
            'Airtel Nigeria': 'AIRTEL',
        };
        return networks[network];
    }
    else {
        const mailOptions = {
            from: '"⚠" <alerts@biya.com.ng>',
            html: `<pre>${JSON.stringify(result)}</pre>`,
            subject: 'Network Error Alert',
            to: 'olumytee.ojo@gmail.com',
        };
        mailer_1.sendMail(mailOptions);
        logger_1.default.error('Could not find network');
        throw Error('Network not found');
    }
};
