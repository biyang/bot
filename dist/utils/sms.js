"use strict";
// const accountSid = process.env.TWILIO_USERNAME;
// const authToken = process.env.TWILIO_PASS;
Object.defineProperty(exports, "__esModule", { value: true });
// // require the Twilio module and create a REST client
// import * as twilio from 'twilio';
// const client = twilio(accountSid, authToken);
// export const sendText = (to, body) => {
//   return client.messages.create({
//     body,
//     from: 'Biya',
//     to
//   });
// };
const axios_1 = require("axios");
const logger_1 = require("../logger");
const shortid = require("simple-id");
let logId;
axios_1.default.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger_1.default.info('Starting Request', Object.assign({ logId: logId, url: request.url }, (request.data && request.data.body)));
    return request;
});
axios_1.default.interceptors.response.use((response) => {
    logger_1.default.info('Response:', Object.assign({}, (response && response.data), { logId: logId }));
    return response;
});
exports.sendSpectranetPin = (transaction, pin, serial) => {
    const text = `Done! Your spectranet ${transaction.amount} PIN is ${pin}. and the serial number is ${serial}.`;
    const postData = {
        from: 'Biya',
        message: text,
        to: transaction.user,
    };
    axios_1.default({
        auth: {
            password: '13bcf950a6366526964fda4e35da1717',
            username: '424df0de3e1a1167fa4c09c60e5d1f24',
        },
        baseURL: 'https://jusibe.com/smsapi/',
        method: 'post',
        params: postData,
        url: `send_sms`,
    })
        .then((info) => logger_1.default.info('done'))
        .catch((err) => logger_1.default.error(err));
};
exports.sendIkedcPin = (transaction, pin, serial, type) => {
    const text = `Done! Your ${type.toUpperCase()} ${transaction.amount} PIN is ${pin}.`;
    const postData = {
        from: 'Biya',
        message: text,
        to: transaction.user,
    };
    axios_1.default({
        auth: {
            password: '13bcf950a6366526964fda4e35da1717',
            username: '424df0de3e1a1167fa4c09c60e5d1f24',
        },
        baseURL: 'https://jusibe.com/smsapi/',
        method: 'post',
        params: postData,
        url: `send_sms`,
    })
        .then((info) => logger_1.default.info('done'))
        .catch((err) => logger_1.default.error(err));
};
