"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CircularJSON = require("circular-json");
const kue = require('kue');
const logger_1 = require("../logger");
const App_1 = require("../App");
const transaction_1 = require("../db/schemas/transaction");
const common_tags_1 = require("common-tags");
const SlackService_1 = require("../monitor/SlackService");
const vtpass_1 = require("../utils/vtpass");
const sms_1 = require("../utils/sms");
var Redis = require('ioredis');
// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});
// var jobs = kue.createQueue();
jobs.on('error', function (err) {
    logger_1.default.error(err);
});
function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();
exports.doElectirictyDispense = (transaction) => {
    const job = jobs
        .create('electricity', transaction)
        .attempts(1)
        .removeOnComplete(true);
    job.on('complete', function () {
        // send message
        transaction_1.Transaction.findOne({ reference: transaction.reference })
            .then((info) => {
            if (info.address) {
                App_1.bot.loadSession(transaction.address, (err, session) => {
                    session.replaceDialog('/notify', Object.assign({}, info.identity, { type: transaction.product }));
                });
            }
            sms_1.sendIkedcPin(info, info.identity.pin, info.identity.serial, info.product);
            SlackService_1.SlackNotify({
                username: 'New Sale!',
                channel: '#sales',
                text: common_tags_1.stripIndents `Successful ${transaction.product.toUpperCase()} dispense`,
                fields: {
                    Service: transaction.product.toUpperCase(),
                    Amount: transaction.amount,
                    Name: transaction.identity.customername,
                    Account: transaction.identity.customerNumber,
                    PIN: info.identity.pin,
                    Units: info.identity.amount,
                    SerialNo: info.identity.serial,
                },
                emoji: ':white_check_mark:',
            });
            logger_1.default.info('Job complete');
        })
            .catch((error) => logger_1.default.error(error));
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.send('Network error, we will automatically try again in a few seconds.');
            });
        }
        logger_1.default.error('Job failed');
    });
    job.save();
};
jobs.process('electricity', async (job, done) => {
    const transaction = job.data;
    logger_1.default.info(transaction);
    try {
        const invoicePeriod = transaction.identity.invoicePeriod;
        const customerNumber = transaction.identity.customerNumber;
        const phone = transaction.identity.phone;
        const amount = transaction.amount;
        const customerName = transaction.identity.customerName;
        logger_1.default.info({
            amount,
            identity: { customerNumber, invoicePeriod, customerName },
        });
        const { data } = await vtpass_1.dispenseWithVTPass(Object.assign({}, transaction, { amount,
            phone, type: transaction.product }));
        if (data.code && data.code === '000') {
            if (data.response_description === 'TRANSACTION PROCESSING - PENDING') {
                const error = data && data.code;
                throw new MyError(error, 'DispenseError', transaction);
            }
            else {
                const pin = data.purchased_code || data.token || data.mainToken || '';
                const serial = '';
                const amountOfPower = data.units || data.mainTokenUnits || '';
                const updateObject = {
                    status: 'success',
                    'identity.pin': pin,
                    'identity.serial': serial || '',
                    'identity.amount': amountOfPower || '',
                };
                await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, updateObject);
            }
            done();
        }
        else {
            const error = data && data.code;
            logger_1.default.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    }
    catch (error) {
        logger_1.default.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
