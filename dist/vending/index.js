"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CircularJSON = require("circular-json");
const parseStringModule = require("xml2js");
const kue = require('kue');
const logger_1 = require("../logger");
const vendbackup_1 = require("../utils/vendbackup");
const App_1 = require("../App");
const transaction_1 = require("../db/schemas/transaction");
const mailer_1 = require("../utils/mailer");
const common_tags_1 = require("common-tags");
const SlackService_1 = require("../monitor/SlackService");
const vtpass_1 = require("../utils/vtpass");
const parseString = parseStringModule.parseString;
var Redis = require('ioredis');
// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});
// var jobs = kue.createQueue();
jobs.on('error', function (err) {
    logger_1.default.error(err);
});
function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();
exports.dispenseAirtime = (transaction) => {
    const job = jobs
        .create('airtime', transaction)
        .attempts(1)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    job.on('complete', function () {
        // send message
        SlackService_1.SlackNotify({
            username: 'New Sale via backup!',
            channel: '#sales',
            text: common_tags_1.stripIndents `Succesful airtime dispense`,
            fields: {
                Network: transaction.description,
                Amount: transaction.amount,
                Number: transaction.identity.phone,
                Name: transaction.identity.customerName,
            },
            emoji: ':white_check_mark:',
        });
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' })
            .then(console.log)
            .catch(console.log);
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    network: transaction.description,
                    amount: transaction.amount,
                    number: transaction.identity.phone,
                    type: transaction.product === 'airtime' ? 'self' : 'otherR',
                });
            });
        }
        logger_1.default.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.send('Network error, we will automatically try again in a few seconds.');
            });
        }
        // dispenseAirtimeB(transaction);
        logger_1.default.error('Job failed');
    });
    job.save();
};
exports.dispenseAirtimeB = (transaction) => {
    const backupjob = jobs
        .create('airtimeBackup', transaction)
        .attempts(6)
        .removeOnComplete(true);
    backupjob
        .on('complete', function () {
        // send message
        SlackService_1.SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: common_tags_1.stripIndents `Succesful airtime dispense`,
            fields: {
                Network: transaction.description,
                Amount: transaction.amount,
                Number: transaction.identity.phone,
                Name: transaction.identity.customerName,
            },
            emoji: ':white_check_mark:',
        });
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' })
            .then(console.log)
            .catch(console.log);
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    network: transaction.description,
                    amount: transaction.amount,
                    number: transaction.identity.phone,
                    type: transaction.product === 'airtime'
                        ? 'self'
                        : 'otherR',
                });
            });
        }
        logger_1.default.info('Job complete');
    })
        .on('failed', function () {
        const mailOptions = {
            from: '"Dispense Error Alert ⚠" <alerts@biya.com.ng>',
            html: `<pre>${transaction}</pre>`,
            subject: 'Dispense Error Alert',
            to: 'olumytee.ojo@gmail.com',
        };
        SlackService_1.SlackNotify({
            username: 'Failed dispense',
            channel: '#errors',
            text: common_tags_1.stripIndents `Failed Airtime dispense`,
            fields: {
                Network: transaction.description,
                Amount: transaction.amount,
                Number: transaction.identity.phone,
                Name: transaction.identity.customerName,
            },
            emoji: ':bangbang:',
        });
        mailer_1.sendMail(mailOptions);
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'pending' })
            .then(console.log)
            .catch(console.log);
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.send('Network error, we will automatically try again in a few seconds.');
            });
        }
        logger_1.default.error('Job failed');
        // dispenseAirtime(transaction);
    });
    backupjob.save();
};
jobs.process('airtime', async (job, done) => {
    const transaction = job.data;
    try {
        const reference = transaction.reference;
        const phone = transaction.identity.phone;
        const amount = transaction.amount;
        const network = transaction.description;
        const dispense = await vendbackup_1.dispenseAirtimeBackup({
            amount,
            phone,
            network,
        });
        if (dispense.data) {
            if (dispense.data && dispense.data.status === 'ORDER_RECEIVED') {
                await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
                done();
            }
            else {
                logger_1.default.error('error 1', dispense.data);
                throw new MyError(dispense.data.message, 'DispenseError', transaction);
            }
        }
        else {
            logger_1.default.error('error 2', dispense.data);
            throw new MyError(dispense.data.message, 'DispenseError', transaction);
        }
        // const dispense = await credit(network, phone, amount, reference);
        // if (dispense.data) {
        //     if (dispense.data.status === 'OK') {
        //         await Transaction.findOneAndUpdate(
        //             { reference: transaction.reference },
        //             { status: 'success' }
        //         );
        //         done();
        //     } else {
        //         logger.error('error 1', dispense.data);
        //         throw new MyError(
        //             dispense.data.message,
        //             'DispenseError',
        //             transaction
        //         );
        //     }
        // } else {
        //     logger.error('error 2', dispense.data);
        //     throw new MyError(
        //         dispense.data.message,
        //         'DispenseError',
        //         transaction
        //     );
        // }
    }
    catch (error) {
        logger_1.default.error('error 3', CircularJSON.stringify(error));
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
jobs.process('airtimeBackup', async (job, done) => {
    logger_1.default.info('running backup job');
    const transaction = job.data;
    try {
        const phone = transaction.identity.phone;
        const amount = transaction.amount;
        const network = transaction.description;
        const { data } = await vtpass_1.dispenseWithVTPass(Object.assign({}, transaction, { network,
            amount,
            phone, type: 'airtime' }));
        if (data.code && data.code === '000') {
            await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
            done();
        }
        else {
            logger_1.default.info(data);
            const error = data && data.code;
            logger_1.default.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    }
    catch (error) {
        logger_1.default.error('error 3', CircularJSON.stringify(error));
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
exports.doDstvDispense = (transaction) => {
    const job = jobs
        .create('dstv', transaction)
        .attempts(5)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    logger_1.default.info(transaction);
    job.on('complete', function () {
        // send message
        SlackService_1.SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: common_tags_1.stripIndents `Successful dstv dispense`,
            fields: {
                Service: 'DSTV',
                Amount: transaction.amount,
                Name: transaction.identity.customerName,
                Account: transaction.identity.customerNumber,
            },
            emoji: ':white_check_mark:',
        });
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' })
            .then((info) => logger_1.default.info(info))
            .catch((error) => logger_1.default.error(error));
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    amount: transaction.amount,
                    type: transaction.product,
                });
            });
        }
        logger_1.default.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.send('Network error, we will automatically try again in a few seconds.');
            });
        }
        logger_1.default.error('Job failed');
    });
    job.save();
};
jobs.process('dstv', async (job, done) => {
    const transaction = job.data;
    logger_1.default.info(transaction);
    try {
        const amount = transaction.amount;
        const phone = transaction.identity.phone;
        const { data } = await vtpass_1.dispenseWithVTPass(Object.assign({}, transaction, { amount,
            phone, type: 'dstv' }));
        if (data.code && data.code === '000') {
            await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
            done();
        }
        else {
            const error = data && data.code;
            logger_1.default.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    }
    catch (error) {
        logger_1.default.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
exports.doGotvDispense = (transaction) => {
    const job = jobs
        .create('gotv', transaction)
        .attempts(5)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    logger_1.default.info(transaction);
    job.on('complete', function () {
        // send message
        SlackService_1.SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: common_tags_1.stripIndents `Successful gotv dispense`,
            fields: {
                Service: 'GoTV',
                Amount: transaction.amount,
                Name: transaction.identity.customerName,
                Account: transaction.identity.customerNumber,
            },
            emoji: ':white_check_mark:',
        });
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' })
            .then((info) => logger_1.default.info(info))
            .catch((error) => logger_1.default.error(error));
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    amount: transaction.amount,
                    type: transaction.product,
                });
            });
        }
        logger_1.default.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.send('Network error, we will automatically try again in a few seconds.');
            });
        }
        logger_1.default.error('Job failed');
    });
    job.save();
};
jobs.process('gotv', async (job, done) => {
    const transaction = job.data;
    logger_1.default.info(transaction);
    try {
        const amount = transaction.amount;
        const phone = transaction.identity.phone;
        const { data } = await vtpass_1.dispenseWithVTPass(Object.assign({}, transaction, { amount,
            phone, type: 'gotv' }));
        if (data.code && data.code === '000') {
            await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
            done();
        }
        else {
            const error = data && data.code;
            logger_1.default.error('error', error);
            throw new MyError(error, 'DispenseError', transaction);
        }
    }
    catch (error) {
        logger_1.default.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
