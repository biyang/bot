"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CircularJSON = require("circular-json");
const parseStringModule = require("xml2js");
const kue = require('kue');
const logger_1 = require("../logger");
const App_1 = require("../App");
const transaction_1 = require("../db/schemas/transaction");
const common_tags_1 = require("common-tags");
const SlackService_1 = require("../monitor/SlackService");
const airvend_1 = require("../utils/airvend");
const parseString = parseStringModule.parseString;
var Redis = require('ioredis');
// create our job queue
const redisURL = process.env.REDIS_URL;
var jobs = kue.createQueue({
    redis: {
        createClientFactory: function () {
            return new Redis(redisURL);
        },
    },
});
// var jobs = kue.createQueue();
jobs.on('error', function (err) {
    logger_1.default.error(err);
});
function MyError(message, name, transaction) {
    this.name = name;
    this.transaction = transaction;
    this.message = message || 'Default Message';
    this.stack = new Error().stack;
}
MyError.prototype = new Error();
exports.doSmileDispense = (transaction) => {
    const job = jobs
        .create('smile', transaction)
        .attempts(5)
        .backoff({ type: 'exponential' })
        .removeOnComplete(true);
    logger_1.default.info(transaction);
    job.on('complete', function () {
        // send message
        SlackService_1.SlackNotify({
            username: 'New Sale!',
            channel: '#sales',
            text: common_tags_1.stripIndents `Successful SMILE dispense`,
            fields: {
                Service: 'SMILE',
                Amount: transaction.amount,
                Name: transaction.identity.customerName,
                Account: transaction.identity.customerNumber,
            },
            emoji: ':white_check_mark:',
        });
        transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' })
            .then((info) => logger_1.default.info(info))
            .catch((error) => logger_1.default.error(error));
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.replaceDialog('/notify', {
                    plan: transaction.description,
                    type: transaction.product,
                });
            });
        }
        logger_1.default.info('Job complete');
    }).on('failed', function () {
        // go to next API
        if (transaction.address) {
            App_1.bot.loadSession(transaction.address, (err, session) => {
                session.send('Network error, we will automatically try again in a few seconds.');
            });
        }
        logger_1.default.error('Job failed');
    });
    job.save();
};
jobs.process('smile', async (job, done) => {
    const transaction = job.data;
    logger_1.default.info(transaction);
    try {
        const invoicePeriod = transaction.identity.invoicePeriod;
        const customerNumber = transaction.identity.customerNumber;
        const amount = transaction.amount;
        const customerName = transaction.identity.customerName;
        logger_1.default.info({
            amount,
            identity: { customerNumber, invoicePeriod, customerName },
        });
        const dispense = await airvend_1.dispenseSMILE(transaction);
        if (dispense.status) {
            if (dispense.status === 200) {
                parseString(dispense.data, async (err, result) => {
                    try {
                        if (err) {
                            logger_1.default.error(err);
                            throw Error('There has been a problem');
                        }
                        if (result &&
                            result.VendResponse.ResponseCode[0] === '0') {
                            await transaction_1.Transaction.findOneAndUpdate({ reference: transaction.reference }, { status: 'success' });
                            done();
                        }
                        else {
                            logger_1.default.error(result);
                            const error = result && result.VendResponse.ResponseCode[0];
                            logger_1.default.error('error', error);
                            throw new MyError(error, 'DispenseError', transaction);
                        }
                    }
                    catch (error) {
                        logger_1.default.error('error 2', CircularJSON.stringify(error));
                        const message = error.message
                            ? error.message
                            : 'There was an error';
                        return done(new Error(message));
                    }
                });
            }
            else {
                logger_1.default.error('error 1', dispense.data);
                throw new MyError(dispense.data.message, 'DispenseError', transaction);
            }
        }
        else {
            logger_1.default.error('error 2', dispense.data);
            throw new MyError(dispense.data.message, 'DispenseError', transaction);
        }
    }
    catch (error) {
        logger_1.default.error('error 3', {
            error: CircularJSON.stringify(error),
            transaction,
        });
        const message = error.message ? error.message : 'There was an error';
        return done(new Error(message));
    }
});
