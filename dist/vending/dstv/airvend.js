"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const shortid = require("simple-id");
const logger_1 = require("../../logger");
let logId;
axios_1.default.interceptors.request.use((request) => {
    logId = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    logger_1.default.info('Starting Request', Object.assign({ logId: logId, url: request.url }, (request.data && request.data.body)));
    return request;
});
axios_1.default.interceptors.response.use((response) => {
    logger_1.default.info('Response:', Object.assign({}, (response && response.data), { logId: logId }));
    return response;
});
const networks = {
    AIRTEL: 1,
    GLO: 3,
    MTN: 2,
    ETISALAT: 4,
};
const airvendUsername = process.env.AIRVEND_USER;
const airvendPassword = process.env.AIRVEND_PASS;
exports.airvend_credit = (amount, phone, network) => {
    const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    const postData = {
        amount: amount,
        msisdn: phone,
        networkid: networks[network],
        password: airvendPassword,
        ref: reference,
        type: '1',
        username: airvendUsername,
    };
    return axios_1.default({
        baseURL: 'https://api.airvendng.net/se/payments',
        method: 'post',
        params: postData,
        url: `/vtu/`,
    });
};
