"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const shortid = require("simple-id");
const CryptoJS = require('crypto-js');
const URL = 'https://api.cowriesys.com:2443';
const ClientId = 'biya.com.ng';
const ClientKey = 'V6Q3dG8Lr+DZwxKTEIQ/b6r1ShBNf7zLgU4uHpQIxUY=';
const sign = (key, nonce, message) => {
    var hmac = CryptoJS.HmacSHA256(nonce + message, CryptoJS.enc.Base64.parse(key));
    var digest = CryptoJS.enc.Base64.stringify(hmac);
    return digest;
};
const networks = {
    AIRTEL: 'AIR',
    GLO: 'GLO',
    MTN: 'MTN',
    ETISALAT: 'ETI'
};
exports.credit = (net, msisdn, amount, xref) => {
    const api = `?net=${networks[net]}&msisdn=${msisdn}&amount=${amount}&xref=${xref}`;
    const nonce = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
    const signature = sign(ClientKey, nonce, api);
    return axios_1.default({
        baseURL: URL,
        method: 'get',
        url: `/airtime/Credit`,
        headers: {
            'Content-Type': 'application/json',
            ClientId: ClientId,
            Nonce: nonce,
            Signature: signature
        },
        params: {
            net: networks[net],
            msisdn: msisdn,
            amount: amount,
            xref: xref
        }
    });
};
