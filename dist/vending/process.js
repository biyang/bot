"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const transaction_1 = require("../db/schemas/transaction");
const index_1 = require("./index");
const electricity_1 = require("./electricity");
const smile_1 = require("./smile");
const logger_1 = require("../logger");
async function processTransaction(reference) {
    // find transaction
    // swicth kue based on transaction type
    const transaction = await transaction_1.Transaction.findOne({ reference });
    logger_1.default.info(transaction, 'transaction');
    if (transaction) {
        if (['init', 'pending'].includes(transaction.status)) {
            switch (transaction.product.toLowerCase()) {
                case 'ikedc':
                case 'ibedc':
                case 'ekdc':
                case 'phdc':
                case 'aedc':
                case 'jed':
                case 'kedco':
                case 'kaedco':
                    electricity_1.doElectirictyDispense(transaction);
                    break;
                case 'airtime':
                case 'otherr':
                    index_1.dispenseAirtimeB(transaction);
                    break;
                case 'dstv':
                    index_1.doDstvDispense(transaction);
                    break;
                case 'gotv':
                    index_1.doGotvDispense(transaction);
                    break;
                case 'smile':
                    smile_1.doSmileDispense(transaction);
                    break;
                default:
                    break;
            }
        }
    }
}
exports.default = processTransaction;
