"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Agenda = require("agenda");
const App_1 = require("../App");
const logger_1 = require("../logger");
const user_1 = require("./schemas/user");
const agenda = new Agenda({
    db: { address: process.env.MONGODB_URI, collection: 'jobs' },
});
agenda.on('ready', () => {
    logger_1.default.info('agenda ready');
    agenda.start();
});
agenda.define('setReminder', (job, done) => {
    // set setReminder to 27 days after data of renewal
    const data = job.attrs.data;
    // find User
    user_1.User.findOne({ userBotId: data.data.userId })
        .then((user) => {
        App_1.bot.beginDialog(user.address, '/reminder', { data: data });
    })
        .catch((error) => {
        logger_1.default.error(error);
    });
    done();
});
exports.default = agenda;
