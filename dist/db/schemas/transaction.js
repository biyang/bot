"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
// remember to map to customer and merchant moodels
exports.transactionSchema = new mongoose_1.Schema({
    amount: { type: String, required: true },
    address: Object,
    description: { type: String, required: true },
    identity: Object,
    merchant: { ref: 'Merchant', type: String },
    product: { type: String, required: true },
    reference: { type: String, required: true, unique: true },
    status: { type: String, required: true },
    tags: { type: Array, required: true },
    user: { ref: 'User', type: String }
}, { timestamps: true });
exports.Transaction = mongoose_1.model('Transaction', exports.transactionSchema);
