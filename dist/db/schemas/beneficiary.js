"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
const logger_1 = require("../../logger");
exports.beneficiarySchema = new mongoose_1.Schema({
    identifier: mongoose_1.Schema.Types.Mixed,
    name: { type: String },
    user: { ref: 'User', type: String }
}, { timestamps: true });
const handleE11000 = (error, res, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        logger_1.default.error(error);
        next(new Error('There was a duplicate key error'));
    }
    else {
        next();
    }
};
exports.beneficiarySchema.post('save', handleE11000);
exports.beneficiarySchema.post('update', handleE11000);
exports.beneficiarySchema.post('findOneAndUpdate', handleE11000);
exports.beneficiarySchema.post('insertMany', handleE11000);
exports.Beneficiary = mongoose_1.model('Beneficiary', exports.beneficiarySchema);
