"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const mongoose_1 = require("mongoose");
const logger_1 = require("../../logger");
/// DB AUTH
// db.auth("username", "password")
exports.userSchema = new mongoose_1.Schema({
    activated: Boolean,
    address: Object,
    addresses: { type: Object },
    whitelisted: Boolean,
    authorization: Object,
    billsAuth: Object,
    beneficiaries: [{ ref: 'Beneficiary', type: mongoose_1.Schema.Types.ObjectId }],
    email: { type: String, required: true },
    initialReference: String,
    name: String,
    network: String,
    passcode: String,
    paymentAuth: Object,
    phone: { type: String, unique: true },
    primaryChannel: String,
    subscriptions: [{ ref: 'Subscription', type: mongoose_1.Schema.Types.ObjectId }],
    transactions: [{ ref: 'Transaction', type: mongoose_1.Schema.Types.ObjectId }],
    userBotId: { type: String },
    wallet: { type: Object }
}, { timestamps: true, strict: false });
exports.userSchema.pre('save', function (next) {
    const user = this;
    if (!user.isModified('passcode')) {
        return next();
    }
    bcrypt.hash(user.passcode, 10, (err, hash) => {
        if (err) {
            return next(err);
        }
        user.passcode = hash;
        next();
    });
});
const handleE11000 = (error, res, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        logger_1.default.error(error);
        next(new Error('There was a duplicate key error'));
    }
    else {
        next();
    }
};
exports.userSchema.post('save', handleE11000);
exports.userSchema.post('update', handleE11000);
exports.userSchema.post('findOneAndUpdate', handleE11000);
exports.userSchema.post('insertMany', handleE11000);
exports.User = mongoose_1.model('User', exports.userSchema);
