"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const mongoose_1 = require("mongoose");
exports.merchantSchema = new mongoose_1.Schema({
    email: { type: String, required: true, unique: true },
    logoURL: String,
    name: String,
    password: String,
    phone: String,
    symbol: { type: String, unique: true },
    transactions: [{ ref: 'Transaction', type: String }],
    wallet: { type: Object },
}, { timestamps: true });
exports.merchantSchema.pre('save', function (next) {
    const merchant = this;
    if (!merchant.isModified('password')) {
        return next();
    }
    bcrypt.hash(merchant.password, 10, (err, hash) => {
        if (err) {
            return next(err);
        }
        merchant.password = hash;
        next();
    });
});
exports.merchantSchema.pre('save', function (next) {
    const merchant = this;
    if (merchant.symbol) {
        return next();
    }
    const symbol = 'BT' + merchant.name.substr(0, 4);
    merchant.symbol = symbol.toUpperCase();
    next();
});
const handleE11000 = (error, res, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        next(new Error('There was a duplicate key error'));
    }
    else {
        next();
    }
};
exports.merchantSchema.post('save', handleE11000);
exports.merchantSchema.post('update', handleE11000);
exports.merchantSchema.post('findOneAndUpdate', handleE11000);
exports.merchantSchema.post('insertMany', handleE11000);
exports.Merchant = mongoose_1.model('Merchant', exports.merchantSchema);
