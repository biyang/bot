"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const mongoose_1 = require("mongoose");
exports.superuserSchema = new mongoose_1.Schema({
    password: { type: String, required: true },
    roles: Array,
    username: { type: String, required: true, unique: true }
}, { timestamps: true });
exports.superuserSchema.pre('save', function (next) {
    const superuser = this;
    if (!superuser.isModified('password')) {
        return next();
    }
    bcrypt.hash(superuser.password, 10, (err, hash) => {
        if (err) {
            return next(err);
        }
        superuser.password = hash;
        next();
    });
});
const handleE11000 = (error, res, next) => {
    if (error.name === 'MongoError' && error.code === 11000) {
        next(new Error('There was a duplicate key error'));
    }
    else {
        next();
    }
};
exports.superuserSchema.post('save', handleE11000);
exports.superuserSchema.post('update', handleE11000);
exports.superuserSchema.post('findOneAndUpdate', handleE11000);
exports.superuserSchema.post('insertMany', handleE11000);
exports.Superuser = mongoose_1.model('Superuser', exports.superuserSchema);
