"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.utteranceSchema = new mongoose_1.Schema({
    event: mongoose_1.Schema.Types.Mixed,
    type: { type: String }
}, { timestamps: true });
exports.Utterances = mongoose_1.model('Utterances', exports.utteranceSchema);
