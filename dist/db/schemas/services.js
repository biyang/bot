"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.serviceSchema = new mongoose_1.Schema({
    service: mongoose_1.Schema.Types.Mixed,
    status: { type: String },
    since: Date
}, { timestamps: true });
exports.Services = mongoose_1.model('Services', exports.serviceSchema);
