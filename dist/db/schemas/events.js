"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.eventSchema = new mongoose_1.Schema({
    event: mongoose_1.Schema.Types.Mixed
}, { timestamps: true });
exports.Events = mongoose_1.model('Events', exports.eventSchema);
