"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = require("mongoose");
exports.subscriptionSchema = new mongoose_1.Schema({
    data: { type: Object, required: true },
    identity: Object,
    address: Object,
    product: String,
    user: { ref: 'User', type: String }
}, { timestamps: true });
exports.Subscription = mongoose_1.model('Subscription', exports.subscriptionSchema);
