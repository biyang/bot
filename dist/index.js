"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
// tslint:disable-next-line
require('dotenv').config({ silent: false });
const debug = require("debug");
const http = require("http");
const logger_1 = require("./logger");
const App_1 = require("./App");
const SlackService_1 = require("./monitor/SlackService");
debug('ts-express:server');
const nPort = normalizePort(process.env.PORT || 3001);
App_1.default.set('port', nPort);
const server = http.createServer(App_1.default);
server.listen(nPort);
server.on('error', onError);
server.on('listening', onListening);
function normalizePort(val) {
    const port = typeof val === 'string' ? parseInt(val, 10) : val;
    if (isNaN(port)) {
        return val;
    }
    else if (port >= 0) {
        return port;
    }
    else {
        return false;
    }
}
function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }
    const bind = typeof nPort === 'string' ? 'Pipe ' + nPort : 'Port ' + nPort;
    switch (error.code) {
        case 'EACCES':
            logger_1.default.error(`${bind} requires elevated privileges`);
            process.exit(1);
            break;
        case 'EADDRINUSE':
            logger_1.default.error(`${bind} is already in use`);
            process.exit(1);
            break;
        default:
            throw error;
    }
    if (process.env.NODE_ENV === 'production') {
        SlackService_1.default.bug('Failed Deployment');
    }
}
function onListening() {
    const addr = server.address();
    const bind = typeof addr === 'string' ? `pipe ${addr}` : `port ${addr.port}`;
    logger_1.default.info(`Listening on ${bind}`);
    debug(`Listening on ${bind}`);
    if (process.env.NODE_ENV === 'production') {
        SlackService_1.default.success('Successfully Deployed');
    }
}
