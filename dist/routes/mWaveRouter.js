"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const _ = require("lodash");
const queryString = require("query-string");
const App_1 = require("../App");
const transaction_1 = require("../db/schemas/transaction");
const user_1 = require("../db/schemas/user");
const logger_1 = require("../logger");
const jwt_1 = require("../utils/jwt");
const mailer_1 = require("../utils/mailer");
const mWave_1 = require("../utils/mWave");
class WaveRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    async testtrans(req, res, next) {
        const ref = req.params.ref;
        const transaction = await transaction_1.Transaction.findOneAndUpdate({ reference: ref }, { status: 'success' });
        const user = await user_1.User.findById(transaction.user);
        res.status(401).send({
            message: 'Format is Authorization: Bearer [token]',
            user: user,
        });
    }
    // link 'http://localhost:3001/v1/mwave/confirm/qdslx9ic?rc=00&transa=200'
    async sendSuccess(req, res, next) {
        let address;
        try {
            const ref = req.params.ref;
            const transaction = await transaction_1.Transaction.findOne({ reference: ref });
            const user = await user_1.User.findById(transaction.user);
            // // save beneficiary
            address = user.addresses[transaction.identity.sourceChannel];
            const url = req.url;
            const parsed = queryString.parse(url.split('?')[1]);
            // verify transaction
            if (parsed['rc'] === '00') {
                await transaction_1.Transaction.findOneAndUpdate({ reference: ref }, { status: 'success' });
                App_1.bot.beginDialog(address, '/notify', {
                    transaction: transaction,
                    type: 'transfer',
                });
                res.redirect('https://biya.com.ng/success');
            }
            else {
                await transaction_1.Transaction.findOneAndUpdate({ reference: ref }, { status: 'failed' });
                const responseCode = parsed['rc'];
                let responseMessage;
                // swicth responsemessage
                switch (responseCode) {
                    case 'RR-57':
                        responseMessage =
                            'Transaction declined by financial institution. Please try again later. Thanks!';
                        break;
                    case 'RR-91':
                        responseMessage =
                            'Bank or Switch network error. Please try again later. Thanks!';
                        break;
                    default:
                        responseMessage = `Transfer could not be completed. Please try again later. Thanks.`;
                        break;
                }
                throw Error(`${responseMessage}`);
            }
            // res.sendStatus(200);
        }
        catch (error) {
            logger_1.default.error(error);
            const mailOptions = {
                from: '"Transfer Error Alert ⚠" <alerts@biya.com.ng>',
                html: `<pre> ${error} </pre>`,
                subject: 'Trabsfer Error Alert',
                to: 'olumytee.ojo@gmail.com',
            };
            mailer_1.sendMail(mailOptions);
            const message = error.message
                ? error.message
                : 'There was an error';
            if (address) {
                App_1.bot.beginDialog(address, '/error', { error: message });
            }
            // res.status(401).json({ message: message, status: 401,})
            res.redirect('https://biya.com.ng/error');
        }
    }
    async chargeWithoutToken(req, res, next) {
        let token;
        if (req.headers && req.headers.authorization) {
            const headers = req.headers;
            const parts = headers.authorization.split(' ');
            if (parts.length === 2) {
                const scheme = parts[0];
                const credentials = parts[1];
                if (/^Bearer$/i.test(scheme)) {
                    token = credentials;
                }
            }
            else {
                logger_1.default.info('Format is Authorization: Bearer [token]');
                res.status(401).send({
                    message: 'Format is Authorization: Bearer [token]',
                });
            }
        }
        else {
            logger_1.default.info('No auth header found');
            res.status(401).send({
                message: 'No authorization header was found',
            });
        }
        const decoded = await jwt_1.asyncVerify(token);
        const parameters = ['card_no', 'cvv', 'expiry_month', 'expiry_year'];
        const formData = _.pick(req.body, parameters);
        try {
            const transaction = await transaction_1.Transaction.findOne({
                reference: decoded.reference,
            });
            const user = await user_1.User.findOne({
                phone: decoded.user,
            });
            const data = {
                amount: transaction.amount,
                card_no: formData.card_no,
                cvv: formData.cvv,
                email: user.email,
                expiry_month: formData.expiry_month,
                expiry_year: formData.expiry_year,
                fee: 5,
                firstname: user.name ? user.name : 'Biya',
                lastname: user.phone,
                medium: 'mobile',
                phonenumber: '+234' + user.phone.slice(1),
                recipient_account_number: transaction.identity.accountNumber,
                recipient_bank: transaction.identity.bankCode,
                redirecturl: `${process.env.CURRENT_ENV}mwave/confirm/${decoded.reference}`,
            };
            const transferInit = await mWave_1.transfer(data);
            if (transferInit.status === 'success') {
                const transactionData = transferInit.data;
                if (transactionData.failedValidation &&
                    transactionData.authurl) {
                    res.status(200).send({
                        data: transactionData.authurl,
                        message: 'url',
                    });
                }
                else {
                    res.status(200).send({
                        message: 'success',
                    });
                }
            }
        }
        catch (error) {
            let message = error.message ? error.message : 'There was an error';
            logger_1.default.error(error.error);
            if (error.msg) {
                message = error.msg;
            }
            res.status(504).send({
                message: message,
                status: 'Error',
            });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/charge', this.chargeWithoutToken);
        this.router.get('/confirm/:ref', this.sendSuccess);
    }
}
exports.WaveRouter = WaveRouter;
// Create the MerchantRouter, and export its configured Express.Router
const mWaveRoutes = new WaveRouter();
mWaveRoutes.init();
exports.default = mWaveRoutes.router;
