"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const passportJwt = require("passport-jwt");
const merchant_1 = require("../../db/schemas/merchant");
const JwtStrategy = passportJwt.Strategy;
const ExtractJwt = passportJwt.ExtractJwt;
const opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
opts.secretOrKey = process.env.jwtSecret;
// opts.issuer = 'api.biya.com.ng';
// opts.audience = 'merchants.biya.com.ng';
// JWT Login Strategy for Merchants
exports.jwtLogin = new JwtStrategy(opts, (payload, done) => {
    merchant_1.Merchant.findOne({ email: payload.email }, (err, merchant) => {
        if (err) {
            return done(err, false);
        }
        if (merchant) {
            return done(null, merchant);
        }
        else {
            return done(null, false);
            // or you could create a new account
        }
    });
});
// API Validation Strategy for Merchants
exports.apiValidation = new JwtStrategy(opts, (payload, done) => {
    merchant_1.Merchant.findOne({ email: payload.email }, (err, merchant) => {
        if (err) {
            return done(err, false);
        }
        if (merchant) {
            return done(null, merchant);
        }
        else {
            return done(null, false);
            // or you could create a new account
        }
    });
});
