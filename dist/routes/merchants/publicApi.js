"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const transaction_1 = require("../../db/schemas/transaction");
const logger_1 = require("../../logger");
class PublicApiRouter {
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    createToken(parameters) {
        //
    }
    async initialize(req, res, next) {
        try {
            // create invoice, expects user phone number, reference, amount, product, description, merchant id
            const details = req.value.body;
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    async verify(req, res, next) {
        try {
            // expects reference
            const reference = req.params.reference;
            const transaction = await transaction_1.Transaction.findOne({ reference: reference });
            if (transaction) {
                const status = transaction.status;
                switch (status) {
                    case 'success':
                        res.status(200).send({
                            data: transaction,
                            message: 'Transaction Success',
                            status: true
                        });
                        break;
                    default:
                        res.status(200).send({
                            data: transaction,
                            message: `Transaction ${status}`,
                            status: false
                        });
                }
            }
            else {
                res.status(404).send({
                    message: 'Transaction does not exist',
                    status: 'NotFound'
                });
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/token', this.createToken);
        this.router.post('/initialize', this.initialize);
        this.router.post('/verify/:reference', this.verify);
    }
}
exports.PublicApiRouter = PublicApiRouter;
// Create the MerchantRouter, and export its configured Express.Router
const apiRoutes = new PublicApiRouter();
apiRoutes.init();
exports.default = apiRoutes.router;
