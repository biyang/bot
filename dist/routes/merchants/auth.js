"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const express_1 = require("express");
const merchant_1 = require("../../db/schemas/merchant");
const validation_1 = require("../../helpers/validation");
const logger_1 = require("../../logger");
const reset_1 = require("../../emails/reset");
const welcome_1 = require("../../emails/welcome");
const jwt_1 = require("../../utils/jwt");
const mailer_1 = require("../../utils/mailer");
class AuthRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    async generateResetEmail(req, res, next) {
        try {
            const details = req.value.body;
            const merchant = await merchant_1.Merchant.findOne({ email: details.email });
            if (merchant) {
                const html = reset_1.resetEmail({
                    name: merchant.name,
                    url: 'http://biya.com.ng'
                });
                const mailOptions = {
                    from: '"Biya" <reset@biya.com.ng>',
                    html: html,
                    subject: 'Reset your password',
                    to: details.email // list of receivers
                };
                await mailer_1.sendMail(mailOptions);
                res.status(200).send({
                    message: 'Success'
                });
            }
            else {
                res.status(404).send({
                    message: 'This merchant does not exist '
                });
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    async handleReset(req, res, next) {
        try {
            const details = req.value.body;
            const merchant = await merchant_1.Merchant.findOne({ email: details.email });
            if (merchant) {
                // send email with token
            }
            else {
                throw Error('User does not exist');
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    async login(req, res, next) {
        try {
            const details = req.value.body;
            const merchant = await merchant_1.Merchant.findOne({ email: details.email });
            if (merchant) {
                // compare password
                bcrypt.compare(details.password, merchant.password, (err, result) => {
                    // res === true
                    if (err) {
                        res.status(401).send({
                            message: 'Invalid Username or Password'
                        });
                    }
                    else {
                        if (result) {
                            merchant.password = undefined;
                            jwt_1.asyncIssue(merchant.toObject())
                                .then(token => {
                                res.status(200).send({
                                    message: 'Success',
                                    token: token
                                });
                            })
                                .catch(error => {
                                logger_1.default.error(error);
                                const message = error.message
                                    ? error.message
                                    : 'There was an error';
                                res.status(400).send({
                                    message: message
                                });
                            });
                        }
                        else {
                            res.status(401).send({
                                message: 'Invalid Username or Password'
                            });
                        }
                    }
                });
            }
            else {
                // user does not exist
                throw Error('This user is not registered with Biya');
            }
        }
        catch (err) {
            logger_1.default.error(err.message);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    async register(req, res, next) {
        try {
            const newMerchant = new merchant_1.Merchant(req.value.body);
            const merchant = await newMerchant.save();
            const html = welcome_1.welcomeEmail({ name: merchant.name, email: merchant.email });
            const mailOptions = {
                from: '"Eniola from Biya" <hello@biya.com.ng>',
                html: html,
                subject: 'Welcome to Biya Merchants',
                to: merchant.email // list of receivers
            };
            await mailer_1.sendMail(mailOptions);
            merchant.password = merchant.updatedAt = merchant.createdAt = undefined;
            const token = await jwt_1.asyncIssue(merchant.toObject());
            res.status(200).send({
                merchant: merchant,
                message: 'Success',
                token: token
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/register', validation_1.validateBody(validation_1.merchantCreateSchema), this.register);
        this.router.post('/login', validation_1.validateBody(validation_1.merchantLoginSchema), this.login);
        this.router.post('/reset', validation_1.validateBody(validation_1.resetSchema), this.generateResetEmail);
        this.router.post('/new-password', validation_1.validateBody(validation_1.resetSchema), this.generateResetEmail);
        this.router.post('/change-password', validation_1.validateBody(validation_1.resetSchema), this.generateResetEmail);
    }
}
exports.AuthRouter = AuthRouter;
// Create the MerchantRouter, and export its configured Express.Router
const authRoutes = new AuthRouter();
authRoutes.init();
exports.default = authRoutes.router;
