"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const express_1 = require("express");
const passport = require("passport");
const transaction_1 = require("../../db/schemas/transaction");
const user_1 = require("../../db/schemas/user");
const dbUtils_1 = require("../../utils/dbUtils");
const App_1 = require("../../App");
const validation_1 = require("../../helpers/validation");
const logger_1 = require("../../logger");
class InvoiceRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    sendText(parameters) {
        //
        const postData = {
            from: 'Biya',
            message: `Hi there! ${parameters.merchant} has requested a payment for ${parameters.product} from you via Biya.
           Please click this link to get started https://biya.com.ng/#get-started`,
            to: parameters.phoneNumber,
        };
        return axios_1.default({
            auth: {
                password: '13bcf950a6366526964fda4e35da1717',
                username: '424df0de3e1a1167fa4c09c60e5d1f24',
            },
            baseURL: 'https://jusibe.com/smsapi/',
            method: 'post',
            params: postData,
            url: `send_sms`,
        });
    }
    async sendInvoice(req, res, next) {
        try {
            const details = req.value.body;
            const user = await user_1.User.findOne({ phone: details.phone });
            if (user) {
                const reference = 'bybt' + Date.now();
                const newTransaction = {
                    amount: details.amount + 100,
                    description: details.description,
                    identity: { type: 'merchantPay' },
                    merchant: req.user.symbol,
                    product: details.product,
                    reference: reference,
                    status: 'init',
                    tags: [''],
                    user: user.phone,
                };
                const transaction = await dbUtils_1.createTransaction(newTransaction);
                if (!user.authorization) {
                    const updateUser = await user_1.User.findOneAndUpdate({ phone: details.phone }, { initialReference: reference });
                }
                App_1.bot.beginDialog(user.addresses[user.primaryChannel], '/tap2pay', {
                    transaction: transaction.toObject(),
                });
                res.status(200).send({
                    message: 'Success, bill sent',
                });
            }
            else {
                // text a link to the number
                const parameters = {
                    merchant: req.user.name,
                    phoneNumber: details.phone,
                    product: details.product,
                };
                const sendText = this.sendText(parameters);
                if (sendText) {
                    res.status(200).send({
                        message: 'Success, link sent',
                    });
                }
                else {
                    throw new Error('Could not send text');
                }
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    async resendInvoice(req, res, next) {
        try {
            const details = req.value.body;
            const transaction = await transaction_1.Transaction.findById(details.transaction);
            const user = await user_1.User.findOne({ phone: transaction.user });
            if (user) {
                App_1.bot.beginDialog(user.addresses[user.primaryChannel], '/tap2pay', {
                    transaction: transaction.toObject(),
                });
                res.status(200).send({
                    message: 'Success, bill sent',
                });
            }
            else {
                // text a websitelink to the number
                const parameters = {
                    merchant: req.user.name,
                    phoneNumber: details.phone,
                    product: details.product,
                };
                const sendText = this.sendText(parameters);
                if (sendText) {
                    res.status(200).send({
                        message: 'Success, link sent',
                    });
                }
                else {
                    throw new Error('Could not send text');
                }
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/send', passport.authenticate('jwt', { session: false }), validation_1.validateBody(validation_1.invoiceSchema), this.sendInvoice);
        this.router.post('/resend', passport.authenticate('jwt', { session: false }), this.resendInvoice);
    }
}
exports.InvoiceRouter = InvoiceRouter;
// Create the MerchantRouter, and export its configured Express.Router
const invoiceRoutes = new InvoiceRouter();
invoiceRoutes.init();
exports.default = invoiceRoutes.router;
