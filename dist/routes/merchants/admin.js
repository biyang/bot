"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const _ = require("lodash");
const merchant_1 = require("../../db/schemas/merchant");
const logger_1 = require("../../logger");
class MerchantRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * GET all merchnats.
     */
    async getAll(req, res, next) {
        try {
            const merchants = await merchant_1.Merchant.find({});
            res.status(200).send({
                merchants: merchants,
                message: 'Success'
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(404).send({
                message: message
            });
        }
    }
    /**
     * GET one merchant.
     */
    async findOne(req, res, next) {
        const id = req.params.id;
        try {
            const merchant = await merchant_1.Merchant.findById(id);
            res.status(200).send({
                merchant: merchant,
                message: 'Success'
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(404).send({
                message: message
            });
        }
    }
    /**
     * GET one merchant.
     */
    async findOneBySymbol(req, res, next) {
        const symbol = req.params.symbol;
        const id = req.params.id;
        try {
            const merchant = await merchant_1.Merchant.findOne({ symbol: symbol });
            res.status(200).send({
                merchant: merchant,
                message: 'Success'
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(404).send({
                message: message
            });
        }
    }
    /**
     * post all merchnats.
     */
    async postOne(req, res, next) {
        const allowedParameters = ['email', 'name', 'password', 'phone', 'symbol'];
        const data = _.pick(req.body, allowedParameters);
        const newMerchant = new merchant_1.Merchant(data);
        try {
            const merchant = await newMerchant.save();
            merchant.password = undefined;
            res.status(200).send({
                merchant: merchant,
                message: 'Success'
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(404).send({
                message: message
            });
        }
    }
    /**
     * put one merhcnats.
     */
    async putOne(req, res, next) {
        const id = req.params.id;
        const allowedParameters = ['email', 'name', 'password', 'phone', 'symbol'];
        const data = _.pick(req.body, allowedParameters);
        try {
            const merchant = await merchant_1.Merchant.findByIdAndUpdate(id, data);
            merchant.password = undefined;
            res.status(200).send({
                merchant: merchant,
                message: 'Success'
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(404).send({
                message: message
            });
        }
    }
    /**
     * delete one
     */
    async deleteOne(req, res, next) {
        const id = req.params.id;
        try {
            const merchant = merchant_1.Merchant.remove({ _id: id });
            res.status(200).send({
                message: 'Success'
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(404).send({
                message: message
            });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.getAll);
        this.router.get('/:id', this.findOne);
        this.router.get('/sym/:symbol', this.findOneBySymbol);
        this.router.post('/', this.postOne);
        this.router.put('/:id', this.putOne);
        this.router.delete('/:id', this.deleteOne);
    }
}
exports.MerchantRouter = MerchantRouter;
// Create the MerchantRouter, and export its configured Express.Router
const merchantRoutes = new MerchantRouter();
merchantRoutes.init();
exports.default = merchantRoutes.router;
