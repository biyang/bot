"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = require("axios");
const express_1 = require("express");
const _ = require("lodash");
const App_1 = require("../App");
const user_1 = require("../db/schemas/user");
const logger_1 = require("../logger");
class Tap2PayRouter {
    /**
     * Initialize the Tap2PayRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * send bill to one user.
     */
    sendBill(req, res, next) {
        function sendText(phoneNumber) {
            //
            const postData = {
                from: 'Biya',
                message: `Hi there! XYZ has requested a payment from you via Biya.
            Please click this link to get started https://biya.com.ng/#get-started`,
                to: phoneNumber,
            };
            return axios_1.default({
                auth: {
                    password: '13bcf950a6366526964fda4e35da1717',
                    username: '424df0de3e1a1167fa4c09c60e5d1f24',
                },
                baseURL: 'https://jusibe.com/smsapi/',
                method: 'post',
                params: postData,
                url: `send_sms`,
            });
        }
        const allowedParameters = [
            'phone',
            'amount',
            'merchant',
            'description',
        ];
        const data = _.pick(req.body, allowedParameters);
        if (!data.phone) {
            res.status(500).send({
                message: 'Phone number required',
                status: res.status,
            });
        }
        if (!data.amount) {
            res.status(500).send({
                message: 'Amount required',
                status: res.status,
            });
        }
        if (!data.merchant) {
            res.status(500).send({
                message: 'Merchant required',
                status: res.status,
            });
        }
        if (!data.description) {
            res.status(500).send({
                message: 'Payment description required',
                status: res.status,
            });
        }
        user_1.User.findOne({ phone: data.phone })
            .then((user) => {
            if (user) {
                const address = user.addresses[user.primaryChannel];
                App_1.bot.beginDialog(address, '/tap2pay', {
                    amount: data.amount,
                    description: data.description,
                    merchant: data.merchant,
                });
                res.status(200).send({
                    message: 'Success, bill sent',
                    status: res.status,
                });
            }
            else {
                // text a link to the number
                sendText(data.phone)
                    .then((result) => {
                    res.status(200).send({
                        message: 'Success, link sent',
                        status: res.status,
                    });
                })
                    .catch((error) => {
                    logger_1.default.error(error);
                    const message = error.message
                        ? error.message
                        : 'There was an error';
                    res.status(500).send({
                        message: message,
                        status: res.status,
                    });
                });
            }
        })
            .catch((error) => {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
                status: res.status,
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.sendBill);
    }
}
exports.Tap2PayRouter = Tap2PayRouter;
// Create the Tap2PayRouter, and export its configured Express.Router
const tap2PayRoutes = new Tap2PayRouter();
tap2PayRoutes.init();
exports.default = tap2PayRoutes.router;
