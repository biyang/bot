"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const transaction_1 = require("../../db/schemas/transaction");
const logger_1 = require("../../logger");
const jwt_1 = require("../../utils/jwt");
class TicketsRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    async verify(req, res, next) {
        try {
            const token = req.query.token;
            jwt_1.verify(token, (err, decoded) => {
                if (err) {
                    logger_1.default.info('token incorrect');
                    res.redirect('https://biya.com.ng/error');
                    // res.status(401)
                    // .send({
                    //     message: "Invalid Token!",
                    //     status: res.status,
                    // });
                }
                else {
                    transaction_1.Transaction.findOne({ reference: decoded.reference })
                        .then(transaction => {
                        if (transaction && transaction.identity.valid) {
                            res.redirect('https://biya.com.ng/success');
                        }
                        else {
                            res.redirect('https://biya.com.ng/error');
                        }
                    })
                        .catch(error => {
                        logger_1.default.error(err);
                        const message = err.message ? err.message : 'There was an error';
                        res.status(400).send({
                            message: message
                        });
                    });
                }
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message
            });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/', this.verify);
    }
}
exports.TicketsRouter = TicketsRouter;
// Create the MerchantRouter, and export its configured Express.Router
const ticketsRoutes = new TicketsRouter();
ticketsRoutes.init();
exports.default = ticketsRoutes.router;
