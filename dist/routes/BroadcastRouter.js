"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const _ = require("lodash");
const moment = require("moment");
const App_1 = require("../App");
const user_1 = require("../db/schemas/user");
const logger_1 = require("../logger");
class BroadcastRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    /**
     * Broadcast to all users.
     */
    broadCast(req, res, next) {
        const formData = req.body;
        user_1.User.find({})
            .then((users) => {
            const message = formData.message;
            const dialog = formData.dialog;
            let redirectDialog;
            if (dialog) {
                redirectDialog = dialog;
            }
            else {
                redirectDialog = 'broadcast';
            }
            users.forEach((user) => {
                App_1.bot.beginDialog(user.addresses[user.primaryChannel], `/${redirectDialog}`, { message: message });
            });
            res.status(200).send({
                message: 'Success',
            });
        })
            .catch((error) => {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        });
    }
    broadCastPeriod(req, res, next) {
        const formData = req.body;
        const days = formData.days;
        user_1.User.find({
            updatedAt: {
                $lte: moment().subtract(parseInt(days, 10), 'days').toDate(),
            },
        })
            .then((users) => {
            // console.log('users', users)
            const message = formData.message;
            const dialog = formData.dialog;
            let redirectDialog;
            if (dialog) {
                redirectDialog = dialog;
            }
            else {
                redirectDialog = 'broadcast';
            }
            _.forEach(users, (user) => {
                App_1.bot.beginDialog(user.addresses[user.primaryChannel], `/${redirectDialog}`, { message: message });
            });
            res.status(200).send({
                message: 'Success',
                status: res.status,
            });
        })
            .catch((error) => {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        });
    }
    cc(req, res, next) {
        user_1.User.find({})
            .then((user) => {
            user.forEach((element) => {
                const channelId = element.primaryChannel;
                user_1.User.findByIdAndUpdate(element._id, {
                    addresses: {
                        [channelId]: element.address,
                    },
                })
                    .then(() => {
                    logger_1.default.info('done');
                    // session.replaceDialog('welcomeUser');
                })
                    .catch((error) => {
                    // logger.error(error);
                });
            });
        })
            .catch((error) => {
            logger_1.default.error(error);
        });
        res.sendStatus(200);
    }
    broadCastChannel(req, res, next) {
        const formData = req.body;
        const channel = formData.channel;
        user_1.User.find({ primaryChannel: channel })
            .then((users) => {
            // console.log('users', users)
            const message = formData.message;
            const dialog = formData.dialog;
            let redirectDialog;
            if (dialog) {
                redirectDialog = dialog;
            }
            else {
                redirectDialog = '/broadcast';
            }
            _.forEach(users, (user) => {
                App_1.bot.beginDialog(user.addresses[user.primaryChannel], `${redirectDialog}`, { message: message });
            });
            res.status(200).send({
                message: 'Success',
            });
        })
            .catch((error) => {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        });
    }
    /**
     * send message to one user.
     */
    singleCast(req, res, next) {
        const formData = req.body;
        user_1.User.findOne({ phone: formData.phone })
            .then((user) => {
            const args = formData.args;
            const dialog = formData.dialog ? formData.dialog : 'broadcast';
            const channel = formData.channel
                ? formData.channel
                : user.primaryChannel;
            App_1.bot.beginDialog(user.addresses[channel], dialog, args);
            res.status(200).send({
                message: 'Success',
                status: res.status,
            });
        })
            .catch((error) => {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        });
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.post('/', this.broadCast);
        this.router.post('/single', this.singleCast);
        this.router.post('/days', this.broadCastPeriod);
        this.router.post('/channel', this.broadCastChannel);
        this.router.post('/ema', this.cc);
    }
}
exports.BroadcastRouter = BroadcastRouter;
// Create the MerchantRouter, and export its configured Express.Router
const broadcastRoutes = new BroadcastRouter();
broadcastRoutes.init();
exports.default = broadcastRoutes.router;
