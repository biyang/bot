"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt = require("bcryptjs");
const express_1 = require("express");
const parseStringModule = require("xml2js");
const App_1 = require("../App");
const superuser_1 = require("../db/schemas/superuser");
const services_1 = require("../db/schemas/services");
const transaction_1 = require("../db/schemas/transaction");
const user_1 = require("../db/schemas/user");
const validation_1 = require("../helpers/validation");
const logger_1 = require("../logger");
const jwt_1 = require("../utils/jwt");
const process_1 = require("../vending/process");
const paystack_1 = require("../utils/paystack");
const secret = process.env.PAYSTACK_TOKEN;
const newSecret = process.env.NEW_PAYSTACK_TOKEN;
const parseString = parseStringModule.parseString;
class SupportRouter {
    /**
     * Initialize the MerchantRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    async login(req, res, next) {
        try {
            const details = req.value.body;
            const admin = await superuser_1.Superuser.findOne({
                username: details.username,
            });
            if (admin) {
                // compare password
                bcrypt.compare(details.password, admin.password, (err, result) => {
                    // res === true
                    if (err) {
                        res.status(401).send({
                            message: 'Invalid Username or Password',
                        });
                    }
                    else {
                        if (result) {
                            admin.password = undefined;
                            jwt_1.asyncIssue(admin.toObject())
                                .then((token) => {
                                res.status(200).send({
                                    message: 'Success',
                                    token: token,
                                });
                            })
                                .catch((error) => {
                                logger_1.default.error(error);
                                const message = error.message
                                    ? error.message
                                    : 'There was an error';
                                res.status(400).send({
                                    message: message,
                                });
                            });
                        }
                        else {
                            res.status(401).send({
                                message: 'Invalid Username or Password',
                            });
                        }
                    }
                });
            }
            else {
                // user does not exist
                throw Error('Not a registered Admin');
            }
        }
        catch (err) {
            logger_1.default.error(err.message);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    async register(req, res, next) {
        try {
            const newAdmin = new superuser_1.Superuser(req.value.body);
            const user = await newAdmin.save();
            user.password = user.updatedAt = user.createdAt = undefined;
            const token = await jwt_1.asyncIssue(user.toObject());
            res.status(200).send({
                message: 'Success',
                token: token,
                user: user,
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    async deleteTrx(req, res, next) {
        try {
            const id = req.params.id;
            const transaction = await transaction_1.Transaction.findById(id);
            if (transaction.status === 'init') {
                await transaction_1.Transaction.remove({ _id: id });
                res.status(200).send({
                    message: 'Success',
                });
            }
            else {
                throw Error('Not Allowed');
            }
        }
        catch (error) {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }
    async markResolved(req, res, next) {
        try {
            const id = req.params.id;
            const transaction = await transaction_1.Transaction.findById(id);
            if (['success', 'pending'].includes(transaction.status)) {
                await transaction_1.Transaction.findByIdAndUpdate(id, {
                    status: 'success',
                });
                const user = await user_1.User.findById(transaction.user);
                const address = transaction.address;
                if (address) {
                    if (transaction.product === 'airtime') {
                        App_1.bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            network: transaction.description,
                            number: transaction.identity.phone,
                            type: 'self',
                        });
                    }
                    else if (transaction.product === 'otherR') {
                        App_1.bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            network: transaction.description,
                            number: transaction.identity.phone,
                            type: 'otherR',
                        });
                    }
                    else if (transaction.product === 'dstv') {
                        App_1.bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            type: 'dstv',
                        });
                    }
                    else if (transaction.product === 'gotv') {
                        App_1.bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            type: 'gotv',
                        });
                    }
                    else if (transaction.product === 'smile') {
                        App_1.bot.beginDialog(address, '/notify', { type: 'smile' });
                    }
                    else if (['ikedc', 'ibedc', 'ekdc', 'phdc', 'enugu'].includes(transaction.product.toLowerCase())) {
                        App_1.bot.beginDialog(address, '/notify', {
                            type: transaction.product,
                            pin: transaction.identity.pin,
                            amount: transaction.identity.amount,
                        });
                    }
                    else if (transaction.product === 'spectranet') {
                        App_1.bot.beginDialog(address, '/notify', {
                            amount: transaction.amount,
                            pin: transaction.identity['pinCode'],
                            serial: transaction.identity['serialNumber'],
                            type: 'spectranet',
                        });
                    }
                }
                res.status(200).send({
                    message: 'Success',
                });
            }
            else {
                throw Error('Not allowed');
            }
        }
        catch (error) {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }
    async autoResolve(req, res, next) {
        try {
            // get transactionId
            // check status
            // resolve if unresolved
            // send recipe
            function MyError(message, name, trx) {
                this.name = name;
                this.transaction = trx;
                this.message = message || 'Default Message';
                this.stack = new Error().stack;
            }
            MyError.prototype = new Error();
            const id = req.params.id;
            const transaction = await transaction_1.Transaction.findById(id);
            const secretToUse = ['airtime', 'otherR'].includes(transaction.product)
                ? secret
                : newSecret;
            const { status } = await paystack_1.verifyTransaction(secretToUse)(transaction.reference);
            if (!(status === 'success')) {
                return res.status(400).send({
                    message: 'Not a successful payment',
                });
            }
            if (['init', 'pending'].includes(transaction.status)) {
                process_1.default(transaction.reference);
                return res.status(200).send({ message: 'Retry queued' });
            }
            else {
                res.status(500).send({
                    message: 'Error',
                });
            }
        }
        catch (error) {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }
    async allUsers(req, res, next) {
        try {
            let page;
            if (req.query) {
                page = parseInt(req.query.page, 10);
            }
            else {
                page = 1;
            }
            const perPage = 50;
            let total;
            let users;
            total = await user_1.User.count({});
            users = await user_1.User.find({})
                .sort({ updatedAt: -1 })
                .limit(perPage)
                .skip((page - 1) * perPage);
            const nextPageUrl = page > 0 && total > perPage
                ? process.env.CURRENT_ENV + `support/users?page=${page + 1}`
                : '';
            const prevPageUrl = page > 1 && total > perPage
                ? process.env.CURRENT_ENV + `support/users?page=${page - 1}`
                : '';
            if (users) {
                res.status(200).send({
                    current_page: page,
                    data: users,
                    from: page * perPage - 49,
                    last_page: Math.floor(total / perPage) + 1,
                    message: 'Success',
                    next_page_url: nextPageUrl,
                    per_page: perPage,
                    prev_page_url: prevPageUrl,
                    to: page * perPage,
                    total: total,
                });
            }
            else {
                res.status(404).send({
                    error: 'No transactions',
                    message: 'Error',
                });
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    async deleteUser(req, res, next) {
        try {
            const id = req.params.id;
            const transaction = await user_1.User.remove({ _id: id });
            res.status(200).send({
                message: 'Success',
            });
        }
        catch (error) {
            logger_1.default.error(error);
            const message = error.message
                ? error.message
                : 'There was an error';
            res.status(500).send({
                message: message,
            });
        }
    }
    /**
     * dispense ikedc
     * endpoints.
     */
    async getTransactions(req, res, next) {
        try {
            const { page: PageNumber = 1, per_page: PerPage = 50, status, product, } = req.query;
            const page = parseInt(PageNumber, 10);
            const per_page = parseInt(PerPage, 10);
            const skip = page > 1 ? (page - 1) * per_page : 0;
            const criteria = Object.assign({}, (status && { status }), (product && { product }));
            const total = await transaction_1.Transaction.count(criteria);
            const transactions = await transaction_1.Transaction.find(criteria)
                .sort({
                updatedAt: -1,
            })
                .sort({ createdAt: -1 })
                .limit(per_page)
                .skip(skip)
                .populate('user');
            const nextPageUrl = page > 0 && total > per_page * page
                ? process.env.CURRENT_ENV +
                    `transactions/transactions?page=${page + 1}`
                : '';
            const prevPageUrl = page > 1
                ? process.env.CURRENT_ENV +
                    `transactions/transactions?page=${page - 1}`
                : '';
            return res.status(200).send({
                current_page: page,
                data: transactions,
                from: page * per_page + 1 - per_page,
                last_page: Math.floor(total / per_page) + 1,
                message: 'Success',
                next_page_url: nextPageUrl,
                per_page: per_page,
                prev_page_url: prevPageUrl,
                to: page * per_page,
                total: total,
            });
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    async allServices(req, res, next) {
        try {
            const services = await services_1.Services.find({});
            if (services) {
                res.status(200).send({
                    data: services,
                    message: 'Success',
                });
            }
            else {
                res.status(404).send({
                    error: 'No Services',
                    message: 'Error',
                });
            }
        }
        catch (err) {
            logger_1.default.error(err);
            const message = err.message ? err.message : 'There was an error';
            res.status(400).send({
                message: message,
            });
        }
    }
    init() {
        this.router.post('/register', validation_1.validateBody(validation_1.adminSchema), this.register);
        this.router.post('/login', validation_1.validateBody(validation_1.adminSchema), this.login);
        this.router.get('/transactions', validation_1.verifyToken(), this.getTransactions);
        this.router.get('/users', validation_1.verifyToken(), this.allUsers);
        this.router.get('/status', validation_1.verifyToken(), this.allServices);
        this.router.delete('/users/:id', validation_1.verifyToken(), this.deleteUser);
        this.router.get('/resolve/:id', validation_1.verifyToken(), this.autoResolve);
        this.router.get('/mark/:id', validation_1.verifyToken(), this.markResolved);
        this.router.delete('/delete/:id', validation_1.verifyToken(), this.deleteTrx);
    }
}
exports.SupportRouter = SupportRouter;
// Create the MerchantRouter, and export its configured Express.Router
const supportRoutes = new SupportRouter();
supportRoutes.init();
exports.default = supportRoutes.router;
