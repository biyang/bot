"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const CircularJSON = require("circular-json");
const express_1 = require("express");
const common_tags_1 = require("common-tags");
const events_1 = require("../db/schemas/events");
const subscription_1 = require("../db/schemas/subscription");
const transaction_1 = require("../db/schemas/transaction");
const user_1 = require("../db/schemas/user");
const SlackService_1 = require("../monitor/SlackService");
const sms_1 = require("../utils/sms");
const airvend_1 = require("../utils/airvend");
const process_1 = require("../vending/process");
const dbUtils_1 = require("../utils/dbUtils");
const tickets_1 = require("../utils/tickets");
const shortid = require("simple-id");
const parseStringModule = require("xml2js");
const App_1 = require("../App");
const logger_1 = require("../logger");
const mailer_1 = require("../utils/mailer");
const parseString = parseStringModule.parseString;
// import verify
const paystack_1 = require("../utils/paystack");
// Instantiate the class
const crypto = require("crypto");
const secret = process.env.PAYSTACK_TOKEN;
const newSecret = process.env.NEW_PAYSTACK_TOKEN;
class PaystackRouter {
    /**
     * Initialize the PaystackRouter
     */
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    async verifyPaystack(req, res, next) {
        // call paystack verify function
        const reference = req.query.trxref;
        const networks = {
            AIRTEL: 1,
            ETISALAT: 4,
            GLO: 3,
            MTN: 2,
        };
        function MyError(message, name, transaction) {
            this.name = name;
            this.transaction = transaction;
            this.message = message || 'Default Message';
            this.stack = new Error().stack;
        }
        MyError.prototype = new Error();
        let address = {};
        try {
            const findTransaction = await transaction_1.Transaction.findOne({
                reference: reference,
            });
            if (findTransaction.status !== 'init') {
                res.redirect('https://biya.com.ng/success');
            }
            let verify;
            const product = findTransaction.product;
            if (product === 'airtime' || product === 'otherR') {
                verify = await paystack_1.verifyTransaction(secret)(reference);
            }
            else {
                verify = await paystack_1.verifyTransaction(newSecret)(reference);
            }
            if (verify.status === 'success') {
                // update authorization code=
                let updateAuthCode;
                if (verify.authorization && verify.authorization.reusable) {
                    if (product === 'airtime' || product === 'otherR') {
                        updateAuthCode = await user_1.User.findOneAndUpdate({ initialReference: reference }, { authorization: verify.authorization });
                    }
                    else {
                        updateAuthCode = await user_1.User.findOneAndUpdate({ initialReference: reference }, { billsAuth: verify.authorization });
                    }
                }
                else {
                    updateAuthCode = await user_1.User.findOne({
                        initialReference: reference,
                    });
                }
                if (updateAuthCode) {
                    address =
                        updateAuthCode.addresses[findTransaction.identity.sourceChannel];
                }
                if (findTransaction.identity.type === 'merchantPay') {
                    await transaction_1.Transaction.findOneAndUpdate({ reference: reference }, { status: 'success' });
                    App_1.bot.beginDialog(address, '/notify', {
                        transaction: findTransaction,
                        type: 'merchantPay',
                    });
                    res.redirect('https://biya.com.ng/success');
                }
                if (findTransaction.product === 'ticket') {
                    await transaction_1.Transaction.findOneAndUpdate({ reference: reference }, { status: 'success' });
                    App_1.bot.beginDialog(address, '/notify', {
                        ticket: await tickets_1.dispenseTicket(findTransaction),
                        transaction: findTransaction,
                        type: 'ticket',
                    });
                    res.redirect('https://biya.com.ng/success');
                }
                return res.redirect('https://biya.com.ng/success');
            }
            else {
                transaction_1.Transaction.findOneAndUpdate({ reference: reference }, { status: 'failed' })
                    .then((r) => logger_1.default.info(r))
                    .catch((e) => {
                    logger_1.default.error(e);
                });
                throw new MyError(verify.gateway_response, 'PaymentError', {
                    reference: reference,
                });
            }
        }
        catch (error) {
            logger_1.default.error(error);
            res.redirect('https://biya.com.ng/error');
        }
    }
    async paystackWebhook(req, res, next) {
        // validate event
        let address;
        try {
            const hash = crypto
                .createHmac('sha512', secret)
                .update(CircularJSON.stringify(req.body))
                .digest('hex');
            const hash2 = crypto
                .createHmac('sha512', newSecret)
                .update(CircularJSON.stringify(req.body))
                .digest('hex');
            if (hash === req.headers['x-paystack-signature'] ||
                hash2 === req.headers['x-paystack-signature']) {
                // Retrieve the request's body
                const event = req.body;
                const newEvent = new events_1.Events({ event: event });
                await newEvent.save();
                // Do something with event
                function MyError(message, name, transaction) {
                    this.name = name;
                    this.transaction = transaction;
                    this.message = message || 'Default Message';
                    this.stack = new Error().stack;
                }
                const _domain = process.env.NODE_ENV === 'staging' ? 'test' : 'live';
                if (event.event === 'charge.success') {
                    const { data } = event;
                    if (data && data.domain === _domain) {
                        process_1.default(data.reference);
                    }
                }
                MyError.prototype = new Error();
                if (event.event === 'invoice.update') {
                    if (event.data && event.data.paid) {
                        // if there's a plan, do dispense
                        const subscriptionCode = event.data.subscription.subscription_code;
                        const userEmail = event.data.customer.email;
                        const subscription = await subscription_1.Subscription.findOne({
                            'data.subscription_code': subscriptionCode,
                        });
                        const user = await user_1.User.findOne({ email: userEmail });
                        const reference = shortid(8, '0123456789abcdefghijklmnopqrstuvwxyz');
                        const transaction = {
                            amount: event.data.subscription.amount / 100,
                            description: subscription.product,
                            identity: subscription.identity,
                            merchant: 'airvend',
                            product: subscription.product,
                            reference: reference,
                            status: 'success',
                            tags: [],
                            user: user.phone,
                            address: subscription.address,
                        };
                        const newTransaction = await dbUtils_1.createTransaction(transaction);
                        let dispense;
                        if (transaction.product === 'dstv') {
                            dispense = await airvend_1.dispenseDSTV(transaction);
                        }
                        if (transaction.product === 'gotv') {
                            dispense = await airvend_1.dispenseGOTV(transaction);
                        }
                        address =
                            user.addresses[transaction.identity.sourceChannel];
                        if (dispense && dispense.data) {
                            parseString(dispense.data, (err, result) => {
                                if (err) {
                                    logger_1.default.error(err);
                                    throw new MyError('There was an error, we will rectify this shortly. Thank you!', 'DispenseError', transaction);
                                }
                                if (result &&
                                    result.VendResponse.ResponseCode[0] === '0') {
                                    const myJSON = result.VendResponse;
                                    if (transaction.product === 'dstv') {
                                        App_1.bot.beginDialog(address, '/notify', {
                                            amount: myJSON.Amount[0],
                                            type: 'dstv',
                                        });
                                    }
                                    else if (transaction.product === 'gotv') {
                                        App_1.bot.beginDialog(address, '/notify', {
                                            amount: myJSON.Amount[0],
                                            type: 'gotv',
                                        });
                                    }
                                }
                                else {
                                    if (result &&
                                        result.VendResponse.ResponseCode[0] ===
                                            '99999997') {
                                        throw new MyError('Invalid smart card number, please try again.', 'InvalidError', transaction);
                                    }
                                    else if (result &&
                                        result.VendResponse.ResponseCode[0] ===
                                            '99999999') {
                                        throw new MyError('There was a network error, we will rectify this shortly. Thank you!', 'NetworkError', transaction);
                                    }
                                    else if (result &&
                                        result.VendResponse.ResponseCode[0] ===
                                            '99999998') {
                                        throw new MyError('There was a problem at our end, we will rectify this shortly. Thank you!', 'InsufficientError', transaction);
                                    }
                                    else {
                                        throw new MyError('There was a problem at our end, we will check and rectify this shortly. Thank you!', 'UnknownError', transaction);
                                    }
                                }
                            });
                        }
                    }
                }
                if (event.event === 'invoice.create') {
                    // send impending notification to user
                    const userEmail = event.data.customer.email;
                    const user = await user_1.User.findOne({ email: userEmail });
                    const userAddress = user.addresses[user.primaryChannel];
                    const subscriptionCode = event.data.subscription.subscription_code;
                    App_1.bot.beginDialog(userAddress, '/subreminder', {
                        amount: 0,
                        type: 'dstv',
                    });
                }
            }
        }
        catch (error) {
            logger_1.default.error(error);
            const mailOptions = {
                from: '"Init Dispense Error Alert ⚠" <alerts@biya.com.ng>',
                html: `<pre>${CircularJSON.stringify(error)}</pre>`,
                subject: 'Init Dispense Error Alert',
                to: 'olumytee.ojo@gmail.com',
            };
            if (error.transaction) {
                transaction_1.Transaction.findOneAndUpdate({ reference: error.transaction.reference }, { status: 'failed' }, (err, doc) => {
                    if (err) {
                        logger_1.default.info(err);
                    }
                    logger_1.default.info('failed transaction');
                });
                mailOptions.html = `<strong>${error.name}</strong><br/>
                                    <strong>${error.message}</strong><br/>
                                    <pre>${CircularJSON.stringify(error.transaction)}</pre>`;
            }
            mailer_1.sendMail(mailOptions);
            const message = error.message
                ? error.message
                : 'There was an error';
            // if (address) {
            //   bot.beginDialog(address, '/error', {});
            // }
        }
        finally {
            // respond to paystack
            res.sendStatus(200);
        }
    }
    async vtpassWebhook(req, res, next) {
        try {
            const { type, data } = req.body;
            SlackService_1.SlackNotify({
                username: 'New Event!',
                channel: '#feed-vtpass',
                text: 'New event: ' + type,
                fields: {
                    Data: JSON.stringify(data),
                },
                emoji: ':pinched_fingers:',
            });
            if (type === 'transaction-update') {
                const { code, requestId } = data;
                const transactionReference = requestId;
                if (code === '000') {
                    let updateObject = { status: 'success' };
                    if (data.token) {
                        const pin = data.token;
                        const serial = '';
                        const amountOfPower = data.units;
                        updateObject = Object.assign({}, {
                            status: 'success',
                            'identity.pin': pin,
                            'identity.serial': serial || '',
                            'identity.amount': amountOfPower || '',
                        });
                    }
                    transaction_1.Transaction.findOneAndUpdate({ reference: transactionReference }, updateObject).then((result) => {
                        transaction_1.Transaction.findOne({ reference: transactionReference })
                            .then((info) => {
                            if (info.address) {
                                App_1.bot.loadSession(
                                // @ts-ignore
                                info.address, (err, session) => {
                                    session.replaceDialog('/notify', Object.assign({}, info.identity, { type: info.product }));
                                });
                            }
                            sms_1.sendIkedcPin(info, info.identity.pin, info.identity.serial, info.product);
                            SlackService_1.SlackNotify({
                                username: 'New Sale!',
                                channel: '#sales',
                                text: common_tags_1.stripIndents `Successful ${info.product.toUpperCase()} dispense`,
                                fields: {
                                    Service: info.product.toUpperCase(),
                                    Amount: info.amount,
                                    Name: info.identity.customername,
                                    Account: info.identity.customerNumber,
                                    PIN: info.identity.pin,
                                    Units: info.identity.amount,
                                },
                                emoji: ':white_check_mark:',
                            });
                            logger_1.default.info('Job complete');
                        })
                            .catch((error) => logger_1.default.error(error));
                    });
                }
                if (code === '040') {
                    transaction_1.Transaction.findOneAndUpdate({ reference: transactionReference }, { status: 'failed' }, (err, doc) => {
                        if (err) {
                            logger_1.default.info(err);
                        }
                        logger_1.default.info('failed transaction');
                    });
                }
            }
            logger_1.default.info(req.body);
        }
        catch (error) {
            logger_1.default.error(error);
        }
        finally {
            // respond to paystack
            res.send({ response: 'success' });
        }
    }
    /**
     * Take each handler, and attach to one of the Express.Router's
     * endpoints.
     */
    init() {
        this.router.get('/verify', this.verifyPaystack);
        this.router.post('/webhook', this.paystackWebhook);
        this.router.post('/vtpasswebhook', this.vtpassWebhook);
    }
}
exports.PaystackRouter = PaystackRouter;
// Create the PaystackRouter, and export its configured Express.Router
const paystackRoutes = new PaystackRouter();
paystackRoutes.init();
exports.default = paystackRoutes.router;
