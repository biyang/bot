"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
class MonitorRouter {
    constructor() {
        this.router = express_1.Router();
        this.init();
    }
    async monitor(req, res, next) {
        res.sendStatus(200);
    }
    init() {
        this.router.get('/', this.monitor);
    }
}
exports.MonitorRouter = MonitorRouter;
// Create the MerchantRouter, and export its configured Express.Router
const monitorRoutes = new MonitorRouter();
monitorRoutes.init();
exports.default = monitorRoutes.router;
