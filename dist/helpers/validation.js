"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("joi");
const jwt_1 = require("../utils/jwt");
exports.idSchema = Joi.object().keys({
    param: Joi.string()
        .regex(/^[0-9a-fA-F]{24}$/)
        .required()
});
exports.merchantLoginSchema = Joi.object().keys({
    email: Joi.string()
        .email()
        .required(),
    password: Joi.string().required()
});
exports.adminSchema = Joi.object().keys({
    password: Joi.string().required(),
    username: Joi.string().required()
});
exports.resetSchema = Joi.object().keys({
    email: Joi.string()
        .email()
        .required()
});
exports.merchantCreateSchema = Joi.object().keys({
    email: Joi.string()
        .email()
        .required(),
    name: Joi.string().required(),
    password: Joi.string().required(),
    phone: Joi.string()
        .regex(/\b\d{11}\b/g)
        .required(),
    symbol: Joi.string()
});
exports.invoiceSchema = Joi.object().keys({
    amount: Joi.number().required(),
    description: Joi.string().required(),
    phone: Joi.string()
        .regex(/\b\d{11}\b/g)
        .required(),
    product: Joi.string().required()
});
exports.validateParams = (schema, name) => {
    return (req, res, next) => {
        const result = Joi.validate({ param: req.params[name] }, schema);
        if (result.error) {
            if (result.error.isJoi) {
                return res.status(400).json({
                    error: result.error.name,
                    message: result.error.details[0].message
                });
            }
            return res.status(400).json(result.error);
        }
        else {
            if (!req.value) {
                req.value = {};
            }
            if (!req.value.params) {
                req.value.params = {};
            }
            req.value.params[name] = result.value.param;
            next();
        }
    };
};
/**
 *
 *
 * @param schema
 * @returns
 */
exports.validateBody = schema => {
    return (req, res, next) => {
        const result = Joi.validate(req.body, schema);
        if (result.error) {
            if (result.error.isJoi) {
                return res.status(400).json({
                    error: result.error.name,
                    message: result.error.details[0].message
                });
            }
            return res.status(400).json(result.error);
        }
        else {
            if (!req.value) {
                req.value = {};
            }
            if (!req.value.body) {
                req.value.body = {};
            }
            req.value.body = result.value;
            next();
        }
    };
};
exports.verifyToken = () => {
    return (req, res, next) => {
        const token = req.headers.authorization;
        if (token) {
            jwt_1.asyncVerify(token)
                .then(decoded => {
                next();
            })
                .catch(error => {
                res.status(401).send({
                    message: 'Invalid Username or Password'
                });
            });
        }
        else {
            res.status(401).send({
                message: 'Auth Token Required'
            });
        }
    };
};
