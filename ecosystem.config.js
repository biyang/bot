module.exports = {
    /**
     * Application configuration section
     * http://pm2.keymetrics.io/docs/usage/application-declaration/
     */
    apps: [
        // First application
        {
            name: 'biya_new',
            script: 'dist/index.js',
            env: {
                COMMON_VARIABLE: 'true',
                NODE_ENV: 'production',
                PORT: '3000',
                DOMAIN: 'https://secure.biya.com.ng/',
                MICROSOFT_APP_ID: '8f713deb-7849-428c-b1ae-85e844a9051b',
                MICROSOFT_APP_PASSWORD: '1VEEPV73JhsBrxCniz1kSGM',
                LUIS_MODEL_URL:
                    'https://westus.api.cognitive.microsoft.com/luis/v2.0/apps/314d6501-2bf8-40ab-b9a9-d7e9f2f722d4?subscription-key=d50aba8e682a4aada3441d64ddd4c931&verbose=true&timezoneOffset=0.0&q=',
                PAYSTACK_TOKEN:
                    'sk_live_e449832152cc49476430e97ef27dfe30597c9e56',
                NEW_PAYSTACK_TOKEN:
                    'sk_live_492754d328df620f3cf5ba9c2c60aa42c684873e',
                PAYSTACK_URL: 'https://api.paystack.co/transaction',
                MONGODB_URI: 'mongodb://10.106.0.3:27017/biya',
                REDIS_URL: 'redis://10.106.0.2:6379',
                RECHARGE_ID: '09058508857',
                RECHARGE_API_KEY: 'a7bf72afaab82dd8602acbb',
                MV_URL: 'https://moneywave.herokuapp.com/v1',
                TWILIO_BASE: 'https://lookups.twilio.com/v1/PhoneNumbers',
                TWILIO_USERNAME: 'ACd1e102e3f6158c6206a2d055b36a37b9',
                TWILIO_PASS: '2b5900ce1837a1ed2b04b08d6ea0ffbe',
                TESTTOKEN: 'sk_test_407c7e0a8fb51f912feb7a494cf21bdb339f517c',
                AIRVEND_USER: 'support1@iwallet.com.ng',
                AIRVEND_PASS: '6478',
                CURRENT_ENV: 'https://api.biya.com.ng/v1/',
                mwapiKey: 'lv_TKXV3P4FFT6DV5NQLBUE',
                mwapiSecret: 'lv_WD15FDC14BQQY09BRPWWVQTDYEKOZ5',
                MIXPANEL: '198fcc047cb570176d23973c24ad16f1',
                minAmount: '100',
                maxAmount: '3000',
            },
        },
    ],

    /**
     * Deployment section
     * http://pm2.keymetrics.io/docs/usage/deployment/
     */
    deploy: {
        production: {
            key: '/Users/olumide/.ssh/google_compute_engine',
            user: 'deploy',
            host: '35.187.85.94',
            ref: 'origin/master',
            repo: 'git@bitbucket.org:biyang/bot.git',
            path: '/home/deploy/biyanew',
            'post-deploy':
                'npm install && pm2 startOrRestart ecosystem.config.js --env production -i max',
        },
        prod: {
            key: '/Users/olumide/.ssh/biya.pem',
            user: 'ubuntu',
            host: '18.224.69.19',
            ref: 'origin/master',
            repo: 'git@bitbucket.org:biyang/bot.git',
            path: '/var/www/production',
            'post-deploy':
                'npm install && pm2 startOrRestart ecosystem.config.js --env production -i max',
            env: {
                NODE_ENV: 'prod',
            },
        },
        prod_do: {
            user: 'root',
            host: '167.172.41.70',
            password: 'MRU0zfn!dae4dtp9dna',
            ref: 'origin/master',
            repo: 'git@bitbucket.org:biyang/bot.git',
            path: '/var/www/production',
            'post-deploy':
                'npm install && pm2 startOrRestart ecosystem.config.js --env production -i max',
            env: {
                NODE_ENV: 'prod',
            },
        },
        dev: {
            user: 'node',
            host: '212.83.163.1',
            ref: 'origin/master',
            repo: 'git@github.com:repo.git',
            path: '/var/www/development',
            'post-deploy':
                'npm install && pm2 startOrRestart ecosystem.config.js --env dev',
            env: {
                NODE_ENV: 'dev',
            },
        },
    },
};
